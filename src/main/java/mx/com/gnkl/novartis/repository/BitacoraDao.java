/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.novartis.repository;

import java.util.List;
import java.util.Map;
import mx.com.gnkl.novartis.model.TbBitacora;

/**
 *
 * @author jmejia
 */
public interface BitacoraDao {
    
    public List<String> getListByIdBitacora(String idBitacora);
    
    public List<TbBitacora> getCustomQueryData(String query, Integer offset, Integer count);
    
    public Long getCountCustomQueryData(String query);
    
    public List<TbBitacora> searchByParametersInfo(String fechaInicial, String fechaFinal, String noTracking, String idBitacora,  String idEnvio, Integer offset, Integer count);
    
    public Integer getIdPaqueteByIdBitacora(Integer idBitacora);
    
    public void deleteBitacoraByIdPaquete(Integer idPaquete);
    
    public Integer getMaxKilometrajeByIdBitacora(Integer idBitacora);
    
    public Long searchByParametersInfo( String idBitacora, String idEnvio );
    
    public List<Map<String,Object>> getListBitacoraByParameters(String fechaInicio,String fechaFin,Integer idOperador, Integer idEnvio, Integer offset, Integer count);
    
    public Long getCountBitacoraByParameters(String fechaInicio,String fechaFin,Integer idOperador, Integer idEnvio);
    
    public List<Map<String,Object>> getListBitacoraWithHistorico(String fechaInicio,String fechaFin,Integer idOperador, Integer idEnvio);
    
    public List<Map<String,Object>> getAllListBitacoraTiempoEstatus(String fechaInicio,String fechaFin,Integer idOperador, Integer idEnvio);
    
    public List<Map<String, Object>> getAllHistoricoByBitacoraId(Integer idBitacora,Integer idEnvio);
    
    public void updateBitacoraRutaActivoInactivo(Integer idRuta, Integer idBitacora, Integer activo);
    
    public void updateBitacoraRutaActivoInactivo(Integer idRuta, Integer activo);
    
    public List<Map<String, Object>> getAllBitacoraByUsuarioEnvio(Integer idUsuario,Integer idEnvio);
    
    public List<Map<String, Object>> getAllBitacoraHistoricoByIdBitacora(Integer idBitacora);
}
