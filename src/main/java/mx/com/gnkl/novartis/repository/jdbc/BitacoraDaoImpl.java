/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.novartis.repository.jdbc;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.sql.DataSource;
import mx.com.gnkl.novartis.model.TbBitacora;
import mx.com.gnkl.novartis.repository.BitacoraDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jmejia
 */
@Repository
public class BitacoraDaoImpl implements BitacoraDao{
    
    private JdbcTemplate jdbcTemplate;
    
    private NamedParameterJdbcTemplate namedTemplate;
    
    //@PersistenceContext(unitName = "novartisPU")
    @PersistenceContext
    private EntityManager em;  
    
    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.setNamedTemplate(new NamedParameterJdbcTemplate(dataSource));
    }
    
    /**
     * @return the jdbcTemplate
     */
    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    /**
     * @param jdbcTemplate the jdbcTemplate to set
     */
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
    

    @Override
    public List<String> getListByIdBitacora(String idBitacora) {
        String query = " SELECT distinct(b.id) id FROM tb_bitacora b where b.id like ? ";
        return jdbcTemplate.queryForList(query, new Object[]{idBitacora}, String.class);
    }
    
    
    @Override
    public List<TbBitacora> searchByParametersInfo(String fechaInicial, String fechaFinal, String noTracking, String idBitacora, String idEnvio, Integer offset, Integer count){
        String queryFechas = "";
        String query = "";
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");        
//        Date parsedDate2 = null;
//        Date parsedDate1 = null;
        
//        try{            
//            if((fechaInicial !=null && !fechaInicial.isEmpty()) && (fechaFinal !=null && !fechaFinal.isEmpty())){
//                if(noTracking!=null && !noTracking.isEmpty()){
//                    queryFechas = " and date(tbp.idBitacora.fecha)>=:fechaInicial and date(tbp.idBitacora.fecha)<=:fechaFinal ";
//                }else{
//                    queryFechas = " and date(b.fecha)>= :fechaInicial and date(b.fecha)<=:fechaFinal ";
//                }
//            }
            
            if(idBitacora!=null && !idBitacora.isEmpty()){
                query = "  select b from TbBitacora b where b.id=:idBitacora "+queryFechas;
            }else{
                query = "  select b from TbBitacora b where b.id is not null "+queryFechas;
            }
            
            if(idEnvio!=null && !idEnvio.isEmpty()){
                query += " AND b.idEnvio.idEnvio=:idEnvio";
            }
            
            Query querycreated = em.createQuery(query);
            querycreated.setFirstResult(offset); 
            querycreated.setMaxResults(count);                
            
//            if((fechaInicial !=null && !fechaInicial.isEmpty()) && (fechaFinal !=null && !fechaFinal.isEmpty())){
//                parsedDate1 = sdf.parse(fechaInicial);
//                parsedDate2 = sdf.parse(fechaFinal);            
//                querycreated.setParameter("fechaInicial", parsedDate1, TemporalType.DATE);
//                querycreated.setParameter("fechaFinal", parsedDate2, TemporalType.DATE);                
//            }
            
            if(noTracking!=null && !noTracking.isEmpty()){
                querycreated.setParameter("noTracking", noTracking);                
            }else if(idBitacora!=null && !idBitacora.isEmpty()){
                querycreated.setParameter("idBitacora", Integer.parseInt(idBitacora));
            }
            
            if(idEnvio!=null && !idEnvio.isEmpty()){
                querycreated.setParameter("idEnvio", Integer.parseInt(idEnvio));
            }
            
            return querycreated.getResultList();            
//        }catch(ParseException ps){
//            ps.printStackTrace();
//        }
//
//        return new ArrayList<TbBitacora>();
    }

    @Override
    public Long searchByParametersInfo( String idBitacora, String idEnvio ){
        String queryFechas = "";
        String query = "";
            
            if(idBitacora!=null && !idBitacora.isEmpty()){
                query = "  select COUNT(b) from TbBitacora b where b.id=:idBitacora ";
            }else{
                query = "  select COUNT(b) from TbBitacora b where b.id is not null ";
            }
            
            if(idEnvio!=null && !idEnvio.isEmpty()){
                query += " AND b.idEnvio.idEnvio=:idEnvio";
            }
            
            Query querycreated = em.createQuery(query);            
            
            if(idBitacora!=null && !idBitacora.isEmpty()){
                querycreated.setParameter("idBitacora", Integer.parseInt(idBitacora));
            }
            
            if(idEnvio!=null && !idEnvio.isEmpty()){
                querycreated.setParameter("idEnvio", Integer.parseInt(idEnvio));
            }
            
            return (Long) querycreated.getSingleResult();            
    }    
    
    @Override
    public List<TbBitacora> getCustomQueryData(String query, Integer offset, Integer count) {
        Query querycreated = em.createQuery(query);
        querycreated.setFirstResult(offset); 
        querycreated.setMaxResults(count);        
        List <TbBitacora> paqueteList = querycreated.getResultList();
        return paqueteList;
    }
    
    @Override
    public Long getCountCustomQueryData(String query) {
        Query querycreated = em.createQuery(query);
        Long total = (Long) querycreated.getSingleResult();
        return total;
    }    
    
    @Override
    public Integer getIdPaqueteByIdBitacora(Integer idBitacora) {
        String query = " SELECT id_paquete FROM tb_bitacora_paquete bp, tb_paquetes p where bp.id_bitacora=? and p.id_paq=bp.id_paquete and p.fase=1 limit 1";
        return jdbcTemplate.queryForObject(query, new Object[]{idBitacora}, Integer.class);
    }    
    
    //@Query(value=" DELETE FROM tb_bitacora_paquete WHERE id_bitacora=:idbitacora ", nativeQuery = true)
    //void deleteBitacoraPaqueteByIdBitacora(@Param("idbitacora") Integer idBitacora);     
    
    @Override
    public void deleteBitacoraByIdPaquete(Integer idPaquete) {
        String queryDelete = " DELETE FROM tb_bitacora_paquete WHERE id_paquete=?";
        int result = jdbcTemplate.update(queryDelete,new Object[]{idPaquete});
        System.out.println(result);
    }
    
    
    @Override
    public Integer getMaxKilometrajeByIdBitacora(Integer idBitacora) {
        String query = " select max(kilometraje) kilometraje from tb_bitacora_historico where id_bitacora=?";
        return jdbcTemplate.queryForObject(query, new Object[]{idBitacora}, Integer.class);
    }    
    
    @Override
    public List<Map<String,Object>> getListBitacoraByParameters(String fechaInicio,String fechaFin,Integer idOperador, Integer idEnvio, Integer page, Integer offset) {
        String queryFechas = "";
        String query = " SELECT b.id, b.nombre, b.fecha, b.fecha_fin, b.km_inicial, b.km_final, bs.des_status, concat(u.nombre,' ',u.apellido_pat,' ',u.apellido_mat) operador, b.id_envio, IF ( b.id_envio = 1, 'RECOLECCIÓN', 'ENTREGA') des_envio, concat(v.no_economico,' ',v.placas) vehiculo, (SELECT 1 FROM tb_archivo_bitacora ab WHERE ab.id_bitacora=b.id limit 1) archivo,  "+
                       " CONCAT('',TIMEDIFF( " +
                       "  (select max(bh1.fecha) from tb_bitacora_historico bh1, tb_bitacora_status bs1 where bh1.id_bitacora=b.id and bh1.estatus=bs1.id and bs1.num_status=5 and bs1.id_envio=b.id_envio), " +
                       "  (SELECT min(bh1.fecha) from tb_bitacora_historico bh1, tb_bitacora_status bs1 where bh1.id_bitacora=b.id and bh1.estatus=bs1.id and bs1.num_status=2 and bs1.id_envio=b.id_envio) " +
                       " )) as total_time  "+
                       " FROM tb_bitacora b,tb_bitacora_status bs,tb_usuarios u, tb_vehiculo v WHERE b.id is not null AND b.id_status = bs.id AND u.id_usu=b.id_operador AND v.id=b.id_vehiculo ";
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");        
        Date parsedDate2 = null;
        Date parsedDate1 = null;
              
        
            if((fechaInicio !=null && !fechaInicio.isEmpty()) && (fechaFin !=null && !fechaFin.isEmpty())){
                    queryFechas = " AND date(b.fecha)>=date(?) and date(b.fecha_fin)<=date(?) ";
                
            }
            
            if(idOperador!=null){
                query += "  AND b.id_operador=? ";
            }

            if(idEnvio!=null){
                query += " AND b.id_envio=? ";
            }
            
//            System.out.println(query+queryFechas+" ORDER BY b.id");
//            Query querycreated = em.createNativeQuery(query+queryFechas+" ORDER BY b.id");
//            querycreated.setFirstResult(offset); 
//            querycreated.setMaxResults(count);                
            List parameters = new ArrayList();

            if(idOperador!=null){
                //querycreated.setParameter("idOperador", idOperador);
                parameters.add(idOperador);
            }
            
            if(idEnvio!=null){
                //querycreated.setParameter("idEnvio", idEnvio);
                parameters.add(idEnvio);
            }
            
            if((fechaInicio !=null && !fechaInicio.isEmpty()) && (fechaFin !=null && !fechaFin.isEmpty())){
                //querycreated.setParameter("fechaInicio", fechaInicio);
                //querycreated.setParameter("fechaFin", fechaFin);
                parameters.add(fechaInicio);
                parameters.add(fechaFin);
                
            }            
            

// Integer offset, Integer count
            query = query+queryFechas+" ORDER BY b.id limit "+page+","+offset;
            System.out.println(query);
            return jdbcTemplate.queryForList(query,parameters.toArray());
        
    }
    
    @Override
    public Long getCountBitacoraByParameters(String fechaInicio,String fechaFin,Integer idOperador, Integer idEnvio) {
        String queryFechas = "";
        String query = " SELECT count(*) "+
                       " FROM tb_bitacora b WHERE b.id is not null ";              
        
            if((fechaInicio !=null && !fechaInicio.isEmpty()) && (fechaFin !=null && !fechaFin.isEmpty())){
                    queryFechas = " AND date(b.fecha)>=date(?) and date(b.fecha_fin)<=date(?) ";
            }
            
            if(idOperador!=null){
                query += "  AND b.id_operador=? ";
            }

            if(idEnvio!=null){
                query += " AND b.id_envio=? ";
            }
            
            List parameters = new ArrayList();

            if(idOperador!=null){
                //querycreated.setParameter("idOperador", idOperador);
                parameters.add(idOperador);
            }
            
            if(idEnvio!=null){
                //querycreated.setParameter("idEnvio", idEnvio);
                parameters.add(idEnvio);
            }
            
            if((fechaInicio !=null && !fechaInicio.isEmpty()) && (fechaFin !=null && !fechaFin.isEmpty())){
                //querycreated.setParameter("fechaInicio", fechaInicio);
                //querycreated.setParameter("fechaFin", fechaFin);
                parameters.add(fechaInicio);
                parameters.add(fechaFin);
                
            }            

            query = query+queryFechas;

            return jdbcTemplate.queryForObject(query,parameters.toArray(),Long.class);
    }    

    @Override
    public List<Map<String,Object>> getListBitacoraWithHistorico(String fechaInicio,String fechaFin,Integer idOperador, Integer idEnvio) {
        String queryFechas = "";
        String query = " SELECT b.id, b.nombre, b.fecha, b.fecha_fin, b.km_inicial, b.km_final, bs.des_status, concat(u.nombre,' ',u.apellido_pat,' ',u.apellido_mat) operador, b.id_envio, IF ( b.id_envio = 1, 'RECOLECCIÓN', 'ENTREGA') des_envio, concat(v.no_economico,' ',v.placas) vehiculo, bsh.des_status estatus_historico, bh.fecha fecha_h, bh.kilometraje, bh.observaciones   "+
                       " FROM tb_bitacora b,tb_bitacora_status bs,tb_usuarios u, tb_vehiculo v, tb_bitacora_status bsh, tb_bitacora_historico bh WHERE b.id is not null AND b.id_status = bs.id AND u.id_usu=b.id_operador AND v.id=b.id_vehiculo AND bh.id_bitacora=b.id AND bsh.id=bh.estatus ";
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");        
        Date parsedDate2 = null;
        Date parsedDate1 = null;
              
        
            if((fechaInicio !=null && !fechaInicio.isEmpty()) && (fechaFin !=null && !fechaFin.isEmpty())){
                    queryFechas = " AND date(b.fecha)>=date(?) and date(b.fecha_fin)<=date(?) ";
                
            }
            
            if(idOperador!=null){
                query += "  AND b.id_operador=? ";
            }

            if(idEnvio!=null){
                query += " AND b.id_envio=? ";
            }
            
            List parameters = new ArrayList();

            if(idOperador!=null){
                //querycreated.setParameter("idOperador", idOperador);
                parameters.add(idOperador);
            }
            
            if(idEnvio!=null){
                //querycreated.setParameter("idEnvio", idEnvio);
                parameters.add(idEnvio);
            }
            
            if((fechaInicio !=null && !fechaInicio.isEmpty()) && (fechaFin !=null && !fechaFin.isEmpty())){
                //querycreated.setParameter("fechaInicio", fechaInicio);
                //querycreated.setParameter("fechaFin", fechaFin);
                parameters.add(fechaInicio);
                parameters.add(fechaFin);
                
            }            
            
            query = query+queryFechas;
            return jdbcTemplate.queryForList(query,parameters.toArray());
        
    }


    @Override
    public List<Map<String,Object>> getAllListBitacoraTiempoEstatus(String fechaInicio,String fechaFin,Integer idOperador, Integer idEnvio) {
        String queryFechas = "";
        String query = " SELECT b.id, b.nombre, b.fecha, b.fecha_fin, b.km_inicial, b.km_final, bs.des_status, concat(u.nombre,' ',u.apellido_pat,' ',u.apellido_mat) operador, b.id_envio, IF ( b.id_envio = 1, 'RECOLECCIÓN', 'ENTREGA') des_envio, concat(v.no_economico,' ',v.placas) vehiculo "+
                       " FROM tb_bitacora b,tb_bitacora_status bs,tb_usuarios u, tb_vehiculo v WHERE b.id is not null AND b.id_status = bs.id AND u.id_usu=b.id_operador AND v.id=b.id_vehiculo ";
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");        
        Date parsedDate2 = null;
        Date parsedDate1 = null;
              
        
            if((fechaInicio !=null && !fechaInicio.isEmpty()) && (fechaFin !=null && !fechaFin.isEmpty())){
                    queryFechas = " AND date(b.fecha)>=date(?) and date(b.fecha_fin)<=date(?) ";
                
            }
            
            if(idOperador!=null){
                query += "  AND b.id_operador=? ";
            }

            if(idEnvio!=null){
                query += " AND b.id_envio=? ";
            }
            
            List parameters = new ArrayList();

            if(idOperador!=null){
                //querycreated.setParameter("idOperador", idOperador);
                parameters.add(idOperador);
            }
            
            if(idEnvio!=null){
                //querycreated.setParameter("idEnvio", idEnvio);
                parameters.add(idEnvio);
            }
            
            if((fechaInicio !=null && !fechaInicio.isEmpty()) && (fechaFin !=null && !fechaFin.isEmpty())){
                //querycreated.setParameter("fechaInicio", fechaInicio);
                //querycreated.setParameter("fechaFin", fechaFin);
                parameters.add(fechaInicio);
                parameters.add(fechaFin);
                
            }            
            
            query = query+queryFechas;
            return jdbcTemplate.queryForList(query,parameters.toArray());
        
    }

    @Override
    public List<Map<String, Object>> getAllHistoricoByBitacoraId(Integer idBitacora,Integer idEnvio) {
        String query = " SELECT bh.id, bh.id_bitacora, bh.fecha,bh.estatus,bs.des_status,bs.num_status,bh.observaciones, "+
        " case bs.num_status "+
        "   when 3 then "+
        "   CONCAT('',TIMEDIFF(  "+
        " 	(select max(bh1.fecha) from tb_bitacora_historico bh1, tb_bitacora_status bs1 where bh1.id_bitacora=bh.id_bitacora and bh1.estatus=bs1.id and bs1.num_status=3 and bs1.id_envio=:idEnvio), "+
        " 	(SELECT max(bh1.fecha) from tb_bitacora_historico bh1, tb_bitacora_status bs1 where bh1.id_bitacora=bh.id_bitacora and bh1.estatus=bs1.id and bs1.num_status=2 and bs1.id_envio=:idEnvio) "+
        " 	))    "+
        "    when 4 then "+
        "    CONCAT('',TIMEDIFF(   "+
        " 	(select max(bh1.fecha) from tb_bitacora_historico bh1, tb_bitacora_status bs1 where bh1.id_bitacora=bh.id_bitacora and bh1.estatus=bs1.id and bs1.num_status=4 and bs1.id_envio=:idEnvio), "+
        " 	(SELECT max(bh1.fecha) from tb_bitacora_historico bh1, tb_bitacora_status bs1 where bh1.id_bitacora=bh.id_bitacora and bh1.estatus=bs1.id and bs1.num_status=3 and bs1.id_envio=:idEnvio) "+
        " 	))       "+
        "    when 5 then "+
        "    CONCAT('',TIMEDIFF(   "+
        " 	(select max(bh1.fecha) from tb_bitacora_historico bh1, tb_bitacora_status bs1 where bh1.id_bitacora=bh.id_bitacora and bh1.estatus=bs1.id and bs1.num_status=5 and bs1.id_envio=:idEnvio), "+
        " 	(SELECT max(bh1.fecha) from tb_bitacora_historico bh1, tb_bitacora_status bs1 where bh1.id_bitacora=bh.id_bitacora and bh1.estatus=bs1.id and bs1.num_status=4 and bs1.id_envio=:idEnvio) "+
        " 	))          "+
        " end as tiempo    "+
        " FROM tb_bitacora_historico bh, tb_bitacora_status bs WHERE bh.id_bitacora=:idBitacora AND bh.estatus=bs.id AND bs.num_status IN (2,3,4,5) order by bh.id ASC ";

        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
        .addValue("idBitacora", idBitacora)
        .addValue("idEnvio", idEnvio); 
        System.out.println("id bitacora: "+idBitacora);
        return namedTemplate.queryForList(query,sqlParameterSource);
    }

    /**
     * @return the namedTemplate
     */
    public NamedParameterJdbcTemplate getNamedTemplate() {
        return namedTemplate;
    }

    /**
     * @param namedTemplate the namedTemplate to set
     */
    public void setNamedTemplate(NamedParameterJdbcTemplate namedTemplate) {
        this.namedTemplate = namedTemplate;
    }
    
    
    @Override
    public void updateBitacoraRutaActivoInactivo(Integer idRuta, Integer idBitacora, Integer activo) {
        String queryDelete = " UPDATE tb_bitacora SET activo=? WHERE id_ruta=? AND id!=?";
        int result = jdbcTemplate.update(queryDelete,new Object[]{activo,idRuta,idBitacora});
        System.out.println(result);
    }    

    @Override
    public void updateBitacoraRutaActivoInactivo(Integer idRuta, Integer activo) {
        String queryDelete = " UPDATE tb_bitacora SET activo=? WHERE id_ruta=?";
        int result = jdbcTemplate.update(queryDelete,new Object[]{activo,idRuta});
        System.out.println(result);
    }
    
    @Override
    public List<Map<String, Object>> getAllBitacoraByUsuarioEnvio(Integer idUsuario,Integer idEnvio) {
        String query = " SELECT b.id, b.nombre,b.km_inicial,b.km_final,b.fecha,b.fecha_fin,b.id_ruta,b.id_status,b.is_final,b.is_pausa,b.activo, bs.des_status, r.nombre nombre_ruta "+
        " FROM tb_bitacora b, tb_bitacora_status bs, tb_ruta r WHERE b.id_operador=:idOperador AND b.id_envio=:idEnvio AND b.id_status=bs.id AND bs.num_status NOT IN (8) AND b.id_ruta=r.id";

        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
        .addValue("idOperador", idUsuario)
        .addValue("idEnvio", idEnvio); 
        return namedTemplate.queryForList(query,sqlParameterSource);
    }    

    @Override
    public List<Map<String, Object>> getAllBitacoraHistoricoByIdBitacora(Integer idBitacora) {
        String query = "  SELECT bh.id, bh.fecha,bh.estatus,bh.kilometraje, bh.observaciones,bs.des_status  "+
        "  FROM tb_bitacora_status bs, tb_bitacora_historico bh WHERE bh.id_bitacora=:idBitacora AND bh.estatus=bs.id order by bh.estatus ASC ";
        SqlParameterSource sqlParameterSource = new MapSqlParameterSource()
        .addValue("idBitacora", idBitacora);
        return namedTemplate.queryForList(query,sqlParameterSource);
    }    
}
