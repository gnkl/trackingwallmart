/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.novartis.repository;

import java.util.List;
import java.util.Map;
import mx.com.gnkl.novartis.model.TbBitacora;

/**
 *
 * @author jmejia
 */
public interface EstadisticaDao {
    
    public List<Map<String, Object>> getTotalByNotInStatus(Integer tipoEnvio, List<Integer> statusList);
    
    public List<Map<String, Object>> getTotalByStatusIn(Integer tipoEnvio, List<Integer> statusList);
    
    public Long getTotalBitacoras(Integer tipoEnvio, List<Integer> statusList);
    
    public Long getCountByOperadorVehiculo(Integer idOperador, Integer idVehiculo);
    
    public List<TbBitacora> getListByOperadorVehiculo(Integer idOperador, Integer idVehiculo);
    
    
    
}
