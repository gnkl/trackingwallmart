package mx.com.gnkl.novartis.repository.jpa;

import java.util.List;
import mx.com.gnkl.novartis.model.TbUsuarios;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
//import mx.com.gnkl.novartis.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface UserRepository extends JpaRepository<TbUsuarios, Integer> {
    
    @Query("SELECT t FROM TbUsuarios t WHERE t.usuario = :usuario")
    TbUsuarios findDataByUsuario(@Param("usuario") String usuario);   

    @Query("SELECT t FROM TbUsuarios t WHERE t.usuario = :usuario and t.pass=:password")
    TbUsuarios findDataByUsuario(@Param("usuario") String usuario, @Param("password") String password);
    
    //@Query(value="SELECT distinct(usu.id_usu) row, usu.* FROM tb_usuarios usu, tb_usuario_rol rel, tb_roles rol WHERE rol.des_rol=:rol AND rol.id_rol=rel.id_rol AND usu.id_usu=rel.id_usu AND usu.status=:estatus AND usu.nombre LIKE :name OR usu.usuario LIKE :name LIMIT 20",nativeQuery = true)
    @Query(value="SELECT distinct(usu.id_usu) row, usu.* FROM tb_usuarios usu, tb_usuario_rol rel, tb_roles rol WHERE rol.des_rol=:rol AND rol.id_rol=rel.id_rol AND usu.id_usu=rel.id_usu AND usu.status=:estatus AND usu.usuario LIKE :name LIMIT 20",nativeQuery = true)
    List<TbUsuarios> getAllOperadoresByEstatus(@Param("rol") String rol, @Param("estatus") String estatus, @Param("name") String name );

    @Query(value="SELECT usu.* FROM tb_usuarios usu, tb_usuario_rol rel, tb_roles rol WHERE rol.des_rol=:rol AND rol.id_rol=rel.id_rol AND usu.id_usu=rel.id_usu AND usu.status=:estatus",nativeQuery = true)
    List<TbUsuarios> getAllOperadoresByEstatus(@Param("rol") String rol, @Param("estatus") String estatus);
    
    //@Query(value="SELECT usu.* FROM tb_usuarios usu, tb_usuario_rol rel, tb_roles rol WHERE rol.des_rol=:rol AND rol.id_rol=rel.id_rol AND usu.id_usu=rel.id_usu AND usu.status=:estatus AND usu.nombre LIKE :name OR usu.usuario LIKE :name AND usu.id_usu=:idusu",nativeQuery = true)
    @Query(value="SELECT usu.* FROM tb_usuarios usu, tb_usuario_rol rel, tb_roles rol WHERE rol.des_rol=:rol AND rol.id_rol=rel.id_rol AND usu.id_usu=rel.id_usu AND usu.status=:estatus AND usu.usuario LIKE :name AND usu.id_usu=:idusu",nativeQuery = true)
    List<TbUsuarios> getAllOperadoresByEstatus(@Param("rol") String rol, @Param("estatus") String estatus, @Param("name") String name, @Param("idusu") Integer idusu);
    
    @Query(value="SELECT usu.* FROM tb_usuarios usu, tb_usuario_rol rel, tb_roles rol WHERE rol.des_rol IN(:roles) AND rol.id_rol=rel.id_rol AND usu.id_usu=rel.id_usu LIMIT :offset,:count" ,nativeQuery = true)
    List<TbUsuarios> getAllOperadoresByRoles(@Param("roles") List<String> roles, @Param("offset") Integer offset, @Param("count") Integer count);

    @Query(value="SELECT usu.* FROM tb_usuarios usu WHERE usu.id_usu not in (SELECT rel.id_usu FROM tb_usuario_rol rel, tb_roles rol WHERE rol.des_rol=:role AND rol.id_rol=rel.id_rol) LIMIT :offset,:count" ,nativeQuery = true)
    List<TbUsuarios> getAllOperadoresNotInRole(@Param("role") String role, @Param("offset") Integer offset, @Param("count") Integer count);    
    
    @Query(value="SELECT count(*) FROM tb_usuarios usu, tb_usuario_rol rel, tb_roles rol WHERE rol.des_rol IN(:roles) AND rol.id_rol=rel.id_rol AND usu.id_usu=rel.id_usu LIMIT :offset,:count",nativeQuery = true)
    Integer getCountOperadoresByRoles(@Param("roles") List<String> roles, @Param("offset") Integer offset, @Param("count") Integer count);
    
    @Query(value="SELECT count(*) FROM tb_usuarios usu WHERE usu.id_usu not in (SELECT rel.id_usu FROM tb_usuario_rol rel, tb_roles rol WHERE rol.des_rol=:role AND rol.id_rol=rel.id_rol)" ,nativeQuery = true)
    Integer getCountOperadoresNotInRole(@Param("role") String role);    

    @Query(value="SELECT t FROM TbUsuarios t WHERE t.usuario LIKE :usuario OR t.nombre LIKE:usuario")
    List<TbUsuarios> getAllUsersLike(@Param("usuario") String usuario, Pageable pageable);    

    @Query(value="SELECT usu.* FROM tb_usuarios usu WHERE usu.nombre like :usuario OR usu.usuario like :usuario and usu.id_usu not in (SELECT rel.id_usu FROM tb_usuario_rol rel, tb_roles rol WHERE rol.des_rol=:role AND rol.id_rol=rel.id_rol) LIMIT :offset,:count" ,nativeQuery = true)
    List<TbUsuarios> getAllUsersNotInRole(@Param("usuario") String usuario, @Param("role") String role, @Param("offset") Integer offset, @Param("count") Integer count);     
}
