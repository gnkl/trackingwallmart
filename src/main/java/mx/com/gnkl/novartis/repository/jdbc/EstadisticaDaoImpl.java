/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.novartis.repository.jdbc;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;
import mx.com.gnkl.novartis.model.TbBitacora;
import mx.com.gnkl.novartis.repository.EstadisticaDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import javax.persistence.Query;

/**
 *
 * @author jmejia
 */
@Repository
public class EstadisticaDaoImpl implements EstadisticaDao{

    private JdbcTemplate jdbcTemplate;
    
    //@PersistenceContext(unitName = "novartisPU")
    @PersistenceContext
    private EntityManager em;  
    
    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }
    
    /**
     * @return the jdbcTemplate
     */
    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    /**
     * @param jdbcTemplate the jdbcTemplate to set
     */
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
    
    
    @Override
    public List<Map<String, Object>> getTotalByNotInStatus(Integer tipoEnvio, List<Integer> statusList) {
        String query = " SELECT count(*) total ,s.des_status, s.id, s.num_status " +
                       " FROM tb_bitacora b, tb_bitacora_status s " +
                       " where b.id_envio=? and b.id_status not in (?) and s.id=b.id_status group by b.id_status ";
        return this.jdbcTemplate.queryForList(query, new Object[]{tipoEnvio, statusList});
    }

    @Override
    public List<Map<String, Object>> getTotalByStatusIn(Integer tipoEnvio, List<Integer> statusList) {
        String listString = "";
        for(int i=0;i<statusList.size();i++){
            if(i+1==statusList.size()){
                listString += statusList.get(i);
            }else{
                listString += statusList.get(i)+",";
            }
        }
        String query = " SELECT count(*) total ,s.des_status, s.id, s.num_status FROM tb_bitacora b, tb_bitacora_status s where b.id_envio="+tipoEnvio+" and b.id_status in ("+listString+") and s.id=b.id_status group by b.id_status ";
        return this.jdbcTemplate.queryForList(query);
    }
    
    @Override
    public Long getTotalBitacoras(Integer tipoEnvio, List<Integer> statusList) {
        String query = " SELECT count(*) total " +
                       " FROM tb_bitacora b" +
                       " where b.id_envio=? and b.id_status not in (?) ";
        return this.jdbcTemplate.queryForObject(query, new Object[]{tipoEnvio, statusList},Long.class);
    }

    @Override
    public Long getCountByOperadorVehiculo(Integer idOperador, Integer idVehiculo) {
        String query = " SELECT count(*) total FROM tb_bitacora b WHERE b.id is not null ";
        List<Object> params = new ArrayList<Object>();
        
        if(idOperador!=null && idOperador>0){
            query += " AND id_operador=?  ";
            params.add(idOperador);
        }
        if(idVehiculo!=null && idVehiculo>0){
            query += " AND id_vehiculo=?  ";
            params.add(idVehiculo);
        }
        return this.jdbcTemplate.queryForObject(query, params.toArray(), Long.class);
    }
    
    @Override
    public List<TbBitacora> getListByOperadorVehiculo(Integer idOperador, Integer idVehiculo){
      
            String query = " SELECT b FROM TbBitacora b WHERE b.id is not null ";
            
            if(idOperador!=null && idOperador>0){
                query += " AND idOperador.idUsu=:idOperador ";
            }
            if(idVehiculo!=null && idVehiculo>0){
                query += " AND idVehiculo.id=:idVehiculo  ";
            }            
            
            Query querycreated = em.createQuery(query);
            
            if(idOperador!=null && idOperador>0){
                querycreated.setParameter("idOperador", idOperador);
            }
            if(idVehiculo!=null && idVehiculo>0){
                querycreated.setParameter("idVehiculo", idVehiculo);
            }            
            
            return querycreated.getResultList();            
    }
    
    
}
