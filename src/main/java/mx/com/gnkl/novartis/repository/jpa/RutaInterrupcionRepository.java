/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.novartis.repository.jpa;

import mx.com.gnkl.novartis.model.TbRutaInterrupcion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author jmejia
 */
public interface RutaInterrupcionRepository extends JpaRepository<TbRutaInterrupcion, Integer> {
    
    @Query(value="SELECT * FROM tb_ruta_interrupcion tri  WHERE tri.id_ruta=:idruta AND tri.status=0 ORDER BY id DESC LIMIT 1", nativeQuery = true)    
    public TbRutaInterrupcion getRutaInterrupcionByIdRuta(@Param("idruta") Integer idRuta);
}
