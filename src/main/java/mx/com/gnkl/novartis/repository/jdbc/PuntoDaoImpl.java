/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.novartis.repository.jdbc;

import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.sql.DataSource;
import mx.com.gnkl.novartis.model.TbPuntoEo;
import mx.com.gnkl.novartis.repository.PuntoDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jmejia
 */
@Repository
public class PuntoDaoImpl implements PuntoDao{

    
    private JdbcTemplate jdbcTemplate;
    
    //@PersistenceContext(unitName = "novartisPU")
    @PersistenceContext
    private EntityManager em;    
    
    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }
    
    /**
     * @return the jdbcTemplate
     */
    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    /**
     * @param jdbcTemplate the jdbcTemplate to set
     */
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }


    @Override
    public List<TbPuntoEo> getCustomQueryData(String query, Integer offset, Integer count) {
        Query querycreated = em.createQuery(query);
        querycreated.setFirstResult(offset); 
        querycreated.setMaxResults(count);        
        List <TbPuntoEo> paqueteList = querycreated.getResultList();
        return paqueteList;
    }

    @Override
    public Long getCountCustomQueryData(String query) {
        Query querycreated = em.createQuery(query);
        Long total = (Long) querycreated.getSingleResult();
        return total;
    }

    @Override
    public void persistPunto(TbPuntoEo punto) {
        try{
            String sql = "INSERT INTO tb_punto_eo (nombre_punto, direccion, estatus, grupo, cliente, entrega, origen, ciudad, estado, comision, telefono, rfc, cp) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE nombre_punto=nombre_punto,cliente=cliente,ciudad=ciudad,estado=estado,cp=cp,grupo=grupo  ";
            jdbcTemplate.update(sql, new Object[] {punto.getNombrePunto(), punto.getDireccion(), punto.getEstatus(), punto.getGrupo(), punto.getCliente(),
                punto.getEntrega(), punto.getOrigen(), punto.getCiudad(), punto.getEstado(), punto.getComision(), punto.getTelefono(), punto.getRfc(), punto.getCp()});
        }catch(Exception ex){
            System.out.println(ex.getLocalizedMessage());
        }
    }
    
    
    @Override
    public List<String> getDistinctGrupo(Integer entrega) {
        String query = " SELECT DISTINCT(grupo) grupo FROM tb_punto_eo where entrega=? ";
        return jdbcTemplate.queryForList(query, new Object[]{entrega}, String.class);
    }         

    @Override
    public List<Integer> getDistinctIdPuntoEntrega(String grupo) {
        String query = " SELECT id FROM tb_punto_eo where grupo=? ";
        return jdbcTemplate.queryForList(query, new Object[]{grupo}, Integer.class);
    } 
    
    @Override
    public List<String> getClientesByGrupoPunto(String grupo) {
        String query = " SELECT cliente FROM tb_punto_eo where grupo=? group by cliente ";
        return jdbcTemplate.queryForList(query, new Object[]{grupo},String.class);
    }     

    @Override
    public List<Integer> getDistinctIdPuntoEntrega(String grupo, String cliente) {
        String query = " SELECT id FROM tb_punto_eo where grupo=? and cliente=? ";
        return jdbcTemplate.queryForList(query, new Object[]{grupo, cliente}, Integer.class);
    }
    
}
