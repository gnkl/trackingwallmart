/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.novartis.repository.jpa;

import java.util.List;
import mx.com.gnkl.novartis.model.TbVehiculo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author jmejia
 */
public interface VehiculoRepository extends JpaRepository<TbVehiculo, Integer>{
    
    @Query("SELECT t FROM TbVehiculo t WHERE t.noEconomico=:noEconomico")
    TbVehiculo getVehiculoByNoEconomico(@Param("noEconomico") String noEconomico);    
}
