package mx.com.gnkl.novartis.repository.jpa;

import java.util.List;
import mx.com.gnkl.novartis.model.TbPaquetes;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface PaqueteRepository extends JpaRepository<TbPaquetes, Integer> {

    @Query(value="SELECT paq FROM TbPaquetes paq LEFT JOIN paq.idOperador ope LEFT JOIN paq.idPuntoorigen LEFT JOIN paq.idUsu LEFT JOIN paq.idStatus LEFT JOIN paq.idPuntoe ORDER BY paq.fechaRecPaq DESC")
    List<TbPaquetes> getAllPaquetes(Pageable pageable);
    
    @Query(value="SELECT paq FROM TbPaquetes paq LEFT JOIN paq.idOperador ope LEFT JOIN paq.idPuntoorigen LEFT JOIN paq.idUsu LEFT JOIN paq.idStatus LEFT JOIN paq.idPuntoe WHERE paq.noEmbarque LIKE :noEmbarque ORDER BY paq.noEmbarque DESC")
    List<TbPaquetes> getAllPaquetesByNoEmbarque(@Param("noEmbarque") String noEmbarque, Pageable pageable);

    //@Query(value="SELECT paq FROM TbPaquetes paq LEFT JOIN paq.idOperador ope LEFT JOIN paq.idPuntoorigen LEFT JOIN paq.idUsu LEFT JOIN paq.idStatus LEFT JOIN paq.idPuntoe WHERE paq.noEmbarque LIKE :noEmbarque AND paq.idStatus.numStatus!=1")
    @Query(value="SELECT paq FROM TbPaquetes paq LEFT JOIN paq.idOperador ope LEFT JOIN paq.idPuntoorigen LEFT JOIN paq.idUsu LEFT JOIN paq.idStatus LEFT JOIN paq.idPuntoe WHERE paq.noEmbarque LIKE :noEmbarque")
    List<TbPaquetes> getAllPaquetesByNoEmbarqueStatus(@Param("noEmbarque") String noEmbarque, Pageable pageable);    
    
    @Query(value="SELECT paq FROM TbPaquetes paq LEFT JOIN paq.idOperador ope LEFT JOIN paq.idPuntoorigen LEFT JOIN paq.idUsu usu LEFT JOIN paq.idStatus LEFT JOIN paq.idPuntoe WHERE paq.noEmbarque LIKE :noEmbarque AND paq.idOperador.idUsu=:idusu ORDER BY paq.fechaRecPaq DESC")
    List<TbPaquetes> getAllPaquetesByNoEmbarque(@Param("noEmbarque") String noEmbarque, @Param("idusu") Integer idusu,Pageable pageable);

    @Query(value="SELECT count(paq) FROM TbPaquetes paq LEFT JOIN paq.idPuntoorigen LEFT JOIN paq.idUsu usu LEFT JOIN paq.idStatus LEFT JOIN paq.idPuntoe WHERE paq.noEmbarque LIKE :noEmbarque AND paq.idOperador.idUsu=:idusu")
    Integer getCountPaquetesByNoEmbarque(@Param("noEmbarque") String noEmbarque, @Param("idusu") Integer idusu);

    @Query(value="SELECT count(paq) FROM TbPaquetes paq LEFT JOIN paq.idPuntoorigen LEFT JOIN paq.idUsu usu LEFT JOIN paq.idStatus LEFT JOIN paq.idPuntoe WHERE paq.noEmbarque=:noEmbarque")
    Integer getCountPaquetesByNoEmbarque(@Param("noEmbarque") String noEmbarque);
    
    @Query(value="SELECT paq FROM TbPaquetes paq LEFT JOIN paq.idOperador ope LEFT JOIN paq.idPuntoorigen LEFT JOIN paq.idUsu LEFT JOIN paq.idStatus LEFT JOIN paq.idPuntoe WHERE paq.noEmbarque=:noEmbarque")
    List<TbPaquetes> getPaqueteByNoEmbarqueFase(@Param("noEmbarque") String noEmbarque);

    @Query(value="SELECT paq FROM TbPaquetes paq LEFT JOIN paq.idOperador ope LEFT JOIN paq.idPuntoorigen LEFT JOIN paq.idUsu LEFT JOIN paq.idStatus LEFT JOIN paq.idPuntoe WHERE paq.noEmbarque=:noEmbarque")
    TbPaquetes getPaqueteByNoEmbarque(@Param("noEmbarque") String noEmbarque);    
    
    @Query(value="SELECT paq FROM TbPaquetes paq LEFT JOIN paq.idOperador ope LEFT JOIN paq.idPuntoorigen LEFT JOIN paq.idUsu usu LEFT JOIN paq.idStatus LEFT JOIN paq.idPuntoe WHERE paq.noEmbarque=:noEmbarque AND paq.idOperador.idUsu=:idusu")
    TbPaquetes getPaqueteByNoEmbarqueIdUser(@Param("noEmbarque") String noEmbarque, @Param("idusu") Integer idusu);
    
    @Query(value="SELECT paq FROM TbPaquetes paq LEFT JOIN paq.idOperador ope LEFT JOIN paq.idPuntoorigen LEFT JOIN paq.idUsu LEFT JOIN paq.idStatus LEFT JOIN paq.idPuntoe WHERE paq.idPaq=:idpaq")
    TbPaquetes getPaqueteByIdPaq(@Param("idpaq") Integer idpaq);
    
    @Query(value="SELECT paq FROM TbPaquetes paq LEFT JOIN paq.idOperador ope LEFT JOIN paq.idPuntoorigen LEFT JOIN paq.idUsu LEFT JOIN paq.idStatus LEFT JOIN paq.idPuntoe WHERE paq.idOperador.usuario=:username")
    List<TbPaquetes> getAllPaquetesByUsername(@Param("username") String username, Pageable pageable);

    @Query(value="SELECT paq FROM TbPaquetes paq LEFT JOIN paq.idOperador ope LEFT JOIN paq.idPuntoorigen LEFT JOIN paq.idUsu LEFT JOIN paq.idStatus LEFT JOIN paq.idPuntoe WHERE paq.idOperador.idUsu=:idusu ORDER BY paq.fechaEntPaq DESC")
    List<TbPaquetes> getAllPaquetesByIdUser(@Param("idusu") Integer idusu, Pageable pageable);

    @Query(value="SELECT count(paq) FROM TbPaquetes paq LEFT JOIN paq.idOperador ope LEFT JOIN paq.idPuntoorigen LEFT JOIN paq.idUsu LEFT JOIN paq.idStatus LEFT JOIN paq.idPuntoe WHERE paq.idOperador.idUsu=:idusu")
    Integer getCountAllPaquetesByIdUser(@Param("idusu") Integer idusu);

    @Query(value="SELECT paq FROM TbPaquetes paq LEFT JOIN paq.idOperador ope LEFT JOIN paq.idPuntoorigen LEFT JOIN paq.idUsu LEFT JOIN paq.idStatus LEFT JOIN paq.idPuntoe WHERE paq.idOperador.idUsu=:idusu AND paq.idRuta.id=:idRuta")
    List<TbPaquetes> getAllPaquetesByIdUserRuta(@Param("idusu") Integer idusu, @Param("idRuta") Integer idRuta, Pageable pageable);

    @Query(value="SELECT count(paq) FROM TbPaquetes paq LEFT JOIN paq.idOperador ope LEFT JOIN paq.idPuntoorigen LEFT JOIN paq.idUsu LEFT JOIN paq.idStatus LEFT JOIN paq.idPuntoe WHERE paq.idOperador.idUsu=:idusu AND paq.idRuta.id=:idRuta")
    Long getCountAllPaquetesByIdUserRuta(@Param("idusu") Integer idusu, @Param("idRuta") Integer idRuta);

    @Query(value="SELECT paq FROM TbPaquetes paq LEFT JOIN paq.idOperador ope LEFT JOIN paq.idPuntoorigen LEFT JOIN paq.idUsu LEFT JOIN paq.idStatus LEFT JOIN paq.idPuntoe WHERE paq.idRuta.id=:idRuta")
    List<TbPaquetes> getAllPaquetesByRuta(@Param("idRuta") Integer idRuta, Pageable pageable);    
    
    @Query(value="SELECT COUNT(paq) FROM TbPaquetes paq LEFT JOIN paq.idOperador ope LEFT JOIN paq.idPuntoorigen LEFT JOIN paq.idUsu LEFT JOIN paq.idStatus LEFT JOIN paq.idPuntoe WHERE paq.idRuta.id=:idRuta")
    Long getCountAllPaquetesByRuta(@Param("idRuta") Integer idRuta);   
    
    @Query(value="SELECT MAX(paq.idPaq) FROM TbPaquetes paq WHERE paq.noEmbarque=:noEmbarque")
    Integer getMaxIdPaqByNoEmbarque(@Param("noEmbarque") String noEmbarque);
    
    @Query(value="SELECT DISTINCT(paq1.no_embarque) FROM tb_paquetes paq1, tb_paquetes paq2 WHERE paq1.id_paq <> paq2.id_paq AND paq1.no_embarque = paq2.no_embarque and paq1.id_empresa=2 and paq1.id_empresa=paq2.id_empresa and paq1.no_embarque like :noEmbarque",nativeQuery = true)
    List<String> getNoEmbarqueCrossDockPaquetes(@Param("noEmbarque") String noEmbarque);

    @Query(value="SELECT paq FROM TbPaquetes paq LEFT JOIN paq.idOperador ope LEFT JOIN paq.idPuntoorigen LEFT JOIN paq.idUsu LEFT JOIN paq.idStatus LEFT JOIN paq.idPuntoe WHERE paq.noEmbarque IN (:listNoEmbarque)")
    List<TbPaquetes> getListCrossDockPaquetes(@Param("listNoEmbarque") List<String> noEmbarque, Pageable pageable);    
    
    @Query(value="SELECT paq FROM TbPaquetes paq LEFT JOIN paq.idOperador ope LEFT JOIN paq.idPuntoorigen LEFT JOIN paq.idUsu LEFT JOIN paq.idStatus LEFT JOIN paq.idPuntoe WHERE paq.noEmbarque LIKE :noEmbarque AND paq.noEmbarque NOT IN (:listNoEmbarque)")
    List<TbPaquetes> getAllPaquetesByNoEmbarqueStatusNotIn(@Param("listNoEmbarque") List<String> list, @Param("noEmbarque") String noEmbarque, Pageable pageable);    

    @Query(value="SELECT DISTINCT(paq1.no_embarque) FROM tb_paquetes paq1 WHERE paq1.id_empresa=2 and paq1.no_embarque like :noEmbarque",nativeQuery = true)
    List<String> getNoTrackingByName(@Param("noEmbarque") String noEmbarque);

    @Query(value="SELECT count(paq.no_embarque) FROM tb_paquetes paq, tb_status stat where paq.id_empresa=:empresaid and stat.id_empresa=:empresaid and stat.id_envio=:tipoenvio and paq.id_status=stat.id_status and no_embarque=:notracking and stat.des_status!='CANCELADO' ",nativeQuery = true)
    Integer getCountNoTrackingByEmpresaEnvio(@Param("empresaid") Integer empresaid, @Param("tipoenvio") Integer tipoenvio, @Param("notracking") String notracking);

    @Query(value="SELECT paq FROM TbPaquetes paq LEFT JOIN paq.idOperador ope LEFT JOIN paq.idPuntoorigen LEFT JOIN paq.idUsu LEFT JOIN paq.idStatus LEFT JOIN paq.idPuntoe WHERE paq.idPuntoorigen.id=:origen AND paq.idPuntoe.id=:destino")
    List<TbPaquetes> getAllPaquetesByPuntos(@Param("origen") Integer ptoOrigen, @Param("destino") Integer ptoDestino,Pageable pageable);

    @Query(value="SELECT paq FROM TbPaquetes paq LEFT JOIN paq.idOperador ope LEFT JOIN paq.idPuntoorigen LEFT JOIN paq.idUsu LEFT JOIN paq.idStatus LEFT JOIN paq.idPuntoe WHERE paq.idPuntoorigen.id=:origen AND paq.idPuntoe.id=:destino AND paq.idStatus.idStatus=:status AND paq.fase=:fase")
    List<TbPaquetes> getAllPaquetesByPuntos(@Param("origen") Integer ptoOrigen, @Param("destino") Integer ptoDestino,@Param("fase") Integer fase, @Param("status") Integer status, Pageable pageable);    

    @Query(value="SELECT COUNT(paq) FROM TbPaquetes paq WHERE paq.idPuntoorigen.id=:origen AND paq.idPuntoe.id=:destino AND paq.idStatus.idStatus=:status AND paq.fase=:fase")
    Integer getCountAllPaquetesByPuntos(@Param("origen") Integer ptoOrigen, @Param("destino") Integer ptoDestino,@Param("fase") Integer fase, @Param("status") Integer status);        
    
    @Query(value="SELECT paq FROM TbPaquetes paq LEFT JOIN paq.idOperador ope LEFT JOIN paq.idPuntoorigen LEFT JOIN paq.idUsu LEFT JOIN paq.idStatus LEFT JOIN paq.idPuntoe WHERE paq.idPuntoorigen.id=:origen AND paq.idPuntoe.id=:destino")
    List<TbPaquetes> getAllPaquetesByPuntos(@Param("origen") Integer ptoOrigen, @Param("destino") Integer ptoDestino);    
    
    @Query(value="SELECT paq FROM TbPaquetes paq WHERE paq.fase=:fase AND paq.idStatus.idStatus=:status")
    List<TbPaquetes> getAllPaquetesByFaseStatus(@Param("fase") Integer fase, @Param("status") Integer status);

    @Query(value="SELECT paq FROM TbPaquetes paq WHERE paq.fase=:fase AND paq.idStatus.idStatus=:status AND paq.idPuntoorigen.id=:origen AND paq.idPuntoe.id=:destino")
    List<TbPaquetes> getAllPaquetesByFaseStatusDestinos(@Param("fase") Integer fase, @Param("status") Integer status, @Param("origen") Integer ptoOrigen, @Param("destino") Integer ptoDestino);

    @Query(value="SELECT paq FROM TbPaquetes paq WHERE paq.idPuntoe.id IN (:listpunto) AND paq.idStatus.idStatus IN (:status) AND fase IN (0,2)")
    List<TbPaquetes> getAllPaquetesByListPuntoStatus(@Param("listpunto") List<Integer> listpunto, @Param("status") List<Integer> status);
    
    @Query(value=" SELECT count(distinct(date(fecha_rec_paq))) as total FROM tb_paquetes WHERE id_puntoe IN (:listpunto) AND id_status IN(:status) AND fase IN(0,2) ", nativeQuery = true)
    Integer getCountPaquetesByListPuntoStatus(@Param("listpunto") List<Integer> listpunto, @Param("status") List<Integer> status);    
        
}
