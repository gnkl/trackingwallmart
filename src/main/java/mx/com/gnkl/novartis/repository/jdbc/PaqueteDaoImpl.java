/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.novartis.repository.jdbc;

import java.util.Date;
import java.util.HashMap;
import mx.com.gnkl.novartis.repository.PaqueteDao;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.sql.DataSource;
import mx.com.gnkl.novartis.model.TbPaquetes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jmejia
 */
@Repository
public class PaqueteDaoImpl implements PaqueteDao{

    
    private JdbcTemplate jdbcTemplate;
    
    //@PersistenceContext(unitName = "novartisPU")
    @PersistenceContext
    private EntityManager em;    
    
    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }
    
    /**
     * @return the jdbcTemplate
     */
    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    /**
     * @param jdbcTemplate the jdbcTemplate to set
     */
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }


    @Override
    public List<Map<String, Object>> getPaqueteInfoBystatus(Integer idenvio) {
        String query = "select stat.des_status, count(paq.id_status) total from tb_paquetes paq, tb_status stat where paq.id_status=stat.id_status and id_envio=? group by paq.id_status";
        return this.jdbcTemplate.queryForList(query, new Object[]{idenvio});    
    }
    
    @Override
    public List<TbPaquetes> getCustomQueryData(String query, Integer offset, Integer count) {
        Query querycreated = em.createQuery(query);
        querycreated.setFirstResult(offset); 
        querycreated.setMaxResults(count);        
        List <TbPaquetes> paqueteList = querycreated.getResultList();
        return paqueteList;
    }

    @Override
    public Long getCountCustomQueryData(String query) {
        Query querycreated = em.createQuery(query);
        Long total = (Long) querycreated.getSingleResult();
        return total;
    }

    @Override
    public Long getTotalViajes(int idEmpresa) {
        String query = "SELECT count(distinct(no_embarque)) FROM tb_paquetes where id_empresa=?";
        return this.jdbcTemplate.queryForObject(query, new Object[]{idEmpresa}, Long.class);
    }

    @Override
    public List<Map<String, Object>> getTotalViajeByStatus(int idEmpresa, int tipoEnvio) {
//        String query = " select stat.des_status, count(paq1.no_embarque) total "+
//                " from tb_paquetes paq1, tb_status stat "+
//                " where paq1.id_paq = (select id_paq from tb_paquetes where no_embarque=paq1.no_embarque and id_empresa=?) "+
//                " and paq1.id_empresa=? "+
//                " and stat.id_status=paq1.id_status "+
//                " and stat.id_empresa=? "+
//                " and stat.id_envio=? "+
//                " group by paq1.id_status ";
        
//            String query=" select stat.des_status, count(paq.no_embarque) total "+
//                " from tb_paquetes paq, "+
//                " tb_status stat, "+
//                " (select distinct(no_embarque) no_embarque, id_paq, max(id_status) id_status from tb_paquetes where id_empresa=? group by no_embarque) db1 "+
//                " where paq.id_paq=db1.id_paq and stat.id_status=paq.id_status and stat.id_empresa=? and stat.id_envio=?  "+
//                " group by paq.id_status";
            String query = "select stat.des_status, count(distinct(m.no_embarque)) total, stat.id_status " +
            "from " +
            "( " +
            "	select distinct(no_embarque) no_embarque, max(id_status) id_status from tb_paquetes where id_empresa=? group by no_embarque " +
            ") AS m " +
            "INNER JOIN tb_paquetes AS paq " +
            "    ON paq.no_embarque = m.no_embarque " +
            "   AND paq.id_status=m.id_status " +
            "INNER JOIN tb_status AS stat " +
            "  ON stat.id_status = m.id_status " +
            "  AND stat.id_empresa=? " +
            "  AND stat.id_envio=? " +
            "group by m.id_status";
        return this.jdbcTemplate.queryForList(query, new Object[]{idEmpresa, idEmpresa, tipoEnvio});
    }

    @Override
    public List<Map<String,Object>> getDetalleViajeByStatus(int idEmpresa, int tipoEnvio, int idStatus){
        String query = "select distinct(m.no_embarque) no_embarque, stat.des_status, paq.fecha_rec_paq, paq.fecha_ent_paq " +
                        " from " +
                        " ( " +
                        "	select distinct(no_embarque) no_embarque, max(id_status) id_status from tb_paquetes where id_empresa=? group by no_embarque " +
                        " ) AS m " +
                        " INNER JOIN tb_paquetes AS paq " +
                        "	ON paq.no_embarque = m.no_embarque " +
                        "   AND paq.id_status=m.id_status " +
                        "   AND m.id_status=? " +
                        " INNER JOIN tb_status AS stat " +
                        "  ON stat.id_status = m.id_status " +
                        "  AND stat.id_empresa=? " +
                        "  AND stat.id_envio=?";        
        return this.jdbcTemplate.queryForList(query, new Object[]{idEmpresa, idStatus, idEmpresa, tipoEnvio});
    }
    
    @Override
    public List<Map<String, Object>> getDetalleTotalVajes(int idEmpresa, int offset, int count) {
        String query = " select paq1.no_embarque, pto.nombre_punto, stat.des_status from tb_paquetes paq1,tb_punto_eo pto, tb_status stat"+
            " where paq1.id_paq = (select max(id_paq) from tb_paquetes where no_embarque=paq1.no_embarque and id_empresa=?) and paq1.id_empresa=? and paq1.id_puntoe=pto.id and stat.id_status=paq1.id_status LIMIT ?,?";
        return this.jdbcTemplate.queryForList(query, new Object[]{idEmpresa, idEmpresa, offset, count});
    }
    
    @Override
    public List<Map<String, Object>> getDetalleTotalVajes(int idEmpresa) {
        String query = " select paq1.no_embarque, pto.nombre_punto, stat.des_status from tb_paquetes paq1,tb_punto_eo pto, tb_status stat"+
            " where paq1.id_paq = (select max(id_paq) from tb_paquetes where no_embarque=paq1.no_embarque and id_empresa=?) and paq1.id_empresa=? and paq1.id_puntoe=pto.id and stat.id_status=paq1.id_status";
        return this.jdbcTemplate.queryForList(query, new Object[]{idEmpresa, idEmpresa});
    }    
    
//    @Override
//    public List<Map<String,Object>> getDetalleViajeFaltante(int idEmpresa, int tipoEnvio, int offset, int count){
//        String query = " SELECT no_embarque, count(no_embarque) total, stat.des_status "+
//                " FROM tb_paquetes paq, tb_status stat  "+
//                " WHERE stat.id_envio=? AND stat.id_status=paq.id_status AND paq.id_empresa=? GROUP BY no_embarque HAVING count(no_embarque)=1 LIMIT ?,?";
//        return this.jdbcTemplate.queryForList(query, new Object[]{tipoEnvio, idEmpresa, offset, count});
//    }
    
    @Override
    public List<Map<String,Object>> getDetalleViajeFaltante(int idEmpresa, int tipoEnvio, int offset, int count){
//        String query = " SELECT no_embarque, count(no_embarque) total, stat.des_status "+
//                " FROM tb_paquetes paq, tb_status stat  "+
//                " WHERE stat.id_envio=? AND stat.id_status=paq.id_status AND paq.id_empresa=? GROUP BY no_embarque HAVING count(no_embarque)=1 LIMIT ?,?";
        String query = " SELECT a.no_embarque, count(a.no_embarque) total, stat.des_status " +
            " FROM tb_paquetes a LEFT JOIN tb_paquetes b ON a.no_embarque = b.no_embarque and b.fase=2,tb_status stat " +
            " WHERE a.id_empresa=? and a.fase=1 AND b.fase is null AND a.id_status=stat.id_status " +
            " GROUP BY a.no_embarque LIMIT ?,?";
        return this.jdbcTemplate.queryForList(query, new Object[]{idEmpresa, offset, count});
    }        
    
    @Override
    public List<Map<String,Object>> getDetalleViajeFaltante(int idEmpresa, int tipoEnvio){
//        String query = " SELECT no_embarque, count(no_embarque) total, stat.des_status "+
//                " FROM tb_paquetes paq, tb_status stat  "+
//                " WHERE stat.id_envio=? AND stat.id_status=paq.id_status AND paq.id_empresa=? GROUP BY no_embarque HAVING count(no_embarque)=1";
        String query = " SELECT a.no_embarque, count(a.no_embarque) total, stat.des_status " +
            " FROM tb_paquetes a LEFT JOIN tb_paquetes b ON a.no_embarque = b.no_embarque and b.fase=2,tb_status stat " +
            " WHERE a.id_empresa=? and a.fase=1 AND b.fase is null AND a.id_status=stat.id_status " +
            " GROUP BY a.no_embarque";
        return this.jdbcTemplate.queryForList(query, new Object[]{idEmpresa});
    }    
 
    @Override
    public Integer getCountViajeFaltante(int idEmpresa, int tipoEnvio){
//        String query = " select count(*) from ( SELECT paq.* "+
//                " FROM tb_paquetes paq, tb_status stat  "+
//                " WHERE stat.id_envio=? AND stat.id_status=paq.id_status AND paq.id_empresa=? GROUP BY no_embarque HAVING count(no_embarque)=1 ) m ";
        String query = " SELECT count(a.no_embarque) " +
            " FROM tb_paquetes a LEFT JOIN tb_paquetes b ON a.no_embarque = b.no_embarque and b.fase=2,tb_status stat " +
            " WHERE a.id_empresa=? and a.fase=1 AND b.fase is null AND a.id_status=stat.id_status";
        return this.jdbcTemplate.queryForObject(query, new Object[]{idEmpresa},Integer.class);
    }
    
    @Override
    public List<Map<String,Object>> getDetalleViajeIncompletos(int idEmpresa, int offset, int count){
//        String query = " select m.no_embarque, stat.des_status from ( "+
//                " SELECT distinct(paq.no_embarque) no_embarque, max(paq.id_status) id_status FROM tb_paquetes paq, tb_status stat WHERE stat.id_status=paq.id_status AND paq.id_empresa=? GROUP BY no_embarque) m,  "+
//                " tb_status stat "+
//                " where m.id_status not in (39,40,43,44) and m.id_status=stat.id_status LIMIT ?,?";
        String query = " select paq.no_embarque, stat.des_status, paq.fase from (  " +
                       " SELECT distinct(paq.no_embarque) no_embarque, max(paq.id_status) id_status FROM tb_paquetes paq, tb_status stat WHERE stat.id_status=paq.id_status AND paq.id_empresa=? GROUP BY no_embarque " +
                       " ) m,  " +
                       " tb_status stat, " +
                       " tb_paquetes paq " +
                       " where m.id_status not in (39,40,43,44) and m.id_status=stat.id_status and paq.no_embarque=m.no_embarque and paq.id_status=m.id_status LIMIT ?,?";
        return this.jdbcTemplate.queryForList(query, new Object[]{idEmpresa, offset, count});
    }    

    @Override
    public List<Map<String,Object>> getDetalleViajeIncompletosAll(int idEmpresa){
//        String query = " select m.no_embarque, stat.des_status from ( "+
//                " SELECT distinct(paq.no_embarque) no_embarque, max(paq.id_status) id_status FROM tb_paquetes paq, tb_status stat WHERE stat.id_status=paq.id_status AND paq.id_empresa=? GROUP BY no_embarque ) m,  "+
//                " tb_status stat "+
//                " where m.id_status not in (39,40,43,44) and m.id_status=stat.id_status";
        String query = " select paq.no_embarque, stat.des_status, paq.fase from (  " +
                       " SELECT distinct(paq.no_embarque) no_embarque, max(paq.id_status) id_status FROM tb_paquetes paq, tb_status stat WHERE stat.id_status=paq.id_status AND paq.id_empresa=? GROUP BY no_embarque " +
                       " ) m,  " +
                       " tb_status stat, " +
                       " tb_paquetes paq " +
                       " where m.id_status not in (39,40,43,44) and m.id_status=stat.id_status and paq.no_embarque=m.no_embarque and paq.id_status=m.id_status";
        return this.jdbcTemplate.queryForList(query, new Object[]{idEmpresa});
    }    
    
    @Override
    public Integer getCountViajeIncompletos(int idEmpresa){
//        String query = " select count(*) from ( "+
//                " SELECT distinct(paq.no_embarque) no_embarque, max(paq.id_status) id_status FROM tb_paquetes paq, tb_status stat WHERE stat.id_status=paq.id_status AND paq.id_empresa=? GROUP BY no_embarque HAVING count(no_embarque)>=1) m  "+
//                " where m.id_status not in (39,40,43,44) ";
        String query = " select count(paq.no_embarque) from ( " +
                " SELECT distinct(paq.no_embarque) no_embarque, max(paq.id_status) id_status FROM tb_paquetes paq, tb_status stat WHERE stat.id_status=paq.id_status AND paq.id_empresa=? GROUP BY no_embarque " +
                " ) m,  " +
                " tb_status stat, " +
                " tb_paquetes paq " +
                " where m.id_status not in (39,40,43,44) and m.id_status=stat.id_status and paq.no_embarque=m.no_embarque and paq.id_status=m.id_status ";
        return this.jdbcTemplate.queryForObject(query, new Object[]{idEmpresa}, Integer.class);
    }    

    @Override
    public void callAllPaquetesByVehiculo() {
        SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate).withProcedureName("getAllPaquetesVehiculo");
        Map<String, Object> inParamMap = new HashMap<String, Object>();
        //inParamMap.put("Array_String", "1,2,3");
        SqlParameterSource in = new MapSqlParameterSource(inParamMap);
        Map<String,Object> result = simpleJdbcCall.execute(in);
    }    

    @Override
    public List<Map<String, Object>> getAllPaquetesByVehiculo(Integer idVehiculo) {
        String query = " select no_embarque, fecha_recepcion, operador, vehiculo, punto_entrega, fecha_entrega, duracion, tiempo_horas, no_tarimas from tb_results where no_embarque IN ( SELECT distinct(paq.no_embarque) no_embarque FROM tb_paquetes paq WHERE paq.id_vehiculo=? ) ";
        return jdbcTemplate.queryForList(query, new Object[]{idVehiculo});
    }    

    @Override
    public List<Map<String, Object>> getAllPaquetesByVehiculo(Integer page, Integer offset) {
        String query = " select * from tb_results LIMIT ?,?";
        return jdbcTemplate.queryForList(query, new Object[]{(page*offset),offset});
    }    

    @Override
    public Integer getCountAllPaquetesByVehiculo() {
        String query = " select count(*) from tb_results";
        return jdbcTemplate.queryForObject(query, Integer.class);
    }

    @Override
    public Integer getCountViajesByPuntoOrigenDestino(Integer origen, Integer destino) {
        String query = " select count(*) from (select date(fecha_rec_paq) from tb_paquetes paq where paq.id_puntoorigen=? and id_puntoe=? group by date(fecha_rec_paq))A";
        return jdbcTemplate.queryForObject(query, new Object[]{origen,destino},Integer.class);
    }

    @Override
    public Integer getCountViajesByPuntoOrigenDestino(Integer origen, Integer destino, Integer status) {
        String query = " select count(*) from (select date(fecha_rec_paq) from tb_paquetes paq where paq.id_puntoorigen=? and id_puntoe=? and id_status=? group by date(fecha_rec_paq))A";
        return jdbcTemplate.queryForObject(query, new Object[]{origen,destino, status},Integer.class);
    }

    @Override
    public Integer getCountViajesByPuntoOrigenDestino(Integer origen, Integer destino, Integer status,Integer fase) {
        String query = " select count(*) from (select date(fecha_rec_paq) from tb_paquetes paq where paq.id_puntoorigen=? and id_puntoe=? and id_status=? and fase=? group by date(fecha_rec_paq))A";
        return jdbcTemplate.queryForObject(query, new Object[]{origen,destino, status, fase},Integer.class);
    }
    
    @Override
    public Integer getTotalByFase(Integer fase, Integer status) {
        String query = " SELECT count(1) total FROM tb_paquetes where fase=? and id_status=? ";
        return jdbcTemplate.queryForObject(query, new Object[]{fase, status},Integer.class);
    }      

    @Override
    public List<Map<String, Object>> getTotalByFase() {
        String query = " SELECT A.fase1 , B.fase2, C.total directo FROM " +
                        "( SELECT COUNT(fecha_recepcion) fase1 FROM ( " +
                        "	SELECT date(fecha_rec_paq) fecha_recepcion FROM tb_paquetes where fase=? AND id_puntoorigen=? and id_puntoe=? AND id_status=? group by date(fecha_rec_paq)) db1 " +
                        ") AS A, " +
                        "(SELECT COUNT(fecha_recepcion) fase2 FROM ( " +
                        "	SELECT date(fecha_rec_paq) fecha_recepcion FROM tb_paquetes where fase=? and id_status=? group by date(fecha_rec_paq)) db2 " +
                        ") AS B, " +
                        "( SELECT COUNT(*) total FROM tb_paquetes where fase=? and id_status=? ) AS C ";
        return jdbcTemplate.queryForList(query, new Object[]{1,3743,3745,37,2,39,0,43});
    }
    
    @Override
    public List<Map<String,Object>> getTotalViajesByCliente(String grupo, String cliente){
        String query=" SELECT count(distinct(date(fecha_ent_paq)))  total, ? cliente " +
                "FROM tb_paquetes paq, tb_punto_eo pto " +
                "WHERE id_puntoe IN ( " +
                "	SELECT id FROM tb_punto_eo where grupo=? AND cliente=? group by cliente " +
                ") AND id_status IN( " +
                "	39,43 " +
                ") AND fase IN(0,2) ";
        return jdbcTemplate.queryForList(query, new Object[]{cliente,grupo,cliente});
    }
    
    @Override
    public List<Map<String,Object>> getFaseNoPaquete(String noPaquete){
        String query=" SELECT paq.no_embarque, max(paq.fase) fase FROM tb_paquetes paq where paq.no_embarque=? ";
        return jdbcTemplate.queryForList(query, new Object[]{noPaquete});
    }    

    @Override
    public List<Map<String,Object>> getNoEmbarqueByDates(String fecha1, String fecha2) {
        String query = " select distinct(p.no_embarque) no_embarque, s.des_status from tb_paquetes p, tb_status s "+
                       " where p.no_embarque IN ( "+
                       " SELECT distinct(paq.no_embarque) as no_embarque FROM tb_paquetes paq " +
                       " where Date(paq.fecha_ent_paq) BETWEEN Date(?) AND Date(?) " +
                       " OR Date(paq.fecha_rec_paq) BETWEEN Date(?) AND Date(?) "+
                       " ) and s.id_status = (select max(id_status) from tb_paquetes where no_embarque=p.no_embarque) ";
        return jdbcTemplate.queryForList(query, new Object[]{fecha1,fecha2,fecha1,fecha2});
    }

    @Override
    public List<Map<String, Object>> getNoEmbarqueByFechaRecepcion(String fecha) {
        String query = " select p.no_embarque, s.des_status from tb_paquetes p, tb_status s "+ 
                       " where p.no_embarque IN (  "+
                       " SELECT distinct(paq.no_embarque) as no_embarque FROM tb_paquetes paq " +
                       " where Date(paq.fecha_rec_paq)=Date(?) "+
                       " ) and s.id_status = (select max(id_status) from tb_paquetes where no_embarque=p.no_embarque) "; 
        return jdbcTemplate.queryForList(query, new Object[]{fecha});
    }
    
    @Override
    public List<Map<String, Object>> getNoEmbarqueByFechaEntrega(String fecha) {
        String query = " select p.no_embarque, s.des_status from tb_paquetes p, tb_status s "+
                       " where p.no_embarque IN (  "+        
                       " SELECT distinct(paq.no_embarque) as no_embarque FROM tb_paquetes paq " +
                       " where Date(paq.fecha_ent_paq)=Date(?) "+
                       " ) and s.id_status = (select max(id_status) from tb_paquetes where no_embarque=p.no_embarque) "; 
        return jdbcTemplate.queryForList(query, new Object[]{fecha});
    }
    
}