/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.novartis.repository.jpa;

import java.util.List;
import mx.com.gnkl.novartis.model.TbStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


/**
 *
 * @author jmejia
 */
public interface StatusRepository extends JpaRepository<TbStatus, Integer>{
    
    @Query(value="SELECT estat FROM TbStatus estat LEFT JOIN estat.idEnvio env  WHERE estat.idStatus=:estatus")
    public TbStatus getTbStatuById(@Param("estatus") Integer status);
    
    @Query(value="SELECT estat FROM TbStatus estat LEFT JOIN estat.idEnvio env  WHERE estat.numStatus=:numstatus AND estat.idEnvio.idEnvio=:idenvio")
    public TbStatus getTbStatusByNumstatusEnvio(@Param("numstatus") Integer numstatus, @Param("idenvio") Integer idenvio);

    @Query(value="SELECT estat FROM TbStatus estat LEFT JOIN estat.idEnvio env  WHERE estat.numStatus=:numstatus AND estat.idEnvio.idEnvio=:idenvio AND estat.idEmpresa=:idempresa")
    public TbStatus getTbStatusByNumstatusEnvio(@Param("numstatus") Integer numstatus, @Param("idenvio") Integer idenvio, @Param("idempresa") Integer idempresa);
    
    @Query(value="SELECT estat FROM TbStatus estat WHERE estat.idEnvio.idEnvio=:idenvio AND estat.desStatus='CANCELADO'")
    public TbStatus getEstatusCancelacion(@Param("idenvio") Integer idenvio );

    @Query(value="SELECT estat FROM TbStatus estat WHERE estat.idEnvio.idEnvio=:idenvio AND estat.desStatus='CANCELADO' AND estat.idEmpresa=:idempresa")
    public TbStatus getEstatusCancelacionByIdEnvioEmpresa(@Param("idenvio") Integer idenvio, @Param("idempresa") Integer idempresa);    
    
    @Query(value="SELECT estat FROM TbStatus estat WHERE estat.idEnvio.idEnvio=:idenvio AND estat.desStatus='CANCELADO' AND estat.idEmpresa=:idempresa")
    public TbStatus getEstatusCancelacion(@Param("idenvio") Integer idenvio,  @Param("idempresa") Integer idempresa);
    
    @Query(value="SELECT estat FROM TbStatus estat WHERE estat.idEnvio.idEnvio=:idenvio AND estat.numStatus=:numestatus")
    public TbStatus getEstatusByNumEstatusEnvio(@Param("idenvio") Integer idenvio , @Param("numestatus") Integer numEstatus);

    @Query(value="SELECT estat FROM TbStatus estat WHERE estat.idEnvio.idEnvio=:idenvio AND estat.numStatus=:numestatus AND estat.idEmpresa=:idempresa")
    public TbStatus getEstatusByNumEstatusEnvio(@Param("idenvio") Integer idenvio , @Param("numestatus") Integer numEstatus, @Param("idempresa") Integer idempresa);

    @Query(value="SELECT estat FROM TbStatus estat WHERE estat.idEnvio.idEnvio=:idenvio AND estat.desStatus=:destatus AND estat.idEmpresa=:idempresa ")
    public TbStatus getEstatusByIdEnvioIdEmpresa(@Param("idenvio") Integer idenvio , @Param("destatus") String destatus, @Param("idempresa") Integer idempresa);

    @Query(value="SELECT estat FROM TbStatus estat WHERE estat.idEnvio.idEnvio=:idenvio AND estat.idEmpresa=:idempresa ")
    public List<TbStatus> getEstatusByIdEnvioIdEmpresa(@Param("idenvio") Integer idenvio , @Param("idempresa") Integer idempresa);
    
}
