/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.novartis.repository.jpa;

import java.util.List;
import mx.com.gnkl.novartis.model.TbRuta;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author jmejia
 */
public interface RutaRepository extends JpaRepository<TbRuta, Integer>{

    @Query("SELECT t FROM TbRuta t WHERE t.nombre LIKE :nombre")
    public List<TbRuta> getRutaByNombre(@Param("nombre") String nombre,  Pageable pageable);
    
    @Query("SELECT t FROM TbRuta t WHERE t.estatus = :estatus")
    public List<TbRuta> getRutaByEstatus(@Param("estatus") Integer estatus,  Pageable pageable);

    @Query("SELECT t FROM TbRuta t ORDER BY nombre DESC")
    public List<TbRuta> getRuta(Pageable pageable);
    
    @Query("SELECT COUNT(t) FROM TbRuta t WHERE t.estatus = :estatus")
    public Long getCountRutaByEstatus(@Param("estatus") Integer estatus);
    
    @Query("SELECT t FROM TbRuta t WHERE t.estatus = :estatus AND t.nombre LIKE :nombre")
    public List<TbRuta> getRutaByEstatusNombre(@Param("estatus") Integer estatus, @Param("nombre") String nombre,  Pageable pageable);

    //@Query("SELECT t FROM TbRuta t WHERE t.id LIKE :id ")
    @Query(value="SELECT * FROM tb_ruta t WHERE t.id LIKE :id ", nativeQuery = true)
    public List<TbRuta> getListRutaById(@Param("id") String id);
    
    @Query(value="SELECT * FROM tb_ruta tbr  WHERE tbr.nombre LIKE :nombre", nativeQuery = true)    
    List<TbRuta> getListByRutaNombre(@Param("nombre") String nombre);
    
    @Query(value="SELECT * FROM tb_ruta tbr  WHERE tbr.nombre LIKE :nombre AND tbr.estatus=:status", nativeQuery = true)    
    List<TbRuta> getListByRutaNombre(@Param("nombre") String nombre, @Param("status") Integer status);
}
