/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.novartis.repository.jdbc;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;
import mx.com.gnkl.novartis.model.TbRuta;
import mx.com.gnkl.novartis.repository.RutaDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jmejia
 */
@Repository
public class RutaDaoImpl implements RutaDao{

    
    private JdbcTemplate jdbcTemplate;
    
    //@PersistenceContext(unitName = "novartisPU")
    @PersistenceContext
    private EntityManager em;    
    
    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }
    
    /**
     * @return the jdbcTemplate
     */
    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    /**
     * @param jdbcTemplate the jdbcTemplate to set
     */
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }


    @Override
    public void persistRuta(TbRuta ruta) {
        try{
            String sql = "INSERT INTO tb_ruta (nombre, estatus,fecha_creacion, fecha_modificacion, id_envio) VALUES (?,?,?,?,?) ON DUPLICATE KEY UPDATE nombre=nombre,fecha_modificacion=fecha_modificacion ";
            jdbcTemplate.update(sql, new Object[] {ruta.getNombre(), ruta.getEstatus(), ruta.getFechaCreacion(), ruta.getFechaModificacion(), ruta.getIdEnvio().getIdEnvio()});
        }catch(Exception ex){
            System.out.println(ex.getLocalizedMessage());
        }
    }
    
    @Override
    public List<TbRuta> searchByParametersInfo(String idRuta, String status, Integer offset, Integer count){
//        String queryFechas = "";
//        String query = "";
//        try{            
//            if(idRuta!=null && !idRuta.isEmpty()){
//                query = "  select b from TbRuta b where b.id=:idRuta ";
//            }else{
//                query = "  select b from TbRuta b where b.id is not null "+queryFechas;
//            }
//            
//            if(status!=null && !status.isEmpty()){
//                query += " AND b.idEnvio.idEnvio=:idEnvio";
//            }
//            
//            Query querycreated = em.createQuery(query);
//            querycreated.setFirstResult(offset); 
//            querycreated.setMaxResults(count);                
//            
//            if((fechaInicial !=null && !fechaInicial.isEmpty()) && (fechaFinal !=null && !fechaFinal.isEmpty())){
//                parsedDate1 = sdf.parse(fechaInicial);
//                parsedDate2 = sdf.parse(fechaFinal);            
//                querycreated.setParameter("fechaInicial", parsedDate1, TemporalType.DATE);
//                querycreated.setParameter("fechaFinal", parsedDate2, TemporalType.DATE);                
//            }
//            
//            if(noTracking!=null && !noTracking.isEmpty()){
//                querycreated.setParameter("noTracking", noTracking);                
//            }else if(idBitacora!=null && !idBitacora.isEmpty()){
//                querycreated.setParameter("idBitacora", Integer.parseInt(idBitacora));
//            }
//            
//            if(idBitacora!=null && !idBitacora.isEmpty() && idEnvio!=null && !idEnvio.isEmpty()){
//                querycreated.setParameter("idEnvio", Integer.parseInt(idEnvio));
//            }
//            
//            return querycreated.getResultList();            
//        }catch(ParseException ps){
//            ps.printStackTrace();
//        }

        return new ArrayList<TbRuta>();
    }    

    @Override
    public List<Map<String, Object>> getTotalTimeByRuta(Integer idRuta) {
        String query = " select tr.id,tr.nombre,tr.estatus,te.des_envio, CONCAT('',A.tiempo_bitacora) tiempo_gral, CONCAT('',B.tiempo_interrupcion) tiempo_interrupcion, CONCAT('',TIMEDIFF(A.tiempo_bitacora, B.tiempo_interrupcion)) tiempo_total FROM "+
                " tb_ruta tr, "+
                " tb_tipoenvio te, "+
                " (SELECT TIMEDIFF(max(fecha_fin), min(fecha)) tiempo_bitacora FROM tb_bitacora where id_ruta=? ) A, "+
                " (SELECT TIMEDIFF(max(fecha_fin), min(fecha_inicio)) tiempo_interrupcion FROM tb_ruta_interrupcion where id_ruta=?) B "+
                " WHERE tr.id=? AND te.id_envio=tr.id_envio ";
        return jdbcTemplate.queryForList(query, new Object[]{idRuta, idRuta, idRuta});
    }

    @Override
    public List<Map<String, Object>> getTotalTimeByRuta(Integer page, Integer offset) {
        String query = " select A.id, A.nombre, A.estatus, A.des_envio, CONCAT('',A.tiempo_gral) tiempo_gral, CONCAT('',A.tiempo_interrupcion) tiempo_interrupcion, CONCAT('',TIME_FORMAT(TIMEDIFF(A.tiempo_gral, IF(A.tiempo_interrupcion is null,0,A.tiempo_interrupcion)),'%H:%i:%s')) tiempo_total from ( "+
                " select tr.id,tr.nombre,tr.estatus,te.des_envio,  "+
                " (SELECT TIMEDIFF(max(fecha_fin), min(fecha)) tiempo_bitacora FROM tb_bitacora where id_ruta=tr.id ) tiempo_gral,  "+
                " (SELECT TIMEDIFF(max(fecha_fin), min(fecha_inicio)) tiempo_interrupcion FROM tb_ruta_interrupcion where id_ruta=tr.id) tiempo_interrupcion "+
                " FROM tb_ruta tr, tb_tipoenvio te "+
                " WHERE tr.estatus=1 AND te.id_envio=tr.id_envio ) AS A ";
        return jdbcTemplate.queryForList(query);
    }    

    @Override
    public List<Map<String, Object>> getListBitacoraToBePause(Integer idRuta, Integer idEnvio) {
    String query = " select * from tb_bitacora where id_ruta=? and id_status in (SELECT id FROM tb_bitacora_status where id_envio=? and num_status IN (1,2,3,4)) and is_final=0 "+
                " union "+
                " select * from tb_bitacora where id_ruta=? and id_status IN (SELECT id FROM tb_bitacora_status where id_envio=? and num_status IN (1,2,3,4,5,6)) and is_final=1 ";
        return jdbcTemplate.queryForList(query, new Object[]{idRuta, idEnvio,idRuta, idEnvio});
    }    
    
}
