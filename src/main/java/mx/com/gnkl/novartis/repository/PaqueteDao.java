/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.novartis.repository;

import java.util.Date;
import java.util.List;
import java.util.Map;
import mx.com.gnkl.novartis.model.TbPaquetes;

/**
 *
 * @author jmejia
 */
public interface PaqueteDao {
    
    public List<Map<String, Object>> getPaqueteInfoBystatus(Integer idenvio);
    
    public List<TbPaquetes> getCustomQueryData(String query, Integer offset, Integer count);
    
    public Long getCountCustomQueryData(String query);
    
    public Long getTotalViajes(int idEmpresa);
    
    public List<Map<String, Object>> getTotalViajeByStatus(int idEmpresa, int tipoEnvio);
    
    public List<Map<String, Object>> getDetalleTotalVajes(int idEmpresa, int offset, int count);
    
    public List<Map<String,Object>> getDetalleViajeByStatus(int idEmpresa, int tipoEnvio, int idStatus);
    
    public List<Map<String,Object>> getDetalleViajeFaltante(int idEmpresa, int tipoEnvio, int offset, int count);
    
    public Integer getCountViajeFaltante(int idEmpresa, int tipoEnvio);
    
    public List<Map<String,Object>> getDetalleViajeIncompletos(int idEmpresa, int offset, int count);
    
    public Integer getCountViajeIncompletos(int idEmpresa);
    
    //public List<Map<String, Object>> getAllPaquetesByVehiculo(Integer[] vehiculos);
    
    public void callAllPaquetesByVehiculo();
    
    public List<Map<String, Object>> getAllPaquetesByVehiculo(Integer idVehiculo);
    
    public List<Map<String, Object>> getAllPaquetesByVehiculo(Integer page, Integer offset);
    
    public Integer getCountAllPaquetesByVehiculo();
    
    public Integer getCountViajesByPuntoOrigenDestino(Integer origen, Integer destino);    
    
    public Integer getCountViajesByPuntoOrigenDestino(Integer origen, Integer destino, Integer estatus);
    
    public Integer getTotalByFase(Integer fase, Integer status);
    
    public List<Map<String, Object>> getTotalByFase();
    
    public List<Map<String,Object>> getTotalViajesByCliente(String grupo, String cliente);
    
    public List<Map<String,Object>> getFaseNoPaquete(String noPaquete);
    
    public List<Map<String,Object>> getNoEmbarqueByDates(String fecha1, String fecha2);
    
    public List<Map<String, Object>> getNoEmbarqueByFechaRecepcion(String fecha);
    
    public List<Map<String, Object>> getNoEmbarqueByFechaEntrega(String fecha);
    
    public List<Map<String,Object>> getDetalleViajeIncompletosAll(int idEmpresa);
    
    public List<Map<String,Object>> getDetalleViajeFaltante(int idEmpresa, int tipoEnvio);
    
    public List<Map<String, Object>> getDetalleTotalVajes(int idEmpresa);
    
    public Integer getCountViajesByPuntoOrigenDestino(Integer origen, Integer destino, Integer status,Integer fase);
    
}
