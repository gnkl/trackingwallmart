/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.novartis.repository.jpa;

import java.util.List;
import mx.com.gnkl.novartis.model.TbBitacoraPaquete;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author jmejia
 */
public interface BitacoraPaqueteRepository extends JpaRepository<TbBitacoraPaquete, Integer> {
    
    @Query(value=" SELECT t FROM TbBitacoraPaquete t WHERE t.idPaquete.idPaq=:idpaq AND t.idBitacora.idStatus.numStatus NOT IN (4,5)")
    List<TbBitacoraPaquete> getAllBitacoraPaqueteByIdPaquete(@Param("idpaq") Integer idPaq);
    
    @Query(value=" SELECT t FROM TbBitacoraPaquete t WHERE t.idPaquete.idPaq=:idpaq AND t.idBitacora.id=:idbitacora")
    List<TbBitacoraPaquete> getAllBitacoraPaqueteByIdPaqueteIdBitacora(@Param("idpaq") Integer idPaq, @Param("idbitacora") Integer idBitacora);

    @Query(value=" SELECT t FROM TbBitacoraPaquete t WHERE t.noTracking=:notracking")
    List<TbBitacoraPaquete> getAllBitacoraPaqueteByNoTracking(@Param("notracking") String noTracking);

    @Query(value=" SELECT t FROM TbBitacoraPaquete t WHERE t.idPaquete.idPaq=:idpaq")
    TbBitacoraPaquete getBitacoraPaqueteByIdPaquete(@Param("idpaq") Integer idPaq);

    @Query(value=" SELECT t FROM TbBitacoraPaquete t WHERE t.idBitacora.id=:idbitacora")
    List<TbBitacoraPaquete> getAllBitacoraPaqueteByIdBitacora(@Param("idbitacora") Integer idBitacora);

//    @Query(value=" DELETE FROM tb_bitacora_paquete WHERE id_bitacora=:idbitacora ", nativeQuery = true)
//    void deleteBitacoraPaqueteByIdBitacora(@Param("idbitacora") Integer idBitacora);    
}
