/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.novartis.repository.jpa;

import mx.com.gnkl.novartis.model.TbArchivos;
import org.springframework.data.jpa.repository.JpaRepository;


/**
 *
 * @author jmejia
 */
public interface ArchivoRepository extends JpaRepository<TbArchivos, Integer>{
    
}
