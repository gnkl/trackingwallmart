/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.novartis.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jmejia
 */
@Entity
@Table(name = "tb_ruta")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbRuta.findAll", query = "SELECT t FROM TbRuta t"),
    @NamedQuery(name = "TbRuta.findById", query = "SELECT t FROM TbRuta t WHERE t.id = :id"),
    @NamedQuery(name = "TbRuta.findByNombre", query = "SELECT t FROM TbRuta t WHERE t.nombre = :nombre"),
    @NamedQuery(name = "TbRuta.findByEstatus", query = "SELECT t FROM TbRuta t WHERE t.estatus = :estatus"),
    @NamedQuery(name = "TbRuta.findByFechaCreacion", query = "SELECT t FROM TbRuta t WHERE t.fechaCreacion = :fechaCreacion")})
public class TbRuta implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @Column(name = "estatus")
    private int estatus;
    @Basic(optional = false)
    @Column(name = "fecha_creacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaCreacion;
    @Basic(optional = false)
    @Column(name = "fecha_modificacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaModificacion;
    @JoinColumn(name = "id_envio", referencedColumnName = "id_envio")
    @ManyToOne(optional = false)
    private TbTipoenvio idEnvio;

    @Basic(optional = false)
    @Column(name = "is_pausa")
    private Integer isPausa;
    
//    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
//    @JoinTable(name = "tb_archivo_ruta", joinColumns = { 
//                    @JoinColumn(name = "id_ruta", nullable = false, updatable = false) }, 
//                    inverseJoinColumns = { @JoinColumn(name = "id_archivo", nullable = false, updatable = false) })   
//    private Set<TbArchivos> archivos;      
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "tb_archivo_ruta", joinColumns = { 
                    @JoinColumn(name = "id_ruta", nullable = false, updatable = false) }, 
                    inverseJoinColumns = { @JoinColumn(name = "id_archivo", 
                                    nullable = false, updatable = false) })   
    private Set<TbArchivos> archivos;     
    
    public TbRuta() {
    }

    public TbRuta(Integer id) {
        this.id = id;
    }

    public TbRuta(Integer id, String nombre, int estatus, Date fechaCreacion) {
        this.id = id;
        this.nombre = nombre;
        this.estatus = estatus;
        this.fechaCreacion = fechaCreacion;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getEstatus() {
        return estatus;
    }

    public void setEstatus(int estatus) {
        this.estatus = estatus;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbRuta)) {
            return false;
        }
        TbRuta other = (TbRuta) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "TbRuta[ id=" + id + " ]";
    }

    /**
     * @return the fechaModificacion
     */
    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    /**
     * @param fechaModificacion the fechaModificacion to set
     */
    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    /**
     * @return the idEnvio
     */
    public TbTipoenvio getIdEnvio() {
        return idEnvio;
    }

    /**
     * @param idEnvio the idEnvio to set
     */
    public void setIdEnvio(TbTipoenvio idEnvio) {
        this.idEnvio = idEnvio;
    }

    /**
     * @return the isPausa
     */
    public Integer getIsPausa() {
        return isPausa;
    }

    /**
     * @param isPausa the isPausa to set
     */
    public void setIsPausa(Integer isPausa) {
        this.isPausa = isPausa;
    }

    /**
     * @return the archivos
     */
    public Set<TbArchivos> getArchivos() {
        return archivos;
    }

    /**
     * @param archivos the archivos to set
     */
    public void setArchivos(Set<TbArchivos> archivos) {
        this.archivos = archivos;
    }
    
}
