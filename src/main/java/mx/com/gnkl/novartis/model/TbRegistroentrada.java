/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.novartis.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jmejia
 */
@Entity
@Table(name = "tb_registroentrada")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbRegistroentrada.findAll", query = "SELECT t FROM TbRegistroentrada t"),
    @NamedQuery(name = "TbRegistroentrada.findById", query = "SELECT t FROM TbRegistroentrada t WHERE t.id = :id"),
    @NamedQuery(name = "TbRegistroentrada.findByUsuario", query = "SELECT t FROM TbRegistroentrada t WHERE t.usuario = :usuario"),
    @NamedQuery(name = "TbRegistroentrada.findByFechaHora", query = "SELECT t FROM TbRegistroentrada t WHERE t.fechaHora = :fechaHora")})
public class TbRegistroentrada implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "usuario")
    private String usuario;
    @Basic(optional = false)
    @Column(name = "fecha_hora")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaHora;
    @Basic(optional = false)
    @Column(name = "tipo")
    private String tipo;
    
    public TbRegistroentrada() {
    }

    public TbRegistroentrada(Integer id) {
        this.id = id;
    }

    public TbRegistroentrada(Integer id, String usuario, Date fechaHora) {
        this.id = id;
        this.usuario = usuario;
        this.fechaHora = fechaHora;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public Date getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(Date fechaHora) {
        this.fechaHora = fechaHora;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbRegistroentrada)) {
            return false;
        }
        TbRegistroentrada other = (TbRegistroentrada) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.mycompany.camelcasetest.TbRegistroentrada[ id=" + id + " ]";
    }

    /**
     * @return the tipo
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * @param tipo the tipo to set
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    
}
