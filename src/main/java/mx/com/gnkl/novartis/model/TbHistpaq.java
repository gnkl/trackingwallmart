/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.novartis.model;

import java.io.Serializable;
import java.util.Date;
import java.util.Set;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

/**
 *
 * @author jmejia
 */
@Entity
@Table(name = "tb_histpaq")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbHistpaq.findAll", query = "SELECT t FROM TbHistpaq t"),
    @NamedQuery(name = "TbHistpaq.findByIdHist", query = "SELECT t FROM TbHistpaq t WHERE t.idHist = :idHist"),
    @NamedQuery(name = "TbHistpaq.findByFechaHoraHist", query = "SELECT t FROM TbHistpaq t WHERE t.fechaHoraHist = :fechaHoraHist")})
public class TbHistpaq implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_hist")
    private Integer idHist;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_hora_hist")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaHoraHist;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "obser_hist")
    private String obserHist;
    
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinTable(name = "tb_archivo_hist", joinColumns = { 
                    @JoinColumn(name = "id_historico", nullable = false, updatable = false) }, 
                    inverseJoinColumns = { @JoinColumn(name = "id_archivo", 
                                    nullable = false, updatable = false) })   
    private Set<TbArchivos> archivos;
    
    @JoinColumn(name = "id_usu", referencedColumnName = "id_usu")
    @ManyToOne(optional = true)
    private TbUsuarios idUsu;
    
    @JoinColumn(name = "id_status", referencedColumnName = "id_status")
    @ManyToOne(optional = true)
    @NotFound(action = NotFoundAction.IGNORE)
    private TbStatus idStatus;
    
    @JoinColumn(name = "id_paq", referencedColumnName = "id_paq")
    @ManyToOne(optional = false)
    private TbPaquetes idPaq;

    public TbHistpaq() {
    }

    public TbHistpaq(Integer idHist) {
        this.idHist = idHist;
    }

    public TbHistpaq(Integer idHist, Date fechaHoraHist, String obserHist) {
        this.idHist = idHist;
        this.fechaHoraHist = fechaHoraHist;
        this.obserHist = obserHist;
    }

    public Integer getIdHist() {
        return idHist;
    }

    public void setIdHist(Integer idHist) {
        this.idHist = idHist;
    }

    public Date getFechaHoraHist() {
        return fechaHoraHist;
    }

    public void setFechaHoraHist(Date fechaHoraHist) {
        this.fechaHoraHist = fechaHoraHist;
    }

    public String getObserHist() {
        return obserHist;
    }

    public void setObserHist(String obserHist) {
        this.obserHist = obserHist;
    }

    public TbUsuarios getIdUsu() {
        return idUsu;
    }

    public void setIdUsu(TbUsuarios idUsu) {
        this.idUsu = idUsu;
    }

    public TbStatus getIdStatus() {
        return idStatus;
    }

    public void setIdStatus(TbStatus idStatus) {
        this.idStatus = idStatus;
    }

    public TbPaquetes getIdPaq() {
        return idPaq;
    }

    public void setIdPaq(TbPaquetes idPaq) {
        this.idPaq = idPaq;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idHist != null ? idHist.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbHistpaq)) {
            return false;
        }
        TbHistpaq other = (TbHistpaq) object;
        if ((this.idHist == null && other.idHist != null) || (this.idHist != null && !this.idHist.equals(other.idHist))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.com.gnkl.novartis.model.TbHistpaq[ idHist=" + idHist + " ]";
    }

    /**
     * @return the archivos
     */
    public Set<TbArchivos> getArchivos() {
        return archivos;
    }

    /**
     * @param archivos the archivos to set
     */
    public void setArchivos(Set<TbArchivos> archivos) {
        this.archivos = archivos;
    }
    
}
