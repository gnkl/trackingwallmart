/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.novartis.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jmejia
 */
@Entity
@Table(name = "tb_bitacora_status")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbBitacoraStatus.findAll", query = "SELECT t FROM TbBitacoraStatus t"),
    @NamedQuery(name = "TbBitacoraStatus.findById", query = "SELECT t FROM TbBitacoraStatus t WHERE t.id = :id"),
    @NamedQuery(name = "TbBitacoraStatus.findByNumStatus", query = "SELECT t FROM TbBitacoraStatus t WHERE t.numStatus = :numStatus"),
    @NamedQuery(name = "TbBitacoraStatus.findByDesStatus", query = "SELECT t FROM TbBitacoraStatus t WHERE t.desStatus = :desStatus"),
    @NamedQuery(name = "TbBitacoraStatus.findByIdEmpresa", query = "SELECT t FROM TbBitacoraStatus t WHERE t.idEmpresa = :idEmpresa"),
    @NamedQuery(name = "TbBitacoraStatus.findByComentario", query = "SELECT t FROM TbBitacoraStatus t WHERE t.comentario = :comentario")})
public class TbBitacoraStatus implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    
    @Basic(optional = false)
    @Column(name = "num_status")
    private int numStatus;
    
    @Basic(optional = false)
    @Column(name = "des_status")
    private String desStatus;
    
    @Basic(optional = false)
    @Column(name = "id_empresa")
    private int idEmpresa;
    
    @Column(name = "comentario")
    private String comentario;
    
    @JoinColumn(name = "id_envio", referencedColumnName = "id_envio")
    @ManyToOne(optional = false)
    private TbTipoenvio idEnvio;    

    public TbBitacoraStatus() {
    }

    public TbBitacoraStatus(Integer id) {
        this.id = id;
    }

    public TbBitacoraStatus(Integer id, int numStatus, String desStatus, int idEmpresa) {
        this.id = id;
        this.numStatus = numStatus;
        this.desStatus = desStatus;
        this.idEmpresa = idEmpresa;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getNumStatus() {
        return numStatus;
    }

    public void setNumStatus(int numStatus) {
        this.numStatus = numStatus;
    }

    public String getDesStatus() {
        return desStatus;
    }

    public void setDesStatus(String desStatus) {
        this.desStatus = desStatus;
    }

    public int getIdEmpresa() {
        return idEmpresa;
    }

    public void setIdEmpresa(int idEmpresa) {
        this.idEmpresa = idEmpresa;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbBitacoraStatus)) {
            return false;
        }
        TbBitacoraStatus other = (TbBitacoraStatus) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.saa.lerma.mavenproject2.TbBitacoraStatus[ id=" + id + " ]";
    }

    /**
     * @return the idEnvio
     */
    public TbTipoenvio getIdEnvio() {
        return idEnvio;
    }

    /**
     * @param idEnvio the idEnvio to set
     */
    public void setIdEnvio(TbTipoenvio idEnvio) {
        this.idEnvio = idEnvio;
    }
    
}
