/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.novartis.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jmejia
 */
@Entity
@Table(name = "tb_ruta_interrupcion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbRutaInterrupcion.findAll", query = "SELECT t FROM TbRutaInterrupcion t"),
    @NamedQuery(name = "TbRutaInterrupcion.findById", query = "SELECT t FROM TbRutaInterrupcion t WHERE t.id = :id"),
    @NamedQuery(name = "TbRutaInterrupcion.findByFechaInicio", query = "SELECT t FROM TbRutaInterrupcion t WHERE t.fechaInicio = :fechaInicio"),
    @NamedQuery(name = "TbRutaInterrupcion.findByFechaFin", query = "SELECT t FROM TbRutaInterrupcion t WHERE t.fechaFin = :fechaFin")})
public class TbRutaInterrupcion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    
    @Basic(optional = false)
    @Column(name = "fecha_inicio")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInicio;
    
    @Basic(optional = false)
    @Column(name = "fecha_fin")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaFin;
    
    @Basic(optional = false)
    @Column(name = "observaciones")
    private String observaciones;
    
    @JoinColumn(name = "id_ruta", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private TbRuta idRuta;

    @Basic(optional = false)
    @Column(name = "status")    
    private Integer status;

    public TbRutaInterrupcion() {
    }

    public TbRutaInterrupcion(Integer id) {
        this.id = id;
    }

    public TbRutaInterrupcion(Integer id, Date fechaInicio, Date fechaFin, String observaciones) {
        this.id = id;
        this.fechaInicio = fechaInicio;
        this.fechaFin = fechaFin;
        this.observaciones = observaciones;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public TbRuta getIdRuta() {
        return idRuta;
    }

    public void setIdRuta(TbRuta idRuta) {
        this.idRuta = idRuta;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbRutaInterrupcion)) {
            return false;
        }
        TbRutaInterrupcion other = (TbRutaInterrupcion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.saa.lerma.mavenproject2.TbRutaInterrupcion[ id=" + id + " ]";
    }

    /**
     * @return the status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * @param status the status to set
     */
    public void setStatus(Integer status) {
        this.status = status;
    }
    
}
