/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.novartis.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author jmejia
 */
@Entity
@Table(name = "tb_bitacora_paquete")
@XmlRootElement
public class TbBitacoraPaquete implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "no_tracking")
    private String noTracking;
    @JoinColumn(name = "id_paquete", referencedColumnName = "id_paq")
    @ManyToOne(optional = false)
    private TbPaquetes idPaquete;
    @JoinColumn(name = "id_bitacora", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private TbBitacora idBitacora;

    public TbBitacoraPaquete() {
    }

    public TbBitacoraPaquete(Integer id) {
        this.id = id;
    }

    public TbBitacoraPaquete(Integer id, String noTracking) {
        this.id = id;
        this.noTracking = noTracking;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNoTracking() {
        return noTracking;
    }

    public void setNoTracking(String noTracking) {
        this.noTracking = noTracking;
    }

    public TbPaquetes getIdPaquete() {
        return idPaquete;
    }

    public void setIdPaquete(TbPaquetes idPaquete) {
        this.idPaquete = idPaquete;
    }

    public TbBitacora getIdBitacora() {
        return idBitacora;
    }

    public void setIdBitacora(TbBitacora idBitacora) {
        this.idBitacora = idBitacora;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbBitacoraPaquete)) {
            return false;
        }
        TbBitacoraPaquete other = (TbBitacoraPaquete) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.saa.lerma.mavenproject2.TbBitacoraPaquete[ id=" + id + " ]";
    }
    
}
