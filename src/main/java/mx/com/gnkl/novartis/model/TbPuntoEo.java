/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.novartis.model;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.xml.bind.annotation.XmlRootElement;


/**
 *
 * @author jmejia
 */
@Entity
@Table(name = "tb_punto_eo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TbPuntoEo.findAll", query = "SELECT t FROM TbPuntoEo t"),
    @NamedQuery(name = "TbPuntoEo.findById", query = "SELECT t FROM TbPuntoEo t WHERE t.id = :id"),
    @NamedQuery(name = "TbPuntoEo.findByNombrePunto", query = "SELECT t FROM TbPuntoEo t WHERE t.nombrePunto = :nombrePunto"),
    @NamedQuery(name = "TbPuntoEo.findByDireccion", query = "SELECT t FROM TbPuntoEo t WHERE t.direccion = :direccion"),
    @NamedQuery(name = "TbPuntoEo.findByEstatus", query = "SELECT t FROM TbPuntoEo t WHERE t.estatus = :estatus"),
    @NamedQuery(name = "TbPuntoEo.findByGrupo", query = "SELECT t FROM TbPuntoEo t WHERE t.grupo = :grupo"),
    @NamedQuery(name = "TbPuntoEo.findByCliente", query = "SELECT t FROM TbPuntoEo t WHERE t.cliente = :cliente"),
    @NamedQuery(name = "TbPuntoEo.findByEntrega", query = "SELECT t FROM TbPuntoEo t WHERE t.entrega = :entrega"),
    @NamedQuery(name = "TbPuntoEo.findByOrigen", query = "SELECT t FROM TbPuntoEo t WHERE t.origen = :origen")})
public class TbPuntoEo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "nombre_punto")
    private String nombrePunto;
    @Basic(optional = false)
    @Column(name = "direccion")
    private String direccion;
    @Basic(optional = false)
    @Column(name = "estatus")
    private int estatus;
    @Basic(optional = false)
    @Column(name = "grupo")
    private String grupo;
    @Basic(optional = false)
    @Column(name = "cliente")
    private String cliente;
    @Basic(optional = false)
    @Column(name = "entrega")
    private int entrega;
    @Basic(optional = false)
    @Column(name = "origen")
    private int origen;
    
    @Basic(optional = false)
    @Column(name = "ciudad")
    private String ciudad;
    @Basic(optional = false)
    @Column(name = "estado")
    private String estado;
    @Basic()
    @Column(name = "comision")
    private String comision;
    @Basic()
    @Column(name = "telefono")
    private String telefono;
    @Basic()
    @Column(name = "rfc")
    private String rfc;
//    @Transient
    @Basic()
    @Column(name = "cp")
    private String cp;
    
    public TbPuntoEo() {
    }

    public TbPuntoEo(Integer id) {
        this.id = id;
    }

    public TbPuntoEo(Integer id, String nombrePunto, String direccion, int estatus, String grupo, String cliente, int entrega, int origen) {
        this.id = id;
        this.nombrePunto = nombrePunto;
        this.direccion = direccion;
        this.estatus = estatus;
        this.grupo = grupo;
        this.cliente = cliente;
        this.entrega = entrega;
        this.origen = origen;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombrePunto() {
        return nombrePunto;
    }

    public void setNombrePunto(String nombrePunto) {
        this.nombrePunto = nombrePunto;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getEstatus() {
        return estatus;
    }

    public void setEstatus(int estatus) {
        this.estatus = estatus;
    }

    public String getGrupo() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public int getEntrega() {
        return entrega;
    }

    public void setEntrega(int entrega) {
        this.entrega = entrega;
    }

    public int getOrigen() {
        return origen;
    }

    public void setOrigen(int origen) {
        this.origen = origen;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TbPuntoEo)) {
            return false;
        }
        TbPuntoEo other = (TbPuntoEo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "mx.gnkl.bitacora.persist.mavenproject2.TbPuntoEo[ id=" + id + " ]";
    }

    /**
     * @return the ciudad
     */
    public String getCiudad() {
        return ciudad;
    }

    /**
     * @param ciudad the ciudad to set
     */
    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }

    /**
     * @return the estado
     */
    public String getEstado() {
        return estado;
    }

    /**
     * @param estado the estado to set
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }

    /**
     * @return the comision
     */
    public String getComision() {
        return comision;
    }

    /**
     * @param comision the comision to set
     */
    public void setComision(String comision) {
        this.comision = comision;
    }

    /**
     * @return the telefono
     */
    public String getTelefono() {
        return telefono;
    }

    /**
     * @param telefono the telefono to set
     */
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    /**
     * @return the rfc
     */
    public String getRfc() {
        return rfc;
    }

    /**
     * @param rfc the rfc to set
     */
    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    /**
     * @return the cp
     */
    public String getCp() {
        return cp;
    }

    /**
     * @param cp the cp to set
     */
    public void setCp(String cp) {
        this.cp = cp;
    }
    
}
