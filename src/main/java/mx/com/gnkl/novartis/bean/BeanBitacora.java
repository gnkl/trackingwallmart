/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.novartis.bean;

/**
 *
 * @author jmejia
 */
public class BeanBitacora {
    private Integer row;
    private Integer cell;
    private String value;

    public BeanBitacora(Integer row, Integer cell, String value){
        this.row=row;
        this.cell=cell;
        this.value=value;
    }
    /**
     * @return the row
     */
    public Integer getRow() {
        return row;
    }

    /**
     * @param row the row to set
     */
    public void setRow(Integer row) {
        this.row = row;
    }

    /**
     * @return the cell
     */
    public Integer getCell() {
        return cell;
    }

    /**
     * @param cell the cell to set
     */
    public void setCell(Integer cell) {
        this.cell = cell;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }
    
}
