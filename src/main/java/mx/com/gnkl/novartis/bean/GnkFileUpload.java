/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.novartis.bean;

import java.io.Serializable;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author jmejia
 */
public class GnkFileUpload implements Serializable  {
    private static final long serialVersionUID = 5284757625916162700L;
    public GnkFileUpload(){}
    
    private String observacion;
    private MultipartFile[] files;
 
    /**
     * @return the observacion
     */
    public String getObservacion() {
        return observacion;
    }

    /**
     * @param observacion the observacion to set
     */
    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    /**
     * @return the files
     */
    public MultipartFile[] getFiles() {
        return files;
    }

    /**
     * @param files the files to set
     */
    public void setFiles(MultipartFile[] files) {
        this.files = files;
    }
}
