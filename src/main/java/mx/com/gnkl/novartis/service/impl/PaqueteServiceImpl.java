/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.novartis.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import mx.com.gnkl.novartis.bean.ObjectBeanVO;
import mx.com.gnkl.novartis.bean.ObjectListVO;
import mx.com.gnkl.novartis.bean.SecurityUserBean;
import mx.com.gnkl.novartis.model.TbBitacora;
import mx.com.gnkl.novartis.model.TbPaquetes;
import mx.com.gnkl.novartis.model.TbRuta;
import mx.com.gnkl.novartis.model.TbStatus;
import mx.com.gnkl.novartis.model.TbTipoenvio;
import mx.com.gnkl.novartis.model.TbUsuarios;
import mx.com.gnkl.novartis.model.TbVehiculo;
import mx.com.gnkl.novartis.repository.BitacoraDao;
import mx.com.gnkl.novartis.repository.PaqueteDao;
import mx.com.gnkl.novartis.repository.jpa.BitacoraPaqueteRepository;
import mx.com.gnkl.novartis.repository.jpa.EnvioRepository;
import mx.com.gnkl.novartis.repository.jpa.PaqueteRepository;
import mx.com.gnkl.novartis.repository.jpa.PuntoRepository;
import mx.com.gnkl.novartis.repository.jpa.RutaRepository;
import mx.com.gnkl.novartis.repository.jpa.StatusRepository;
import mx.com.gnkl.novartis.repository.jpa.UserRepository;
import mx.com.gnkl.novartis.repository.jpa.VehiculoRepository;
import mx.com.gnkl.novartis.service.BitacoraService;
import mx.com.gnkl.novartis.service.HistoricoService;
import mx.com.gnkl.novartis.service.PaqueteService;
import mx.gnkl.process.file.ProcessExcelFile;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author jmejia
 */
@Service
public class PaqueteServiceImpl implements PaqueteService{
    @Value("${operador.path}")
    private String pathOperador;    
    
    @Autowired
    private PaqueteRepository paqueteRepository;
    
    @Autowired
    private StatusRepository statusRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private EnvioRepository envioRepository;

    @Autowired
    private PaqueteDao paqueteDao;

    @Autowired
    private RutaRepository rutaRepository;

    @Autowired
    private VehiculoRepository vehiculoRepository;
        
    @Autowired
    private HistoricoService historicoService;

    @Autowired
    private BitacoraService bitacoraService;
    
    @Autowired
    private ProcessExcelFile processExcel;
    
    @Autowired
    private PuntoRepository puntoRepository;

//    @Autowired
//    private BitacoraPaqueteRepository bitacoraPaqueteRepository;    
    @Autowired
    private BitacoraDao bitacoraDao;    
    
    @Value("${grid.limit}")
    private Integer limitGrid;    

    @Value("${fase1.origen}")
    private Integer idFase1Origen;    

    @Value("${fase2.origen}")
    private Integer idFase2Origen;    

    @Value("${fase1.destino}")
    private Integer idFase1Destino;
    
    @Override
    public ObjectListVO findAll(int page, int maxResults) {
        PageRequest pageRequest = new PageRequest(page, maxResults);
        List<TbPaquetes> paquetes = (List<TbPaquetes>) paqueteRepository.getAllPaquetes(pageRequest);
        Page<TbPaquetes>  result = new PageImpl(paquetes);
        return new ObjectListVO(result.getTotalPages(), result.getTotalElements(), result.getContent());
    }

    @Override
    @Transactional
    public TbPaquetes save(TbPaquetes paquete) {
        TbStatus status = new TbStatus();
        TbTipoenvio tipoenvio = paquete.getIdStatus().getIdEnvio();
        User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        switch(tipoenvio.getIdEnvio()){
            case 1:
                status = statusRepository.getTbStatusByNumstatusEnvio(1,1, (paquete.getIdEmpresa()!=null?paquete.getIdEmpresa():0));
                break;
            case 2:
                status = statusRepository.getTbStatusByNumstatusEnvio(1,2, (paquete.getIdEmpresa()!=null?paquete.getIdEmpresa():0));
                break;
            case 3://PARA JALOMA VIAJE DIRECTO
                status = statusRepository.getTbStatusByNumstatusEnvio(1,3, (paquete.getIdEmpresa()!=null?paquete.getIdEmpresa():0));
                break;                
        }        
        TbUsuarios idUsuarios = userRepository.findDataByUsuario(user.getUsername());
        
        
//	Calendar calendarCurrent = Calendar.getInstance();
//        calendarCurrent.setTime(new Date());

//	Calendar calendarRecep = Calendar.getInstance();
//        calendarRecep.setTime(paquete.getFechaRecPaq());
//        calendarRecep.set(Calendar.HOUR, calendarCurrent.get(Calendar.HOUR));
//        calendarRecep.set(Calendar.MINUTE, calendarCurrent.get(Calendar.MINUTE));
//        calendarRecep.set(Calendar.SECOND, calendarCurrent.get(Calendar.SECOND));
        
//	Calendar calendarEnt = Calendar.getInstance();
//        calendarEnt.setTime(paquete.getFechaEntPaq());
//        calendarEnt.set(Calendar.HOUR, calendarCurrent.get(Calendar.HOUR));
//        calendarEnt.set(Calendar.MINUTE, calendarCurrent.get(Calendar.MINUTE));
//        calendarEnt.set(Calendar.SECOND, calendarCurrent.get(Calendar.SECOND));
        
                
        paquete.setIdStatus(status);
        paquete.setIdUsu(idUsuarios);
        paquete.setFecCargaPaq(new Date());
//        paquete.setFechaRecPaq(calendarRecep.getTime());
//        paquete.setFechaEntPaq(calendarEnt.getTime());
        //System.out.println(paquete);
        TbPaquetes response = paqueteRepository.save(paquete);
        if(response.getIdRuta()!=null){
            TbRuta rutaAct = response.getIdRuta();
            rutaAct.setEstatus(2);
            rutaRepository.save(rutaAct);            
        }
        
//        TbPaqueteVehiculo paqVehi = new TbPaqueteVehiculo();
//        paqVehi.setIdPaquete(paquete);
//        paqVehi.setIdVehiculo(paquete.getVehiculo());
//        paqVehi.setTarimas(0);
//        paqueteVehiculoRepository.save(paqVehi);
        
        return response;
    }

    @Override
    public TbPaquetes update(TbPaquetes paquete) {
        TbStatus status = new TbStatus();
        TbTipoenvio tipoenvio = paquete.getIdStatus().getIdEnvio();
        TbPaquetes paqueteFind = paqueteRepository.findOne(paquete.getIdPaq());
        Integer idStatus=paqueteFind.getIdStatus().getNumStatus();
        switch(tipoenvio.getIdEnvio()){
            case 1:
                status = statusRepository.getTbStatusByNumstatusEnvio(idStatus,1, (paquete.getIdEmpresa()!=null?paquete.getIdEmpresa():0));
                break;
            case 2:
                status = statusRepository.getTbStatusByNumstatusEnvio(idStatus,2, (paquete.getIdEmpresa()!=null?paquete.getIdEmpresa():0));
                break;
            case 3://PARA JALOMA VIAJE DIRECTO
                status = statusRepository.getTbStatusByNumstatusEnvio(idStatus,3, (paquete.getIdEmpresa()!=null?paquete.getIdEmpresa():0));
                break;      
        }
//	Calendar calendarCurrent = Calendar.getInstance();
//        calendarCurrent.setTime(new Date());
        
//	Calendar calendarRecep = Calendar.getInstance();
//        calendarRecep.setTime(paquete.getFechaRecPaq());
//        calendarRecep.set(Calendar.HOUR, calendarCurrent.get(Calendar.HOUR));
//        calendarRecep.set(Calendar.MINUTE, calendarCurrent.get(Calendar.MINUTE));
//        calendarRecep.set(Calendar.SECOND, calendarCurrent.get(Calendar.SECOND));
        
//	Calendar calendarEnt = Calendar.getInstance();
//        calendarEnt.setTime(paquete.getFechaEntPaq());
//        calendarEnt.set(Calendar.HOUR, calendarCurrent.get(Calendar.HOUR));
//        calendarEnt.set(Calendar.MINUTE, calendarCurrent.get(Calendar.MINUTE));
//        calendarEnt.set(Calendar.SECOND, calendarCurrent.get(Calendar.SECOND));
        
        paquete.setIdStatus(status);
        paquete.setFase(paqueteFind.getFase());
//        paquete.setFechaEntPaq(calendarEnt.getTime());
//        paquete.setFechaRecPaq(calendarRecep.getTime());
        return paqueteRepository.save(paquete);
    }
    
    @Secured("ROLE_MASTER")
    @Override    
    public void delete(int paqueteId) {
        //TODO DEBE CANCELARCE EN LUGAR DE ELIMINARSE
//        TbPaquetes paquete = paqueteRepository.findOne(paqueteId);
//        TbStatus status = paquete.getIdStatus();
//        TbTipoenvio tipoenvio = status.getIdEnvio();
//        Integer idEmpresa = paquete.getIdEmpresa();
//        TbStatus canstat = statusRepository.getEstatusCancelacionByIdEnvioEmpresa(tipoenvio.getIdEnvio(),(idEmpresa!=null?idEmpresa:0));
//        paquete.setIdStatus(canstat);
//        paquete.setNoEmbarque(paquete.getNoEmbarque()+"_CANCELADO");
//        paqueteRepository.save(paquete);
        //paqueteRepository.delete(paqueteId);
        bitacoraDao.deleteBitacoraByIdPaquete(paqueteId);
        TbPaquetes paquete = paqueteRepository.findOne(paqueteId);
//        TbStatus status = paquete.getIdStatus();
//        TbTipoenvio tipoenvio = status.getIdEnvio();
//        Integer idEmpresa = paquete.getIdEmpresa();
//        TbStatus canstat = statusRepository.getEstatusCancelacionByIdEnvioEmpresa(tipoenvio.getIdEnvio(),(idEmpresa!=null?idEmpresa:0));
//        paquete.setIdStatus(canstat);
//        paquete.setNoEmbarque(paquete.getNoEmbarque()+"_CANCELADO");
        paqueteRepository.delete(paquete);
    }

    @Override
    public ObjectListVO findByNameLike(int page, int maxResults, String name) {
        PageRequest pageRequest = new PageRequest(page, maxResults);
        //List<TbPaquetes> paquetes = paqueteRepository.getAllPaquetes(page,maxResults);
        String namelike = name!=null?"%"+name+"%":"";
        List<TbPaquetes> paquetes = (List<TbPaquetes>) paqueteRepository.getAllPaquetesByNoEmbarque(namelike, pageRequest);// getAllPaquetes(pageRequest);
        Page<TbPaquetes>  result = new PageImpl(paquetes);
        //Page<TbPaquetes>  result = paqueteRepository.getAllPaquetes(pageRequest);
        return new ObjectListVO(result.getTotalPages(), result.getTotalElements(), result.getContent());   
    }

    @Override
    public Integer getTotalPaquetes() {
        return (int) paqueteRepository.count();
    }
    
    @Override
    public Integer getTotalPaquetes(Integer idusu) {
        return (Integer) paqueteRepository.getCountAllPaquetesByIdUser(idusu);
    }
    
    private Sort sortByNameASC() {
        return new Sort(Sort.Direction.ASC, "name");
    }    


    @Override
    public ObjectListVO getOperadoresByNameLike(String name) {
        String namelike = "%"+name+"%";
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        List<TbUsuarios> listoperadores = new ArrayList<TbUsuarios>();
        //ObjectListVO paqueteListVO = paqueteService.findByNameLike(page, maxResults, name);
        if(attr.getRequest().isUserInRole("ROLE_ADMINISTRADOR") || attr.getRequest().isUserInRole("ROLE_MASTER")){
            listoperadores = userRepository.getAllOperadoresByEstatus("ROLE_OPERADOR","A",namelike);
        }else{
            SecurityUserBean user = (SecurityUserBean)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            //listoperadores = userRepository.getAllOperadoresByEstatus("ROLE_OPERADOR","A",namelike, user.getIdUsu());
            listoperadores = userRepository.getAllOperadoresByEstatus("ROLE_OPERADOR","A",namelike);
        }        
        Page<TbUsuarios> result = new PageImpl(listoperadores);
        return new ObjectListVO(result.getTotalPages(), result.getTotalElements(), result.getContent());   
    }

    @Override
    public ObjectListVO getAllOperadores() {
        List<TbUsuarios> listoperadores = new ArrayList<TbUsuarios>();
        listoperadores = userRepository.getAllOperadoresByEstatus("ROLE_OPERADOR","A");
        Page<TbUsuarios> result = new PageImpl(listoperadores);
        return new ObjectListVO(result.getTotalPages(), result.getTotalElements(), result.getContent());   
    }
    
    @Override
    public ObjectListVO getAllEnvio() {
         List<TbTipoenvio> listenvio = envioRepository.findAll();
         Page<TbTipoenvio> result = new PageImpl(listenvio);
         return new ObjectListVO(result.getTotalPages(), result.getTotalElements(), result.getContent());        
    }

    @Override
    public ObjectBeanVO getPaqueteByNoEmbarque(String noEmbarque) {
       TbPaquetes paquete = paqueteRepository.getPaqueteByNoEmbarque(noEmbarque);
       return new ObjectBeanVO(1, 1, paquete);          
    }

    @Override
    public TbPaquetes updateEstatus(Integer idPaq) {
        TbStatus currentEstatus = new TbStatus();
        TbPaquetes paquete = paqueteRepository.getPaqueteByIdPaq(idPaq);
        currentEstatus = paquete.getIdStatus();
        
        TbTipoenvio tipoenvio = paquete.getIdStatus().getIdEnvio();
        Integer idEmpresa = paquete.getIdEmpresa();
        TbStatus statusCancel = statusRepository.getEstatusCancelacion(tipoenvio.getIdEnvio(), (idEmpresa!=null?idEmpresa:0));
        
        Integer numcancel = statusCancel.getNumStatus();
        Integer statusNew = currentEstatus.getNumStatus();
        statusNew=statusNew+1;
        if(statusNew<numcancel){
            TbStatus statusNewObj = statusRepository.getEstatusByNumEstatusEnvio(tipoenvio.getIdEnvio(), statusNew, (idEmpresa!=null?idEmpresa:0));
            paquete.setIdStatus(statusNewObj);            
            return paqueteRepository.save(paquete);
        }else{
            return paquete;
        }
    }

    
    @Override
    @Transactional
    public TbPaquetes updateEstatusJaloma(Integer idPaq, String observaciones, MultipartFile file, Integer kilometraje) {
        //TbPaquetes paquete1 = new TbPaquetes();
        TbPaquetes paquete = paqueteRepository.getPaqueteByIdPaq(idPaq);
        Integer currentIdStatus=paquete.getIdStatus().getNumStatus();

        TbStatus currentEstatus = paquete.getIdStatus();
        TbPaquetes processPaquete = null;
//        TbTipoenvio tipoenvio = paquete.getIdStatus().getIdEnvio();
        
        if(paquete.getFase()!=null){
            TbStatus statusNewObj = getNextStatusJaloma(paquete);
            if(statusNewObj!=null && (statusNewObj.getNumStatus()>currentEstatus.getNumStatus())){
                paquete.setIdStatus(statusNewObj);
                processPaquete = paqueteRepository.save(paquete);                
            }
        }
        
        if(processPaquete!=null && !currentIdStatus.equals(paquete.getIdStatus().getNumStatus())){
            historicoService.saveHistorial(paquete, observaciones, file);
        }        
        
        if(processPaquete!=null){
            //PARA FINALIZAR LA BITACORA JUNTO CON SU PAQUETE
            TbBitacora resultBitacora = finishPaqueteBitacora(processPaquete, kilometraje);
        }        
        return paquete;
    }
    
    @Override
    public byte[] getImageOperador(Integer idoperador)throws IOException {
//        TbOperadores operador = operadorRepository.findOne(idoperador);
//        String pathop = (operador.getFoto()!=null && !operador.getFoto().isEmpty())?operador.getFoto():pathOperador+"default.png";
//
//        Path path = Paths.get(pathop);
//        byte[] data = Files.readAllBytes(path);
//        return data;
        TbUsuarios operador = userRepository.findOne(idoperador);
        String pathop = (operador.getFoto()!=null && !operador.getFoto().isEmpty())?operador.getFoto():pathOperador+"default.png";

        Path path = Paths.get(pathop);
        byte[] data = Files.readAllBytes(path);
        return data;        
    }

    @Override
    public List<Map<String, Object>> getDataByStatus(Integer idenvio) {
        return paqueteDao.getPaqueteInfoBystatus(idenvio);
    }

    @Override
    public TbPaquetes getPaqueteByIdPaq(Integer idpaq) {
        return paqueteRepository.getPaqueteByIdPaq(idpaq);
    }

    @Override
    public ObjectListVO findAll(String username, int page, int maxResults) {
        PageRequest pageRequest = new PageRequest(page, maxResults);
        List<TbPaquetes> paquetes = (List<TbPaquetes>) paqueteRepository.getAllPaquetesByUsername(username, pageRequest);
        Page<TbPaquetes>  result = new PageImpl(paquetes);
        return new ObjectListVO(result.getTotalPages(), result.getTotalElements(), result.getContent());        
    }


    @Override
    public ObjectListVO findAll(int page, int maxResults, Integer idUsu) {
        PageRequest pageRequest = new PageRequest(page, maxResults);
        List<TbPaquetes> paquetes = (List<TbPaquetes>) paqueteRepository.getAllPaquetesByIdUser(idUsu, pageRequest);
        Page<TbPaquetes>  result = new PageImpl(paquetes);
        return new ObjectListVO(result.getTotalPages(), result.getTotalElements(), result.getContent());  
    }

    @Override
    public ObjectBeanVO getPaqueteByNoEmbarque(String noEmbarque, Integer idusu) {
       TbPaquetes paquete = paqueteRepository.getPaqueteByNoEmbarqueIdUser(noEmbarque, idusu);
       return new ObjectBeanVO(1, 1, paquete);           
    }

    @Override
    public ObjectListVO findByNameLike(int page, int maxResults, String name, Integer idusu) {
        String nameLike = name!=null?"%"+name+"%":"";
        PageRequest pageRequest = new PageRequest(page, maxResults);
        List<TbPaquetes> paquetes = (List<TbPaquetes>) paqueteRepository.getAllPaquetesByNoEmbarque(nameLike, idusu, pageRequest);// getAllPaquetes(pageRequest);
        Page<TbPaquetes>  result = new PageImpl(paquetes);
        Integer totaldata = paqueteRepository.getCountPaquetesByNoEmbarque(nameLike, idusu);
        return new ObjectListVO(result.getTotalPages(), totaldata, result.getContent());  
    }


    @Override
    public ObjectListVO findByNameLike(int page, int maxResults, Map<String, Object> data) {
        String querySelect = "SELECT t FROM TbPaquetes t WHERE t.idPaq IS NOT NULL ";
        String queryCount = "SELECT COUNT(t) FROM TbPaquetes t WHERE t.idPaq IS NOT NULL";
        String queryBuilder = "";
        String noEmbarque = (String) data.get("noEmbarque");
        if(noEmbarque!=null && !noEmbarque.isEmpty()){
            queryBuilder +=" AND t.noEmbarque LIKE '%"+data.get("noEmbarque")+"%' ";
        }
        if(data.get("idOperador")!=null){
            queryBuilder +=" AND t.idOperador.idUsu = "+data.get("idOperador");
        }
        if(data.get("idOrigen")!=null){
            queryBuilder +=" AND t.idPuntoorigen.id = "+data.get("idOrigen");
        }
        if(data.get("idEntrega")!=null){
            queryBuilder +=" AND t.idPuntoe.id = "+data.get("idEntrega");
        }
        if(data.get("usuario")!=null){
            queryBuilder +=" AND t.idOperador.idUsu="+data.get("usuario");
        }
        if(data.get("idStatus")!=null){
            if(data.get("idStatus").toString().contains(",")){
                queryBuilder +=" AND t.idStatus.idStatus IN ("+data.get("idStatus")+") ";
            }else{
                queryBuilder +=" AND t.idStatus.idStatus="+data.get("idStatus");
            }            
        }        
        queryBuilder +=" ORDER BY t.fechaRecPaq DESC ";
        List<TbPaquetes> list = paqueteDao.getCustomQueryData(querySelect+queryBuilder,page*maxResults,maxResults);
        Long total = paqueteDao.getCountCustomQueryData(queryCount+queryBuilder);
        return new ObjectListVO(page, total, list);     
    }
    
    @Override
    public ObjectListVO getPaquetesByNoEmbarque(String noEmbarque, Integer page) {
        PageRequest pageRequest = new PageRequest(page, limitGrid);
        System.out.println("noEmbarque: "+noEmbarque);
        String searchFor = "%"+noEmbarque+"%";
        List<TbPaquetes> result = paqueteRepository.getAllPaquetesByNoEmbarque(searchFor, pageRequest);
        Integer totalRecords = result.size();
        return new ObjectListVO(page, totalRecords, result);
    }

    @Override
    public ObjectListVO getPaquetesByNoEmbarque(String noEmbarque, Integer idusu, Integer page) {
        PageRequest pageRequest = new PageRequest(page, limitGrid);
        List<TbPaquetes> result = paqueteRepository.getAllPaquetesByNoEmbarque(noEmbarque,idusu,pageRequest);
        Long totalRecords = paqueteRepository.count();
        return new ObjectListVO(page, totalRecords, result);
    }

    @Override
    @Transactional
    public TbPaquetes updateEstatusJalomaCrossDock(TbPaquetes paquete, Integer currentIdStatus, String observaciones, MultipartFile file, Integer kilometraje) {
        
//        Integer idEmpresa = paquete.getIdEmpresa();
        TbStatus currentEstatus = paquete.getIdStatus();
        TbPaquetes processPaquete = null;
//        TbTipoenvio tipoenvio = paquete.getIdStatus().getIdEnvio();
        
        if(paquete.getFase()!=null){
            TbStatus statusNewObj = getNextStatusJaloma(paquete);
            if(statusNewObj!=null && (statusNewObj.getNumStatus()>currentEstatus.getNumStatus())){
                paquete.setIdStatus(statusNewObj);
                processPaquete = paqueteRepository.save(paquete);                
            }
        }
        
        if(processPaquete!=null && !currentIdStatus.equals(paquete.getIdStatus().getNumStatus())){
            historicoService.saveHistorial(paquete, observaciones, file);
        }
        
        if(processPaquete!=null){
            //PARA FINALIZAR LA BITACORA JUNTO CON SU PAQUETE
            TbBitacora resultBitacora = finishPaqueteBitacora(processPaquete, kilometraje);
        }

        
        return paquete;
    }
    
    public TbBitacora finishPaqueteBitacora(TbPaquetes paquete, Integer kilometraje){
        TbBitacora bitacoraResult = null;
        if(paquete.getFase()!=null){
            switch(paquete.getFase()){
                case 1:
                    if(paquete.getIdStatus().getNumStatus()==3){
                        bitacoraResult = bitacoraService.finishBitacoraByPaquete(paquete, kilometraje);
                    }
                    break;
                case 2:
                    if(paquete.getIdStatus().getNumStatus()==5){
                        bitacoraResult = bitacoraService.finishBitacoraByPaquete(paquete, kilometraje);    
                    }                        
                    break;
                case 0:
                    if(paquete.getIdStatus().getNumStatus()==3){
                        bitacoraResult = bitacoraService.finishBitacoraByPaquete(paquete, kilometraje);    
                    }                        
                    break;                    
            }
        }
        return bitacoraResult;
    }
    
    @Override
    public TbStatus getNextStatusJaloma(TbPaquetes paquete){
        Integer idEmpresa = paquete.getIdEmpresa();
        TbStatus currentEstatus = paquete.getIdStatus();
        TbTipoenvio tipoenvio = paquete.getIdStatus().getIdEnvio();
        TbStatus statusNewObj=null;
        
        if(paquete.getFase()!=null){
            switch(paquete.getFase()){
                
                case 0:
                    //Usar status 2 y 3
                    if(currentEstatus.getNumStatus()==1 || currentEstatus.getNumStatus()==2){
                        Integer status = currentEstatus.getNumStatus()+1;
                        statusNewObj = statusRepository.getEstatusByNumEstatusEnvio(tipoenvio.getIdEnvio(), status, idEmpresa);
                    }else{
                        statusNewObj = statusRepository.getEstatusByNumEstatusEnvio(tipoenvio.getIdEnvio(), currentEstatus.getNumStatus(), idEmpresa);
                    }
                break;                
                case 1:
                    if(currentEstatus.getNumStatus()==1 || currentEstatus.getNumStatus()==2){
                        Integer status = currentEstatus.getNumStatus()+1;
                        statusNewObj = statusRepository.getEstatusByNumEstatusEnvio(tipoenvio.getIdEnvio(), status, idEmpresa);
                    }else{
                        statusNewObj = statusRepository.getEstatusByNumEstatusEnvio(tipoenvio.getIdEnvio(), currentEstatus.getNumStatus(), idEmpresa);
                    }
                break;
                case 2:
                    switch(currentEstatus.getNumStatus()){
                        case 1:
                            statusNewObj = statusRepository.getEstatusByNumEstatusEnvio(tipoenvio.getIdEnvio(), 4, idEmpresa);
                            break;
                        case 4:
                            statusNewObj = statusRepository.getEstatusByNumEstatusEnvio(tipoenvio.getIdEnvio(), currentEstatus.getNumStatus()+1, idEmpresa);
                            break;
                        case 5:
                            statusNewObj = statusRepository.getEstatusByNumEstatusEnvio(tipoenvio.getIdEnvio(), currentEstatus.getNumStatus(), idEmpresa);
                            break;
                    }
                break;
            }            
        }
        return statusNewObj;
    }


    @Override
    public ObjectListVO getPaquetesByNoEmbarqueClienteJaloma(String noEmbarque, Integer page) {
        PageRequest pageRequest = new PageRequest(page, limitGrid);
        List<TbPaquetes> listPaquetes = new ArrayList<TbPaquetes>();
        List<TbPaquetes> result = paqueteRepository.getAllPaquetesByNoEmbarqueStatus(noEmbarque, pageRequest);
        if(result!=null && !result.isEmpty() && result.size()>1 && result.get(0)!=null && result.get(1)!=null){
            listPaquetes.add(getPaqueteClienteJaloma(result.get(0), result.get(1)));
            //Long totalRecords = listPaquetes.size();
            return new ObjectListVO(page, listPaquetes.size(), listPaquetes);
        }else{
            //Long totalRecords = paqueteRepository.count();
            if(result!=null && !result.isEmpty()){
                TbPaquetes paquete = result.get(0);
                if(paquete.getIdStatus().getNumStatus()==1){
                    return new ObjectListVO(page, 0, new ArrayList<TbPaquetes>());
                }else{
                    return new ObjectListVO(page, 1, result);
                }                
            }else{
                return new ObjectListVO(page, 0, new ArrayList<TbPaquetes>());
            }
        }
    }
    
    @Override
    public List<String> getNoTrackingList(String name){
        if(name!=null && !name.isEmpty()){
            name = "%"+name+"%";
            return paqueteRepository.getNoTrackingByName(name);
        }else{
            return new ArrayList<String>();
        }
        
    }
    
    
    public List<TbPaquetes> getAllCrossDockPaquetes(List<String> listNoEmbCrossDock, String noEmbarque, Integer page){
        PageRequest pageReq = new PageRequest(page,limitGrid);
        //List<String> noEmbarqueList = paqueteRepository.getNoEmbarqueCrossDockPaquetes(noEmbarque);
        List<TbPaquetes> listCrossDockPaquetes = new ArrayList<TbPaquetes>();
        for(String item : listNoEmbCrossDock){
            List<TbPaquetes> listPaquetes = paqueteRepository.getAllPaquetesByNoEmbarque(item, pageReq);
            if(listPaquetes!=null && !listPaquetes.isEmpty() && listPaquetes.size()==2){
                TbPaquetes fase1 = listPaquetes.get(0);
                TbPaquetes fase2 = listPaquetes.get(1);
                TbPaquetes resultPaq = getPaqueteClienteJaloma(fase1, fase2);
                if(resultPaq.getIdPaq()!=null){
                    listCrossDockPaquetes.add(resultPaq);
                }
                
            }
        }
        return listCrossDockPaquetes;
    }
    
    @Transactional
    @Override
    public TbPaquetes persistPaqueteWithHistorico(TbPaquetes paquete, Integer idBitacora){
        TbPaquetes  paq1 = null;
        paq1 = save(paquete);
        historicoService.saveHistorial(paq1, "CREACIÓN INICIAL DE TRACKING", null);
        //TODO asociar el paquete con el numero de bitacora
        if(idBitacora!=null && idBitacora>0) {
            bitacoraService.associatePaqueteBitacora(paq1, idBitacora);
        }        
        return paq1;
    }
    
    private TbPaquetes getPaqueteClienteJaloma(TbPaquetes paquete0, TbPaquetes paquete1){
            if(paquete0.getIdStatus().getNumStatus()==1 && paquete1.getIdStatus().getNumStatus()==1){
                return new TbPaquetes();
            }else if(paquete0.getIdStatus().getNumStatus()==1 && paquete1.getIdStatus().getNumStatus()>1){
                TbPaquetes paqueteCliente;
                paqueteCliente=paquete1;
                paqueteCliente.setIdPuntoorigen(paquete0.getIdPuntoorigen());
                paqueteCliente.setFechaRecPaq(paquete0.getFechaRecPaq());
                paqueteCliente.setIdStatus(paquete1.getIdStatus());
                paqueteCliente.setIdOperador(paquete1.getIdOperador());
                return paqueteCliente;
            }else if(paquete0.getIdStatus().getNumStatus()>1 && paquete1.getIdStatus().getNumStatus()==1){
                TbPaquetes paqueteCliente;
                paqueteCliente=paquete1;
                paqueteCliente.setIdPuntoorigen(paquete0.getIdPuntoorigen());
                paqueteCliente.setFechaRecPaq(paquete0.getFechaRecPaq());
                paqueteCliente.setIdStatus(paquete0.getIdStatus());
                paqueteCliente.setIdOperador(paquete0.getIdOperador());
                return paqueteCliente;
            }else{
                TbPaquetes paqueteCliente;
                paqueteCliente=paquete1;
                paqueteCliente.setIdPuntoorigen(paquete0.getIdPuntoorigen());
                paqueteCliente.setFechaRecPaq(paquete0.getFechaRecPaq());
                return paqueteCliente;
            }
    }

    @Override
    public Integer getCountNoTrackingBy(String noTracking) {
        return paqueteRepository.getCountNoTrackingByEmpresaEnvio(2, 1, noTracking);
    }
    
    @Override
    public List<TbVehiculo> getAllVehiculos(){
        return vehiculoRepository.findAll();
    }

    @Override
    public void getAllPaquetesByVehiculos(String fileName) {
        File xx = new File(pathOperador+fileName+".xlsx");
        if (xx.exists()) {
           xx.delete();     
        }        
        
        List<TbVehiculo> listVehiculo = vehiculoRepository.findAll();
        Integer[] vehiculos = new Integer[listVehiculo.size()];
        for(int i=0; i < listVehiculo.size();i++){
            vehiculos[i]=listVehiculo.get(i).getId();
        }
        try{
            //List<Map<String,Object>> result = paqueteDao.getAllPaquetesByVehiculo(vehiculos);
            paqueteDao.callAllPaquetesByVehiculo();
            Map<Integer, String> mapHeader = new HashMap<Integer,String>();
            mapHeader.put(0, "NO EMBARQUE");
            mapHeader.put(1, "FECHA RECEPCION");
            mapHeader.put(2, "OPERADOR");
            //mapHeader.put(3, "EMPRESA");
            mapHeader.put(3, "VEHICULO");
            mapHeader.put(4, "PUNTO ENTREGA");
            mapHeader.put(5, "FECHA ENTREGA");
            mapHeader.put(6, "DURACION");
            mapHeader.put(7, "TIEMPO TRANSCURRIDO");
            mapHeader.put(8, "TARIMAS");
            List<Map<Integer, List<Map<String,Object>>>> listTotal = new ArrayList<Map<Integer, List<Map<String,Object>>>>();
            for(int j=0;j<vehiculos.length;j++){
                List<Map<String,Object>> result = paqueteDao.getAllPaquetesByVehiculo(vehiculos[j]);
                Map<Integer, List<Map<String,Object>>> mapVehiculo = new HashMap<Integer, List<Map<String,Object>>>();
                mapVehiculo.put(j, result);
                listTotal.add(mapVehiculo);
            } 
            processExcel.writeList(pathOperador+fileName+".xlsx", "Vehiculo",listTotal , mapHeader);
        }catch(Exception ex ){
            ex.printStackTrace();
        }    
    }
    
    @Override
    public  ObjectListVO<List>  getAllPaquetesByVehiculos(Integer page) {
        List<Map<String,Object>> result = paqueteDao.getAllPaquetesByVehiculo(page,limitGrid);
        Integer totalRecords = paqueteDao.getCountAllPaquetesByVehiculo();
        return new ObjectListVO(page, totalRecords, result);                        
    }    
    

    @Override
    public byte[] getFileReporteHistorico(String fileName) throws IOException {
            //pathOperador+fileName
            String fileZipName = String.valueOf(fileName + (new Date().getTime())) + ".zip";
            File zipfile = new File(pathOperador+fileZipName);
            // Create a buffer for reading the files
            byte[] buf = new byte[1024];
            // create the ZIP file
            ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipfile));
                Path pathFile = Paths.get(pathOperador+fileName+".xlsx");
                File file = pathFile.toFile();
                FileInputStream in = new FileInputStream(file);
                // add ZIP entry to output stream
                out.putNextEntry(new ZipEntry(file.getName()));
                // transfer bytes from the file to the ZIP file
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
                // complete the entry
                out.closeEntry();
                in.close();
            out.close();
            byte[] data = Files.readAllBytes(zipfile.toPath());
            return data;       
    }
    
    @Override
    public Integer getCountViajesByPuntoOrigenDestino(Integer origen, Integer destino){
        return paqueteDao.getCountViajesByPuntoOrigenDestino(origen, destino);
    }

    @Override
    public Integer getCountViajesByPuntoOrigenDestino(Integer origen, Integer destino, Integer status){
        return paqueteDao.getCountViajesByPuntoOrigenDestino(origen, destino, status);
    }

    @Override
    public Integer getCountViajesByPuntoOrigenDestino(Integer origen, Integer destino, Integer status, Integer fase){
        return paqueteDao.getCountViajesByPuntoOrigenDestino(origen, destino, status, fase);
    }
    
    @Override
    public ObjectListVO getPaquetesByPuntoOrigenDestino(Integer origen, Integer destino, Integer page) {
        PageRequest pageReq = new PageRequest(page,limitGrid);
        List<TbPaquetes> result = paqueteRepository.getAllPaquetesByPuntos(origen, destino, pageReq);
        Integer totalRecords = paqueteDao.getCountViajesByPuntoOrigenDestino(origen, destino);
        return new ObjectListVO(page, totalRecords, result);        
    }

    @Override
    public ObjectListVO getPaquetesByPuntoOrigenDestino(Integer origen, Integer destino, Integer page, Integer fase, Integer status) {
        PageRequest pageReq = new PageRequest(page,limitGrid);
        List<TbPaquetes> result = paqueteRepository.getAllPaquetesByPuntos(origen, destino, fase, status, pageReq);
        Integer totalRecords = paqueteRepository.getCountAllPaquetesByPuntos(origen, destino, fase, status);
        return new ObjectListVO(page, totalRecords, result);        
    }
    
    @Override
    public List<Map<String,Object>> getFasesPaquetes(String noEmbarque) {
        List<Map<String,Object>> result1 = new ArrayList<Map<String,Object>>();
        List<Map<String,Object>> result = paqueteDao.getFaseNoPaquete(noEmbarque);
        for(Map<String,Object> map: result){
            String no_embarque = (String) map.get("no_embarque");
            Integer fase = (Integer) map.get("fase");
            if(fase==null){
                map.put("origen", puntoRepository.findOne(idFase1Origen));
                map.put("destino", puntoRepository.findOne(idFase1Destino));                                        
            }else{
                switch(fase){
                    case 1:
                        map.put("origen", puntoRepository.findOne(idFase2Origen));
                        map.put("destino", null);                        
                        break;
                    case 0:    
                    case 2:    
                    default:
                        map.put("origen", null);
                        map.put("destino", null);                        
                        break;
                }
            }
            result1.add(map);
        }
        return result1;
    }    

    @Override
    public List<Map<String,Object>> getListNoEmbarqueByDate(Date fecha1, Date fecha2) {
        DateFormat format = new SimpleDateFormat("yyyy-mm-dd");
        return paqueteDao.getNoEmbarqueByDates(format.format(fecha1), format.format(fecha2));
    }

    @Override
    public List<Map<String, Object>> getListNoEmbarqueByDate(Date fecha1, Integer tipo) {
        DateFormat format = new SimpleDateFormat("yyyy-mm-dd");
        switch(tipo){
            case 1:
                return paqueteDao.getNoEmbarqueByFechaRecepcion(format.format(fecha1));
            case 2:
                return paqueteDao.getNoEmbarqueByFechaEntrega(format.format(fecha1));
            default:
                return new ArrayList<Map<String, Object>>();
        }
    }
    
    @Override
    public void getPaquetesByPuntoOrigenDestino(Integer origen, Integer destino, String nombre, Integer fase, Integer status) throws Exception {
        //List<TbPaquetes> result = paqueteRepository.getAllPaquetesByPuntos(origen, destino);
        List<TbPaquetes> result = paqueteRepository.getAllPaquetesByFaseStatusDestinos(fase, status, origen, destino);
        List<Map<Integer, Object>> collection = new ArrayList<Map<Integer, Object>>();
        Map<Integer, String> header = new HashMap<Integer, String>();
        header.put(0, "No. Tracking");
        header.put(1, "Fecha Recepción");
        header.put(2, "Fecha Entrega");
        header.put(3, "Punto Origen");
        header.put(4, "Punto Destino");
        header.put(5, "Operador");
        for(TbPaquetes paquete : result){
            Map<Integer, Object> item = new HashMap<Integer, Object>();
            item.put(0, paquete.getNoEmbarque());
            item.put(1, paquete.getFechaRecPaq());
            item.put(2, paquete.getFechaEntPaq());
            item.put(3, paquete.getIdPuntoorigen().getNombrePunto());
            item.put(4, paquete.getIdPuntoe().getNombrePunto());
            item.put(5, paquete.getIdOperador().getNombre()+" "+paquete.getIdOperador().getApellidoPat()+" "+paquete.getIdOperador().getApellidoMat());
            collection.add(item);
        }
        String fileName = pathOperador+nombre;
        processExcel.writeCollectionMap(fileName, nombre, collection, header);
    }  
    
    @Override
    public void getPaquetesByFase(Integer fase, Integer status, String nombre) throws Exception {
        List<TbPaquetes> result = paqueteRepository.getAllPaquetesByFaseStatus(fase, status);
        List<Map<Integer, Object>> collection = new ArrayList<Map<Integer, Object>>();
        Map<Integer, String> header = new HashMap<Integer, String>();
        header.put(0, "No. Tracking");
        header.put(1, "Fecha Recepción");
        header.put(2, "Fecha Entrega");
        header.put(3, "Punto Origen");
        header.put(4, "Punto Destino");
        header.put(5, "Operador");
        for(TbPaquetes paquete : result){
            Map<Integer, Object> item = new HashMap<Integer, Object>();
            item.put(0, paquete.getNoEmbarque());
            item.put(1, paquete.getFechaRecPaq());
            item.put(2, paquete.getFechaEntPaq());
            item.put(3, paquete.getIdPuntoorigen().getNombrePunto());
            item.put(4, paquete.getIdPuntoe().getNombrePunto());
            item.put(5, paquete.getIdOperador().getNombre()+" "+paquete.getIdOperador().getApellidoPat()+" "+paquete.getIdOperador().getApellidoMat());
            collection.add(item);
        }
        String fileName = pathOperador+nombre;
        processExcel.writeCollectionMap(fileName, nombre, collection, header);
    }

    @Override
    public void getPaquetesByFaseDestino(Integer fase, Integer status, Integer origen, Integer destino, String nombre) throws Exception {
        List<TbPaquetes> result = paqueteRepository.getAllPaquetesByFaseStatusDestinos(fase, status, origen, destino);
        List<Map<Integer, Object>> collection = new ArrayList<Map<Integer, Object>>();
        Map<Integer, String> header = new HashMap<Integer, String>();
        header.put(0, "No. Tracking");
        header.put(1, "Fecha Recepción");
        header.put(2, "Fecha Entrega");
        header.put(3, "Punto Origen");
        header.put(4, "Punto Destino");
        header.put(5, "Operador");
        for(TbPaquetes paquete : result){
            Map<Integer, Object> item = new HashMap<Integer, Object>();
            item.put(0, paquete.getNoEmbarque());
            item.put(1, paquete.getFechaRecPaq());
            item.put(2, paquete.getFechaEntPaq());
            item.put(3, paquete.getIdPuntoorigen().getNombrePunto());
            item.put(4, paquete.getIdPuntoe().getNombrePunto());
            item.put(5, paquete.getIdOperador().getNombre()+" "+paquete.getIdOperador().getApellidoPat()+" "+paquete.getIdOperador().getApellidoMat());
            collection.add(item);
        }
        String fileName = pathOperador+nombre;
        processExcel.writeCollectionMap(fileName, nombre, collection, header);
    }    
    
    @Override
    public void getPaquetesByPuntoOrigenDestino(Integer origen, Integer destino, String nombre) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public Integer getFaseByPaquete(String noTracking, Integer tipoEnvio) {
        Integer fase=null;
        List<TbPaquetes> result = paqueteRepository.getPaqueteByNoEmbarqueFase(noTracking);
        if(result.isEmpty()){
            switch(tipoEnvio){
                case 1:
                    fase = 1;
                    break;
                case 3:
                    fase = 0;
                    break;
            }
        }else if(!result.isEmpty()){
            switch(tipoEnvio){
                case 1:
                    fase = 2;
                    break;
                case 3:
                    fase = 0;
                    break;
            }
        }
        return fase;
    }

}
