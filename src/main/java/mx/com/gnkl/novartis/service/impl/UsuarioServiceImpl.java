/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.novartis.service.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.Resource;
import mx.com.gnkl.novartis.bean.BeanUsuario;
import mx.com.gnkl.novartis.bean.ObjectListVO;
import mx.com.gnkl.novartis.model.TbRoles;
import mx.com.gnkl.novartis.model.TbUsuarios;
import mx.com.gnkl.novartis.repository.jpa.RoleRepository;
import mx.com.gnkl.novartis.repository.jpa.UserRepository;
import mx.com.gnkl.novartis.service.UsuarioService;
//import mx.gnkl.process.file.ProcessExcelFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author jmejia
 */
@Service
public class UsuarioServiceImpl implements UsuarioService  {

    @Value("${operador.path}")
    private String pathImages;
    
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository rolRepository;
    
    @Value("${role.master}")
    private String roleMaster;

    @Value("${role.admin}")
    private String roleAdmin;    

    @Value("${role.operador}")
    private String roleOperador;     

    @Value("${role.cliente}")
    private String roleCliente; 
    
    @Value("${punto.path}")
    private String xlsxfilePath;
    
//    @Resource
//    private ProcessExcelFile fileProcesorXlsx;    
        
    @Override
    public ObjectListVO findAll(int page, int maxResults) {
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        PageRequest pageRequest = new PageRequest(page, maxResults);
        long totalUsers = 0;
        Page<TbUsuarios>  result;
        if(attr.getRequest().isUserInRole(roleMaster)){
            totalUsers = userRepository.count();
            result = userRepository.findAll(pageRequest);            
        }else{
            Integer offset = pageRequest.getPageNumber()*pageRequest.getPageSize();
            List<TbUsuarios> list = userRepository.getAllOperadoresNotInRole(roleMaster, offset,pageRequest.getPageSize());
            result = new PageImpl(list);
            totalUsers = userRepository.getCountOperadoresNotInRole(roleMaster);
        }

        return new ObjectListVO(result.getTotalPages(), totalUsers, result.getContent());
    }

    @Override
    public TbUsuarios saveUsuario(TbUsuarios usuario) {
        TbUsuarios usuarioresp = userRepository.save(usuario);
        return usuarioresp;
    }

    @Override
    public TbUsuarios getUsuarioByUsername(String username) {
        return userRepository.findDataByUsuario(username);
    }

    @Override
    public ObjectListVO getAllRoles(String role) {
        List<TbRoles> listRoles = new ArrayList<TbRoles>();
        if(role!=null && !role.isEmpty()){
            listRoles = rolRepository.getAllRolesExcept(role);
        }else{
            listRoles = rolRepository.findAll();
        }
        Page<TbRoles>  result = new PageImpl(listRoles);
        return new ObjectListVO(result.getTotalPages(), result.getTotalElements(), result.getContent());        
    }

    @Override
    public TbUsuarios assignRolesByUser(Integer idUsu, List<TbRoles> roles) {
        TbUsuarios usuario = userRepository.findOne(idUsu);
        Set<TbRoles> set = new HashSet<TbRoles>(roles);
        usuario.setRoles(set);
        return userRepository.save(usuario);
    }

    @Override
    public boolean verifyPassword(Integer idusu, String passwordAnterior) {
        TbUsuarios usuario = userRepository.findOne(idusu);
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String encodedPassword = passwordEncoder.encode(passwordAnterior);
        return usuario.getPass().equals(encodedPassword);
    }

    @Override
    public TbUsuarios saveNewPassword(Integer idusu, String passwordNuevo) {
        TbUsuarios usuario = userRepository.findOne(idusu);
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        String encodedPassword = passwordEncoder.encode(passwordNuevo);
        usuario.setPass(encodedPassword);
        return userRepository.save(usuario);
    }

    @Override
    public TbUsuarios saveUsuarioImagen(Integer idusu, MultipartFile file) throws IOException, NullPointerException{
        TbUsuarios usuario = userRepository.findOne(idusu);
        String path = persistFileToDirectory(file, usuario.getIdUsu());
        usuario.setFoto(path);
        return userRepository.save(usuario);        
    }
    
//    @Override
//    public void processXlsxFile(MultipartFile file) throws IOException, IllegalStateException, Exception{
//        String orgName = file.getOriginalFilename();
//        String filePath = xlsxfilePath + orgName;
//        List<TbUsuarios> listUsuarioGnk = new ArrayList<TbUsuarios>();
//        File dest = new File(filePath);
//
//        file.transferTo(dest);
//        List result = fileProcesorXlsx.getBeanListFromXlsx("mx.com.gnkl.novartis.bean.BeanUsuario", filePath);
//        
//        System.out.println(result.size());
//        System.out.println(result);
//        
//        for(Object item : result){
//            BeanUsuario beanUsuario = (BeanUsuario) item;
//            String username="";
//            if(beanUsuario.getNombre()!=null && beanUsuario.getApellidopaterno()!=null){
//                username = beanUsuario.getNombre()+beanUsuario.getApellidopaterno().substring(0, 2);
//            }
//            TbUsuarios usuario = new TbUsuarios();
//            usuario.setApellidoMat(beanUsuario.getApellidomaterno());
//            usuario.setApellidoPat(beanUsuario.getApellidopaterno());
//            usuario.setEmail(beanUsuario.getCorreo());
//            usuario.setNombre(beanUsuario.getNombre());
//            usuario.setStatus("I");
//            usuario.setTelefono(beanUsuario.getTelefono());
//            usuario.setUsuario(username.toLowerCase());
//            usuario.setPass("T3mporal");
//            listUsuarioGnk.add(usuario);
//        }
//        for(TbUsuarios user : listUsuarioGnk){
//            try{
//                userRepository.save(user);
//            }catch(Exception ex ){
//                System.out.println(ex.getLocalizedMessage());
//            }
//            
//        }
////        List<TbPuntoEo> resultPuntoList = puntoRepository.save(listPuntoGnk);
//        System.out.println(listUsuarioGnk.size());
//
//    }    
    
    private String persistFileToDirectory(MultipartFile file, Integer id) throws IOException{
        InputStream inputStream = null;
        OutputStream outputStream = null;
                
        String fileName = file.getOriginalFilename();
        File newFile = new File(pathImages+ id +"/"+fileName);
        
        File newdirectory = new File(pathImages+ id);

            inputStream = file.getInputStream();
            
            if(!newdirectory.exists()){
                newdirectory.mkdir();
            }
            
            if (!newFile.exists()) {
                newFile.createNewFile();
            }
            outputStream = new FileOutputStream(newFile);
            int read = 0;
            byte[] bytes = new byte[1024];

            while ((read = inputStream.read(bytes)) != -1) {
                outputStream.write(bytes, 0, read);
            }
            return newFile.getPath();
    }    
    
}
