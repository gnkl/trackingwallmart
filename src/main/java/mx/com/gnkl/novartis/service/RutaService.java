/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.novartis.service;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import mx.com.gnkl.novartis.model.TbBitacora;
import mx.com.gnkl.novartis.model.TbRuta;
import mx.com.gnkl.novartis.model.TbRutaInterrupcion;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author jmejia
 */
public interface RutaService {
    
    public TbRuta persistRuta(TbRuta ruta);
    
    public TbRuta ivalidateRuta(Integer idRuta);
    
    public TbRuta getRutaById(Integer id);
    
    public List<TbBitacora> getListBitacoraByRuta(Integer idRuta);
    
    public List<TbRuta> getListRutas(String idRuta);
        
    public void processWallmartRutasFile(MultipartFile file, Integer tipoEnvio )throws Exception;
    
    public List<TbRuta> getRutasListById(String idRuta, Integer status);
    
    public List<TbRuta> getRutasListByName(String idRuta);
    
    public TbRutaInterrupcion setRutaInterrupcion(Integer idRuta, String comentario);
    
    public List<Map<String,Object>> getListTimesRutasByIdRuta(Integer idRuta);
    
    public List<Map<String,Object>> getListTimesRutasByIdRuta(Integer offset, Integer page);
    
    public TbRuta saveRutaImagen(Integer idRuta, MultipartFile file) throws IOException;
    
    //public TbBitacora getActualBitacoraByRuta(Integer idRuta);
    public List<TbBitacora> getActualBitacoraByRuta(Integer idRuta);
    
}
