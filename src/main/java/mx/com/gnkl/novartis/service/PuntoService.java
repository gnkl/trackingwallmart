/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.novartis.service;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import mx.com.gnkl.novartis.bean.ObjectListVO;
import mx.com.gnkl.novartis.model.TbPuntoEo;
//import mx.com.gnkl.novartis.model.TbPuntosentrega;
//import mx.com.gnkl.novartis.model.TbPuntosorigen;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author jmejia
 */
public interface PuntoService {
    
//    public TbPuntosorigen savePuntoOrigen(TbPuntosorigen origen);
//    
//    public TbPuntosentrega savePuntoEntrega(TbPuntosentrega entrega);
    
    public List<TbPuntoEo> getAllPuntoByTipoStatus(Integer entrega,Integer origen, Integer estatus);
    
    public List<TbPuntoEo> getAllPuntoByTipo(Integer entrega, Integer origen);
    
    public TbPuntoEo savePuntoEo(TbPuntoEo punto);
    
    public void persistCsvFilePunto(String pathFile);
    
    public ObjectListVO getPuntoOrigen(Integer page);
    
    public ObjectListVO getPuntoEntrega(Integer page);
    
    public List<TbPuntoEo> getAllPuntoByTipo(Integer entrega, Integer origen, Integer page);
    
    public ObjectListVO getPunto(Integer page);
    
    public ObjectListVO getPuntoOrigen(String nombre);
    
    public ObjectListVO getPuntoEntrega(String nombre);
    
    public ObjectListVO getPuntoOrigen();
    
    public ObjectListVO getPuntoEntrega();
    
    public void processCsvFilePunto(MultipartFile file) throws IOException, IllegalStateException, Exception;
    
    public ObjectListVO findByCustomSearch(int page, int max, Map<String, Object> dataSearch);
    
    public void processXlsxFile(MultipartFile file) throws IOException, IllegalStateException, Exception;
    
    public List<String> getNombrePuntoDireccionList(Integer tipo, String name);
    
    public void processWallmartXlsxFile(MultipartFile file);

}
