/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.novartis.service.impl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import mx.com.gnkl.novartis.bean.BeanPunto;
import mx.com.gnkl.novartis.bean.ObjectListVO;
import mx.com.gnkl.novartis.model.TbPuntoEo;
import mx.com.gnkl.novartis.repository.PuntoDao;
import mx.com.gnkl.novartis.repository.jpa.PuntoRepository;
import mx.com.gnkl.novartis.service.PuntoService;
import mx.gnkl.process.file.ProcessCsvFile;
import mx.gnkl.process.file.ProcessExcelFile;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author jmejia
 */
@Service
public class PuntoServiceImpl implements PuntoService {
    private static final Logger logger = Logger.getLogger(HistoricoServiceImpl.class);

    @Autowired
    private PuntoRepository puntoRepository;

    @Autowired
    private PuntoDao puntoDao;
    
    @Value("${punto.limit}")
    private Integer limitpunto;
    
    @Value("${grid.limit}")
    private Integer limitGrid;

    @Value("${punto.path}")
    private String csvfilePath;
    
    @Resource
    private ProcessCsvFile fileProcesorCsv;
    
    @Resource
    private ProcessExcelFile fileProcesorExcel;    

    @Override
    public List<TbPuntoEo> getAllPuntoByTipoStatus(Integer entrega, Integer origen, Integer estatus) {
        PageRequest page = new PageRequest(0,limitGrid);
        if(entrega!=null && origen!=null && estatus!=null){
            return puntoRepository.getPuntoByTipoEstatusPageable(entrega, origen, estatus, page);
        }else if(entrega!=null && origen!=null && estatus==null){
            return puntoRepository.getPuntoByTipoPageable(entrega, origen, page);
        }else{
            return new ArrayList<TbPuntoEo>();
        }
    }

    @Override
    public List<TbPuntoEo> getAllPuntoByTipo(Integer entrega, Integer origen) {
        PageRequest page = new PageRequest(0,limitpunto);
        Integer estatus = 1;
        if(entrega!=null && origen!=null && estatus!=null){
            return puntoRepository.getPuntoByTipoEstatusPageable(entrega, origen, estatus, page);
        }else if(entrega!=null && origen!=null && estatus==null){
            return puntoRepository.getPuntoByTipoPageable(entrega, origen, page);
        }else{
            return new ArrayList<TbPuntoEo>();
        }
    }

    @Override
    public List<TbPuntoEo> getAllPuntoByTipo(Integer entrega, Integer origen, Integer page) {
        PageRequest pageRequest = new PageRequest(page,limitGrid);
        return puntoRepository.getPuntoByTipoEstatusPageable(entrega, origen, 1, pageRequest);
    }    
    
    @Override
    public ObjectListVO getPuntoOrigen(Integer page) {
        List<TbPuntoEo> listPuntos = getAllPuntoByTipo(0, 1);
        return new ObjectListVO(1, listPuntos.size(), listPuntos);
    }

    @Override
    public ObjectListVO getPuntoEntrega(Integer page) {
        List<TbPuntoEo> result = getAllPuntoByTipo(1, 0);
        return new ObjectListVO(1, result.size(), result);
    }    

    @Override
    public ObjectListVO getPuntoOrigen(String nombre) {
        if(nombre!=null && !nombre.isEmpty()){
            nombre = "%"+nombre+"%";
            List<TbPuntoEo> listPuntos = puntoRepository.getAllPuntoOrigenByNameEstatus(1, nombre);
            return new ObjectListVO(1, listPuntos.size(), listPuntos);
        }else{
            return new ObjectListVO(0, 0, null);
        }
    }

    @Override
    public ObjectListVO getPuntoEntrega(String nombre) {
        if(nombre!=null && !nombre.isEmpty()){
            nombre = "%"+nombre+"%";
            List<TbPuntoEo> result = puntoRepository.getAllPuntoEntregaByNameEstatus(1, nombre);
        return new ObjectListVO(1, result.size(), result);
        } else {
            return new ObjectListVO(0, 0, null);
        }
    }    
    
    
    @Override
    public ObjectListVO getPuntoOrigen() {
        List<TbPuntoEo> listPuntos = puntoRepository.getAllPuntoOrigen();
        return new ObjectListVO(1, listPuntos.size(), listPuntos);
    }
    
    
    @Override
    public ObjectListVO getPuntoEntrega() {
        List<TbPuntoEo> listPuntos = puntoRepository.getAllPuntoEntrega();
        return new ObjectListVO(1, listPuntos.size(), listPuntos);
    }        
    
    @Override
    public ObjectListVO getPunto(Integer page) {
        List<TbPuntoEo> result = puntoRepository.getPuntoByEstatusPageable(1,new PageRequest(page, limitGrid));
        Long total = puntoRepository.getCountPuntoByEstatus(1);
        return new ObjectListVO(1, total, result);
    }
    
    
    @Override
    public TbPuntoEo savePuntoEo(TbPuntoEo punto) {
        return puntoRepository.save(punto);
    }

    @Override
    public void persistCsvFilePunto(String pathFile) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void processCsvFilePunto(MultipartFile file) throws IOException, IllegalStateException, Exception{
        String orgName = file.getOriginalFilename();
        String filePath = csvfilePath + orgName;
        List<TbPuntoEo> listPuntoGnk = new ArrayList<TbPuntoEo>();
        File dest = new File(filePath);

        file.transferTo(dest);
        List result = fileProcesorCsv.getBeanListFromCsv("mx.com.gnkl.novartis.bean.BeanPunto", filePath);
        
        System.out.println(result.size());
        System.out.println(result);
        
        for(Object item : result){
            BeanPunto beanPunto = (BeanPunto) item;
            TbPuntoEo puntoEo = new TbPuntoEo();
            puntoEo.setCliente(beanPunto.getCliente());
            String direccion = beanPunto.getEstado()+" "+beanPunto.getPoblacion()+" "+beanPunto.getCp()+" "+beanPunto.getColonia()+" "+beanPunto.getCalle();
            puntoEo.setDireccion(direccion);
            puntoEo.setEntrega(1);
            puntoEo.setEstatus(1);
            puntoEo.setGrupo(beanPunto.getGrupodecliente());
            String nombre = beanPunto.getDestinatario()+" "+beanPunto.getEstado()+" "+beanPunto.getPoblacion();
            puntoEo.setNombrePunto(nombre);
            puntoEo.setOrigen(1);
            listPuntoGnk.add(puntoEo);
        }
        
        List<TbPuntoEo> resultPuntoList = puntoRepository.save(listPuntoGnk);
        System.out.println(resultPuntoList.size());

    }
    
    
    @Override
    public void processXlsxFile(MultipartFile file){
        try{

            String orgName = file.getOriginalFilename();
            String filePath = csvfilePath + orgName;
            File dest = new File(filePath);

            file.transferTo(dest);
            Map<String, int[]> columnsMap = new HashMap<String, int[]>();
            //columnsMap.put("grupo", new int[]{0});
            columnsMap.put("nocliente", new int[]{1});
            columnsMap.put("nombrepunto", new int[]{2});
            columnsMap.put("direccion", new int[]{3,4,5,6});
            columnsMap.put("ciudad", new int[]{5});
            columnsMap.put("estado", new int[]{6});
            columnsMap.put("comision", new int[]{7});
            columnsMap.put("telefono", new int[]{8});
            columnsMap.put("rfc", new int[]{9});
            List<Map<String, String>> result = fileProcesorExcel.getMapListFromXlsx(filePath, columnsMap);
            List<TbPuntoEo> listPuntos = new ArrayList<TbPuntoEo>();
            if(result!=null && !result.isEmpty()){
                for(Map<String, String> item : result){
                    TbPuntoEo punto = new TbPuntoEo();
                    if(item.get("nocliente")!=null && !item.get("nocliente").isEmpty() && !item.get("ciudad").isEmpty() && !item.get("estado").isEmpty()){
                        punto.setCiudad(item.get("ciudad"));
                        punto.setCliente(item.get("nocliente"));
                        punto.setComision(item.get("comision"));
                        punto.setDireccion(item.get("direccion"));
                        punto.setEntrega(1);
                        punto.setEstado(item.get("estado"));
                        punto.setEstatus(1);
                        punto.setGrupo("JALOMA");
                        punto.setNombrePunto(item.get("nombrepunto"));
                        punto.setOrigen(0);
                        punto.setRfc(item.get("rfc"));
                        punto.setTelefono(item.get("telefono"));
                        
                        listPuntos.add(punto);                    
                    }
                }
            }
            /*///TODO verificar que esta lista sea menor a 200 puntos y persistir cada 200
            try{
                puntoRepository.save(listPuntos);    
            }catch(DataIntegrityViolationException ex){
                logger.error("Error volacion de integridad", ex);
            }*/
            System.out.println("total size: "+listPuntos.size());
            int total = 0;
            for(TbPuntoEo item : listPuntos){
                try{
                    if(item.getCliente()!=null && !item.getCliente().trim().equals("") && !item.getCiudad().isEmpty()){
                        total++;
                        System.out.println("cliente: "+item.getCliente());
                        //puntoRepository.saveAndFlush(item);
                        puntoDao.persistPunto(item);
                    }
                }catch(Exception ex){
                    //logger.error(ex.getLocalizedMessage());
                    System.out.println(ex.getLocalizedMessage());
                }
            }
            System.out.println("total valid: "+total);
                        
            
        }catch(IOException| IllegalStateException ex){
            System.out.println(ex.getLocalizedMessage());
        }
    }           

    @Override
    public void processWallmartXlsxFile(MultipartFile file){
        try{

            String orgName = file.getOriginalFilename();
            String filePath = csvfilePath + orgName;
            File dest = new File(filePath);

            file.transferTo(dest);
            Map<String, int[]> columnsMap = new HashMap<String, int[]>();
            columnsMap.put("store", new int[]{0});
            columnsMap.put("formato", new int[]{1});
            columnsMap.put("unidad", new int[]{2});
            columnsMap.put("poblacion", new int[]{6});
            columnsMap.put("estado", new int[]{7});
            columnsMap.put("cp", new int[]{5});
            columnsMap.put("domicilio", new int[]{8});
            List<Map<String, String>> result = fileProcesorExcel.getMapListFromXlsxAllString(filePath, columnsMap);
            List<TbPuntoEo> listPuntos = new ArrayList<TbPuntoEo>();
            if(result!=null && !result.isEmpty()){
                for(Map<String, String> item : result){
                    TbPuntoEo punto = new TbPuntoEo();
                    if(item.get("store")!=null && !item.get("store").isEmpty() && !item.get("poblacion").isEmpty() && !item.get("poblacion").isEmpty()){
                        punto.setNombrePunto(item.get("store"));
                        punto.setEstatus(1);
                        punto.setGrupo(item.get("formato"));
                        punto.setCliente(item.get("unidad"));
                        punto.setEntrega(1);
                        punto.setOrigen(1);
                        punto.setCiudad(item.get("poblacion"));
                        punto.setEstado(item.get("estado"));
                        punto.setComision("0");
                        punto.setTelefono(item.get("N/A"));
                        punto.setRfc(item.get("N/A"));
                        String cp = item.get("cp")!=null?item.get("cp").trim():"N/A";
                        punto.setCp(cp);                        
                        punto.setDireccion(item.get("domicilio"));
                        listPuntos.add(punto);                    
                    }
                }
            }
            
            System.out.println("total size: "+listPuntos.size());
            int total = 0;
            for(TbPuntoEo item : listPuntos){
                try{
                    if(item.getCliente()!=null && !item.getCliente().trim().equals("") && !item.getCiudad().isEmpty()){
                        total++;
                        puntoDao.persistPunto(item);
                    }
                }catch(Exception ex){
                    //logger.error(ex.getLocalizedMessage());
                    System.out.println(ex.getLocalizedMessage());
                }
            }
            System.out.println("total valid: "+total);
                        
            
        }catch(IOException| IllegalStateException ex){
            System.out.println(ex.getLocalizedMessage());
        }
    }               
    
    @Override
    public ObjectListVO findByCustomSearch(int page, int max, Map<String, Object> dataSearch) {
        String querySelect = "SELECT t FROM TbPuntoEo t WHERE t.nombrePunto IS NOT NULL ";
        String queryCount = "SELECT COUNT(t) FROM TbPuntoEo t WHERE t.nombrePunto IS NOT NULL";
        String queryBuilder = "";
        String noEmbarque = (String) dataSearch.get("nombre");
        if(noEmbarque!=null && !noEmbarque.isEmpty()){
            queryBuilder +=" AND t.nombrePunto LIKE '%"+dataSearch.get("nombre")+"%' ";
        }
        if(dataSearch.get("direccion")!=null){
            queryBuilder +=" AND t.direccion LIKE '%"+dataSearch.get("direccion")+"%' ";
        }
        if(dataSearch.get("estatus")!=null){
            queryBuilder +=" AND t.estatus = "+dataSearch.get("estatus");
        }
        System.out.println(querySelect+queryBuilder);
        System.out.println(queryCount+queryBuilder);
        List<TbPuntoEo> list = puntoDao.getCustomQueryData(querySelect+queryBuilder,page*max,max);
        Long total = puntoDao.getCountCustomQueryData(queryCount+queryBuilder);
        return new ObjectListVO(page, total, list);          
    }

    @Override
    public List<String> getNombrePuntoDireccionList(Integer tipo, String name) {
        List<String> listPuntoEo = new ArrayList<String>();
        switch(tipo){
            case 1://PARA obtener direcciones
                listPuntoEo = puntoRepository.getAllPuntoByDireccionEstatus("%"+name+"%");
            break;
            case 0:// Para obtener nombres
            default:    
                listPuntoEo = puntoRepository.getAllPuntoByNameEstatus("%"+name+"%");
            break;    
        }
        return listPuntoEo;
    }
    
}
