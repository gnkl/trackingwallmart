/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.novartis.service;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import mx.com.gnkl.novartis.bean.ObjectListVO;
import mx.com.gnkl.novartis.model.TbHistpaq;
import mx.com.gnkl.novartis.model.TbPaquetes;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author jmejia
 */
public interface HistoricoService {
    
    public ObjectListVO findAll(int page, int maxResults);
    
    public ObjectListVO findByNameLike(int page, int maxResults, String name);
    
    public ObjectListVO findByNameLike(int page, int maxResults, String name, Integer idusu);
    
    public Integer getTotalHistPaquetes();
    
    //public void saveHistorial(TbPaquetes paquete, String observaciones);

    public TbHistpaq saveHistorial(TbPaquetes paquete, String observaciones, MultipartFile file);
    
    public byte[] getImageFromHistorico(Integer idHist) throws IOException;
    
    public File getImageFileFromHistorico(Integer idHist) throws IOException;
    
    public TbHistpaq saveHistorialImagen(Integer idHist, MultipartFile file)throws IOException;
    
    public void deleteArchivosByIdHist(Integer idHist)throws IOException;
    
    public ObjectListVO findByIdPaq(int page, int maxResults, Integer idPaq);
    
    public ObjectListVO findByIdPaqCliente(int page, int maxResults, Integer idPaq);
    
    public TbHistpaq getTbHistPaqByIdPaqIdStatus(Integer idPaq, Integer idStatus);
}
