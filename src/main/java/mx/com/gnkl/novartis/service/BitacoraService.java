/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.novartis.service;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import mx.com.gnkl.novartis.bean.BeanBitacora;
import mx.com.gnkl.novartis.bean.ObjectListVO;
import mx.com.gnkl.novartis.model.TbBitacora;
import mx.com.gnkl.novartis.model.TbBitacoraHistorico;
//import mx.com.gnkl.novartis.model.TbBitacoraInterrupcion;
import mx.com.gnkl.novartis.model.TbBitacoraPaquete;
import mx.com.gnkl.novartis.model.TbBitacoraStatus;
import mx.com.gnkl.novartis.model.TbPaquetes;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author jmejia
 */
public interface BitacoraService {
    
    public TbBitacora createBitacora(TbBitacora bitacora);
    
    public TbBitacora updateBitacora(TbBitacora bitacora);
    
    public TbBitacora getBitacoraById(Integer idBitacora);
    
    public TbBitacora updateBitacoraStatus(Integer idBitacora, Integer kilometraje, String observaciones);
    
    //public TbBitacoraStatus getNextStatus(Integer status);
    //public TbBitacoraStatus getNextStatus(Integer status, Integer tipoEnvio);
    public TbBitacoraStatus getNextStatus(Integer status, Integer tipoEnvio, Integer isFinal);
    
    public TbBitacora cancelBitacoraStatus(Integer idBitacora);
    
    public List<TbBitacoraHistorico> getListHistoricoBitacoraByIdBitacora(Integer idBitacora);
    
    public List<TbBitacoraHistorico> getListHistoricoBitacoraByIdBitacora(Integer idBitacora, List<Integer> statusList);
    
    public List<Map<String,Object>> getDefaultPuntoEOBitacoras();
    
//    public TbBitacora getBitacoraByDate(Integer idOperador);
    public TbBitacora getBitacoraByDate(Integer idOperador, Integer tipoEnvio);
    
    public TbBitacora finishBitacoraByPaquete(TbPaquetes paquete, Integer kilometraje);
    
    public TbBitacoraPaquete associatePaqueteBitacora(TbPaquetes paquete, Integer idBitacora);
    
    public List<TbBitacora> getListBitacorasById(String idBitacora);
    
    public List<TbBitacora> getListBitacorasByName(String name);
    
    public ObjectListVO searchByParameters(String fechaInicial, String fechaFinal, String idBitacora, String noTracking, String idEnvio, Integer page, Integer maxResults);
    
    public List<BeanBitacora> getListToPutIntoBitacoraExcel(String idBitacora);
    
    public TbBitacoraPaquete getPaqueteBitacora(TbPaquetes paquete);
    
    public TbBitacora saveBitacoraImagen(Integer idBitacora, MultipartFile file) throws IOException;
    
    public TbBitacoraHistorico saveHistoricoImagen(Integer idBitacora, Integer status,MultipartFile file) throws IOException;
    
    public byte[] getImageFromBitacora(Integer idBitacora) throws IOException;
    
    public void deleteArchivosByIdBitacora(Integer idBitacora)throws IOException;
    
    public TbBitacoraHistorico updateHistoricoObservaciones(Integer idHistorico, String Observaciones);
    
    public byte[] getImageFromHistorico(Integer historico) throws IOException;
    
    public void setOthersBitacorasNotFinal(Integer idRuta, Integer idBitacora);
    
    public void setLastBitacoraFinalIfEmptyList(Integer idRuta);
    
    public List<Map<String, Object>> searchBitacoraByParameters(String fechaInicio, String fechaFin, Integer idOperador, Integer idEnvio, Integer page, Integer maxResults);
    
    public Long getCountBitacoraByParameters(String fechaInicio,String fechaFin,Integer idOperador, Integer idEnvio);
    
//    public TbBitacoraInterrupcion addUpdateInterrucionToBitacora(Integer idBitacora);
}
