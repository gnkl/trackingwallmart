/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.novartis.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import mx.com.gnkl.novartis.bean.ObjectListVO;
import mx.com.gnkl.novartis.model.TbArchivos;
import mx.com.gnkl.novartis.model.TbHistpaq;
import mx.com.gnkl.novartis.model.TbPaquetes;
import mx.com.gnkl.novartis.repository.jpa.ArchivoRepository;
import mx.com.gnkl.novartis.repository.jpa.HistoricoRepository;
import mx.com.gnkl.novartis.repository.jpa.PaqueteRepository;
import mx.com.gnkl.novartis.repository.jpa.PuntoRepository;
import mx.com.gnkl.novartis.service.HistoricoService;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.apache.log4j.Logger;

/**
 *
 * @author jmejia
 */
@Service
public class HistoricoServiceImpl implements HistoricoService{
    
    private static final Logger logger = Logger.getLogger(HistoricoServiceImpl.class);

    @Value("${historico.path}")
    private String pathImages;
    
    @Autowired
    private HistoricoRepository historicoRepository;
    
    @Autowired
    private PaqueteRepository paqueteRepository;
    
    @Autowired
    private ArchivoRepository archivoRepository;
        
    @Override
    public ObjectListVO findAll(int page, int maxResults) {
        PageRequest pageRequest = new PageRequest(page, maxResults);
        List<TbHistpaq> paquetes = (List<TbHistpaq>) historicoRepository.getAllPaquetes(pageRequest);
        Page<TbPaquetes>  result = new PageImpl(paquetes);
        return new ObjectListVO(result.getTotalPages(), result.getTotalElements(), result.getContent());
    }

    @Override
    public ObjectListVO findByNameLike(int page, int maxResults, String name) {
        PageRequest pageRequest = new PageRequest(page, maxResults);
        List<TbHistpaq> paquetes = (List<TbHistpaq>) historicoRepository.getAllPaquetesByNoEmbarque(name, pageRequest);
        Page<TbPaquetes>  result = new PageImpl(paquetes);
        Integer totalData = historicoRepository.getCountAllPaquetesByNoEmbarque(name);
        return new ObjectListVO(result.getTotalPages(), totalData, result.getContent());   
    }

    @Override
    public Integer getTotalHistPaquetes() {
        return (int) historicoRepository.count();
    }

    
    @Override //TODO add posibility to add more files 
    public TbHistpaq saveHistorial(TbPaquetes paquete, String observaciones, MultipartFile file) {
        TbHistpaq histPaquete = new TbHistpaq();
        if(file != null){
            try{
                TbArchivos archivo = new TbArchivos();
                Set<TbArchivos> setterList = new HashSet<TbArchivos>();
                archivo.setPathArchivo(persistFileToDirectory(file, paquete.getIdPaq()));
                setterList.add(archivo);
                histPaquete.setArchivos(setterList);                            
            }catch(IOException ex){
                logger.error(ex);
            }
        }
        
        histPaquete.setFechaHoraHist(new Date());
        histPaquete.setIdPaq(paquete);
        histPaquete.setIdStatus(paquete.getIdStatus());
        histPaquete.setIdUsu(paquete.getIdUsu());
        histPaquete.setObserHist(observaciones);
        
        histPaquete = historicoRepository.save(histPaquete);
        
        return histPaquete;
    }    
    
    @Override //TODO add posibility to add more files 
    public TbHistpaq saveHistorialImagen(Integer idHist, MultipartFile file) throws IOException {
        TbHistpaq histPaquete = historicoRepository.findOne(idHist);
        TbArchivos archivo = new TbArchivos();
        archivo.setPathArchivo(persistFileToDirectory(file, histPaquete.getIdPaq().getIdPaq()));
        histPaquete.getArchivos().add(archivo);
        return historicoRepository.save(histPaquete);
    }    
    

    @Override
    public byte[] getImageFromHistorico(Integer idHist) throws IOException {
        TbHistpaq historico = historicoRepository.findOne(idHist);
        if(historico.getArchivos()!=null && !historico.getArchivos().isEmpty()){
            String filename = String.valueOf(idHist + (new Date().getTime())) + ".zip";
            File zipfile = new File(pathImages+filename);
            // Create a buffer for reading the files
            byte[] buf = new byte[1024];
            // create the ZIP file
            ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipfile));
            int i=0;
            Iterator<TbArchivos> archivos = historico.getArchivos().iterator();
            System.out.println(historico.getArchivos().size());
            while (archivos.hasNext()) {
                i++;
                TbArchivos archivo = archivos.next();
                //String filepath = archivo.getPathArchivo();
                Path path = Paths.get(archivo.getPathArchivo());
                File file = path.toFile();
                FileInputStream in = new FileInputStream(file);
                // add ZIP entry to output stream
                out.putNextEntry(new ZipEntry(i+file.getName()));
                // transfer bytes from the file to the ZIP file
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
                // complete the entry
                out.closeEntry();
                in.close();
            }
            out.close();
            byte[] data = Files.readAllBytes(zipfile.toPath());
            return data;       
        }
         return null;
    }    

    @Override
    public File getImageFileFromHistorico(Integer idHist) throws IOException {
        TbHistpaq historico = historicoRepository.findOne(idHist);
        File file=null;
        if(historico.getArchivos()!=null && !historico.getArchivos().isEmpty()){
            Set<TbArchivos> arhivosList = historico.getArchivos();
            while(arhivosList.iterator().hasNext()){
                file = new File(arhivosList.iterator().next().getPathArchivo());
            }
        }
        return file;
//        Path path = Paths.get(historico.getDirArchivo());
//        byte[] data = Files.readAllBytes(path);
//        return data;
    }    

    
    private String persistFileToDirectory(MultipartFile file, Integer id)throws IOException{
        InputStream inputStream = null;
        OutputStream outputStream = null;
                
        String fileName = file.getOriginalFilename();
        File newFile = new File(pathImages+ id +"/"+fileName);
        
        File newdirectory = new File(pathImages+ id);
        inputStream = file.getInputStream();
        if(!newdirectory.exists()){
            newdirectory.mkdir();
        }

        if (!newFile.exists()) {
            newFile.createNewFile();
        }
        outputStream = new FileOutputStream(newFile);
        int read = 0;
        byte[] bytes = new byte[1024];

        while ((read = inputStream.read(bytes)) != -1) {
            outputStream.write(bytes, 0, read);
        }
        return newFile.getPath();
    }

    @Override
    public ObjectListVO findByNameLike(int page, int maxResults, String name, Integer idusu) {
        PageRequest pageRequest = new PageRequest(page, maxResults);
        List<TbHistpaq> paquetes = (List<TbHistpaq>) historicoRepository.getAllPaquetesByNoEmbarqueIdUser(name, idusu, pageRequest);
        Page<TbPaquetes>  result = new PageImpl(paquetes);
        Integer totalData = historicoRepository.getCountAllPaquetesByNoEmbarqueIdUser(name, idusu);
        return new ObjectListVO(result.getTotalPages(), totalData, result.getContent());           
    }

    @Override
    public void deleteArchivosByIdHist(Integer idHist)throws IOException {
        TbHistpaq result = historicoRepository.findOne(idHist);
        List<TbArchivos> listarchivos = new ArrayList<TbArchivos>();
        listarchivos.addAll(result.getArchivos());
        
        result.setArchivos(null);
        historicoRepository.save(result);        
        
        for(TbArchivos archivo: listarchivos){
            archivoRepository.delete(archivo);
        }

        FileUtils.deleteDirectory(new File(pathImages+idHist));
        
    }

    @Override
    public ObjectListVO findByIdPaq(int page, int maxResults, Integer idPaq) {
        PageRequest pageRequest = new PageRequest(page, maxResults);
        List<TbHistpaq> paquetes = (List<TbHistpaq>) historicoRepository.getAllPaquetesByIdPaq(idPaq, pageRequest);
        Page<TbPaquetes>  result = new PageImpl(paquetes);
        Integer totalData = historicoRepository.getCountAllPaquetesByIdPaq(idPaq);
        return new ObjectListVO(result.getTotalPages(), totalData, result.getContent());  
    }

    @Override
    public ObjectListVO findByIdPaqCliente(int page, int maxResults, Integer idPaq) {
        PageRequest pageRequest = new PageRequest(page, maxResults);
        TbPaquetes paq = paqueteRepository.findOne(idPaq);
        List<TbHistpaq> paquetes = (List<TbHistpaq>) historicoRepository.getAllPaquetesByNoEmbarqueStatus(paq.getNoEmbarque(),1);
        Page<TbPaquetes>  result = new PageImpl(paquetes);
        Integer totalData = historicoRepository.getCountAllPaquetesByIdPaq(idPaq);
        return new ObjectListVO(result.getTotalPages(), totalData, result.getContent());  
    }    

    @Override
    public TbHistpaq getTbHistPaqByIdPaqIdStatus(Integer idPaq, Integer idStatus) {
        List<TbHistpaq> result = historicoRepository.getAllPaquetesByIdPaqStatus(idPaq, idStatus);
        if(!result.isEmpty()){
            return result.get(0);
        }else{
            return null;
        }
    }          
}
