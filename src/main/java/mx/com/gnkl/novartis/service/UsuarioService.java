/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.novartis.service;

import java.io.IOException;
import java.util.List;
import mx.com.gnkl.novartis.bean.ObjectListVO;
import mx.com.gnkl.novartis.model.TbRoles;
import mx.com.gnkl.novartis.model.TbUsuarios;
import org.springframework.web.multipart.MultipartFile;
/**
 *
 * @author jmejia
 */
public interface UsuarioService {

public ObjectListVO findAll(int page, int maxResults);

public TbUsuarios saveUsuario(TbUsuarios usuario);

public TbUsuarios getUsuarioByUsername(String username);

public ObjectListVO getAllRoles(String roleName);
    
public TbUsuarios assignRolesByUser(Integer idUsu, List<TbRoles> roles);

public boolean verifyPassword(Integer idusu, String passwordAnterior);

public TbUsuarios saveNewPassword(Integer idusu, String passwordNuevo);

//public TbUsuarios saveUsuarioImagen(Integer idusu, MultipartFile file);
public TbUsuarios saveUsuarioImagen(Integer idusu, MultipartFile file) throws IOException, NullPointerException;

//public void processXlsxFile(MultipartFile file) throws IOException, IllegalStateException, Exception;
}
