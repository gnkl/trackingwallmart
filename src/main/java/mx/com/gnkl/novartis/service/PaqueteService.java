/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.novartis.service;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import mx.com.gnkl.novartis.bean.ObjectBeanVO;
import mx.com.gnkl.novartis.bean.ObjectListVO;
import mx.com.gnkl.novartis.model.TbPaquetes;
import mx.com.gnkl.novartis.model.TbStatus;
import mx.com.gnkl.novartis.model.TbVehiculo;
import org.springframework.web.multipart.MultipartFile;

/**
 *
 * @author jmejia
 */
public interface PaqueteService {
    
    public ObjectListVO findAll(int page, int maxResults);
    
    public ObjectListVO findAll(String username, int page, int maxResults);
    
    public ObjectListVO findAll(int page, int maxResults, Integer idUsu);
    
    public TbPaquetes save(TbPaquetes paquete);
    
    public TbPaquetes update(TbPaquetes paquete);
    
    public TbPaquetes updateEstatus(Integer idPaq);
    
    public void delete(int contactId);
    
    public ObjectListVO findByNameLike(int page, int maxResults, String name);
    
    public ObjectListVO findByNameLike(int page, int maxResults, String name, Integer idusu);    

    public ObjectListVO findByNameLike(int page, int maxResults, Map<String, Object> data);
    
    public Integer getTotalPaquetes();
    
    public Integer getTotalPaquetes(Integer idusu);
    
    public ObjectListVO getOperadoresByNameLike(String name);
    
    public ObjectListVO getAllEnvio();
    
    public ObjectBeanVO getPaqueteByNoEmbarque(String noEmbarque);
    
    public ObjectBeanVO getPaqueteByNoEmbarque(String noEmbarque, Integer idusu);
    
    public byte[] getImageOperador(Integer idoperador)throws IOException;
    
    public List<Map<String, Object>> getDataByStatus(Integer idenvio);
    
    public TbPaquetes getPaqueteByIdPaq(Integer idpaq);
    
    public ObjectListVO getAllOperadores();

    public ObjectListVO getPaquetesByNoEmbarque(String noEmbarque, Integer page);
    
    public ObjectListVO getPaquetesByNoEmbarqueClienteJaloma(String noEmbarque, Integer page);
    
    public ObjectListVO getPaquetesByNoEmbarque(String noEmbarque, Integer idusu, Integer page);
    
    public TbPaquetes persistPaqueteWithHistorico(TbPaquetes paquete, Integer idBitacora);
    
    public TbPaquetes updateEstatusJalomaCrossDock(TbPaquetes paquete, Integer currentIdStatus, String observaciones, MultipartFile file, Integer kilometraje);
    
    public TbPaquetes updateEstatusJaloma(Integer idPaq, String observaciones, MultipartFile file, Integer kilometraje);
    
    public List<String> getNoTrackingList(String name);
    
    public Integer getCountNoTrackingBy(String noTracking);
    
    public List<TbVehiculo> getAllVehiculos();
    
    public void getAllPaquetesByVehiculos(String fileName);
    
    public byte[] getFileReporteHistorico(String fileName) throws IOException;
    
    public  ObjectListVO<List>  getAllPaquetesByVehiculos(Integer page);
    
    public Integer getCountViajesByPuntoOrigenDestino(Integer origen, Integer destino);
    
    public Integer getCountViajesByPuntoOrigenDestino(Integer origen, Integer destino, Integer status);
    
    public ObjectListVO getPaquetesByPuntoOrigenDestino(Integer origen, Integer destino, Integer page);
    
    public List<Map<String,Object>> getFasesPaquetes(String noEmbarque);
    
    public TbStatus getNextStatusJaloma(TbPaquetes paquete);
    
    public List<Map<String,Object>> getListNoEmbarqueByDate(Date fecha1, Date fecha2);
    
    public List<Map<String, Object>> getListNoEmbarqueByDate(Date fecha1, Integer tipo);
    
    public void getPaquetesByPuntoOrigenDestino(Integer origen, Integer destino, String nombre) throws Exception;
    
    public void getPaquetesByFase(Integer fase, Integer status, String nombre) throws Exception;
    
    public Integer getCountViajesByPuntoOrigenDestino(Integer origen, Integer destino, Integer status, Integer fase);
    
    public ObjectListVO getPaquetesByPuntoOrigenDestino(Integer origen, Integer destino, Integer page, Integer fase, Integer status);
    
    public void getPaquetesByPuntoOrigenDestino(Integer origen, Integer destino, String nombre, Integer fase, Integer status) throws Exception;
    
    public void getPaquetesByFaseDestino(Integer fase, Integer status, Integer origen, Integer destino, String nombre) throws Exception;
    
    public Integer getFaseByPaquete(String noTracking, Integer tipoEnvio);
}
