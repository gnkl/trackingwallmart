package mx.com.gnkl.novartis.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import javax.annotation.Resource;
import mx.com.gnkl.novartis.bean.ObjectBeanVO;
import mx.com.gnkl.novartis.bean.ObjectListVO;
import mx.com.gnkl.novartis.bean.SecurityUserBean;
import mx.com.gnkl.novartis.component.ApplicationMailer;
import mx.com.gnkl.novartis.model.TbRoles;
import mx.com.gnkl.novartis.model.TbUsuarios;
import mx.com.gnkl.novartis.repository.jpa.UserRepository;
import mx.com.gnkl.novartis.service.UsuarioService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;


@Controller
@RequestMapping(value = "/protected/usuario")
public class UsuarioController {
    
    private static final Logger logger = Logger.getLogger(UsuarioController.class);

    private static final String DEFAULT_PAGE_DISPLAYED_TO_USER = "0";
    
    @Value("${grid.limit}")
    private int maxResults;
    
    @Autowired
    private UsuarioService usuarioService;
    
    @Autowired
    private UserRepository userRepository;
    
    @Resource
    private ApplicationMailer mailer;       
    
    @Value("${role.admin}")
    private String roleAdmin;
    
    @Value("${role.master}")
    private String roleMaster;    

    @Value("T3mporal.2016")
    private String temppass;
    
    @Value("${gnk.gnklmails}")
    private String gnklMails;    
    
    
    @Value("${gnk.ccomail}")
    private String ccoGnkMail;
    
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView welcome() {
        return new ModelAndView("crudusuarios");
    }
    
    @RequestMapping(value = "/getusuarios/", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> searchusuario(@RequestParam(defaultValue = DEFAULT_PAGE_DISPLAYED_TO_USER) int page,
                                    Locale locale) {
        if(page>0){
            page = page -1;
        }
        ObjectListVO objlist = usuarioService.findAll(page, maxResults);
        return new ResponseEntity<ObjectListVO>(objlist, HttpStatus.OK);
    }
        
    
    @RequestMapping(value = "/createusuario", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> create(@RequestBody TbUsuarios user) {
        TbUsuarios usuario = usuarioService.getUsuarioByUsername(user.getUsuario());
        if(usuario!=null){
            ObjectBeanVO objbean = new ObjectBeanVO();
            objbean.setActionMessage("Usuario existente");
            return new ResponseEntity<ObjectBeanVO>(objbean, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        user.setPass(passwordEncoder.encode(temppass));
        TbUsuarios  paq1 = usuarioService.saveUsuario(user);
        //ObjectListVO objlist = usuarioService.findAll(0, maxResults);
        //String emailUser = paq1.getEmail()!=null?paq1.getEmail():"";
//        if(paq1.getEmail()!=null){
//            StringBuilder sb = new StringBuilder();
//            sb.append("<br><br>El usuario ha sido creado: ").append(paq1.getNombre()).append(" ").append(paq1.getApellidoPat()).append(" ").append(paq1.getApellidoMat()).append(" <br><br>");
//            sb.append("<br><br>Username: ").append(paq1.getUsuario()).append(" <br><br>");
//            sb.append("<br><br>Es necesario crear una contraseña para este usuario* ");            
//            mailer.sendTemplateMail(paq1.getEmail(), "Usuario creado sistema de bitácoras",sb.toString(),gnklMails);
//        }
        ObjectBeanVO objbean = new ObjectBeanVO();
        objbean.setData(paq1);
        return new ResponseEntity<ObjectBeanVO>(objbean, HttpStatus.OK);
    }    
    
    @RequestMapping(value = "/addimage/{idusu}", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> addImageHist(@PathVariable("idusu") int idusu,
                                    @RequestParam("file") MultipartFile file) {
        try{
            TbUsuarios result = usuarioService.saveUsuarioImagen(idusu, file);
            ObjectBeanVO histVO = new ObjectBeanVO();
            histVO.setData(result);
            return new ResponseEntity<ObjectBeanVO>(histVO, HttpStatus.OK);            
        }catch(IOException | NullPointerException ex){
            logger.error(ex);
            ObjectBeanVO histVO = new ObjectBeanVO();
            histVO.setActionMessage(ex.getLocalizedMessage());
            return new ResponseEntity<ObjectBeanVO>(histVO, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }    
    
    
    @RequestMapping(value = "/updateusuario/{idusu}", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> update(@PathVariable("idusu") Integer idusu, 
                                    @RequestBody TbUsuarios user) {
        if (idusu!=null && !Objects.equals(idusu, user.getIdUsu())) {
            return new ResponseEntity<String>("Bad Request", HttpStatus.BAD_REQUEST);
        }        
        TbUsuarios usuario = usuarioService.getUsuarioByUsername(user.getUsuario());
        if(usuario==null){
            return new ResponseEntity<String>("Usuario inexistente en el sistema", HttpStatus.BAD_REQUEST);
        }
        user.setPass(usuario.getPass());
        user.setUsuario(usuario.getUsuario());
        user.setFoto(usuario.getFoto());
        user.setRoles(usuario.getRoles());
        TbUsuarios  paq1 = usuarioService.saveUsuario(user);
        ObjectListVO objlist = usuarioService.findAll(0, maxResults);
        return new ResponseEntity<ObjectListVO<TbUsuarios>>(objlist, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/getroles", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> getAllRoles(Locale locale) {
        
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        ObjectListVO objlist = new ObjectListVO();
        //ObjectListVO paqueteListVO = paqueteService.findByNameLike(page, maxResults, name);
        if(attr.getRequest().isUserInRole(roleMaster)){
            objlist = usuarioService.getAllRoles(null);
        }else{
            objlist = usuarioService.getAllRoles(roleMaster);
        }        
        //ObjectListVO objlist = usuarioService.getAllRoles();
        return new ResponseEntity<ObjectListVO>(objlist, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/saveroles/{idusu}", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> saveRoles(@PathVariable("idusu") Integer idusu, 
                                    @RequestBody List<TbRoles> roles) {
//        if (idusu!=null && !Objects.equals(idusu, user.getIdUsu())) {
//            return new ResponseEntity<String>("Bad Request", HttpStatus.BAD_REQUEST);
//        }        
//        TbUsuarios usuario = usuarioService.getUsuarioByUsername(user.getUsuario());
//        if(usuario==null){
//            return new ResponseEntity<String>("Usuario inexistente en el sistema", HttpStatus.BAD_REQUEST);
//        }
//        user.setPass(usuario.getPass());
        TbUsuarios  paq1 = usuarioService.assignRolesByUser(idusu,roles);
//        if(paq1.getEmail()!=null){
//            StringBuilder sb = new StringBuilder();
//            sb.append("<br><br>El usuario: ").append(paq1.getNombre()).append(" ").append(paq1.getApellidoPat()).append(" ").append(paq1.getApellidoMat()).append(" <br><br>");
//            sb.append("<br><br>Username: ").append(paq1.getUsuario()).append(" <br><br>");
//            sb.append("<br><br>cambio de roles* ");            
//            mailer.sendTemplateMail(paq1.getEmail(), "Roles actualizados",sb.toString(),gnklMails);
//        }         
        ObjectListVO objlist = usuarioService.findAll(0, maxResults);
        return new ResponseEntity<ObjectListVO<TbUsuarios>>(objlist, HttpStatus.OK);
    }

    @RequestMapping(value = "/changepassword/{idusu}", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> changePasswordUser(@PathVariable("idusu") Integer idusu, 
                                    @RequestBody List<String> passwords) {
        
        if (passwords.size()!=3) {
            return new ResponseEntity<String>("Bad Request", HttpStatus.BAD_REQUEST);
        }        
        //String passwordAnterior = passwords.get(0);
        String passwordNuevo = passwords.get(1);
        String passwordNuevoRep = passwords.get(2);
        if(!passwordNuevo.equals(passwordNuevoRep)){
            return new ResponseEntity<String>("Bad Request", HttpStatus.BAD_REQUEST);
        }
        /*boolean result = usuarioService.verifyPassword(idusu, passwordAnterior);
        if(!result){
            return new ResponseEntity<String>("Error contraseña", HttpStatus.BAD_REQUEST);
        }*/
        TbUsuarios  paq1 = usuarioService.saveNewPassword(idusu, passwordNuevo);
        if(paq1.getEmail()!=null){
            StringBuilder sb = new StringBuilder();
            sb.append("<br>Se cambiaron las credenciales para acceso al sistema de tracking: ");
            sb.append("<br>El usuario: ").append(paq1.getNombre()).append(" ").append(paq1.getApellidoPat()).append(" ").append(paq1.getApellidoMat()).append(" <br><br>");
            sb.append("<br>Username: ").append(paq1.getUsuario()).append(" <br>");
            sb.append("<br>Contraseña: ").append(passwordNuevoRep).append(" <br>");
            sb.append("<br>Para ingresar al Sistema seguir la siguiente ruta: <br>");
            sb.append("<br>1. www.gnkl.mx").append(" <br>");
            sb.append("<br>2. Acceso a Clientes").append(" <br>");
            sb.append("<br>3. Dar click en logo de la empresa").append(" <br>");
            sb.append("<br>Su contraseña ha sido Creada* ");            
            mailer.sendTemplateMail(paq1.getEmail(), "Contraseña actualizada",sb.toString(),"",ccoGnkMail);
        }        
        ObjectListVO objlist = new ObjectListVO();
        return new ResponseEntity<ObjectListVO<TbUsuarios>>(objlist, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/getloggeduser", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> searchorigen(Locale locale) {
        SecurityUserBean user = (SecurityUserBean)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return new ResponseEntity<Object>(user, HttpStatus.OK);
    }

    @RequestMapping(value = "/getusuario/", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> getUsuario(Locale locale) {
        SecurityUserBean user = (SecurityUserBean)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        TbUsuarios usuario = usuarioService.getUsuarioByUsername(user.getUsername());
        return new ResponseEntity<TbUsuarios>(usuario, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/search/{name}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> search(@PathVariable("name") String name,
                                    @RequestParam(required = false, defaultValue = DEFAULT_PAGE_DISPLAYED_TO_USER) int page,
                                    Locale locale) {
        String usernameLike="%"+name+"%";
        page=page>0?page-1:page;
        PageRequest pageRequest = new PageRequest(page, maxResults);
        List<TbUsuarios> getListUsers = new ArrayList<TbUsuarios>();
        //Long total=0L;
        if(hasRole("ROLE_MASTER")){
            getListUsers = userRepository.getAllUsersLike(usernameLike, pageRequest);  
        }else{
            getListUsers = userRepository.getAllUsersNotInRole(usernameLike, roleMaster, 0, 10);
        }
        Page<TbUsuarios>  result = new PageImpl(getListUsers);
        ObjectListVO ObjListVo = new ObjectListVO(result.getTotalPages(), result.getTotalElements(), result.getContent());
        return new ResponseEntity<ObjectListVO<TbUsuarios>>(ObjListVo, HttpStatus.OK);        
    }
    
//    @RequestMapping(value = "/uploadfile", method = RequestMethod.POST, produces = "application/json")
//    public ResponseEntity<?> uploadFile(@RequestParam("file") MultipartFile file){
//        try {
//            usuarioService.processXlsxFile(file);
//            ObjectBeanVO histVO = new ObjectBeanVO();
//            return new ResponseEntity<ObjectBeanVO>(histVO, HttpStatus.OK);
//        } catch (Exception ex) {
//            logger.error(ex);
//            ObjectBeanVO histVO = new ObjectBeanVO();
//            histVO.setActionMessage(ex.getLocalizedMessage());
//            return new ResponseEntity<ObjectBeanVO>(histVO, HttpStatus.INTERNAL_SERVER_ERROR);
//        }
//    }    
    
    private boolean hasRole(String role) {
        Collection<GrantedAuthority> authorities = (Collection<GrantedAuthority>) SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        boolean hasRole = false;
        for (GrantedAuthority authority : authorities) {
            hasRole = authority.getAuthority().equals(role);
            if (hasRole) {
                break;
            }
        }
        return hasRole;
    }     

}