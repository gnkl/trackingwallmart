package mx.com.gnkl.novartis.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.annotation.Resource;
import javax.naming.NamingException;
import mx.com.gnkl.novartis.bean.BeanBitacora;
import mx.com.gnkl.novartis.bean.ObjectBeanVO;
import mx.com.gnkl.novartis.bean.ObjectListVO;
import mx.com.gnkl.novartis.bean.SecurityUserBean;
import mx.com.gnkl.novartis.component.ApplicationMailer;
import mx.com.gnkl.novartis.model.TbBitacora;
import mx.com.gnkl.novartis.model.TbBitacoraHistorico;
//import mx.com.gnkl.novartis.model.TbBitacoraInterrupcion;
import mx.com.gnkl.novartis.model.TbBitacoraStatus;
import mx.com.gnkl.novartis.model.TbDomainConfig;
import mx.com.gnkl.novartis.model.TbUsuarios;
import mx.com.gnkl.novartis.repository.BitacoraDao;
import mx.com.gnkl.novartis.repository.jpa.BitacoraHistoricoRepository;
import mx.com.gnkl.novartis.repository.jpa.BitacoraRepository;
import mx.com.gnkl.novartis.repository.jpa.DomainConfigRepository;
import mx.com.gnkl.novartis.repository.jpa.HistoricoRepository;
import mx.com.gnkl.novartis.repository.jpa.StatusRepository;
import mx.com.gnkl.novartis.service.BitacoraService;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/protected/bitacoras")
@Secured({"ROLE_ADMINISTRADOR","ROLE_OPERADOR","ROLE_MASTER"})
public class BitacoraController {

    private static final Logger logger = Logger.getLogger(BitacoraController.class);
    private static final String DEFAULT_PAGE_DISPLAYED_TO_USER = "0";
    private static final String pathJasper = "/archivo/tracking/jasper/";
    
    @Autowired
    private StatusRepository statusRepository;
    
    @Autowired
    private DomainConfigRepository configRepository;
    
    @Autowired
    private BitacoraRepository bitacoraRepository;
    
    @Autowired
    private BitacoraHistoricoRepository bitacoraHistorico;

    @Autowired
    private HistoricoRepository historicoRepository;
    
    @Autowired
    private BitacoraService bitacoraService;
    
    @Autowired
    private BitacoraDao bitacoraDao;
    
    @Resource
    private ApplicationMailer mailer;    
    
    @Value("${grid.limit}")
    private int maxResults;
    
    @Value("${role.admin}")
    private String roleAdmin;
    
    @Value("${role.master}")
    private String roleMaster;

    @Value("${role.operador}")
    private String roleOperador;

    @Value("${role.cliente}")
    private String roleCliente;    
    
    @Value("${gnk.clientmails}")
    private String clientmails;
    
    SimpleDateFormat dt1 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    SimpleDateFormat hr1 = new SimpleDateFormat("HH:mm:ss");
    
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView welcome() {
        return new ModelAndView("bitacoraList");
    }
    
    
    @RequestMapping(value = "/listbitacoras", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> listAll(@RequestParam(required = false) Integer tipoEnvio, @RequestParam int page, Locale locale) {
        if(page>0){
            page = page -1;
        }
        return createListAllResponse(tipoEnvio, page, locale, null);
    }
    
    @ResponseBody
    @RequestMapping(value = "/create", method = RequestMethod.POST, produces = "application/json")
    public TbBitacora create(@RequestBody TbBitacora bitacora) {
        TbBitacora result = null;
        if(bitacora.getId()!=null){
            result = bitacoraService.updateBitacora(bitacora);
            
            if(result.getIsFinal()==1){
                bitacoraService.setOthersBitacorasNotFinal(result.getIdRuta().getId(), result.getId());
            }
            bitacoraService.setLastBitacoraFinalIfEmptyList(bitacora.getIdRuta().getId());
            
            TbUsuarios operador = result.getIdOperador();
            String bodyemail = " Se actualizó la siguiente bitácora: <br>"
                    + " <br> Id Bitácora: " + result.getId() + "<br>"
                    + " <br> Fecha creación: " + dt1.format(result.getFechaCreacion()) + " <br> "
                    + " <br> Fecha recepción: " + dt1.format(result.getFecha()) + " <br> "
                    + " <br> Punto Origen: " + result.getPtoOrigen().getNombrePunto() + "<br>"
                    + " <br> Dirección Punto Origen: " + result.getPtoOrigen().getDireccion() + "<br>"
                    + " <br> Punto Destino: " + result.getPtoDestino().getNombrePunto() + "<br>"
                    + " <br> Dirección Punto Destino: " + result.getPtoDestino().getDireccion() + "<br>"
                    + " <br> Operador: " + (operador.getNombre() != null ? operador.getNombre() : "") + " " + (operador.getApellidoPat() != null ? operador.getApellidoPat() : "") + " " + (operador.getApellidoMat() != null ? operador.getApellidoMat() : "") + "<br>"
                    + " <br> Vehículo: " + result.getIdVehiculo().getNoEconomico()+"  "+result.getIdVehiculo().getPlacas()+" "+result.getIdVehiculo().getMarca() + "<br>";
            TbDomainConfig config = configRepository.findOne("gnk.mails");
            mailer.sendTemplateMail(result.getIdOperador().getEmail(), result.getNombre(), bodyemail, config.getConfigValue(), "");             
        }else{
            result = bitacoraService.createBitacora(bitacora);
            TbUsuarios operador = result.getIdOperador();
            String bodyemail = " Se creó la siguiente bitácora: <br>"
                    + " <br> Id Bitácora: " + result.getId() + "<br>"
                    + " <br> Fecha creación: " + dt1.format(result.getFechaCreacion()) + " <br> "
                    + " <br> Fecha recepción: " + dt1.format(result.getFecha()) + " <br> "
                    + " <br> Punto Origen: " + result.getPtoOrigen().getNombrePunto() + "<br>"
                    + " <br> Dirección Punto Origen: " + result.getPtoOrigen().getDireccion() + "<br>"
                    + " <br> Punto Destino: " + result.getPtoDestino().getNombrePunto() + "<br>"
                    + " <br> Dirección Punto Destino: " + result.getPtoDestino().getDireccion() + "<br>"
                    + " <br> Operador: " + (operador.getNombre() != null ? operador.getNombre() : "") + " " + (operador.getApellidoPat() != null ? operador.getApellidoPat() : "") + " " + (operador.getApellidoMat() != null ? operador.getApellidoMat() : "") + "<br>"
                    + " <br> Vehículo: " + result.getIdVehiculo().getNoEconomico()+"  "+result.getIdVehiculo().getPlacas()+" "+result.getIdVehiculo().getMarca() + "<br>";
            TbDomainConfig config = configRepository.findOne("gnk.mails");
            //mailer.sendTemplateMail(result.getIdOperador().getEmail(), "Bitácora creada", bodyemail, config.getConfigValue(), "");
            mailer.sendTemplateMail(result.getIdOperador().getEmail(), result.getNombre(), bodyemail, config.getConfigValue(), "");
        }
        //return createListAllResponse(0, null, null);
        return result;
    }
    
    
    @ResponseBody
    @RequestMapping(value = "/updatestatus", method = RequestMethod.GET, produces = "application/json")
    public TbBitacora updateStatusBitacora(@RequestParam("idbitacora") int idBitacora, @RequestParam("kmActual") int kmActual, @RequestParam("observaciones") String observaciones, Locale locale) {
        TbBitacora bitacora = bitacoraService.updateBitacoraStatus(idBitacora, kmActual, observaciones);
        TbUsuarios operador = bitacora.getIdOperador();
        String bodyemail = " Se actualizó la siguiente bitácora: <br>"
                + " <br> Id Bitácora: " + bitacora.getId() + "<br>"
                + " <br> Fecha recepción: " + dt1.format(bitacora.getFecha()) + " <br> "
                + " <br> Punto Origen: " + bitacora.getPtoOrigen().getNombrePunto() + "<br>"
                + " <br> Dirección Punto Origen: " + bitacora.getPtoOrigen().getDireccion() + "<br>"
                + " <br> Punto Destino: " + bitacora.getPtoDestino().getNombrePunto() + "<br>"
                + " <br> Dirección Punto Destino: " + bitacora.getPtoDestino().getDireccion() + "<br>"
                + " <br> Operador: " + (operador.getNombre() != null ? operador.getNombre() : "") + " " + (operador.getApellidoPat() != null ? operador.getApellidoPat() : "") + " " + (operador.getApellidoMat() != null ? operador.getApellidoMat() : "") + "<br>"
                + " <br> Estatus: "+bitacora.getIdStatus().getDesStatus()+"<br>";                
        TbDomainConfig config = configRepository.findOne("gnk.mails");
        mailer.sendTemplateMail(bitacora.getIdOperador().getEmail(), "Actualización de estatus: "+bitacora.getNombre(), bodyemail, config.getConfigValue(), "");        
        return bitacora;
    }
    
    
    @ResponseBody
    @RequestMapping(value = "/updateObservaciones", method = RequestMethod.GET, produces = "application/json")
    public TbBitacoraHistorico updateStatusObservaciones(@RequestParam("historico") Integer historico, @RequestParam("observaciones") String observaciones, Locale locale) {
        if(historico > 0){
            return bitacoraService.updateHistoricoObservaciones(historico, observaciones);
        }else{
            return null;
        }
//        TbBitacora bitacora = bitacoraService.updateBitacoraStatus(idBitacora, kmActual, observaciones);
//        TbUsuarios operador = bitacora.getIdOperador();
//        String bodyemail = " Se actualizó la siguiente bitácora: <br>"
//                + " <br> Id Bitácora: " + bitacora.getId() + "<br>"
//                + " <br> Fecha recepción: " + dt1.format(bitacora.getFecha()) + " <br> "
//                + " <br> Punto Origen: " + bitacora.getPtoOrigen().getNombrePunto() + "<br>"
//                + " <br> Dirección Punto Origen: " + bitacora.getPtoOrigen().getDireccion() + "<br>"
//                + " <br> Punto Destino: " + bitacora.getPtoDestino().getNombrePunto() + "<br>"
//                + " <br> Dirección Punto Destino: " + bitacora.getPtoDestino().getDireccion() + "<br>"
//                + " <br> Operador: " + (operador.getNombre() != null ? operador.getNombre() : "") + " " + (operador.getApellidoPat() != null ? operador.getApellidoPat() : "") + " " + (operador.getApellidoMat() != null ? operador.getApellidoMat() : "") + "<br>"
//                + " <br> Estatus: "+bitacora.getIdStatus().getDesStatus()+"<br>";                
//        TbDomainConfig config = configRepository.findOne("gnk.mails");
//        mailer.sendTemplateMail(bitacora.getIdOperador().getEmail(), "Actualización de estatus: "+bitacora.getNombre(), bodyemail, config.getConfigValue(), "");        
//        return bitacora;
    }    
    
    @ResponseBody
    @RequestMapping(value = "/nextstatus", method = RequestMethod.GET, produces = "application/json")
    public TbBitacoraStatus getNextStatusBitacora(@RequestParam("status") Integer status, @RequestParam("tipoEnvio") Integer tipoEnvio, @RequestParam("isFinal") Integer isFinal,Locale locale) {
        TbBitacoraStatus nextStatus = bitacoraService.getNextStatus(status,tipoEnvio, isFinal);
        return nextStatus;
    }    
    
    @ResponseBody
    @RequestMapping(value = "/currentkm", method = RequestMethod.GET, produces = "application/json")
    public Integer getNextStatusBitacora(@RequestParam("idBitacora") Integer idBitacora,Locale locale) {
        return bitacoraHistorico.getCountKmHistoricoByBitacora(idBitacora);
    }   

    
    @ResponseBody
    @RequestMapping(value = "/delete", method = RequestMethod.POST, produces = "application/json")
    public TbBitacora cancelacionBitacora(@RequestParam("idbitacora") int idbitacora, Locale locale) {
        TbBitacora bitacora = bitacoraService.cancelBitacoraStatus(idbitacora);
        return bitacora;
    }    

    @ResponseBody
    @RequestMapping(value = "/gethistoricobitacora", method = RequestMethod.GET, produces = "application/json")
    public List<TbBitacoraHistorico> getHistoricoBitacora(@RequestParam("idbitacora") int idbitacora, Locale locale) {
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        if(attr.getRequest().isUserInRole(roleCliente)){
            List<Integer> listStatus = new ArrayList<Integer>();
            listStatus.add(6);
            listStatus.add(7);
            listStatus.add(8);
            return bitacoraService.getListHistoricoBitacoraByIdBitacora(idbitacora,listStatus);
        } else {
            return bitacoraService.getListHistoricoBitacoraByIdBitacora(idbitacora);   
        }
    }

    @ResponseBody
    @RequestMapping(value = "/getdefaultpunto", method = RequestMethod.GET, produces = "application/json")
    public List<Map<String,Object>> getDefaultPunto(Locale locale) {
         return bitacoraService.getDefaultPuntoEOBitacoras();
    }
    
    @ResponseBody
    @RequestMapping(value = "/gettodaybitacora", method = RequestMethod.GET, produces = "application/json")
    public TbBitacora getBitacoraByToday(@RequestParam(value="idOperador",required = true) Integer idOperador, @RequestParam(value="tipoenvio",required = true) Integer tipoEnvio, Locale locale) {
         return bitacoraService.getBitacoraByDate(idOperador,tipoEnvio);
    }
    
    
    @ResponseBody
    @RequestMapping(value = "/getidbitacora", method = RequestMethod.GET, produces = "application/json")
    public List<TbBitacora> getIdBitacora(@RequestParam(value = "idBitacora", required = false) String idBitacora, 
                                    @RequestParam(value="nombre",required = false) String nombre,
                                    Locale locale) {
        if(idBitacora!=null && !idBitacora.isEmpty()){
            return bitacoraService.getListBitacorasById(idBitacora);   
        }else{
            return bitacoraService.getListBitacorasByName(nombre);   
        } 
    }            


    @RequestMapping(value = "/searchBitacora", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> searchBitacoraParameters(@RequestParam(value="fechaInicial", required = false) String fechaInicial, 
                                                     @RequestParam(value="fechaFinal", required = false) String fechaFinal,
                                                     @RequestParam(value="idBitacora", required = false) String idBitacora,
                                                     @RequestParam(value="idEnvio", required = false) String idEnvio,
                                                     @RequestParam(value="noTracking", required = false) String noTracking,
                                                     Integer page,
                                                     Locale locale) {
        
            ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
            if(attr.getRequest().isUserInRole(roleOperador)){
                return createListAllResponse(null,page, locale, null);
            }else{
                ObjectListVO<TbBitacora> bitacoraListVO = new ObjectListVO<TbBitacora>();         
                bitacoraListVO = bitacoraService.searchByParameters(fechaInicial,fechaFinal,idBitacora,noTracking,idEnvio, page,maxResults);
                return new ResponseEntity<ObjectListVO<TbBitacora>>(bitacoraListVO, HttpStatus.OK);                
            }
    }
          
    
    @ResponseBody
    @RequestMapping(value = "/generateReport", method = RequestMethod.GET, produces = {MediaType.IMAGE_JPEG_VALUE, MediaType.IMAGE_PNG_VALUE, MediaType.APPLICATION_OCTET_STREAM_VALUE, MediaType.APPLICATION_PDF_VALUE})
    public byte[] generateReport(@RequestParam(value="id") String idBitacora) throws ParseException, NamingException, IOException, FileNotFoundException {            
        String pathJasper = "/archivo/tracking/jasper/";
        String pathExcel = "/archivo/tracking/excel/";

        SimpleDateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
        
        String reportFileName = "BitacoraRecepcionGnkl";
        String reportFileNameExcel = "ISO_GNKL_BITACORA_VIAJE.xlsx";
        String today = dateformat.format(new Date());
        String copyReporte = pathExcel+today+reportFileNameExcel;
        
        List<BeanBitacora> listData = bitacoraService.getListToPutIntoBitacoraExcel(idBitacora);
        
        if(listData!=null && !listData.isEmpty()){
            File myFile = new File(pathExcel+reportFileNameExcel);
            
            FileInputStream fis = new FileInputStream(myFile);
            
            XSSFWorkbook myWorkBook = new XSSFWorkbook (fis);
            
            XSSFSheet mySheet = myWorkBook.getSheetAt(0);

            for(BeanBitacora item: listData){
                Row rowR = mySheet.getRow(item.getRow());//cell 14
                Cell cellR = rowR.createCell(item.getCell());
                cellR.setCellValue(item.getValue());                          
            }
            
            FileOutputStream os = new FileOutputStream(new File(copyReporte));
            myWorkBook.write(os);
            
            return FileUtils.readFileToByteArray(new File(copyReporte));                      
        }
        return null;
    }
    
    @RequestMapping(value = "/addimage", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> addImageBitacora(@RequestParam("idbitacora") int idbitacora, @RequestParam("file") MultipartFile file){
        try{
            TbBitacora response = bitacoraService.saveBitacoraImagen(idbitacora, file);
            ObjectBeanVO histVO = new ObjectBeanVO();
            histVO.setData(response);
            return new ResponseEntity<ObjectBeanVO>(histVO, HttpStatus.OK);                      
        }catch(IOException ex){
            logger.error(ex);
            ObjectBeanVO histVO = new ObjectBeanVO();
            histVO.setActionMessage(ex.getLocalizedMessage());
            return new ResponseEntity<ObjectBeanVO>(histVO, HttpStatus.INTERNAL_SERVER_ERROR);            
        }
    }

    @RequestMapping(value = "/addhistoricoimage", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> addImageHistorico(@RequestParam("idBitacora") Integer idBitacora, @RequestParam("status") Integer status,@RequestParam("file") MultipartFile file){
        try{
            TbBitacoraHistorico response = bitacoraService.saveHistoricoImagen(idBitacora, status, file);
            ObjectBeanVO histVO = new ObjectBeanVO();
            histVO.setData(response);
            return new ResponseEntity<ObjectBeanVO>(histVO, HttpStatus.OK);                      
        }catch(IOException ex){
            logger.error(ex);
            ObjectBeanVO histVO = new ObjectBeanVO();
            histVO.setActionMessage(ex.getLocalizedMessage());
            return new ResponseEntity<ObjectBeanVO>(histVO, HttpStatus.INTERNAL_SERVER_ERROR);            
        }
    }    
    
    @ResponseBody
    @RequestMapping(value = "/getimage", method = RequestMethod.GET, produces = {MediaType.IMAGE_JPEG_VALUE, MediaType.IMAGE_PNG_VALUE, MediaType.APPLICATION_OCTET_STREAM_VALUE})
    public byte[] displayImage(@RequestParam("idbitacora") Integer id,
                                    Locale locale) throws IOException {
        return bitacoraService.getImageFromBitacora(id);
    }
    
    @ResponseBody
    @RequestMapping(value = "/getimagehistorico", method = RequestMethod.GET, produces = {MediaType.IMAGE_JPEG_VALUE, MediaType.IMAGE_PNG_VALUE, MediaType.APPLICATION_OCTET_STREAM_VALUE})
    public byte[] displayImageHistorico(@RequestParam("historico") Integer id,
                                    Locale locale) throws IOException {
        return bitacoraService.getImageFromHistorico(id);
    }    
    
    @RequestMapping(value = "/deleteFiles", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> deleteImagesByIdBitacora(@RequestParam("idbitacora") Integer idBitacora) {
        ObjectBeanVO bitacoraVO = new ObjectBeanVO();
        try{
            bitacoraService.deleteArchivosByIdBitacora(idBitacora);
            bitacoraVO.setActionMessage("OK");
        }catch(IOException ex){
            bitacoraVO.setActionMessage("Error en el borrado de archivos bitacora");
            logger.error(ex);
        }
        return new ResponseEntity<ObjectBeanVO>(bitacoraVO, HttpStatus.OK);
    }     

//    @ResponseBody
//    @RequestMapping(value = "/interrumpirBitacora", method = RequestMethod.GET, produces = "application/json")
//    public  TbBitacoraInterrupcion addIInterrupcionToBitacora(@RequestParam("idbitacora") Integer idBitacora){
//        return bitacoraService.addUpdateInterrucionToBitacora(idBitacora);
//    }
    
    @ResponseBody
    @RequestMapping(value = "/searchBitacoraByRuta", method = RequestMethod.GET, produces = "application/json")
    public List<TbBitacora> searchBitacoraByRuta(@RequestParam(value="idruta", required = true) Integer idRuta, 
                                                     Locale locale) {
        return bitacoraRepository.getAllBitacorasByRuta(idRuta);
    }    
    
    private ResponseEntity<?> createListAllResponse(Integer tipoEnvio, int page, Locale locale, String messageKey) {
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        ObjectListVO<TbBitacora> bitacoraListVO = new ObjectListVO<TbBitacora>();
        PageRequest pageRequest = new PageRequest(page, maxResults);  
        if(attr.getRequest().isUserInRole(roleAdmin) || attr.getRequest().isUserInRole(roleMaster)){
              List<TbBitacora> result = bitacoraRepository.findAll(pageRequest).getContent();
              Long totalData = bitacoraRepository.count();
              bitacoraListVO.setData(result);
              bitacoraListVO.setTotalData(totalData);
        }else if(attr.getRequest().isUserInRole(roleCliente)){
            List<Integer> statusList = new ArrayList<Integer>();    
            statusList.add(1);
            statusList.add(8);
            if(tipoEnvio==null){
                tipoEnvio = 3;
            }            
              //List<TbBitacora> result = bitacoraRepository.findAll(pageRequest).getContent();
            Long totalData = bitacoraRepository.getCountAllBitacorasForCliente(statusList,tipoEnvio);            
            List<TbBitacora> result = bitacoraRepository.getAllBitacorasForCliente(statusList , tipoEnvio ,pageRequest);
            bitacoraListVO.setData(result);
            bitacoraListVO.setTotalData(totalData);            
        }else{
            if(tipoEnvio==null){
                tipoEnvio = 3;
            }
            SecurityUserBean user = (SecurityUserBean)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            List<Integer> statusList = new ArrayList<Integer>();    
            statusList.add(8);
            
            List<TbBitacora> noFinalList = bitacoraRepository.getAllBitacorasByOperadorFinalTipoEnvio(user.getIdUsu(), statusList, tipoEnvio, pageRequest);
            Integer totalData = bitacoraRepository.getCountBitacorasByOperadorTipoEnvio(user.getIdUsu(),tipoEnvio);
            
            bitacoraListVO.setData(noFinalList);
            bitacoraListVO.setTotalData(totalData);
        }

        return new ResponseEntity<ObjectListVO<TbBitacora>>(bitacoraListVO, HttpStatus.OK);
    }    
}