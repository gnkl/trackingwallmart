package mx.com.gnkl.novartis.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.annotation.Resource;
import mx.com.gnkl.novartis.bean.ObjectBeanVO;
import mx.com.gnkl.novartis.bean.SecurityUserBean;
import mx.com.gnkl.novartis.component.ApplicationMailer;
import mx.com.gnkl.novartis.model.TbBitacora;
import mx.com.gnkl.novartis.model.TbDomainConfig;
import mx.com.gnkl.novartis.model.TbPaquetes;
import mx.com.gnkl.novartis.model.TbRuta;
import mx.com.gnkl.novartis.model.TbRutaInterrupcion;
import mx.com.gnkl.novartis.model.TbUsuarios;
import mx.com.gnkl.novartis.model.TbVehiculo;
import mx.com.gnkl.novartis.repository.jpa.DomainConfigRepository;
import mx.com.gnkl.novartis.repository.jpa.RutaRepository;
import mx.com.gnkl.novartis.repository.jpa.UserRepository;
import mx.com.gnkl.novartis.repository.jpa.VehiculoRepository;
import mx.com.gnkl.novartis.service.BitacoraService;
import mx.com.gnkl.novartis.service.RutaService;
import mx.gnkl.process.file.ProcessExcelFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value = "/protected/rutas")
@Secured({"ROLE_ADMINISTRADOR","ROLE_OPERADOR","ROLE_MASTER"})
public class RutaController {

    private static final String DEFAULT_PAGE_DISPLAYED_TO_USER = "0";

    @Autowired
    private RutaService rutaService;

    @Resource
    private ApplicationMailer mailer;
    
    @Value("${gnk.mails}")
    private String mails;    
    
    @Value("${grid.limit}")
    private int maxResults;    
    
    @Autowired
    private RutaRepository rutaRepository; 
    
    @Autowired
    private UserRepository userRepository;        
    
    @Autowired
    private VehiculoRepository vehiculoRepository;
    
    @Autowired
    private ProcessExcelFile processExcel;       

    @Value("${reporte.bitacora.path}")
    private String pathReporte;    
    
    @Value("${role.operador}")
    private String roleOperador;
            
    SimpleDateFormat dt1 = new SimpleDateFormat("dd/MM/yyyy");
    
    SimpleDateFormat dt2 = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
    
    @Autowired
    private DomainConfigRepository configRepository;    
    
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView welcome() {
        return new ModelAndView("rutasList");
    }
    

    @RequestMapping(value = "/listrutas", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> listAll(@RequestParam int page, Locale locale) {
        if(page>0){
            page = page -1;
        }
        PageRequest pageReq = new PageRequest(page,maxResults);
        List<TbRuta> resultList = rutaRepository.getRuta(pageReq);
        Long total = rutaRepository.count();
        Map<String, Object> result = new HashMap<String, Object>();
        result.put("total", total);
        result.put("data", resultList);
        return new ResponseEntity(result, HttpStatus.OK);
    }
    
    
    @RequestMapping(value = "/create", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> create(@RequestBody TbRuta ruta) {
        TbRuta  rutaResult = rutaService.persistRuta(ruta);
        SecurityUserBean user = (SecurityUserBean)SecurityContextHolder.getContext().getAuthentication().getPrincipal(); 
        String bodyemail = "La ruta con nombre: "+rutaResult.getNombre()+"<br>"+
                           " Fue creada el dia: "+dt1.format(rutaResult.getFechaCreacion())+"<br>"+
                           " Actualizada: "+dt1.format(rutaResult.getFechaModificacion())+"<br>";
        mailer.sendTemplateMail(user.getEmail(), "Ruta Creada/Actualizada: "+rutaResult.getNombre(), bodyemail ,"","");
        return new ResponseEntity(rutaResult, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/update/{id}", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> update(@PathVariable("id") int paqueteId,
                                    @RequestBody TbPaquetes paquete,
                                    @RequestParam(required = false) String searchFor,
                                    @RequestParam(required = false, defaultValue = DEFAULT_PAGE_DISPLAYED_TO_USER) int page,
                                    Locale locale) {
    return null;
    }

    @RequestMapping(value = "/delete/{rutaId}", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> delete(@PathVariable("rutaId") int id,
                                    @RequestParam(required = false) String searchFor,
                                    @RequestParam(required = false, defaultValue = DEFAULT_PAGE_DISPLAYED_TO_USER) int page,
                                    Locale locale) {
        TbRuta ruta = rutaService.ivalidateRuta(id);
        return new ResponseEntity<TbRuta>(ruta, HttpStatus.OK);
    }

    @ResponseBody
    @RequestMapping(value = "/getBitacorasByRuta", method = RequestMethod.GET, produces = "application/json")
    public List<TbBitacora> getListBitacoraByRuta(@RequestParam("idruta") int idRuta, Locale locale) {
        return rutaService.getListBitacoraByRuta(idRuta);
    }

    @ResponseBody
    @RequestMapping(value = "/getListRuta", method = RequestMethod.GET, produces = "application/json")
    public List<TbRuta> getListRutas(@RequestParam("idRuta") String idRuta, Locale locale) {
        if(idRuta!=null && !idRuta.isEmpty()){
            return rutaService.getListRutas(idRuta);
        }else{
            return new ArrayList<TbRuta>();
        }
    }    
    
    @RequestMapping(value = "/uploadfile", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> uploadFile(@RequestParam("tipoEnvio") Integer tipoEnvio, @RequestParam("file") MultipartFile file){
        try {
            //puntoService.processXlsxFile(file);
            //TODO verificar el tipo de envio si es recoleccion o entrega
            SecurityUserBean user = (SecurityUserBean)SecurityContextHolder.getContext().getAuthentication().getPrincipal(); 
            if(tipoEnvio!=null && tipoEnvio>0){
                rutaService.processWallmartRutasFile(file,tipoEnvio);
                String bodyemail = " El archivo: "+file.getName()+" fué cargado satisfactoriamente"+
                                   " Fue cargado el día: "+dt1.format(new Date())+"<br>";
                mailer.sendTemplateMail(user.getEmail(), "Archivo cargado "+file.getName(), bodyemail ,"","");                
            }else{
                throw new Exception("Sin tipo envio");
            }
            
            ObjectBeanVO histVO = new ObjectBeanVO();
            return new ResponseEntity<ObjectBeanVO>(histVO, HttpStatus.OK);
        } catch (Exception ex) {
            ex.printStackTrace();
//            logger.error(ex);
            ObjectBeanVO histVO = new ObjectBeanVO();
            histVO.setActionMessage(ex.getLocalizedMessage());
            return new ResponseEntity<ObjectBeanVO>(histVO, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
       
    @ResponseBody
    @RequestMapping(value = "/searchRuta", method = RequestMethod.GET, produces = "application/json")
    public List<TbRuta> getIdRuta(@RequestParam(value = "idRuta", required = false) String idRuta, 
                                    @RequestParam(value="status",required = false) Integer status,
                                    Locale locale) {
        if(idRuta!=null && !idRuta.isEmpty()){
            return rutaService.getRutasListById(idRuta,status);
        }
        return new ArrayList<TbRuta>();
    }  
    
    @ResponseBody
    @RequestMapping(value = "/getrutas", method = RequestMethod.GET, produces = "application/json")
    public List<TbRuta> getListRutasByName(@RequestParam(value = "idRuta", required = false) String idRuta,
                                    Locale locale) {
            return rutaService.getRutasListByName(idRuta);
    }    
    
    @ResponseBody
    @RequestMapping(value = "/getvehiculo", method = RequestMethod.GET, produces = "application/json")
    public List<TbVehiculo> getListActiveVehiculos(Locale locale) {
        return vehiculoRepository.findAll();
    } 

    @ResponseBody
    @RequestMapping(value = "/getoperador", method = RequestMethod.GET, produces = "application/json")
    public List<TbUsuarios> getListActiveOperador(Locale locale) {
    return userRepository.getAllOperadoresByEstatus(roleOperador, "A");
    }
    
    @ResponseBody
    @RequestMapping(value = "/interrumpirRuta", method = RequestMethod.GET, produces = "application/json")
    public TbRutaInterrupcion addIInterrupcionToBitacora(@RequestParam("idruta") Integer idRuta, @RequestParam("comentario") String comentario){
         TbRutaInterrupcion result = rutaService.setRutaInterrupcion(idRuta,comentario);
         TbRuta ruta = rutaService.getRutaById(idRuta);
         SecurityUserBean user = (SecurityUserBean)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
         String bodyemail = "";
         TbDomainConfig config = configRepository.findOne("gnk.mails");
         //interrupcion.setStatus(0);//0 es en proceso
         //interrupcion.setStatus(1);// el proceso de interrupcion ya fue terminado        
         if(result!=null && ruta!=null){
             switch(result.getStatus()){
                 case 0:
                    bodyemail = "La ruta con nombre: "+ruta.getNombre()+"<br>"+
                                " Se ha pausado, la hora y fecha de pausa es:"+dt2.format(result.getFechaInicio());
                     mailer.sendTemplateMail(user.getEmail(), "Interrupción de ruta: "+ruta.getNombre(), bodyemail ,config.getConfigValue(),"");
                     break;
                 case 1:
                    bodyemail = "La ruta con nombre: "+ruta.getNombre()+"<br>"+
                                " Se ha retirado la pausa, la hora y fecha para continuar es:  "+dt2.format(result.getFechaFin());
                     mailer.sendTemplateMail(user.getEmail(), "Interrupción de ruta: "+ruta.getNombre(), bodyemail ,config.getConfigValue(),"");
                     break;
             }
         }
         return result;
    }        
    
    @ResponseBody
    @RequestMapping(value = "/getRutaData", method = RequestMethod.GET, produces = "application/json")
    public List<Map<String,Object>> getListTimeByIdRuta(@RequestParam(name = "idruta", required = false) Integer idRuta){
        if(idRuta!=null){
            return rutaService.getListTimesRutasByIdRuta(idRuta);
        }else{
            return rutaService.getListTimesRutasByIdRuta(0,10);
        }
    }  


    @ResponseBody
    @RequestMapping(value = "/reporteRutaTiemposExport", method = RequestMethod.GET, produces = "application/json")
    public byte[] getReporteTiempoByRuta(@RequestParam(name = "idruta", required = false) Integer idRuta) throws Exception{
            List<Map<String,Object>> result = rutaService.getListTimesRutasByIdRuta(0,10);

        DateFormat format = new SimpleDateFormat("yyyyMMdd");
        DateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String fecha = format.format(new Date());            
        
        Map<Integer, String> mapHeader = new HashMap<Integer,String>();
        mapHeader.put(0, "Nombre");
        mapHeader.put(1, "Estatus");
        mapHeader.put(2, "Tipo");
        mapHeader.put(3, "Tiempo total");
        mapHeader.put(4, "Interrupciones");
        mapHeader.put(5, "Tiempo final");        
        
        List<Map<Integer, Object>> listPrint = new ArrayList<Map<Integer, Object>>();
        for(Map<String,Object> item: result){
            Map<Integer, Object> mapData = new HashMap<Integer, Object>();
            mapData.put(0, item.get("nombre"));
            Integer idStatus = (Integer) item.get("estatus"); 
            if(idStatus!=null){
                mapData.put(1, idStatus.equals(1)?"ACTIVO":"INACTIVO");
            }else{
                mapData.put(1, "");
            }
            mapData.put(2, item.get("des_envio"));
            mapData.put(3, item.get("tiempo_gral"));
            mapData.put(4, item.get("tiempo_interrupcion")!=null?item.get("tiempo_interrupcion"):"");
            mapData.put(5, item.get("tiempo_total"));
            listPrint.add(mapData);
        }        
        String filename="ListadoRutasTiempos";
        processExcel.writeCollectionMap(pathReporte+filename+fecha+".xlsx", "Detalle", listPrint, mapHeader);
        return getFileReporteHistorico(filename+fecha);        
    }      
    
    public byte[] getFileReporteHistorico(String fileName) throws IOException {
            //pathOperador+fileName
            String fileZipName = String.valueOf(fileName + (new Date().getTime())) + ".zip";
            File zipfile = new File(pathReporte+fileZipName);
            // Create a buffer for reading the files
            byte[] buf = new byte[1024];
            // create the ZIP file
            ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipfile));
                Path pathFile = Paths.get(pathReporte+fileName+".xlsx");
                File file = pathFile.toFile();
                FileInputStream in = new FileInputStream(file);
                // add ZIP entry to output stream
                out.putNextEntry(new ZipEntry(file.getName()));
                // transfer bytes from the file to the ZIP file
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
                // complete the entry
                out.closeEntry();
                in.close();
            out.close();
            byte[] data = Files.readAllBytes(zipfile.toPath());
            return data;       
    }     
    
    //addrutaimage
    
    @RequestMapping(value = "/addrutaimage", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> addImageRuta(@RequestParam("idruta") Integer idRuta, @RequestParam("file") MultipartFile file){
        try{
            TbRuta rutaDemo = rutaService.saveRutaImagen(idRuta, file);
            ObjectBeanVO histVO = new ObjectBeanVO();
            histVO.setData(rutaDemo);
            return new ResponseEntity<ObjectBeanVO>(histVO, HttpStatus.OK);                
        }catch(IOException ex){
            //logger.error(ex);
            ObjectBeanVO histVO = new ObjectBeanVO();
            histVO.setActionMessage(ex.getLocalizedMessage());
            return new ResponseEntity<ObjectBeanVO>(histVO, HttpStatus.INTERNAL_SERVER_ERROR);            
        }            
    }
    
    @ResponseBody
    @RequestMapping(value = "/getCurrentBitacoraRuta", method = RequestMethod.GET, produces = "application/json")
    public List<TbBitacora> getCurrentUnfinishOrLastBitacora(@RequestParam(name = "idruta") Integer idRuta){
        return rutaService.getActualBitacoraByRuta(idRuta);
    }     
    
}