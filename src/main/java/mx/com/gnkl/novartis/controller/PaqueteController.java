package mx.com.gnkl.novartis.controller;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import mx.com.gnkl.novartis.service.PaqueteService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.Locale;
import java.util.Map;
import javax.annotation.Resource;
import mx.com.gnkl.novartis.bean.ObjectListVO;
import mx.com.gnkl.novartis.bean.SecurityUserBean;
import mx.com.gnkl.novartis.component.ApplicationMailer;
import mx.com.gnkl.novartis.model.TbDomainConfig;
import mx.com.gnkl.novartis.model.TbFolio;
import mx.com.gnkl.novartis.model.TbPaquetes;
import mx.com.gnkl.novartis.model.TbRuta;
import mx.com.gnkl.novartis.model.TbStatus;
import mx.com.gnkl.novartis.model.TbTipoenvio;
import mx.com.gnkl.novartis.model.TbUsuarios;
import mx.com.gnkl.novartis.model.TbVehiculo;
import mx.com.gnkl.novartis.repository.jpa.DomainConfigRepository;
import mx.com.gnkl.novartis.repository.jpa.FolioRepository;
import mx.com.gnkl.novartis.repository.jpa.PaqueteRepository;
import mx.com.gnkl.novartis.repository.jpa.StatusRepository;
import mx.com.gnkl.novartis.service.HistoricoService;
import mx.com.gnkl.novartis.service.PuntoService;
import mx.com.gnkl.novartis.service.RutaService;
import org.apache.log4j.Logger;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@Controller
@RequestMapping(value = "/protected/paquetes")
@Secured({"ROLE_ADMINISTRADOR","ROLE_OPERADOR","ROLE_MASTER"})
public class PaqueteController {

     private static final Logger logger = Logger.getLogger(PaqueteController.class);
    private static final String DEFAULT_PAGE_DISPLAYED_TO_USER = "0";

    @Autowired
    private PaqueteService paqueteService;

    @Autowired
    private MessageSource messageSource;
    
    @Autowired
    private HistoricoService historicoService;     

    @Autowired
    private PuntoService puntoService;

    @Autowired
    private RutaService rutaService;
    
    @Autowired
    private PaqueteRepository paqueteRepository;

    @Autowired
    private FolioRepository folioRepository;
    
    @Autowired
    private StatusRepository statusRepository;
    
    @Autowired
    private DomainConfigRepository configRepository;
    
    @Resource
    private ApplicationMailer mailer;    
    
    @Value("${grid.limit}")
    private int maxResults;
    
    @Value("${role.admin}")
    private String roleAdmin;
    
    @Value("${role.master}")
    private String roleMaster;

//    @Value("${gnk.mails}")
//    private String mails;
//    configRepository    

    @Value("${gnk.clientmails}")
    private String clientmails;
    
    SimpleDateFormat dt1 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView welcome() {
        return new ModelAndView("paquetesList");
    }
    
    @RequestMapping(value = "/listpaquetes", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> listAll(@RequestParam int page, Locale locale) {
        if(page>0){
            page = page -1;
        }
        return createListAllResponse(page, locale, null);
    }
    
    @RequestMapping(value = "/create", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> create(@RequestBody TbPaquetes paquete, @RequestParam(value="idBitacora") int idBitacora) {
        //TODO verify if exists more than one idtracking para jaloma crossdock
        System.out.println(idBitacora);
        System.out.println(paquete);
        TbTipoenvio tipoEnvio = paquete.getIdStatus().getIdEnvio();
        Integer total = paqueteService.getCountNoTrackingBy(paquete.getNoEmbarque());
        Integer fase = 0;
        if (total == 0) {
            fase = 1;
        } else if (total < 2) {
            fase = 2;
        }
        if(tipoEnvio!= null && tipoEnvio.getIdEnvio()==3){
            fase = 0;
        }
        if (total < 2) {
//            if (paquete.getIdStatus() != null) {
//                TbTipoenvio tipoEnvio = paquete.getIdStatus().getIdEnvio();
//                if (tipoEnvio != null && tipoEnvio.getIdEnvio() != null) {
//                    fase = tipoEnvio.getIdEnvio();
//                }
//            }
            paquete.setFase(fase);
            TbPaquetes paq1 = paqueteService.persistPaqueteWithHistorico(paquete,idBitacora);
            SecurityUserBean user = (SecurityUserBean) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            TbUsuarios operador = paq1.getIdOperador();
            String bodyemail = " Se realizó el siguiente viaje: " + paq1.getNoEmbarque() + "<br>"
                    + " <br> No. de Tracking: " + paq1.getNoEmbarque() + "<br>"
                    + " <br> Fecha inicio: " + dt1.format(paq1.getFechaRecPaq()) + " <br> "
                    + " <br> Fecha llegada: " + dt1.format(paq1.getFechaEntPaq())
                    + " <br> Punto Origen: " + paq1.getIdPuntoorigen().getNombrePunto() + "<br>"
                    + " <br> Dirección Punto Origen: " + paq1.getIdPuntoorigen().getDireccion() + "<br>"
                    + " <br> Punto Destino: " + paq1.getIdPuntoe().getNombrePunto() + "<br>"
                    + " <br> Dirección Punto Destino: " + paq1.getIdPuntoe().getDireccion() + "<br>"
                    + " <br> Operador: " + (operador.getNombre() != null ? operador.getNombre() : "") + " " + (operador.getApellidoPat() != null ? operador.getApellidoPat() : "") + " " + (operador.getApellidoMat() != null ? operador.getApellidoMat() : "") + "<br>";
//    @Value("${gnk.mails}")
//    private String mails;
//    configRepository    
            TbDomainConfig config = configRepository.findOne("gnk.mails");
            mailer.sendTemplateMail(paquete.getIdOperador().getEmail(), "Viaje creado", bodyemail, config.getConfigValue(), "");
        }
        ResponseEntity<?> result = createListAllResponse(0, new Locale(""), null);
        ObjectListVO<TbPaquetes> paqueteListVO = (ObjectListVO<TbPaquetes>) result.getBody();
        paqueteListVO.setExtraParam(paqueteService.getCountNoTrackingBy(paquete.getNoEmbarque()));
        return new ResponseEntity<ObjectListVO<TbPaquetes>>(paqueteListVO, HttpStatus.OK);
    }
    
    @ResponseBody
    @RequestMapping(value = "/totalviaje", method = RequestMethod.GET, produces = "application/json")
    public Integer searchFolio(@RequestParam(required = true) String name, Locale locale) {
        Integer total = paqueteService.getCountNoTrackingBy(name);
        return total;
    }      
    

    @RequestMapping(value = "/listrutapaquete", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> listAll(@RequestParam int page, @RequestParam(value = "idruta" ,required = true) int idRuta, Locale locale) {
        if(page>0){
            page = page -1;
        }
        Map<String, Object> result = getListAllByIdRutaResponse(page, idRuta, locale, null);
        return new ResponseEntity(result, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/create/{idruta}", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> createRuta(@PathVariable("idruta") int idruta, 
                                    @RequestBody TbPaquetes paquete) {
        TbRuta ruta = rutaService.getRutaById(idruta);
        paquete.setIdRuta(ruta);
        TbPaquetes  paq1 = paqueteService.save(paquete);
        historicoService.saveHistorial(paq1, "CREACIÓN INICIAL DE TRACKING", null);
        SecurityUserBean user = (SecurityUserBean)SecurityContextHolder.getContext().getAuthentication().getPrincipal(); 
        String bodyemail = " El paquete con número de embarque: "+paq1.getNoEmbarque()+"<br>"+
                           " fue creada el dia: "+dt1.format(paq1.getFecCargaPaq())+"<br>"+
                           " Con estatus: "+paq1.getIdStatus().getDesStatus()+"<br>"+
                           " Con fecha recepción: "+dt1.format(paq1.getFechaRecPaq())+" Y dia de entrega: "+dt1.format(paq1.getFechaEntPaq())+
                           " Con Origen: "+paq1.getIdPuntoorigen().getNombrePunto()+"<br>"+
                           " Con Entrega: "+paq1.getIdPuntoe().getNombrePunto();
        TbDomainConfig config = configRepository.findOne("gnk.mails");
        mailer.sendTemplateMail(user.getEmail(), "Ruta Creada", bodyemail ,config.getConfigValue(),"");        
        return createListAllResponse(0, new Locale(""), null);
    }
    
    
    @RequestMapping(value = "/update/{id}", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> update(@PathVariable("id") int paqueteId,
                                    @RequestBody TbPaquetes paquete,
                                    @RequestParam(required = false) String searchFor,
                                    @RequestParam(required = false, defaultValue = DEFAULT_PAGE_DISPLAYED_TO_USER) int page,
                                    Locale locale) {
        if (paqueteId != paquete.getIdPaq()) {
            return new ResponseEntity<String>("Bad Request", HttpStatus.BAD_REQUEST);
        }

        TbPaquetes paq1 = paqueteService.update(paquete);
        SecurityUserBean user = (SecurityUserBean)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        TbDomainConfig config = configRepository.findOne("gnk.mails");
        mailer.sendTemplateMail(user.getEmail(), "Ruta Actualizada", "La ruta con numero de embarque: "+paq1.getNoEmbarque()+"<br> Fue creada el dia: "+dt1.format(paq1.getFecCargaPaq())+" Con estatus: "+paq1.getIdStatus().getDesStatus()+" fecha recepción: "+dt1.format(paq1.getFechaRecPaq())+" Y dia de entrega: "+dt1.format(paq1.getFechaEntPaq()),config.getConfigValue(),"");                
        return createListAllResponse(page, locale, "message.update.success");
    }

    @RequestMapping(value = "/delete/{paqueteId}", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> delete(@PathVariable("paqueteId") int idpaq,
                                    @RequestParam(required = false) String searchFor,
                                    @RequestParam(required = false, defaultValue = DEFAULT_PAGE_DISPLAYED_TO_USER) int page,
                                    Locale locale) {
        try {
            TbPaquetes paquete = paqueteService.getPaqueteByIdPaq(idpaq);
            paqueteService.delete(idpaq);
//            TbPaquetes paquete = paqueteService.getPaqueteByIdPaq(idpaq);
//            historicoService.saveHistorial(paquete, "CANCELACIÓN DE PAQUETE",null);
            SecurityUserBean user = (SecurityUserBean)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            TbDomainConfig config = configRepository.findOne("gnk.mails");
            mailer.sendTemplateMail(user.getEmail(), "Viaje Eliminado", "La ruta con numero de embarque: "+paquete.getNoEmbarque()+" fue borrada ", config.getConfigValue(), "");
        } catch (AccessDeniedException e) {
            logger.error(e);
            return new ResponseEntity<Object>(HttpStatus.FORBIDDEN);
        }

        if (isSearchActivated(searchFor)) {
            return search(searchFor, page, locale, "message.delete.success");
        }

        return createListAllResponse(page, locale, "message.delete.success");
    }

    
    @RequestMapping(value = "/searchorigen/{name}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> searchorigen(@PathVariable("name") String name,
                                    @RequestParam(required = false, defaultValue = DEFAULT_PAGE_DISPLAYED_TO_USER) int page,
                                    Locale locale) {
        //ObjectListVO origenListVO = paqueteService.getPuntoOrigenFormat(name);
        ObjectListVO origenListVO = puntoService.getPuntoOrigen(name);
        Object[] args = {name};
        addSearchMessageToVO(origenListVO, locale, "message.search.for.active", args);
        return new ResponseEntity<ObjectListVO>(origenListVO, HttpStatus.OK);
    }
    

    @RequestMapping(value = "/searchorigen", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> searchorigen(@RequestParam(required = false, defaultValue = DEFAULT_PAGE_DISPLAYED_TO_USER) int page,
                                    Locale locale) {
        //ObjectListVO origenListVO = paqueteService.getAllPuntoOrigen();
        ObjectListVO origenListVO = puntoService.getPuntoOrigen();
        Object[] args = {"All origenes"};
        addSearchMessageToVO(origenListVO, locale, "message.search.for.active", args);
        return new ResponseEntity<ObjectListVO>(origenListVO, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/searchenvio/", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> searchenvio(Locale locale) {
        ObjectListVO origenListVO = paqueteService.getAllEnvio();
        return new ResponseEntity<ObjectListVO>(origenListVO, HttpStatus.OK);
    }    
    
    @RequestMapping(value = "/searchentrega/{name}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> searchEntrega(@PathVariable("name") String name,
                                    @RequestParam(required = false, defaultValue = DEFAULT_PAGE_DISPLAYED_TO_USER) int page,
                                    Locale locale) {
        //ObjectListVO entregaListVO =  paqueteService.getPuntoEntregaFormat(name);  
        ObjectListVO entregaListVO =  puntoService.getPuntoEntrega(name);
        Object[] args = {name};
        addSearchMessageToVO(entregaListVO, locale, "message.search.for.active", args);
        return new ResponseEntity<ObjectListVO>(entregaListVO, HttpStatus.OK);
    }

    @RequestMapping(value = "/searchentrega", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> searchEntrega(@RequestParam(required = false, defaultValue = DEFAULT_PAGE_DISPLAYED_TO_USER) int page,
                                    Locale locale) {
//        ObjectListVO entregaListVO = paqueteService.getAllPuntoEntrega();  
        ObjectListVO entregaListVO = puntoService.getPuntoEntrega();
        Object[] args = {"all "};
        addSearchMessageToVO(entregaListVO, locale, "message.search.for.active", args);
        return new ResponseEntity<ObjectListVO>(entregaListVO, HttpStatus.OK);
    }
    

    @RequestMapping(value = "/operadorbyname", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> searchOperadorByName(@RequestParam("name") String name,
                                    @RequestParam(required = false, defaultValue = DEFAULT_PAGE_DISPLAYED_TO_USER) int page,
                                    Locale locale) {
        ObjectListVO origenListVO = paqueteService.getOperadoresByNameLike(name);
        Object[] args = {name};
        addSearchMessageToVO(origenListVO, locale, "message.search.for.active", args);
        return new ResponseEntity<ObjectListVO>(origenListVO, HttpStatus.OK);
    }
    

    @RequestMapping(value = "/searchoperador", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> searchOperador(@RequestParam(required = false, defaultValue = DEFAULT_PAGE_DISPLAYED_TO_USER) int page,
                                    Locale locale) {
        ObjectListVO origenListVO =  paqueteService.getAllOperadores();
        Object[] args = {"All"};
        addSearchMessageToVO(origenListVO, locale, "message.search.for.active", args);
        return new ResponseEntity<ObjectListVO>(origenListVO, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/search/{name}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> search(@PathVariable("name") String name,
                                    @RequestParam(required = false, defaultValue = DEFAULT_PAGE_DISPLAYED_TO_USER) int page,
                                    Locale locale) {
        return search(name, page, locale, null);
    }

    @ResponseBody
    @RequestMapping(value = "/folios/{tipo}", method = RequestMethod.GET, produces = "application/json")
    public Long searchFolio(@PathVariable("tipo") Integer tipo, @RequestParam(required = false, defaultValue = DEFAULT_PAGE_DISPLAYED_TO_USER) int page, Locale locale) {
        TbFolio folios = folioRepository.findOne(1);
        Long folio=0L;
        switch(tipo){
            case 1:
                folio=folios.getFolioJaloma();
                folios.setFolioJaloma(folio+1);
                break;
            case 2:
                folio=folios.getFolioNovartis();
                folios.setFolioNovartis(folio+1);
                break;
            case 3:
                folio=folios.getFolioDegasa();
                folios.setFolioDegasa(folio+1);
                break;
            default:
                folio=0L;
                break;
        }
        folioRepository.save(folios);
        return folio;
    }    
    
    @RequestMapping(value = "/search", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> search(@RequestParam(value = "noembarque" ,required = false) String noembarque,
                                    @RequestParam(value = "idoperador" ,required = false) Integer idoperador,
                                    @RequestParam(value = "puntoorigen" ,required = false) Integer ptoorigen,
                                    @RequestParam(value = "puntoentrega" ,required = false) Integer ptoentrega,
                                    @RequestParam(value = "status" ,required = false) String status,
                                    @RequestParam(required = false, defaultValue = DEFAULT_PAGE_DISPLAYED_TO_USER) int page,
                                    Locale locale) {
        Map<String, Object> dataSearch = new HashMap<String, Object>();
        dataSearch.put("noEmbarque", noembarque);
        dataSearch.put("idOperador", idoperador);
        dataSearch.put("idOrigen", ptoorigen);
        dataSearch.put("idEntrega", ptoentrega);
        dataSearch.put("idStatus", status);
        
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        //ObjectListVO paqueteListVO = paqueteService.findByNameLike(page, maxResults, dataSearch);
        if(attr.getRequest().isUserInRole(roleAdmin) || attr.getRequest().isUserInRole(roleMaster)){
            
        }else{
            SecurityUserBean user = (SecurityUserBean)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            dataSearch.put("usuario", user.getIdUsu());
        }
        ObjectListVO paqueteListVO = paqueteService.findByNameLike(page, maxResults, dataSearch);        

//        if (!StringUtils.isEmpty(actionMessageKey)) {
//            addActionMessageToVO(paqueteListVO, locale, actionMessageKey, null);
//        }
//
        Object[] args = {dataSearch.toString()};
        addSearchMessageToVO(paqueteListVO, locale, "message.search.for.active", args);
        return new ResponseEntity<ObjectListVO>(paqueteListVO, HttpStatus.OK);
    }
    
    
    @ResponseBody
    @RequestMapping(value = "/getstatus", method = RequestMethod.GET, produces = "application/json")
    public List<TbStatus> searchFolio(@RequestParam("empresa") int empresa, @RequestParam("page") int page, @RequestParam("tipoenvio") int tipoenvio, Locale locale) {
        List<TbStatus> statusList = statusRepository.getEstatusByIdEnvioIdEmpresa(tipoenvio , empresa);
        return statusList;
    }       

    @ResponseBody
    @RequestMapping(value = "/getvehiculos/", method = RequestMethod.GET, produces = "application/json")
    public List<TbVehiculo> getVehiculos(Locale locale) {
        List<TbVehiculo> listVehiculo = paqueteService.getAllVehiculos();
        return listVehiculo;
    }
    
    @ResponseBody
    @RequestMapping(value = "/searchExacly", method = RequestMethod.GET, produces = "application/json")
    public List<Map<String,Object>> searchExacly(@RequestParam("name") String name, Locale locale) {
        return paqueteService.getFasesPaquetes(name);
    }    
    
    @ResponseBody
    @RequestMapping(value = "/getFaseByEmbarque", method = RequestMethod.GET, produces = "application/json")
    public Integer getFaseByNoEmbarque(@RequestParam("notracking") String noEmbarque, @RequestParam("tipoenvio") Integer tipoEnvio, Locale locale) {
        return paqueteService.getFaseByPaquete(noEmbarque,tipoEnvio);
    }     
    
    private ResponseEntity<?> search(String name, int page, Locale locale, String actionMessageKey) {
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        ObjectListVO paqueteListVO = new ObjectListVO();
        //ObjectListVO paqueteListVO = paqueteService.findByNameLike(page, maxResults, name);
        if(attr.getRequest().isUserInRole(roleAdmin) || attr.getRequest().isUserInRole(roleMaster)){
            paqueteListVO = paqueteService.findByNameLike(page, maxResults, name);
        }else{
            SecurityUserBean user = (SecurityUserBean)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            paqueteListVO = paqueteService.findByNameLike(page, maxResults, name, user.getIdUsu());
        }

        if (!StringUtils.isEmpty(actionMessageKey)) {
            addActionMessageToVO(paqueteListVO, locale, actionMessageKey, null);
        }

        Object[] args = {name};

        addSearchMessageToVO(paqueteListVO, locale, "message.search.for.active", args);

        return new ResponseEntity<ObjectListVO>(paqueteListVO, HttpStatus.OK);
    }    


    private Map<String, Object> getListAllByIdRutaResponse(int page, int idRuta, Locale locale, String messageKey) {
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        Map<String, Object> result = new HashMap<String, Object>();
        if(attr.getRequest().isUserInRole(roleAdmin) || attr.getRequest().isUserInRole(roleMaster)){
//            List<TbPaquetes> list = paqueteRepository.get
//            paqueteService.getTotalPaquetes(idruta);
            PageRequest pageReq = new PageRequest(page, maxResults);
            List<TbPaquetes> paquetes = paqueteRepository.getAllPaquetesByRuta(idRuta,pageReq);
            Long total = paqueteRepository.getCountAllPaquetesByRuta(idRuta);
            result.put("totalData", total);
            result.put("data", paquetes);
        }else{
            SecurityUserBean user = (SecurityUserBean)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//            paqueteService.findAll(page, maxResults, user.getIdUsu(), idRuta);
//            paqueteService.getTotalPaquetes(user.getIdUsu(),idRuta);
            PageRequest pageReq = new PageRequest(page, maxResults);
            List<TbPaquetes> paquetes = paqueteRepository.getAllPaquetesByIdUserRuta(user.getIdUsu(), idRuta, pageReq);
            Long total = paqueteRepository.getCountAllPaquetesByIdUserRuta(user.getIdUsu(), idRuta);
            result.put("totalData", total);
            result.put("data", paquetes);            
        }
        return result;

    }    
    
    private ResponseEntity<?> createListAllResponse(int page, Locale locale, String messageKey) {
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        ObjectListVO<TbPaquetes> paqueteListVO = new ObjectListVO<TbPaquetes>();
        if(attr.getRequest().isUserInRole(roleAdmin) || attr.getRequest().isUserInRole(roleMaster)){
            paqueteListVO = paqueteService.findAll(page, maxResults);
            paqueteListVO.setTotalData(paqueteService.getTotalPaquetes());   
        }else{
            SecurityUserBean user = (SecurityUserBean)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            paqueteListVO = paqueteService.findAll(page, maxResults, user.getIdUsu());
            paqueteListVO.setTotalData(paqueteService.getTotalPaquetes(user.getIdUsu()));                    
        }
        addActionMessageToVO(paqueteListVO, locale, messageKey, null);
        return new ResponseEntity<ObjectListVO<TbPaquetes>>(paqueteListVO, HttpStatus.OK);
    }

    private ObjectListVO addActionMessageToVO(ObjectListVO contactListVO, Locale locale, String actionMessageKey, Object[] args) {
        if (StringUtils.isEmpty(actionMessageKey)) {
            return contactListVO;
        }

        contactListVO.setActionMessage(messageSource.getMessage(actionMessageKey, args, null, locale));

        return contactListVO;
    }

    private ObjectListVO addSearchMessageToVO(ObjectListVO contactListVO, Locale locale, String actionMessageKey, Object[] args) {
        if (StringUtils.isEmpty(actionMessageKey)) {
            return contactListVO;
        }

        contactListVO.setSearchMessage(messageSource.getMessage(actionMessageKey, args, null, locale));

        return contactListVO;
    }

    private boolean isSearchActivated(String searchFor) {
        return !StringUtils.isEmpty(searchFor);
    }
}