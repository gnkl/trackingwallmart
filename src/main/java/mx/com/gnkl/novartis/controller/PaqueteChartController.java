package mx.com.gnkl.novartis.controller;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import mx.com.gnkl.novartis.service.PaqueteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;


@Controller
@RequestMapping(value = "/protected/paquetechart")
public class PaqueteChartController {
    @Autowired
    private PaqueteService paqueteService;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView welcome() {
        return new ModelAndView("chartpaquete");
    }
    
    @ResponseBody
    @RequestMapping(value = "/countbystatus/{tipoenvio}", method = RequestMethod.GET, produces = "application/json")
    public List<Map<String, Object>> update(@PathVariable("tipoenvio") int idenvio, Locale locale) {
        return paqueteService.getDataByStatus(idenvio);
    }    
    
}