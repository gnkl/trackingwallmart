package mx.com.gnkl.novartis.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import mx.com.gnkl.novartis.bean.ObjectBeanVO;
import mx.com.gnkl.novartis.bean.ObjectListVO;
import mx.com.gnkl.novartis.model.TbPuntoEo;
import mx.com.gnkl.novartis.service.PaqueteService;
import mx.com.gnkl.novartis.service.PuntoService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;


@Controller
@RequestMapping(value = "/protected/punto")
public class PuntoController {
    
    private static final Logger logger = Logger.getLogger(PuntoController.class);

    private static final String DEFAULT_PAGE_DISPLAYED_TO_USER = "0";
    
    @Value("${grid.limit}")
    private int maxResults;
    
    @Autowired
    private PaqueteService paqueteService; 
    
    @Autowired
    private PuntoService puntoService;

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView welcome() {
        return new ModelAndView("crudpunto");
    }

    @RequestMapping(value = "/getpunto", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> searchPunto(@RequestParam(defaultValue = DEFAULT_PAGE_DISPLAYED_TO_USER) int page,
                                    Locale locale) {
        if(page>0){
            page = page -1;
        }
        ObjectListVO objlist = puntoService.getPunto(page);
        return new ResponseEntity<ObjectListVO>(objlist, HttpStatus.OK);
    }

    
    
//    @RequestMapping(value = "/getorigen/", method = RequestMethod.GET, produces = "application/json")
//    public ResponseEntity<?> searchorigen(@RequestParam(defaultValue = DEFAULT_PAGE_DISPLAYED_TO_USER) int page,
//                                    Locale locale) {
//        if(page>0){
//            page = page -1;
//        }
//        ObjectListVO objlist = puntoService.getPuntoOrigen(page);
//        return new ResponseEntity<ObjectListVO>(objlist, HttpStatus.OK);
//    }
    
//    @RequestMapping(value = "/getentrega/", method = RequestMethod.GET, produces = "application/json")
//    public ResponseEntity<?> searchenvio(@RequestParam(defaultValue = DEFAULT_PAGE_DISPLAYED_TO_USER) int page,Locale locale) {
//        if(page>0){
//            page = page -1;
//        }        
//        ObjectListVO entregaListVO = puntoService.getPuntoEntrega(page);
//        return new ResponseEntity<ObjectListVO>(entregaListVO, HttpStatus.OK);
//    }
    
//    @RequestMapping(value = "/createptoorigen", method = RequestMethod.POST, produces = "application/json")
//    public ResponseEntity<?> create(@RequestBody TbPuntosorigen origen) {
//        //TbPuntosorigen  origenresult = puntoService.savePuntoOrigen(origen);
//        ObjectListVO<TbPuntosorigen> origenListVO = new ObjectListVO<TbPuntosorigen>();
////        origenListVO = paqueteService.getPuntoOrigen(0);
////        origenListVO.setTotalData(origenListVO.getTotalData());
//        return new ResponseEntity<ObjectListVO<TbPuntosorigen>>(origenListVO, HttpStatus.OK);
//    }    

//    @RequestMapping(value = "/createptoentrega", method = RequestMethod.POST, produces = "application/json")
//    public ResponseEntity<?> create(@RequestBody TbPuntosentrega entrega) {
////        TbPuntosentrega  entregaresult = puntoService.savePuntoEntrega(entrega);
//        ObjectListVO<TbPuntosentrega> entregaListVO = new ObjectListVO<TbPuntosentrega>();
////        entregaListVO = paqueteService.getPuntoEntrega(0);
////        entregaListVO.setTotalData(entregaListVO.getTotalData());
//        return new ResponseEntity<ObjectListVO<TbPuntosentrega>>(entregaListVO, HttpStatus.OK);
//    }    

    @RequestMapping(value = "/createpto", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> createPto(@RequestBody TbPuntoEo punto) {
        TbPuntoEo  puntoresult = puntoService.savePuntoEo(punto);
        ObjectListVO<TbPuntoEo> puntoListVO = new ObjectListVO<TbPuntoEo>();
//        origenListVO = paqueteService.getPuntoOrigen(0);
//        origenListVO.setTotalData(origenListVO.getTotalData());
        return new ResponseEntity<ObjectListVO<TbPuntoEo>>(puntoListVO, HttpStatus.OK);
    }    
    
    @RequestMapping(value = "/uploadfile", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> uploadFile(@RequestParam("file") MultipartFile file){
        try {
            //puntoService.processXlsxFile(file);
            puntoService.processWallmartXlsxFile(file);
            ObjectBeanVO histVO = new ObjectBeanVO();
            return new ResponseEntity<ObjectBeanVO>(histVO, HttpStatus.OK);
        } catch (Exception ex) {
            ex.printStackTrace();
//            logger.error(ex);
            ObjectBeanVO histVO = new ObjectBeanVO();
            histVO.setActionMessage(ex.getLocalizedMessage());
            return new ResponseEntity<ObjectBeanVO>(histVO, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    
    @RequestMapping(value = "/search", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> search(@RequestParam(value = "nombre" ,required = false) String nombre,
                                    @RequestParam(value = "direccion" ,required = false) String direccion,
                                    @RequestParam(value = "estatus" ,required = false) Integer estatus,
                                    @RequestParam(required = false, defaultValue = DEFAULT_PAGE_DISPLAYED_TO_USER) int page,
                                    Locale locale) {
        Map<String, Object> dataSearch = new HashMap<String, Object>();
        dataSearch.put("nombre", nombre);
        dataSearch.put("direccion", direccion);
        dataSearch.put("estatus", estatus);

        ObjectListVO puntoListVO = puntoService.findByCustomSearch(page, maxResults, dataSearch);
        return new ResponseEntity<ObjectListVO>(puntoListVO, HttpStatus.OK);
    }
    
    @ResponseBody
    @RequestMapping(value = "/getnombre", method = RequestMethod.GET, produces = "application/json")
    public List<String> searchByNombre(@RequestParam(required = true) String name, Locale locale) {
        List<String> list = new ArrayList<String>();
        list.addAll(puntoService.getNombrePuntoDireccionList(0,name));
        return list;
    }     
    
    @ResponseBody
    @RequestMapping(value = "/getdireccion", method = RequestMethod.GET, produces = "application/json")
    public List<String> searchByDireccion(@RequestParam(required = true) String name, Locale locale) {
        List<String> noTrackingList = new ArrayList<String>();
        noTrackingList.addAll(puntoService.getNombrePuntoDireccionList(1,name));
        return noTrackingList;
    }     

    
}
