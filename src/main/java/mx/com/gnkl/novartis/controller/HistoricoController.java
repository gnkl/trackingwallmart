package mx.com.gnkl.novartis.controller;

import java.io.IOException;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import java.util.Locale;
import mx.com.gnkl.novartis.bean.ObjectListVO;
import mx.com.gnkl.novartis.service.HistoricoService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@Controller
@RequestMapping(value = "/protected/historico")
public class HistoricoController {

    private static final String DEFAULT_PAGE_DISPLAYED_TO_USER = "0";

    @Autowired
    private HistoricoService historicoService;

    @Autowired
    private MessageSource messageSource;

    @Value("${grid.limit}")
    private int maxResults;
    
    @Value("${role.admin}")
    private String roleAdmin;
    
    @Value("${role.master}")
    private String roleMaster;    

    @Value("${role.cliente}")
    private String roleCliente;    

    @Value("${role.operador}")
    private String roleOperador;   
    
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView welcome() {
        return new ModelAndView("historico");
    }
            

    @RequestMapping(value = "/search/{name}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> search(@PathVariable("name") String name,
                                    @RequestParam(required = false, defaultValue = DEFAULT_PAGE_DISPLAYED_TO_USER) int page,
                                    Locale locale) {
        return search(name, page, locale, null);
    }

    @RequestMapping(value = "/searchbyidpaq/{idpaq}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> searchByIdPaq(@PathVariable("idpaq") Integer idPaq,
                                    @RequestParam(required = false, defaultValue = DEFAULT_PAGE_DISPLAYED_TO_USER) int page,
                                    Locale locale) {
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        ObjectListVO paqueteListVO = new ObjectListVO();
        if(page>0){
            page=page-1;
        }
        
        if(attr.getRequest().isUserInRole(roleCliente)){
            paqueteListVO = historicoService.findByIdPaqCliente(page, maxResults, idPaq);
        }else{
            paqueteListVO = historicoService.findByIdPaq(page, maxResults, idPaq);
        }        

        Object[] args = {idPaq};

        addSearchMessageToVO(paqueteListVO, locale, "message.search.for.active", args);

        return new ResponseEntity<ObjectListVO>(paqueteListVO, HttpStatus.OK);        
    }
    
    
    @ResponseBody
    @RequestMapping(value = "/getimage/{idhist}", method = RequestMethod.GET, produces = {MediaType.IMAGE_JPEG_VALUE, MediaType.IMAGE_PNG_VALUE, MediaType.APPLICATION_OCTET_STREAM_VALUE})
    public byte[] displayImage(@PathVariable("idhist") Integer id,
                                    Locale locale) throws IOException {
        
        return historicoService.getImageFromHistorico(id);    
    }    
    

    private ResponseEntity<?> search(String name, int page, Locale locale, String actionMessageKey) {
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        ObjectListVO paqueteListVO = new ObjectListVO();
        if(page>0){
            page=page-1;
        }
        
        paqueteListVO = historicoService.findByNameLike(page, maxResults, name);            

        if (!StringUtils.isEmpty(actionMessageKey)) {
            addActionMessageToVO(paqueteListVO, locale, actionMessageKey, null);
        }

        Object[] args = {name};

        addSearchMessageToVO(paqueteListVO, locale, "message.search.for.active", args);

        return new ResponseEntity<ObjectListVO>(paqueteListVO, HttpStatus.OK);
    }

    private ObjectListVO addActionMessageToVO(ObjectListVO contactListVO, Locale locale, String actionMessageKey, Object[] args) {
        if (StringUtils.isEmpty(actionMessageKey)) {
            return contactListVO;
        }

        contactListVO.setActionMessage(messageSource.getMessage(actionMessageKey, args, null, locale));

        return contactListVO;
    }

    private ObjectListVO addSearchMessageToVO(ObjectListVO contactListVO, Locale locale, String actionMessageKey, Object[] args) {
        if (StringUtils.isEmpty(actionMessageKey)) {
            return contactListVO;
        }

        contactListVO.setSearchMessage(messageSource.getMessage(actionMessageKey, args, null, locale));

        return contactListVO;
    }

}