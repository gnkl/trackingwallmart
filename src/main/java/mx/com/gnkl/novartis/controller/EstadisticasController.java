package mx.com.gnkl.novartis.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import mx.com.gnkl.novartis.bean.ObjectListVO;
import mx.com.gnkl.novartis.model.TbBitacora;
import mx.com.gnkl.novartis.model.TbBitacoraHistorico;
import mx.com.gnkl.novartis.repository.BitacoraDao;
import mx.com.gnkl.novartis.repository.EstadisticaDao;
import mx.com.gnkl.novartis.repository.PuntoDao;
import mx.com.gnkl.novartis.repository.jpa.BitacoraHistoricoRepository;
import mx.com.gnkl.novartis.repository.jpa.BitacoraRepository;
import mx.com.gnkl.novartis.repository.jpa.BitacoraStatusRepository;
import mx.com.gnkl.novartis.service.BitacoraService;
import mx.gnkl.process.file.ProcessExcelFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.ModelAndView;


@Controller
@RequestMapping(value = "/protected/statistics")
public class EstadisticasController {
    
    private static final String DEFAULT_PAGE_DISPLAYED_TO_USER = "0";
    private static final Integer JALOMA_ID=2;

    @Autowired
    private EstadisticaDao estadisticaDao;    

    @Autowired
    private BitacoraStatusRepository bitacoraStatusRepository;    

    @Autowired
    private BitacoraRepository bitacoraRepository;
    
    @Autowired
    private PuntoDao puntoDao;    
    
    @Autowired
    private BitacoraDao bitacoraDao;
    
    @Autowired
    private BitacoraService bitacoraService;
    
    @Autowired
    private BitacoraHistoricoRepository historicoRepository;
    
    @Value("${grid.limit}")
    private Integer limitGrid;  
    
//    @Value("${operador.path}")
//    private String pathOperador;
    
    @Value("${reporte.bitacora.path}")
    private String pathReporte;
    
    @Value("${role.cliente}")
    private String roleCliente;
    
    @Value("${grid.limit}")
    private int maxResults;    
    
    @Autowired
    private ProcessExcelFile processExcel;       
    

    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView welcome() {
        return new ModelAndView("statistics");
    }
    
    @ResponseBody
    @RequestMapping(value = "/counttotal", method = RequestMethod.GET, produces = "application/json")
    public Long countTotal(@RequestParam() Integer tipoEnvio, Locale locale) {
        List<Integer> listStatus = new ArrayList<Integer>();
        listStatus.add(30);
        return estadisticaDao.getTotalBitacoras(tipoEnvio, listStatus);
    }    

    @ResponseBody
    @RequestMapping(value = "/countbystatus", method = RequestMethod.GET, produces = "application/json")
    public List<Map<String, Object>> countByStatus(Locale locale, @RequestParam() Integer tipoEnvio) {
        List<Integer> listStatus = new ArrayList<Integer>();
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        if(attr.getRequest().isUserInRole(roleCliente)){
            switch(tipoEnvio){
                case 1:
                    listStatus = Arrays.asList(new Integer[]{7,8,9,10,11,29});
                    break;
                case 3:   
                    listStatus = Arrays.asList(new Integer[]{22,23,24,25,26,31});
                    break;
            }
            
            List<Map<String, Object>> result = estadisticaDao.getTotalByStatusIn(tipoEnvio, listStatus);
            Long totalStatus57 = 0L;
            for(Map<String,Object> item : result){
                Integer numStatus = (Integer) item.get("num_status");
                if(numStatus==5 || numStatus==7){
                    Long total = (Long) item.get("total");
                    totalStatus57 += total;
                }
            }

            List<Map<String, Object>> finalResult = new ArrayList<Map<String, Object>>();

            for(Map<String,Object> item : result){
                Integer numStatus = (Integer) item.get("num_status");
                if(numStatus!=7){
                    if(numStatus==5){
                        item.put("total", totalStatus57);
                        finalResult.add(item);
                    }else{
                        finalResult.add(item);   
                    }
                }
            }
            
            return finalResult;
        }else{
            if(tipoEnvio.equals(1)){
                listStatus.add(30);
            }else{
                listStatus.add(32);
            }
            return estadisticaDao.getTotalByNotInStatus(tipoEnvio, listStatus);
        }
    }
    
    
    @ResponseBody
    @RequestMapping(value = "/countByOperadorVehiculo", method = RequestMethod.GET, produces = "application/json")
    public Long countByOperadorVehiculo(@RequestParam() Integer idVehiculo, @RequestParam() Integer idOperador , Locale locale) {
        return estadisticaDao.getCountByOperadorVehiculo(idOperador, idVehiculo);
    }    

    @ResponseBody
    @RequestMapping(value = "/getByOperadorVehiculo", method = RequestMethod.GET, produces = "application/json")
    public List<TbBitacora> getByOperadorVehiculo(@RequestParam(required = false) Integer idVehiculo, @RequestParam(required = false) Integer idOperador , Locale locale) {
        if(idOperador!=null || idVehiculo!=null){
            return estadisticaDao.getListByOperadorVehiculo(idOperador, idVehiculo);
        }else{
            return new ArrayList<TbBitacora>();
        }
        
    }        
    
    @ResponseBody
    @RequestMapping(value = "/getBitacoraDetalle", method = RequestMethod.GET, produces = "application/json")
    public List<TbBitacora> getDetalleBitacoraByStatusEnvio(@RequestParam(required = false) Integer[] listStatus, @RequestParam(required = false) Integer tipoEnvio , Locale locale) {
        if(listStatus!=null && tipoEnvio!=null){
            List<Integer> list = Arrays.asList(listStatus);
            return bitacoraRepository.getAllBitacorasByStatusEnvio(list,tipoEnvio);
        }
        return new ArrayList<TbBitacora>();
    }    
    
    @ResponseBody
    @RequestMapping(value = "/getBitacoraDetalleReporte", method = RequestMethod.GET, produces = "application/json")
    public byte[] getDetalleBitacoraReporte(@RequestParam(required = false) Integer[] listStatus, @RequestParam(required = false) Integer tipoEnvio , @RequestParam(required = true) String filename, Locale locale) throws Exception {
        List<Integer> list = Arrays.asList(listStatus);
        List<TbBitacora> result = bitacoraRepository.getAllBitacorasByStatusEnvio(list,tipoEnvio);
        
        DateFormat format = new SimpleDateFormat("yyyyMMdd");
        DateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String fecha = format.format(new Date());
        
        Map<Integer, String> mapHeader = new HashMap<Integer,String>();
        mapHeader.put(0, "No. Tienda");
        mapHeader.put(1, "Operador");
        mapHeader.put(2, "Vehículo");
        mapHeader.put(3, "Estatus");
        mapHeader.put(4, "Fecha Inicio");
        mapHeader.put(5, "Fecha Termino");
        
        List<Map<Integer, Object>> listPrint = new ArrayList<Map<Integer, Object>>();
        for(TbBitacora bitacora: result){
            Map<Integer, Object> mapData = new HashMap<Integer, Object>();
            mapData.put(0, bitacora.getNombre());
            mapData.put(1, bitacora.getIdOperador().getNombre()+" "+bitacora.getIdOperador().getApellidoPat()+" "+bitacora.getIdOperador().getApellidoMat());
            mapData.put(2, bitacora.getIdVehiculo().getPlacas()+" "+" "+bitacora.getIdVehiculo().getMarca()+" "+bitacora.getIdVehiculo().getSubmarca());
            String status = "";
            if(bitacora.getIdStatus().getNumStatus()==7){
                if(tipoEnvio.equals(1)){
                    status = "RECOLECCIÓN COMPLETA FINALIZADA";
                }else{
                    status = "ENTREGA COMPLETA FINALIZADA";
                }
            }else{
                status=bitacora.getIdStatus().getDesStatus();
            }
            mapData.put(3, status);
            mapData.put(4,dateformat.format(bitacora.getFecha()));
            mapData.put(5,bitacora.getFechaFin()!=null?dateformat.format(bitacora.getFechaFin()):"");
            listPrint.add(mapData);
        }
        
        processExcel.writeCollectionMap(pathReporte+filename+fecha+".xlsx", "Detalle", listPrint, mapHeader);
        return getFileReporteHistorico(filename+fecha);
    }        
    
    //@Override
    public byte[] getFileReporteHistorico(String fileName) throws IOException {
            //pathOperador+fileName
            String fileZipName = String.valueOf(fileName + (new Date().getTime())) + ".zip";
            File zipfile = new File(pathReporte+fileZipName);
            // Create a buffer for reading the files
            byte[] buf = new byte[1024];
            // create the ZIP file
            ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zipfile));
                Path pathFile = Paths.get(pathReporte+fileName+".xlsx");
                File file = pathFile.toFile();
                FileInputStream in = new FileInputStream(file);
                // add ZIP entry to output stream
                out.putNextEntry(new ZipEntry(file.getName()));
                // transfer bytes from the file to the ZIP file
                int len;
                while ((len = in.read(buf)) > 0) {
                    out.write(buf, 0, len);
                }
                // complete the entry
                out.closeEntry();
                in.close();
            out.close();
            byte[] data = Files.readAllBytes(zipfile.toPath());
            return data;       
    }    
    
    
    @ResponseBody
    @RequestMapping(value = "/searchBitacoraDetalle", method = RequestMethod.GET, produces = "application/json")
    public  ResponseEntity<?> getDetalleBitacoraByStatusEnvio(
                                                            @RequestParam(required = false) String fechaInicio, 
                                                            @RequestParam(required = false) String fechaFin,
                                                            @RequestParam(required = false) Integer idOperador,
                                                            @RequestParam(required = false) Integer idEnvio,
                                                            Integer page,
                                                            Locale locale) {
        
        ObjectListVO<Map<String, Object>> bitacoraListVO = new ObjectListVO<Map<String, Object>>();
        if(page>0){
            page=page-1;
        }
        if((fechaInicio!=null && fechaFin!=null ) || idOperador!=null || idEnvio!=null ){
            List<Map<String,Object>> result = bitacoraService.searchBitacoraByParameters(fechaInicio, fechaFin, idOperador, idEnvio, page,maxResults);
            Long totalData = bitacoraService.getCountBitacoraByParameters(fechaInicio, fechaFin, idOperador, idEnvio);
            bitacoraListVO.setData(result);
            bitacoraListVO.setTotalData(totalData);
        }else{
            bitacoraListVO.setData(new ArrayList<Map<String, Object>>());
            bitacoraListVO.setTotalData(0);            
        }
        
        return new ResponseEntity<ObjectListVO<Map<String,Object>>>(bitacoraListVO, HttpStatus.OK);
    }      
    
    @ResponseBody
    @RequestMapping(value = "/reporteBitacoraDetalle1", method = RequestMethod.GET, produces = "application/json")
    public byte[] getDetalleBitacoraReporte1(
                                            @RequestParam(required = false) String fechaInicio, 
                                            @RequestParam(required = false) String fechaFin,
                                            @RequestParam(required = false) Integer idOperador,
                                            @RequestParam(required = false) Integer idEnvio,
                                            Integer page,
                                            Locale locale) throws Exception {
        
        List<Map<String,Object>> result = bitacoraDao.getListBitacoraWithHistorico(fechaInicio,fechaFin,idOperador, idEnvio);
        
        DateFormat format = new SimpleDateFormat("yyyyMMdd");
        DateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String fecha = format.format(new Date());
        
        Map<Integer, String> mapHeader = new HashMap<Integer,String>();
        mapHeader.put(0, "No. Tienda");
        mapHeader.put(1, "Operador");
        mapHeader.put(2, "Vehículo");
        mapHeader.put(3, "Estatus");
        mapHeader.put(4, "Fecha Inicio");
        mapHeader.put(5, "Fecha Termino");
        mapHeader.put(6, "Tipo");
        mapHeader.put(7, "Estatus Bitácora");
        mapHeader.put(8, "Fecha Estatus Bitácora");
        mapHeader.put(9, "Observaciones");
        
        List<Map<Integer, Object>> listPrint = new ArrayList<Map<Integer, Object>>();
        for(Map<String, Object> map: result){
            Map<Integer, Object> mapData = new HashMap<Integer, Object>();
            mapData.put(0, map.get("nombre"));
            mapData.put(1, map.get("operador"));
            mapData.put(2, map.get("vehiculo"));
            mapData.put(3, map.get("des_status"));
            
            //Date fecha1 = dateformat.parse((String) map.get("fecha"));
            mapData.put(4, map.get("fecha"));
            //Date fecha2 = dateformat.parse((String) map.get("fecha_fin"));
            mapData.put(5, map.get("fecha_fin"));
            mapData.put(6, map.get("des_envio"));
            
            mapData.put(7, map.get("estatus_historico"));
            mapData.put(8, map.get("fecha_h"));
            mapData.put(9, map.get("observaciones"));

            listPrint.add(mapData);
        }
        String fileName = "Detalle_FileName_123346";
        processExcel.writeCollectionMap(pathReporte+fileName+fecha+".xlsx", "Detalle", listPrint, mapHeader);
        return getFileReporteHistorico(fileName+fecha);
    }            


    @ResponseBody
    @RequestMapping(value = "/reporteBitacoraDetalle2", method = RequestMethod.GET, produces = "application/json")
    public byte[] getDetalleBitacoraReporte2(
                                            @RequestParam(required = false) String fechaInicio, 
                                            @RequestParam(required = false) String fechaFin,
                                            @RequestParam(required = false) Integer idOperador,
                                            @RequestParam(required = false) Integer idEnvio,
                                            Integer page,
                                            Locale locale) throws Exception {
        
        DateFormat format = new SimpleDateFormat("yyyyMMdd");
        String fecha = format.format(new Date());
        
        List<Map<String,Object>> result = bitacoraDao.getAllListBitacoraTiempoEstatus(fechaInicio,fechaFin,idOperador, idEnvio);

        Map<Integer, String> mapHeader = new HashMap<Integer,String>();
        mapHeader.put(0, "No. Tienda");
        mapHeader.put(1, "Operador");
        mapHeader.put(2, "Vehículo");
        mapHeader.put(3, "Estatus");
        mapHeader.put(4, "Fecha Inicio");
        mapHeader.put(5, "Fecha Termino");
        mapHeader.put(6, "Tipo");
        mapHeader.put(7, "Estatus Bitácora");
        mapHeader.put(8, "Fecha Estatus Bitácora");
        mapHeader.put(9, "Tiempo transcurrido");
        mapHeader.put(10, "Observaciones");

        List<Map<Integer, Object>> listPrint = new ArrayList<Map<Integer, Object>>();
        
        for(Map<String,Object> map : result){         
            List<Map<String,Object>> historicoList =bitacoraDao.getAllHistoricoByBitacoraId((Integer) map.get("id"), (Integer) map.get("id_envio"));
            //for(Map<String,Object> mapHistorico : historicoList){
            for(int i=0;i<historicoList.size();i++){
                
                Map<Integer, Object> mapData = new HashMap<Integer, Object>();
                
                Map<String,Object> mapHistorico = historicoList.get(i);
                
                mapData.put(0, map.get("nombre"));
                mapData.put(1, map.get("operador"));
                mapData.put(2, map.get("vehiculo"));
                mapData.put(3, map.get("des_status"));
                mapData.put(4, map.get("fecha"));
                mapData.put(5, map.get("fecha_fin"));
                mapData.put(6, map.get("des_envio"));            
                mapData.put(7, mapHistorico.get("des_status"));
                mapData.put(8, mapHistorico.get("fecha"));
                String tiempoCell = (String) mapHistorico.get("tiempo"); 
                System.out.println("tiempo cell: "+tiempoCell);
                mapData.put(9, tiempoCell!=null?tiempoCell:"");
                mapData.put(10, mapHistorico.get("observaciones"));         
                listPrint.add(mapData);
            }    
        }
        String fileName = "DetalleFileNameTiemposXHistoricoEstatus";
        processExcel.writeCollectionMap(pathReporte+fileName+fecha+".xlsx", "Detalle", listPrint, mapHeader);
        return getFileReporteHistorico(fileName+fecha);        
    }
}