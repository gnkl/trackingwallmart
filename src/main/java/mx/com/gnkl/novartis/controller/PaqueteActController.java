package mx.com.gnkl.novartis.controller;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import mx.com.gnkl.novartis.service.PaqueteService;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import javax.annotation.Resource;
import mx.com.gnkl.novartis.bean.ObjectBeanVO;
import mx.com.gnkl.novartis.bean.ObjectListVO;
import mx.com.gnkl.novartis.bean.SecurityUserBean;
import mx.com.gnkl.novartis.component.ApplicationMailer;
import mx.com.gnkl.novartis.model.TbBitacora;
import mx.com.gnkl.novartis.model.TbBitacoraPaquete;
import mx.com.gnkl.novartis.model.TbDomainConfig;
import mx.com.gnkl.novartis.model.TbHistpaq;
import mx.com.gnkl.novartis.model.TbPaquetes;
import mx.com.gnkl.novartis.model.TbStatus;
import mx.com.gnkl.novartis.repository.jpa.DomainConfigRepository;
import mx.com.gnkl.novartis.service.BitacoraService;
import mx.com.gnkl.novartis.service.HistoricoService;
import org.apache.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;

@Controller
@RequestMapping(value = "/protected/paqueteact")
public class PaqueteActController {
    
    private static final Logger logger = Logger.getLogger(PaqueteActController.class);

    private static final String DEFAULT_PAGE_DISPLAYED_TO_USER = "0";

    @Autowired
    private PaqueteService paqueteService;

    @Autowired
    private MessageSource messageSource;

    @Autowired
    private HistoricoService historicoService;

    @Autowired
    private BitacoraService bitacoraService;
    
    @Autowired
    private DomainConfigRepository configRepository;

    @Resource
    private ApplicationMailer mailer;     
    
    @Value("${grid.limit}")
    private int maxResults;
    
    @Value("${role.admin}")
    private String roleAdmin;
    
    @Value("${role.master}")
    private String roleMaster;    

    @Value("${role.cliente}")
    private String roleCliente;

    @Value("${role.operador}")
    private String roleOperador;
    
//    @Value("${gnk.mails}")
//    private String mails;    

//    @Value("${gnk.clientmails}")
//    private String clientmails;
    
    @Value("${gnkl.urlsist}")
    private String urlproy;
    
    @RequestMapping(method = RequestMethod.GET)
    public ModelAndView welcome() {
        return new ModelAndView("modifystatus");
    }
    
    
    @RequestMapping(value = "/update/{id}", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> update(@PathVariable("id") int paqueteId,
                                    @RequestBody TbPaquetes paquete,
                                    @RequestParam(required = false) String searchFor,
                                    @RequestParam(required = false, defaultValue = DEFAULT_PAGE_DISPLAYED_TO_USER) int page,
                                    Locale locale) {
        if (paqueteId != paquete.getIdPaq()) {
            return new ResponseEntity<String>("Bad Request", HttpStatus.BAD_REQUEST);
        }

        paqueteService.update(paquete);

//        if (isSearchActivated(searchFor)) {
//            return search(searchFor, page, locale, "message.update.success");
//        }

        return createListAllResponse(page, locale, "message.update.success");
    }
    

    @RequestMapping(value = "/updatestatus/{idpaq}", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> updateStatus(@PathVariable("idpaq") int idPaq,
                                    @RequestParam("observaciones") String observaciones,
                                    @RequestParam("kilometraje") Integer kilometraje,
                                    @RequestParam(value="file",required=false) MultipartFile file) {
        TbPaquetes paqueteFound = paqueteService.getPaqueteByIdPaq(idPaq);
        //2 es para jaloma
        if(paqueteFound.getIdEmpresa()!=null && paqueteFound.getIdEmpresa()==2 && paqueteFound.getIdStatus().getIdEnvio().getIdEnvio()==1){
            Integer currentIdStatus=paqueteFound.getIdStatus().getNumStatus();
            TbPaquetes paquete = paqueteService.updateEstatusJalomaCrossDock(paqueteFound,currentIdStatus,  observaciones, file, kilometraje);
            if(paquete!=null && currentIdStatus.equals(paquete.getIdStatus().getNumStatus())){
                ObjectBeanVO histVO = new ObjectBeanVO();
                histVO.setData(null);
                histVO.setSearchMessage("Los estatus se han completado");
                return new ResponseEntity<ObjectBeanVO>(new ObjectBeanVO(), HttpStatus.OK);
            }else if(paquete!=null){
                TbHistpaq result = historicoService.getTbHistPaqByIdPaqIdStatus(idPaq, paquete.getIdStatus().getIdStatus());
                ObjectBeanVO histVO = new ObjectBeanVO();
                histVO.setData(result);
                //if(!paqueteFound.getIdStatus().getIdStatus().equals(paquete.getIdStatus().getIdStatus())){
                SendMailJaloma(paquete, result, observaciones);
                //}
                return new ResponseEntity<ObjectBeanVO>(histVO, HttpStatus.OK);                                        
            }else{
                return new ResponseEntity<ObjectBeanVO>(new ObjectBeanVO(), HttpStatus.OK);
            }
        }else if( paqueteFound.getIdEmpresa()!=null && paqueteFound.getIdEmpresa()==2 && paqueteFound.getIdStatus().getIdEnvio().getIdEnvio()==3){
            TbPaquetes paquete = paqueteService.updateEstatusJaloma(idPaq, observaciones, file, kilometraje);
//            TbHistpaq result =historicoService.saveHistorial(paquete, observaciones, file);
            TbHistpaq result = historicoService.getTbHistPaqByIdPaqIdStatus(idPaq, paquete.getIdStatus().getIdStatus());
            ObjectBeanVO histVO = new ObjectBeanVO();
            histVO.setData(result);
            //if(!paqueteFound.getIdStatus().getIdStatus().equals(paquete.getIdStatus().getIdStatus())){
            SendMailJaloma(paquete, result, observaciones);
            //}
            return new ResponseEntity<ObjectBeanVO>(histVO, HttpStatus.OK);                        
        }else{
            TbPaquetes paquete = paqueteService.updateEstatus(idPaq);
            TbHistpaq result =historicoService.saveHistorial(paquete, observaciones, file);
            ObjectBeanVO histVO = new ObjectBeanVO();
            histVO.setData(result);
            SecurityUserBean user = (SecurityUserBean)SecurityContextHolder.getContext().getAuthentication().getPrincipal(); 
            TbDomainConfig config = configRepository.findOne("gnk.mails");
            mailer.sendTemplateMail(config.getConfigValue(), "Actualización de estatus: "+paquete.getIdStatus().getDesStatus(), "Observaciones: "+result.getObserHist(), "","");
            return new ResponseEntity<ObjectBeanVO>(histVO, HttpStatus.OK);            
        }        
    }

    @RequestMapping(value = "/deleteimages/{idhist}", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> deleteImagesByIdHist(@PathVariable("idhist") int idHist) {
        ObjectBeanVO histVO = new ObjectBeanVO();
        try{
            historicoService.deleteArchivosByIdHist(idHist);
            histVO.setActionMessage("OK");
        }catch(IOException ex){
            histVO.setActionMessage("Error enel borrado de archivos historicos");
            logger.error(ex);
        }
        return new ResponseEntity<ObjectBeanVO>(histVO, HttpStatus.OK);
    }    
    
    
    @RequestMapping(value = "/addimage/{idhist}", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> addImageHist(@PathVariable("idhist") int idhist,
                                    @RequestParam("file") MultipartFile file){
        try{
            TbHistpaq result = historicoService.saveHistorialImagen(idhist, file);
            ObjectBeanVO histVO = new ObjectBeanVO();
            histVO.setData(result);
            return new ResponseEntity<ObjectBeanVO>(histVO, HttpStatus.OK);            
        }catch(IOException ex ){
            logger.error(ex);
            ObjectBeanVO histVO = new ObjectBeanVO();
            histVO.setActionMessage(ex.getLocalizedMessage());
            return new ResponseEntity<ObjectBeanVO>(histVO, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }    
    
//    @RequestMapping(value = "/search/{name}", method = RequestMethod.GET, produces = "application/json")
//    public ResponseEntity<?> search(@PathVariable("name") String name,
//                                    @RequestParam(required = false, defaultValue = DEFAULT_PAGE_DISPLAYED_TO_USER) int page,
//                                    Locale locale) {        
//        
//        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
//        ObjectListVO<TbPaquetes> paqueteListVO = new ObjectListVO<TbPaquetes>();
//        if (attr.getRequest().isUserInRole(roleOperador)) {
//            SecurityUserBean user = (SecurityUserBean) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//            paqueteListVO = paqueteService.getPaquetesByNoEmbarque(name, user.getIdUsu(), page);
//        } else if (attr.getRequest().isUserInRole(roleCliente)) {
//            paqueteListVO = paqueteService.getPaquetesByNoEmbarqueClienteJaloma(name, page);
//        } else {
//            paqueteListVO = paqueteService.getPaquetesByNoEmbarque(name, page);
//        }
//        return new ResponseEntity<ObjectListVO>(paqueteListVO, HttpStatus.OK);
//    }


    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/notracking", method = RequestMethod.GET, produces = "application/json")
    public List<String> searchFolio(@RequestParam(required = true) String name, Locale locale) {
        List<String> noTrackingList = new ArrayList<String>();
        noTrackingList.addAll(paqueteService.getNoTrackingList(name));
        return noTrackingList;
    }     
    
    
    @RequestMapping(value = "/search/", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> searchParam(@RequestParam("name") String name,
                                    @RequestParam(required = false, defaultValue = DEFAULT_PAGE_DISPLAYED_TO_USER) int page,
                                    Locale locale) {        
        
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        ObjectListVO<TbPaquetes> paqueteListVO = new ObjectListVO<TbPaquetes>();
        if (attr.getRequest().isUserInRole(roleOperador)) {
            SecurityUserBean user = (SecurityUserBean) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            paqueteListVO = paqueteService.getPaquetesByNoEmbarque(name, user.getIdUsu(), page);
        } else if (attr.getRequest().isUserInRole(roleCliente)) {
            if(name!=null && !name.isEmpty()){
                paqueteListVO = paqueteService.getPaquetesByNoEmbarqueClienteJaloma(name, page);
            }
        } else {
            paqueteListVO = paqueteService.getPaquetesByNoEmbarque(name, page);
        }
        return new ResponseEntity<ObjectListVO>(paqueteListVO, HttpStatus.OK);
    }
    
    @RequestMapping(value = "/searchlist/{idpaq}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> searchIdPaq(@PathVariable("idpaq") Integer idPaq,
                                    @RequestParam(required = false, defaultValue = DEFAULT_PAGE_DISPLAYED_TO_USER) int page,
                                    Locale locale) {
        
            ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
            ObjectBeanVO paqueteVO = new ObjectBeanVO();
//            if(attr.getRequest().isUserInRole(roleOperador)){
//                SecurityUserBean user = (SecurityUserBean)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//                paqueteVO = paqueteService.getPaqueteByNoEmbarque(name, user.getIdUsu());            
//            }else{
//                paqueteVO = paqueteService.getPaqueteByNoEmbarque(name);
//            }
            TbPaquetes paquete = paqueteService.getPaqueteByIdPaq(idPaq);
            paqueteVO = new ObjectBeanVO(1, 1, paquete);
            return new ResponseEntity<ObjectBeanVO>(paqueteVO, HttpStatus.OK);
            
    }    
    
   
    @ResponseBody
    @RequestMapping(value = "/getimage/{id}", method = RequestMethod.GET, produces = {MediaType.IMAGE_JPEG_VALUE, MediaType.IMAGE_PNG_VALUE, MediaType.APPLICATION_OCTET_STREAM_VALUE})
    public byte[] displayImage(@PathVariable("id") Integer id,
                                    Locale locale) throws IOException {
        
        return paqueteService.getImageOperador(id);
    }   
    
    
    @ResponseBody
    @RequestMapping(value = "/getnextstatus", method = RequestMethod.GET,produces = "application/json")
    public TbStatus getNextStatusByIdPaq(@RequestParam("idpaq") Integer idpaq, Locale locale) throws IOException {
        TbPaquetes paquete = paqueteService.getPaqueteByIdPaq(idpaq);
        return paqueteService.getNextStatusJaloma(paquete);
    }    
    
    @ResponseBody
    @RequestMapping(value = "/getBitacoraStatus", method = RequestMethod.GET,produces = "application/json")
    public TbBitacora getBitacoraByIdPaquete(@RequestParam("idpaq") Integer idpaq, Locale locale) throws IOException {
        TbPaquetes paquete = paqueteService.getPaqueteByIdPaq(idpaq);
        TbBitacoraPaquete bitacoraPaquete =  bitacoraService.getPaqueteBitacora(paquete);
        if(bitacoraPaquete!=null)
            return bitacoraPaquete.getIdBitacora();
        else
            return new TbBitacora();
    }    
    
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/searchByDate", method = RequestMethod.GET, produces = "application/json")
    public List<Map<String,Object>> searchPaquetesByDate(@RequestParam(required = true) String date1, @RequestParam(required = true) String date2, Locale locale) {
        try {
            DateFormat format = new SimpleDateFormat("dd/mm/yyyy");
            Date fecha1 = format.parse(date1);
            Date fecha2 = format.parse(date2);
            return paqueteService.getListNoEmbarqueByDate(fecha1, fecha2);
        } catch (ParseException ex) {
            java.util.logging.Logger.getLogger(PaqueteActController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new ArrayList<Map<String,Object>>();
    }     

    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    @RequestMapping(value = "/searchByFechaRecepcionEntrega", method = RequestMethod.GET, produces = "application/json")
    public List<Map<String,Object>> searchByFechaRecepcionEntrega(@RequestParam(required = true) String fecha, @RequestParam(required = true) Integer tipo, Locale locale) {
        try {
            DateFormat format = new SimpleDateFormat("dd/mm/yyyy");
            Date fecha1 = format.parse(fecha);
            return paqueteService.getListNoEmbarqueByDate(fecha1, tipo);
        } catch (ParseException ex) {
            java.util.logging.Logger.getLogger(PaqueteActController.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new ArrayList<Map<String,Object>>();
    }
    
    private void SendMailJaloma(TbPaquetes paquete, TbHistpaq result, String observaciones){
        SecurityUserBean user = (SecurityUserBean)SecurityContextHolder.getContext().getAuthentication().getPrincipal(); 
        StringBuilder sb = new StringBuilder();
        sb.append("<br>Se ha actualizado el estatus del tracking por: ").append(paquete.getIdStatus().getDesStatus()).append("<br>");
        sb.append("<br>No de tracking: ").append(paquete.getNoEmbarque()).append("<br>");
        sb.append("<br>Observaciones: ").append(observaciones).append("<br>");
        sb.append("<br>Ver estatus: ").append(urlproy).append("/protected/paqueteact").append("<br>");

        if(result.getIdStatus()!=null && result.getIdStatus().getNumStatus()>1){
            TbDomainConfig config = configRepository.findOne("gnk.clientmails");
            TbDomainConfig config1 = configRepository.findOne("gnk.mails");
            mailer.sendTemplateMail(config.getConfigValue(), "Actualización de estatus: "+paquete.getIdStatus().getDesStatus(), sb.toString(), config1.getConfigValue(),"");   
        }else{
            TbDomainConfig config = configRepository.findOne("gnk.mails");
            mailer.sendTemplateMail(config.getConfigValue(), "Actualización de estatus: "+paquete.getIdStatus().getDesStatus(), sb.toString(),"","");   
        }        
    }
    
    private ResponseEntity<?> search(String name, int page, Locale locale, String actionMessageKey) {
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        ObjectBeanVO paqueteVO = new ObjectBeanVO();
        if(attr.getRequest().isUserInRole(roleOperador)){
            SecurityUserBean user = (SecurityUserBean)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            paqueteVO = paqueteService.getPaqueteByNoEmbarque(name, user.getIdUsu());            
        }else{
            paqueteVO = paqueteService.getPaqueteByNoEmbarque(name);
        }
        return new ResponseEntity<ObjectBeanVO>(paqueteVO, HttpStatus.OK);
    }    


    private ResponseEntity<?> createListAllResponse(int page, Locale locale, String messageKey) {
        System.out.println("page: "+page);
        ObjectListVO<TbPaquetes> paqueteListVO = new ObjectListVO<TbPaquetes>();
        if(hasRole("ROLE_MASTER")){
            paqueteListVO = paqueteService.findAll(page, maxResults);
        }else{
            User userlogged = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            paqueteListVO = paqueteService.findAll(userlogged.getUsername(), page, maxResults);
        }
        //ObjectListVO<TbPaquetes> paqueteListVO = paqueteService.findAll(page, maxResults);
        paqueteListVO.setTotalData(paqueteService.getTotalPaquetes());
        addActionMessageToVO(paqueteListVO, locale, messageKey, null);

        return new ResponseEntity<ObjectListVO<TbPaquetes>>(paqueteListVO, HttpStatus.OK);
    }

    private ObjectListVO addActionMessageToVO(ObjectListVO contactListVO, Locale locale, String actionMessageKey, Object[] args) {
        if (StringUtils.isEmpty(actionMessageKey)) {
            return contactListVO;
        }

        contactListVO.setActionMessage(messageSource.getMessage(actionMessageKey, args, null, locale));

        return contactListVO;
    }

    private ObjectListVO addSearchMessageToVO(ObjectListVO contactListVO, Locale locale, String actionMessageKey, Object[] args) {
        if (StringUtils.isEmpty(actionMessageKey)) {
            return contactListVO;
        }

        contactListVO.setSearchMessage(messageSource.getMessage(actionMessageKey, args, null, locale));

        return contactListVO;
    }

    private boolean isSearchActivated(String searchFor) {
        return !StringUtils.isEmpty(searchFor);
    }
    
    private boolean hasRole(String role) {
        Collection<GrantedAuthority> authorities = (Collection<GrantedAuthority>) SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        boolean hasRole = false;
        for (GrantedAuthority authority : authorities) {
            hasRole = authority.getAuthority().equals(role);
            if (hasRole) {
                break;
            }
        }
        return hasRole;
    }    
}