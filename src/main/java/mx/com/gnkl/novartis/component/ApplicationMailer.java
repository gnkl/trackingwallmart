/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.com.gnkl.novartis.component;

import freemarker.template.Configuration;
import freemarker.template.TemplateException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.Resource;
import javax.mail.internet.MimeMessage;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailSender;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.scheduling.annotation.Async;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
/**
 *
 * @author jmejia
 */

@Component
public class ApplicationMailer {
    @Resource
    private MailSender mailSender;

    @Autowired
    private Configuration configuration;
    
	
    @Autowired
    @Qualifier("mailSender")
    private JavaMailSender javaMailSender;    
    
//    @Value("${gnk.mails}")
//    private String mails;

    @Value("${freemarker.template}")
    private String freemarkerTemplate;
    
//    @Value("${gnk.bkpmail}")
//    private String bkpmail;

//    @Value("${gnk.ccomail}")
//    private String ccomail;    
    /**
     * This method will send compose and send the message 
     * */
    @Async(value="myExecutor")
    public void sendMail(String to, String subject, String body, String mails) {
        SimpleMailMessage message = new SimpleMailMessage();
        if(to!=null && !to.isEmpty() && isValidEmailAddress(to)){
            message.setTo(to);
            message.setCc(mails);
            message.setSubject(subject);
            message.setText(body);
            mailSender.send(message);            
        }
    }
    @Async(value="myExecutor")
    public void sendTemplateMail(final String to, final String subject, String body, final String ccmails, final String ccomails){
        Date today = new Date();
        SimpleDateFormat dt1 = new SimpleDateFormat("dd/MM/yyyy");
        //System.out.println(dt1.format(today));
        
        final Map model = new HashMap();
        model.put("name", "Tim");
        model.put("subject", subject);
        model.put("body", body);
        model.put("fecha", dt1.format(today));


        MimeMessagePreparator preparator;
        try {
            preparator = new MimeMessagePreparator() {
                String messageText = FreeMarkerTemplateUtils.processTemplateIntoString(configuration.getTemplate(freemarkerTemplate), model);
                
                @Override
                public void prepare(MimeMessage message) throws Exception {
                    MimeMessageHelper helper = new MimeMessageHelper(message, true, "UTF-8");
                    if(to!=null && to.contains(",")){
                        String[] tomail = to.split(",");
                        for(String mail : tomail){
                            helper.addTo(mail);
                        }                        
                    }else if(to!=null && isValidEmailAddress(to)){
                        helper.addTo(to);
                    }
                    
                    if(ccmails!=null && ccmails.contains(",")){
                        String[] ccmail = ccmails.split(",");
                        for(String mail : ccmail){
                            helper.addCc(mail);
                        }
                    }else if(ccmails!=null && isValidEmailAddress(ccmails)){
                        helper.addCc(ccmails);
                    }
                    
                    if(ccomails!=null && ccomails.contains(",")){
                        String[] ccomail = ccomails.split(",");
                        for(String mail : ccomail){
                            helper.addBcc(mail);
                        }                        
                    }else if(ccomails!=null && isValidEmailAddress(ccomails)){
                        helper.addBcc(ccomails);
                    }
                    helper.setSubject(subject);
                    helper.setText(messageText, true);
                    //helper.addInline("myLogo", new ClassPathResource("classpath*:/resources/img/GNKL_Small.JPG"));
                }
            };

            this.javaMailSender.send(preparator);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

//    @Async(value="myExecutor")
//    public void sendTemplateMail(final String to, final String subject, String body, final String mails, final String clientmails){
//        Date today = new Date();
//        SimpleDateFormat dt1 = new SimpleDateFormat("dd/MM/yyyy");
//        //System.out.println(dt1.format(today));
//        
//        final Map model = new HashMap();
//        model.put("name", "Tim");
//        model.put("subject", subject);
//        model.put("body", body);
//        model.put("fecha", dt1.format(today));
//
//
//        MimeMessagePreparator preparator;
//        try {
//            preparator = new MimeMessagePreparator() {
//                String messageText = FreeMarkerTemplateUtils.processTemplateIntoString(configuration.getTemplate(freemarkerTemplate), model);
//                
//                @Override
//                public void prepare(MimeMessage message) throws Exception {
//                    MimeMessageHelper helper = new MimeMessageHelper(message, true);
//                    if(isValidEmailAddress(to)){
//                        helper.addTo(to);
//                    }else{
//                        helper.addTo(bkpmail);        
//                    }
//                    
//                    if(clientmails.contains(",")){
//                        String[] tomail = mails.split(",");
//                        for(String mail : tomail){
//                            helper.addCc(mail);
//                        }                        
//                    }
//                    
//                    if(mails.contains(",")){
//                        String[] ccmail = mails.split(",");
//                        for(String mail : ccmail){
//                            helper.addCc(mail);
//                        }
//                    }
//                    if(ccomail!=null && !ccomail.isEmpty()){
//                        helper.addBcc(ccomail);
//                    }
//                    helper.setSubject(subject);
//                    helper.setText(messageText, true);
//                    
//                    //helper.addInline("myLogo", new ClassPathResource("classpath*:/resources/img/GNKL_Small.JPG"));
//                }
//            };
//
//            this.javaMailSender.send(preparator);
//
//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
//
//    }    
    
    public boolean isValidEmailAddress(String email) {
           String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
           java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
           java.util.regex.Matcher m = p.matcher(email);
           return m.matches();
    }    
}
