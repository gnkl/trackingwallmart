<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<div class="container" ng-app="nvd3chart"  ng-controller="GenericChartCtrl as chartctrl">                   
    <!--<nvd3 options="optionsline" data="dataline"></nvd3>-->
    <!--<div style="width:50%;display: inline-block; float:left;">-->
        <nvd3 options="optionsbarTConfchart" data="databarterrestrechart"></nvd3>
    <!--</div>-->
    <!--<div style="width:50%; display: inline-block; float:left;">-->
        <nvd3 options="optionsbarAConfchart" data="databaraereochart"></nvd3>
    <!--</div>-->
    
</div>        
<script type="text/javascript" src="${contextPath}/resources/js/pages/chartpaquete.js" /></script>