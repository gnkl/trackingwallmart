<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<div ng-app="paqueteact"  ng-controller="PaqueteActCtrl as paqact">                   
    <h4>
        <div ng-class="{'': state == 'list', 'none': state != 'list'}">
            <p class="text-center">
                <spring:message code="message.total.records.found"/>:&nbsp;{{page.totalContacts}}
            </p>
        </div>
    </h4>
            <!--ng-if="searchFormStatus===true"-->
    <div ng-show="searchFormStatus">
        <div class="panel panel-info" style="width: 100% !important;">
        <div class="panel-heading collapsed" data-toggle="collapse" data-target="#collapse1">
            <h3 class="panel-title">Busqueda No. Tracking por fecha</h3>
            <br>
        </div>
        <div id="collapse1" class="collapse">
        <div class="modal-body">
            <div class="col-md-6">
                <label>* Fecha Recepcion:</label>                            
                <div class="row">
                    <div class="col-md-10">
                        <p class="input-group " style="margin-top: 32px;">
                            <input type="text" class="form-control" uib-datepicker-popup="{{format}}" ng-model="fechaRecepcionConsulta" is-open="popup3.opened" datepicker-options="dateOptions" ng-required="true" name="fecreceppaq" close-text="Close" ng-readonly="true"/>
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-default" ng-click="open3()"><i class="glyphicon glyphicon-calendar"></i></button>
                                <input class="btn btn-primary" value="Ir" type="submit" data-dismiss="modal" ng-click="searchPaquetes(fechaRecepcionConsulta,1);">
                            </span>
                        </p>
                    </div>                  
                </div>
            </div>                                
            <div class="col-md-6">
                <label>* Fecha de entrega:</label>
                <div class="row">
                    <div class="col-md-10">
                        <p class="input-group" style="margin-top: 32px;">
                            <input type="text" class="form-control" uib-datepicker-popup="{{format}}" ng-model="fechaEntregaConsulta" is-open="popup4.opened" datepicker-options="{minDate: fechaRecPaq1}" ng-required="true" close-text="Close" alt-input-formats="altInputFormats" name="fecenrtregapaq" ng-readonly="true"/>
                            <span class="input-group-btn">
                              <button type="button" class="btn btn-default" ng-click="open4()"><i class="glyphicon glyphicon-calendar"></i></button>
                              <input class="btn btn-primary" value="Ir" type="submit" data-dismiss="modal" ng-click="searchPaquetes(fechaEntregaConsulta,2);">
                            </span>
                        </p>
                    </div>
                </div>
            </div>
            <br>
            <div class="col-md-6">
                <label>* Fecha inicial:</label>                            
                <div class="row">
                    <div class="col-md-10">
                        <p class="input-group " style="margin-top: 32px;">
                            <input type="text" class="form-control" uib-datepicker-popup="{{format}}" ng-model="fechaInicio" is-open="popup2.opened" datepicker-options="dateOptions" ng-required="true" name="fechaInicio" close-text="Close" ng-readonly="true"/>
                            <span class="input-group-btn">
                                <button type="button" class="btn btn-default" ng-click="open2()"><i class="glyphicon glyphicon-calendar"></i></button>
                            </span>
                        </p>
                    </div>
                    <div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationError &amp;&amp; updatePaqueteForm.fecreceppaq.$error.required">
                        <spring:message code="required"></spring:message>
                    </div>                                
                </div>
            </div>                                
            <div class="col-md-6">
                <label>* Fecha final:</label>
                <div class="row">
                    <div class="col-md-10">
                        <p class="input-group" style="margin-top: 32px;">
                            <input type="text" class="form-control" uib-datepicker-popup="{{format}}" ng-model="fechaFin" is-open="popup3.opened" datepicker-options="{minDate: fechaInicio}" ng-required="true" close-text="Close" alt-input-formats="altInputFormats" name="fechaFin" ng-readonly="true"/>
                            <span class="input-group-btn">
                              <button type="button" class="btn btn-default" ng-click="open3()"><i class="glyphicon glyphicon-calendar"></i></button>
                            </span>
                        </p>
                    </div>
                    <div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationError &amp;&amp; newPaqueteForm.fecenrtregapaq.$error.required">
                        <spring:message code="required"></spring:message>
                    </div>
                </div>                                
            </div>        
            <div class="modal-footer">
                <input class="btn btn-primary" ng-click="searchPaquetesPeriod();" value="<spring:message code="search"></spring:message>" type="submit" data-dismiss="modal">
                <button class="btn btn-default" data-dismiss="modal" ng-click="clearSearchPaquetes();" aria-hidden="true">
                    <spring:message code="cancel"></spring:message>
                </button>
            </div>
                
            <div class="modal-body" style="height:200px;overflow-y: scroll;">
                <table class="table table-striped table-bordered" st-table="noTrackingLeadCollection" st-pipe="serverPeriod">
                    <thead>
                        <tr>
                           <th>No. Tracking</th>
                           <th>Estatus</th>
                        </tr>                                 
                        <tr>
                           <th><input st-search="no_embarque"/></th>
                        </tr>                        
                    </thead>            
                    <tbody>
                        <tr data-toggle="modal" data-target="#modal-source" ng-repeat="source in noTrackingLeadCollection">                    
                            <td>
                                <a role="button"
                                   title="Ver detalle"
                                   data-toggle="modal" ng-click="selectNoEmbarque(source.no_embarque);">
                                   {{source.no_embarque}}
                                </a>                                     
                            </td>
                            <td>{{source.des_status}}</td>                            
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="10">
                                <div class="pull-right" st-pagination="" st-items-by-page="itemsByPage" st-displayed-pages="3"></div>
                            </td>
                        </tr>
                    </tfoot>
                </table>                
            </div>
        </div>
        </div>
        </div>
        
        <div class="modal-body">
            <form id="searchPaqueteActForm" name="searchPaqueteActForm" novalidate>
                <label><spring:message code="search.for.tracking"/></label>
                <div>
                    <!--<input type="text" class="form-control" autofocus required ng-model="searchNoEmbarque" name="searchFor" placeholder="<spring:message code='contact'/>&nbsp;<spring:message code='contacts.name'/> " ng-change="searchPaqueteAct(searchPaqueteActForm, false);"/>-->
                    <!--<input type="text" class="form-control" autofocus required ng-model="searchNoEmbarque" name="searchFor" placeholder="<spring:message code='contact'/>&nbsp;<spring:message code='contacts.name'/>"/>-->
                    <div>
                        <ui-select ng-model="searchNoEmbarque"  ng-required="true" name="idoperador" on-select="onSelectCallback($item, $model);" theme="selectize">
                          <ui-select-match placeholder="Select No. Tracking..." class="ui-select-match">
                               <span ng-bind="$select.selected"></span>
                          </ui-select-match>
                          <ui-select-choices class="ui-select-choices" refresh="getNoTracking($select)" repeat="obj in listnotracking">
                            <span ng-bind-html="obj | highlight: $select.search"></span>
                          </ui-select-choices>
                        </ui-select>
                        <div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationError &amp;&amp; updatePaqueteForm.idoperador.$invalid">
                            <spring:message code="required"></spring:message>
                        </div>                            
                    </div>                    
                    <div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationError && searchContactForm.searchFor.$error.required">
                        <spring:message code="required"></spring:message>
                    </div>                                   
                </div>
            </form>   
            <!--<button class="btn btn-primary glyphicon glyphicon-search" ng-click="searchPaqueteAct(searchPaqueteActForm, false);"/>             
            <button class="btn btn-danger glyphicon glyphicon-erase" ng-click="resetSearch();"></button>-->
        </div>
        <div class="table-gnkl">
            <table class="table table-striped table-bordered" st-reset="isReset" st-table="paquetesCollection" st-pipe="paqueteFilter">
                <thead>
                    <tr>
                        <th st-sort="idPaq" >ID GNKL</th>
                        <th st-sort="noEmbarque" >No. Tracking</th>
                        <th st-sort="operador">Operador</th>
                        <th st-sort="ptoorigen">Pto. Origen</th>
                        <th st-sort="ptoentrega">Pto. Entrega</th>
                        <th st-sort="fechaRecPaq">Fecha Recepción</th>
                        <th st-sort="fechaEntPaq">Fecha Entrega</th>
                    <sec:authorize access="hasAnyRole('ROLE_MASTER', 'ROLE_ADMINISTRADOR', 'ROLE_OPERADOR')">
                        <th>Usuario</th>
                    </sec:authorize>    
                        <th>Estatus</th>
                        <th>Acciones</th>
                    </tr>
                </thead>            
                <tbody>
                    <tr data-toggle="modal" data-target="#modal-source" ng-repeat="source in paquetesCollection">
                        <td>{{source.idPaq}}</td>
                        <td>{{source.noEmbarque}}</td>
                        <td>{{source.idOperador.nombre}}&nbsp;{{source.idOperador.apellidoPat}}&nbsp;{{source.idOperador.apellidoMat}}</td>
                        <td>{{source.idPuntoorigen.nombrePunto}}</td>
                        <td>{{source.idPuntoe.nombrePunto}}</td>
                        <td ng-bind="source.fechaRecPaq |  date:'dd/MM/yyyy'"></td>
                        <td ng-bind="source.fechaEntPaq |  date:'dd/MM/yyyy'"></td>
                        <!--<td ng-bind="source.fechaEntPaq |  date:'dd/MM/yyyy HH:mm:ss'"></td>
                        <td ng-bind="source.fechaRecPaq |  date:'dd/MM/yyyy HH:mm:ss'"></td>-->
                    <sec:authorize access="hasAnyRole('ROLE_MASTER', 'ROLE_ADMINISTRADOR', 'ROLE_OPERADOR')">
                        <td>{{source.idUsu.usuario}}</td>
                    </sec:authorize>    
                        <td>{{source.idStatus.desStatus}}</td>
                        <td>
                            <sec:authorize access="hasAnyRole('ROLE_MASTER', 'ROLE_ADMINISTRADOR', 'ROLE_OPERADOR', 'ROLE_CLIENTE')">
                            <a href="#"
                               role="button"
                               ng-click="selectPaqueteAct(source);"
                               title="<spring:message code="paquete.viewdetalle"/>"
                               class="btn btn-default glyphicon glyphicon-edit" data-toggle="modal">
                            </a>                        
                            </sec:authorize>                                                         
                        </td>            
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="10">
                            <div class="pull-right" st-pagination="" st-items-by-page="itemsByPage"></div>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <sec:authorize access="hasAnyRole('ROLE_MASTER', 'ROLE_ADMINISTRADOR', 'ROLE_OPERADOR')">
        <div ng-if="displayInformationPaquete === true">
            <div class="panel panel-info" style="width: 100% !important;">
                <div class="panel-heading collapsed" data-toggle="collapse" data-target="#collapse3">
                    <h3 class="panel-title">No. Tracking: {{paquete.noEmbarque}} | Operador: {{paquete.idOperador.nombre}} {{paquete.idOperador.apellidoPat}} {{paquete.idOperador.apellidoMat}} | Estatus: {{paquete.idStatus.desStatus}} | Pto. Origen: {{paquete.idPuntoorigen.nombrePunto}} | Pto. Entrega: {{paquete.idPuntoe.nombrePunto}}</h3>
                </div>
                <div id="collapse3" class="collapse">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-3 col-lg-3 " align="center">
                                <img ng-src="{{ imageUrl }}" alt="" class="img-responsive" />
                            </div>

                            <div class=" col-md-9 col-lg-9 "> 
                                <table class="table table-user-information">
                                    <tbody>
                                        <tr>
                                            <td><spring:message code="paquete.idpaq"></spring:message>:</td>
                                                <td>{{paquete.idPaq}} </td>
                                            </tr>
                                            <tr>
                                                <td><spring:message code="paquete.noembarque"></spring:message>:</td>
                                                <td>{{paquete.noEmbarque}}</td>
                                            </tr>
                                            <tr>
                                                <td><spring:message code="paquete.fechacarga"></spring:message></td>
                                                <td ng-bind="paquete.fecCargaPaq |  date:'dd/MM/yyyy HH:mm:ss'"></td>
                                            </tr>
                                            <tr>
                                                <td><spring:message code="paquete.fecenrtregapaq"></spring:message></td>
                                                <td ng-bind="paquete.fechaEntPaq |  date:'dd/MM/yyyy HH:mm:ss'"></td>
                                            </tr>
                                            <tr>
                                                <td><spring:message code="paquete.fecrecpaq"></spring:message></td>
                                                <td ng-bind="paquete.fechaRecPaq |  date:'dd/MM/yyyy HH:mm:ss'"></td>
                                            </tr>                                                   
                                            <tr>
                                                <td><spring:message code="paquete.idusuario"></spring:message></td>
                                                <td>{{paquete.idUsu.usuario}}&nbsp;-&nbsp;{{paquete.idUsu.nombre}}</td>
                                            </tr>
                                            <tr>
                                                <td><spring:message code="paquete.id_puntoorigen"></spring:message></td>
                                                <td>{{paquete.idPuntoorigen.nombrePunto}} {{paquete.idPuntoorigen.direccion}}</td>
                                            </tr>
                                            <tr>
                                                <td><spring:message code="paquete.idpuntoe"></spring:message></td>
                                                <td>{{paquete.idPuntoe.nombrePunto}} {{paquete.idPuntoe.direccion}}</td>
                                            </tr>
                                            <tr>
                                                <td><spring:message code="paquete.idstatus"></spring:message></td>
                                                <td>{{paquete.idStatus.desStatus}}</td>
                                            </tr>    
                                            <tr>
                                                <td><spring:message code="paquete.idOperador"></spring:message></td>
                                            <td>{{paquete.idOperador.nombre}} {{paquete.idOperador.apellidoPat}} {{paquete.idOperador.apellidoMat}}</td>
                                        </tr> 
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <sec:authorize access="hasAnyRole('ROLE_MASTER', 'ROLE_ADMINISTRADOR', 'ROLE_OPERADOR')">
                    <div class="panel-footer" ng-show="paquete !== undefined && paquete.idPaq!==undefined">
                            <input type="hidden" value="{{paquete.idPaq}}"/>
                            <a ng-show="paquete.idStatus.desStatus!=='CANCELADO'" href="#actPaqueteModal" role="button" title="<spring:message code="paquete.modifyestatus"/>" class="btn btn-primary glyphicon glyphicon-pencil" data-toggle="modal" ng-click="verifyNextStatus(paquete);"></a>
                            <button class="btn btn-primary glyphicon glyphicon-refresh right" ng-click="searchPaqueteAct(searchPaqueteActForm, false);" title="Recargar Historico"/>        
                    </div>
                </sec:authorize>
            </div>           
        </div>
    </sec:authorize>
    <!-- PARA EL DATAGRID HISTORICO-->
    <div>
        <!--<div class="modal-body" ng-if="searchFormStatus===true">
            <form name="searchPaqueteHistForm" novalidate>
                <label><spring:message code="search.for.historico"/></label>
                <div>
                    <input type="text" class="form-control" autofocus required ng-model="searchHist" name="searchHist" placeholder="<spring:message code='contact'/>&nbsp;<spring:message code='contacts.name'/> "/>                            
                    <div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationError && searchContactForm.searchFor.$error.required">
                        <spring:message code="required"></spring:message>
                    </div>                                   
                </div>
            </form> 
            <div class="form-group">
                <button class="btn btn-primary glyphicon glyphicon-search" ng-click="searchHistorico(searchPaqueteHistForm, false);" value='<spring:message code="search"/>'/>             
                <button class="btn btn-danger glyphicon glyphicon-erase" ng-click="resetSearchHistorico();"/>
                <button class="btn btn-primary glyphicon glyphicon-refresh right" ng-click="searchPaqueteAct(searchPaqueteActForm, false);"/>
            </div>
        </div>-->
        <!--<div class="row">
            <button class="btn btn-primary glyphicon glyphicon-refresh right" ng-click="searchPaqueteAct(searchPaqueteActForm, false);" value="Recargar Historico"/>
        </div>-->
        
        <div class="table-gnkl">
            <table class="table table-striped table-bordered" st-reset="isReset" st-table="leadsCollection" st-pipe="serverFilter">
                <thead>
                    <tr>
                        <sec:authorize access="hasAnyRole('ROLE_MASTER', 'ROLE_ADMINISTRADOR', 'ROLE_OPERADOR')">
                        <th >ID</th>
                        </sec:authorize>
                        <th >Fecha</th>
                        <th >Observaciones</th>
                        <th >Operador</th>
                        <th >Estatus</th>
                        <th >No. Tracking</th>
                        <sec:authorize access="hasAnyRole('ROLE_MASTER', 'ROLE_ADMINISTRADOR', 'ROLE_OPERADOR')">
                        <th>Acciones</th>
                        </sec:authorize>                        
                    </tr>
                </thead>            
                <tbody>
                    <tr data-toggle="modal" data-target="#modal-source" ng-repeat="source in leadsCollection" ng-click="rowClick($index);">
                        <sec:authorize access="hasAnyRole('ROLE_MASTER', 'ROLE_ADMINISTRADOR', 'ROLE_OPERADOR')">
                        <td>{{source.idHist}}</td>
                        </sec:authorize>
                        <td ng-bind="source.fechaHoraHist |  date:'dd/MM/yyyy'"></td>
                        <td>{{source.obserHist}}</td>
                        <td>{{source.idPaq.idOperador.nombre}} {{source.idPaq.idOperador.apellidoPat}} {{source.idPaq.idOperador.apellidoMat}}</td>
                        <td>{{source.idStatus.desStatus}}</td>
                        <td>{{source.idPaq.noEmbarque}}</td> 
                        <sec:authorize access="hasAnyRole('ROLE_CLIENTE')">
                            <td>                
                                <a ng-if="source.archivos.length !== null && source.archivos.length>0" href="" ng-click="getImagen(source);" role="button"  class="btn btn-info glyphicon glyphicon-save-file"></a>
                            </td>                 
                        </sec:authorize>                        
                        <sec:authorize access="hasAnyRole('ROLE_MASTER', 'ROLE_ADMINISTRADOR', 'ROLE_OPERADOR')">
                            <td>                
                                <a ng-if="source.archivos.length !== null && source.archivos.length>0" href="" ng-click="getImagen(source);" role="button"  class="btn btn-info glyphicon glyphicon-save-file"></a>
                                <a href="" ng-click="showConfirmHist(source);" role="button"
                                   title="<spring:message code="delete"/>&nbsp;<spring:message code="images"/>"
                                   class="btn btn-danger glyphicon glyphicon-warning-sign" data-toggle="modal">
                                </a>                                                        
                                <a href="#uploadImage" ng-click="selectedHistPaquete(source);" role="button"
                                   title="<spring:message code="upload.files"/>&nbsp;"
                                   class="btn btn-primary glyphicon glyphicon glyphicon-upload" data-toggle="modal">
                                </a>                            
                            </td>                 
                        </sec:authorize>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="8">
                            <div class="pull-right" st-pagination="" st-items-by-page="itemsByPage"></div>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
        
    <jsp:include page="dialogs/paqueteDialogs.jsp"/>
    
</div>        
<script type="text/javascript" src="${contextPath}/resources/js/pages/paqueteAct.js" /></script>