<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<div class="row" ng-app="historico"  ng-controller="PaqueteHistoricoCtrl as histctrl">                   
    <h4>
        <div ng-class="{'': state == 'list', 'none': state != 'list'}">
            <p class="text-center">
                <spring:message code="message.total.records.found"/>:&nbsp;{{page.totalContacts}}
            </p>
        </div>
    </h4>

    <div>
        <!--<div id="loadingModal" class="modal hide fade in centering" role="dialog" aria-labelledby="deletePaqueteModalLabel" aria-hidden="true">
            <div id="divLoadingIcon" class="text-center">
                <div class="icon-align-center loading"></div>
            </div>
        </div>-->        

        <div ng-class="{'alert badge-inverse': displayMessageToUser == true, 'none': displayMessageToUser == false}">
            <h4 class="displayInLine">
                <p class="messageToUser displayInLine"><i class="icon-info-sign"></i>&nbsp;{{page.actionMessage}}</p>
            </h4>
        </div>

        <div ng-class="{'alert alert-block alert-error': state == 'error', 'none': state != 'error'}">
            <h4><i class="icon-info-sign"></i> <spring:message code="error.generic.header"/></h4><br/>

            <p><spring:message code="error.generic.text"/></p>
        </div>

        <div ng-class="{'alert alert-info': state == 'noresult', 'none': state != 'noresult'}">
            <h4><i class="icon-info-sign"></i> <spring:message code="contacts.emptyData"/></h4><br/>

            <p><spring:message code="contacts.emptyData.text"/></p>
        </div>        
    </div>    

    <div class="container">
    <div class="modal-body">
        <form name="searchPaqueteHistForm" novalidate>
            <label><spring:message code="search.for"/></label>
            <div>
                <input type="text" class="form-control" autofocus required ng-model="searchFor" name="searchFor" placeholder="<spring:message code='contact'/>&nbsp;<spring:message code='contacts.name'/> "/>                            
                <div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationError && searchContactForm.searchFor.$error.required">
                    <spring:message code="required"></spring:message>
                </div>                                   
            </div>
        </form>   
        <input type="submit" class="btn btn-primary" ng-click="searchHistorico(searchPaqueteHistForm, false);" value='<spring:message code="search"/>'/>             
        <input type="submit" class="btn btn-danger" ng-click="resetSearch();" value='<spring:message code="search.reset"/>'/>                 
    </div>
        
        <table class="table table-striped table-bordered" st-reset="isReset" st-table="leadsCollection" st-pipe="serverFilter">
            <thead>
                <tr>
                        <th st-sort="idHist" >ID</th>
                        <th st-sort="fechaHoraHist" >Fecha</th>
                        <th st-sort="obserHist">Observaciones</th>
                        <th st-sort="usuario">Usuario</th>
                        <th st-sort="status">Estatus</th>
                        <th st-sort="noEmbarque">No. Embarque</th>
                        <th>Archivo</th>
                </tr>
            </thead>            
        <tbody>
           <tr data-toggle="modal" data-target="#modal-source" ng-repeat="source in leadsCollection" ng-click="rowClick($index);">
                <td>{{source.idHist}}</td>
                <td>{{source.fechaHoraHist}}</td>
                <td>{{source.obserHist}}</td>
                <td>{{source.idUsu.usuario}}</td>
                <td>{{source.idStatus.desStatus}}</td>
                <td>{{source.idPaq.noEmbarque}}</td>        
                <td ng-if="source.dirArchivo !== null && source.dirArchivo !== '' && source.dirArchivo!==undefined && source.dirArchivo.length>0">
                    <a href="" ng-click="getImagen(source);" role="button"  class="btn btn-info">Descargar</a>
                </td>                 
           </tr>
        </tbody>
        <tfoot>
          <tr>
             <td colspan="8">
                <div class="pull-right" st-pagination="" st-items-by-page="itemsByPage"></div>
             </td>
          </tr>
        </tfoot>
        </table>
    
    </div>                
</div>        
<script type="text/javascript" src="${contextPath}/resources/js/pages/paqueteHistorico.js" /></script>