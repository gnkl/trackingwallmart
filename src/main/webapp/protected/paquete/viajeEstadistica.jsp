<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<div class="" ng-app="estadistica"  ng-controller="EstadisticaCtrl as estadisticactrl">
    
    <div class="panel panel-default" style="width: 100% !important;">
        <div class="panel-heading collapsed" data-toggle="collapse" data-target="#collapse7">
            <h3 class="panel-title">Reporte de paquetes por vehículo</h3>
            <br>
            <button class="btn btn-info glyphicon glyphicon-download" ng-click="createExcelVehiculo();" value="Descargar Reporte" title="Descargar Reporte"/>
            <button class="btn btn-info glyphicon glyphicon-refresh" ng-click="refreshReporteCamion();" value="Descargar Reporte" title="Descargar Reporte"/>
        </div>
        <div id="collapse7" class="collapse">
            <div class="panel-body">
                <table class="table table-striped table-bordered" st-reset="isReset" st-table="leadsVehiculoCollection" st-pipe="serverPaquetesCamion">
                    <thead>
                        <tr>
                            <th>No. Tracking</th>
                            <th>Fecha Recepción</th>
                            <th>Operadores</th>
                            <th>Vehículo</th>
                            <th>Punto Entrega</th>
                            <th>Fecha Entrega</th>
                            <th>Duración</th>
                            <th>Tiempo total</th>
                            <th>No. Tarimas</th>
                        </tr>
                    </thead>            
                    <tbody>
                        <tr ng-repeat="row in leadsVehiculoCollection">
                            <td>{{row.no_embarque}}</td>
                            <td ng-bind="row.fecha_recepcion |  date:'dd/MM/yyyy HH:mm:ss'"></td>
                            <td>{{row.operador}}</td>
                            <td>{{row.vehiculo}}</td>
                            <td>{{row.punto_entrega}}</td>
                            <td ng-bind="row.fecha_entrega |  date:'dd/MM/yyyy HH:mm:ss'"></td>
                            <td>{{row.duracion}}</td>
                            <td>{{row.tiempo_horas}}</td>
                            <td>{{row.no_tarimas}}</td>
                        </tr>                        
                    </tbody>
                    <tfoot>
                        <td colspan="5" class="text-center">
                            <div st-pagination="" st-items-by-page="itemsByPage" st-displayed-pages=""></div>
                        </td>                        
                    </tfoot>
                </table>                
            </div>
        </div>        
    </div>
    <div class="panel panel-info" style="width: 100% !important;">
        <div class="panel-heading" data-toggle="collapse" data-target="#collapse9">
            <h3 class="panel-title">Viajes realizados</h3>
        </div>        
        <div id="collapse9" >
            <div class="panel-body">
                <div class="form-group row">
                    <label class="col-sm-2 form-control-label"><spring:message code="paquete.id_puntoorigen"></spring:message>:</label>
                    <div class="col-sm-10">
                    <ui-select theme="selectize" ng-model="selected.puntoo" data-ng-disabled="disabled" name="idptoorigen" on-select="onSelectPtoOrigenCallback($item, $model);">
                      <ui-select-match placeholder="Seleccionar punto de origen" class="ui-select-match">
                           <span ng-bind="$select.selected.nombrePunto+' | '+$select.selected.direccion"></span>
                      </ui-select-match>
                      <ui-select-choices class="ui-select-choices" refresh="getOrigenes($select)" repeat="obj in origenes">
                        <span ng-bind-html="obj.nombrePunto+' | '+obj.direccion | highlight: $select.search"></span>
                      </ui-select-choices>
                    </ui-select>
                    <div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationError &amp;&amp; newPaqueteForm.idptoorigen.$error.required">
                        <spring:message code="required"></spring:message>
                    </div> 
                     </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 form-control-label"><spring:message code="paquete.idpuntoe"></spring:message>:</label>
                    <div class="col-sm-10">
                    <ui-select theme="selectize" ng-model="selected.puntoe" data-ng-disabled="disabled" name="idpuntoe" on-select="onSelectPtoEntregaCallback($item, $model);">
                      <ui-select-match placeholder="Seleccionar punto de entrega" class="ui-select-match">
                           <span ng-bind="$select.selected.nombrePunto+' | '+$select.selected.direccion"></span>
                      </ui-select-match>
                      <ui-select-choices class="ui-select-choices" refresh="getEntrega($select)" repeat="obj in entregas">
                        <span ng-bind-html="obj.nombrePunto+' | '+obj.direccion | highlight: $select.search"></span>
                      </ui-select-choices>
                    </ui-select>  
                    <div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationError &amp;&amp; newPaqueteForm.idpuntoe.$error.required">
                        <spring:message code="required"></spring:message>
                    </div>
                    </div>
                </div>   
                <div class="form-group row">
                    <label class="col-sm-2 form-control-label">
                        Encontrados: 
                    </label>
                    <a href="#showDetallePaquetesModal1"
                       ng-click="getDetalleViajes();"
                       role="button"
                       title="Ver detalle"
                       class="" data-toggle="modal">
                       {{totalVecesEncontrado}}
                    </a>                                             
                </div>
            </div>        
        </div>    
    </div>
    <div class="panel panel-info" style="width: 100% !important;">
        <div class="panel-heading" data-toggle="collapse" data-target="#collapse10">
            <h3 class="panel-title">Viajes realizados entregados</h3>
        </div>        
        <div id="collapse10" >
            <div class="panel-body">
                <button class="btn btn-link pull-right" ng-click="exportData('tableTotalViajesEntregados','Viajes_Realizados')">
                  <span class="glyphicon glyphicon-share"></span>
                </button>                 
                <div id="tableTotalViajesEntregados">
                    <table class="table table-striped table-bordered" st-reset="isReset" st-table="leadsViajeCompletoCollection" st-pipe="serverViajesCompletos">
                        <thead>
                            <tr>
                                <th>Jaloma Zapopan - Sendero Entregas</th>
                                <th>Sendero Salidas - Punto Entrega final</th>
                                <th>Viaje Directo</th>
                                <th>Total General</th>
                            </tr>
                        </thead>            
                        <tbody>
                            <tr ng-repeat="row in leadsViajeCompletoCollection">
                                <td>
                                    <a href="#showDetallePaquetesModal2"
                                       ng-click="selectedByFaseStatus(1,37);"
                                       role="button"
                                       title="Ver detalle viajes"
                                       class="" data-toggle="modal">
                                        {{row.fase1}}
                                    </a>                                  
                                </td>
                                <td>
                                    <a href="#showDetallePaquetesModal2"
                                       ng-click="selectedByFaseStatus(2,39);"
                                       role="button"
                                       title="Ver detalle viajes"
                                       class="" data-toggle="modal">
                                        {{row.fase2}}
                                    </a>
                                </td>
                                <td>
                                    <a href="#showDetallePaquetesModal2"
                                       ng-click="selectedByFaseStatus(0,43);"
                                       role="button"
                                       title="Ver detalle viajes"
                                       class="" data-toggle="modal">
                                        {{row.directo}}
                                    </a>                                
                                </td>
                                <td>
                                    {{row.directo+row.fase2+row.fase1}}
                                </td>                            
                            </tr>                        
                        </tbody>
                        <tfoot>
                                <tr>
                                    <td colspan="5" class="text-center">
                                        <div st-pagination="" st-items-by-page="itemsByPage" st-displayed-pages=""></div>
                                    </td>
                                </tr>                        
                        </tfoot>
                    </table>
                </div>
            </div>        
        </div>    
    </div>
    <div class="panel panel-info" style="width: 100% !important;">
        <div class="panel-heading" data-toggle="collapse" data-target="#collapse11">
            <h3 class="panel-title">Viajes por grupos</h3>            
        </div>        
        <div id="collapse11" >
            <div class="panel-body">
                <div>
                    <label>* <spring:message code="statistics.grupo"></spring:message>:</label>
                    <!--<select name="empresa" id="empresaselect" class="form-control" ng-model="gruposelected" ng-change="onselectGrupo()">
                    <option ng-repeat="opt in listGrupo" value="{{opt}}">{{opt}}</option>
                    </select>-->                                                       
                    <ui-select theme="selectize" ng-model="gruposelected"  title="Grupo" name="selectgrupo" on-select="onSelectGrupoCallback($item, $model);">
                        <ui-select-match placeholder="Seleccionar grupo" class="ui-select-match">
                            <span ng-bind="$select.selected"></span>
                        </ui-select-match>
                        <ui-select-choices repeat="obj in listGrupo | filter: $select.search" class="ui-select-choices">
                          <span ng-bind-html="obj | highlight: $select.search"></span>
                        </ui-select-choices>
                     </ui-select> 

                </div>   
                <div class="row">
                    Número de veces visitado: {{totalVecesGrupo}}
                    <button class="btn btn-link pull-right" ng-click="exportData('novecesvisit','Numero_Veces_Viajes')">
                      <span class="glyphicon glyphicon-share"></span>
                    </button>                      
                </div>                  
                <div id="novecesvisit">                                        
                <table class="table table-striped table-bordered" st-reset="isReset" st-table="leadsGrupoCollection" st-pipe="serverGrupoCompletos">
                    <thead>
                        <tr>
                            <th>Total</th>
                            <th>Cliente</th>
                        </tr>
                    </thead>            
                    <tbody>
                        <tr ng-repeat="row in leadsGrupoCollection">
                            <td>
                                {{row.total}}
                            </td>
                            <td>
                                <a href="#showDetallePaquetesModal3"
                                   ng-click="getDetalleByGrupo(row.cliente);"
                                   role="button"
                                   title="Ver detalle"
                                   data-toggle="modal">
                                   {{row.cliente}}
                                </a>                                   
                            </td>
                        </tr>                        
                    </tbody>
                    <tfoot>
                            <tr>
                                <td colspan="5" class="text-center">
                                    <div st-pagination="" st-items-by-page="itemsByPage" st-displayed-pages=""></div>
                                </td>
                            </tr>                        
                    </tfoot>
                </table>                    
                </div>    
            </div>        
        </div>    
    </div>
                    
    <div class="panel panel-info" style="width: 100% !important;">
        <div class="panel-heading collapsed" data-toggle="collapse" data-target="#collapse3">
            <h3 class="panel-title">Total NT: {{totalViajes}} | Detalle de NT</h3>
        </div>
        <div id="collapse3" class="collapse">
            <button class="btn btn-link" ng-click="createReporteZip('Reporte_Total_NT',3)">
              <span class="glyphicon glyphicon-share"></span>
            </button>             
            <div class="panel-body">
                <table class="table table-striped table-bordered" st-reset="isReset" st-table="leadsCollection" st-pipe="serverFilter">
                    <thead>
                        <tr>
                            <th>No. Tracking</th>
                            <th>Punto Entrega</th>
                            <th>Estatus</th>
                        </tr>
                    </thead>            
                    <tbody>
                        <tr ng-repeat="row in leadsCollection">
                            <td>{{row.no_embarque}}</td>
                            <td>{{row.nombre_punto}}</td>
                            <td>{{row.des_status}}</td>
                        </tr>                        
                    </tbody>
                    <tfoot>
                            <tr>
                                <td colspan="5" class="text-center">
                                    <div st-pagination="" st-items-by-page="itemsByPage" st-displayed-pages=""></div>
                                </td>
                            </tr>                        
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
    <div class="panel panel-info" style="width: 100% !important;">
        <div class="panel-heading" data-toggle="collapse" data-target="#collapse4">
            <h3 class="panel-title">Detalle por Estatus</h3>
        </div>        
        <div id="collapse4" >
            <button class="btn btn-link" ng-click="exportData('graphicsJaloma','Reporte_Detalle_Estatus')" data-toggle="tooltip" data-placement="top" title="Exportar a Excel">
              <span class="glyphicon glyphicon-share"></span>
            </button>            
            <div id="graphicsJaloma" class="panel-body">
                <div class="col-md-6 col-lg-6" align="center">
                <div style="min-height: 250px;">
                    <h3 class="panel-title">Cross Dock</h3>
                    <table st-table="viajesCrossdock" class="table table-striped">
                        <thead>
                            <tr>
                                <th>Estatus</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="row in viajesCrossdock">
                                <td>{{row.des_status}}</td>
                                <td>
                                    <a href="#showViajeModal"
                                       ng-click="selectedStatus(row,1);"
                                       role="button"
                                       title="Ver viajes en status"
                                       class="" data-toggle="modal">
                                        {{row.total}}
                                    </a>                                    
                                </td>
                            </tr>
                        </tbody>
                        <tfoot></tfoot>           
                    </table>
                </div>
                    <nvd3 options="optionsbarCDConfchart" data="viajesCrossdock"></nvd3>
                </div>
                <div class="col-md-6 col-lg-6 col-sm-6" align="center" >
                    <div style="min-height: 250px;">
                    <h3 class="panel-title">Entrega Directa</h3>
                    <!--<pre>{{viajesDirecto}}</pre>-->
                    <table st-table="viajesDirecto" class="table table-striped right">
                        <thead>
                            <tr>
                                <th>Estatus</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="row in viajesDirecto">
                                <td>{{row.des_status}}</td>
                                <td>
                                    <a href="#showViajeModal"
                                       ng-click="selectedStatus(row,3);"
                                       role="button"
                                       title="Ver viajes en status"
                                       class="" data-toggle="modal">
                                       {{row.total}}
                                    </a>                                                                    
                                </td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="5" class="text-center">
                                    <div st-pagination="" st-items-by-page="itemsByPage" st-displayed-pages="3"></div>
                                </td>
                            </tr>
                        </tfoot>           
                    </table>
                    </div>
                    <nvd3 options="optionsbarDConfchart" data="viajesDirecto"></nvd3>
                </div>
            </div>        
        </div>    
    </div>
    <div class="panel panel-info" style="width: 100% !important;">
        <div class="panel-heading collapsed" data-toggle="collapse" data-target="#collapse5">
            <h3 class="panel-title">Detalle de viajes faltantes segunda fase: {{totalViajesFaltantes}}</h3>
        </div>
        <div id="collapse5" class="collapse">
            <button class="btn btn-link" ng-click="createReporteZip('Reporte_Viajes_Faltantes_Segunda_Fase',2)" data-toggle="tooltip" data-placement="top" title="Exportar a Excel"> 
              <span class="glyphicon glyphicon-share"></span>
            </button>            
            <div class="panel-body">
                <table class="table table-striped table-bordered" st-reset="isReset" st-table="leadsFaltanteCollection" st-pipe="serverFaltantes">
                    <thead>
                        <tr>
                            <th>No. Tracking</th>
                            <th>Total</th>
                            <th>Estatus</th>
                        </tr>
                    </thead>            
                    <tbody>
                        <tr ng-repeat="row in leadsFaltanteCollection">
                            <td>
                                <a href="../protected/paqueteact#/?noembarque={{row.no_embarque}}"
                                   role="button"
                                   title="Ver viaje">
                                    {{row.no_embarque}}
                                </a>                                  
                            </td>
                            <td>{{row.total}}</td>
                            <td>{{row.des_status}}</td>
                        </tr>                        
                    </tbody>
                    <tfoot>
                        <td colspan="5" class="text-center">
                            <div st-pagination="" st-items-by-page="itemsByPage" st-displayed-pages=""></div>
                        </td>                        
                    </tfoot>
                </table>                
            </div>
        </div>
    </div>    
    <div class="panel panel-info" style="width: 100% !important;">
        <div class="panel-heading collapsed" data-toggle="collapse" data-target="#collapse6">
            <h3 class="panel-title">Detalle de viajes incompletos: {{totalViajesIncompletos}}</h3>
        </div>
        <div id="collapse6" class="collapse">
            <button class="btn btn-link" ng-click="createReporteZip('Reporte_Viajes_Incompletos',1)" data-toggle="tooltip" data-placement="top" title="Exportar a Excel">
              <span class="glyphicon glyphicon-share"></span>
            </button>            
            <div class="panel-body">
                <table class="table table-striped table-bordered" st-reset="isReset" st-table="leadsIncompletoCollection" st-pipe="serverIncompletos">
                    <thead>
                        <tr>
                            <th>No. Tracking</th>
                            <th>Estatus</th>
                        </tr>
                    </thead>            
                    <tbody>
                        <tr ng-repeat="row in leadsIncompletoCollection">
                            <td>
                                <a href="../protected/paqueteact#/?noembarque={{row.no_embarque}}"
                                   role="button"
                                   title="Ver viaje">
                                    {{row.no_embarque}}
                                </a>                                   
                            </td>
                            <td>{{row.des_status}}</td>
                        </tr>                        
                    </tbody>
                    <tfoot>
                        <td colspan="5" class="text-center">
                            <div st-pagination="" st-items-by-page="itemsByPage" st-displayed-pages=""></div>
                        </td>                        
                    </tfoot>
                </table>                
            </div>
        </div>
    </div>    
    <jsp:include page="dialogs/viajeEstadisticaDialogs.jsp"/> 
</div>
<script type="text/javascript" src="${contextPath}/resources/js/pages/viajeEstadistica.js" /></script>