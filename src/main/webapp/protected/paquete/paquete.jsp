<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<div ng-app="paquete"  ng-controller="PaqueteCtrl as demo">                   
    <div>       
        <div ng-class="{'alert alert-block alert-error': state == 'error', 'none': state != 'error'}">
            <h4><i class="icon-info-sign"></i> <spring:message code="error.generic.header"/></h4><br/>

            <p><spring:message code="error.generic.text"/></p>
        </div>      
    </div>    

    <div class="left">
    <label><spring:message code="search"/></label>
    <hr>    
    <div class="form-horizontal">
        <form name="searchPaqueteForm" novalidate>
            <fieldset>
                <div class="form-group row">
                    <label class="col-sm-2 form-control-label"><spring:message code="search.for.tracking"/></label>
                    <div  class="col-sm-10">
                        <div>
                            <ui-select ng-model="selected.notracking"  name="idtracking" on-select="onSelectNoTrackingCallback($item, $model);" theme="selectize">
                              <ui-select-match placeholder="Seleccionar No. Tracking" class="ui-select-match">
                                   <span ng-bind="$select.selected"></span>
                              </ui-select-match>
                              <ui-select-choices class="ui-select-choices" refresh="getNoTracking($select)" repeat="obj in listnotracking">
                                <small ng-bind-html="obj | highlight: $select.search"></small>
                              </ui-select-choices>
                            </ui-select>
                            <div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationError &amp;&amp; updatePaqueteForm.idoperador.$invalid">
                                <spring:message code="required"></spring:message>
                            </div>                            
                        </div>                    
                        <div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationError && searchContactForm.searchFor.$error.required">
                            <spring:message code="required"></spring:message>
                        </div>                                   
                    </div>                    
                </div>
                <sec:authorize access="hasAnyRole('ROLE_MASTER', 'ROLE_ADMINISTRADOR')">
                <div class="form-group row">
                    <label class="col-sm-2 form-control-label"><spring:message code="paquete.idOperador"></spring:message>:</label>
                    <div class="col-sm-10">
                    <ui-select theme="selectize" ng-model="selected.operadorid"  title="Operador" name="selectoperador" on-select="onSelectOperadorCallback($item, $model);">
                        <ui-select-match placeholder="Seleccionar operador" class="ui-select-match">
                            <span ng-bind="$select.selected.usuario+' , '+$select.selected.nombre+' '+$select.selected.apellidoPat+' '+$select.selected.apellidoMat"></span>
                        </ui-select-match>
                        <ui-select-choices repeat="obj in searchformoperador | filter: $select.search" class="ui-select-choices">
                          <span ng-bind-html="obj.usuario+' | '+obj.nombre+' '+obj.apellidoPat+' '+obj.apellidoMat | highlight: $select.search"></span>
                        </ui-select-choices>
                     </ui-select> 
                    <div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationError && searchPaqueteForm.selectoperador.$invalid">
                        <spring:message code="required"></spring:message>
                    </div>
                    </div>
                </div>
                </sec:authorize>                     
                <div class="form-group row">
                    <label class="col-sm-2 form-control-label"><spring:message code="paquete.id_puntoorigen"></spring:message>:</label>
                    <div class="col-sm-10">
                    <ui-select theme="selectize" ng-model="selected.puntoo" data-ng-disabled="disabled" name="idptoorigen" on-select="onSelectPtoOrigenCallback($item, $model);">
                      <ui-select-match placeholder="Seleccionar punto de origen" class="ui-select-match">
                           <span ng-bind="$select.selected.nombrePunto+' | '+$select.selected.direccion"></span>
                      </ui-select-match>
                      <ui-select-choices class="ui-select-choices" refresh="getOrigenes($select)" repeat="obj in origenes">
                        <span ng-bind-html="obj.nombrePunto+' | '+obj.direccion | highlight: $select.search"></span>
                      </ui-select-choices>
                    </ui-select>
                    <div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationError &amp;&amp; newPaqueteForm.idptoorigen.$error.required">
                        <spring:message code="required"></spring:message>
                    </div> 
                     </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 form-control-label"><spring:message code="paquete.idpuntoe"></spring:message>:</label>
                    <div class="col-sm-10">
                    <ui-select theme="selectize" ng-model="selected.puntoe" data-ng-disabled="disabled" name="idpuntoe" on-select="onSelectPtoEntregaCallback($item, $model);">
                      <ui-select-match placeholder="Seleccionar punto de entrega" class="ui-select-match">
                           <span ng-bind="$select.selected.nombrePunto+' | '+$select.selected.direccion"></span>
                      </ui-select-match>
                      <ui-select-choices class="ui-select-choices" refresh="getEntrega($select)" repeat="obj in entregas">
                        <span ng-bind-html="obj.nombrePunto+' | '+obj.direccion | highlight: $select.search"></span>
                      </ui-select-choices>
                    </ui-select>  
                    <div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationError &amp;&amp; newPaqueteForm.idpuntoe.$error.required">
                        <spring:message code="required"></spring:message>
                    </div>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 form-control-label"><spring:message code="paquete.tipoEnvio"></spring:message>:</label>
                    <div class="col-sm-10">
                        <select name="tipoenvio" id="tipoEnvioSelect" class="form-control" ng-model="tipoEnvioSelected" ng-change="onChangeTipoEnvio()">
                          <option ng-repeat="opt in tipoenvio" value="{{opt.idEnvio}}">{{opt.desEnvio}}</option>
                        </select>                            
                        <div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationError && newPaqueteForm.tipoenvio.$error.required">
                        <spring:message code="required"></spring:message>
                        </div>
                    </div>
                </div>                    
                <div class="form-group row">
                    <label class="col-sm-2 form-control-label"><spring:message code="paquete.idstatus"></spring:message>:</label>
                    <div class="col-sm-10">
                        <select name="estatus" id="statusselect" class="form-control" ng-model="statusselected" ng-change="onChangeStatus()">
                          <option ng-repeat="opt in statuslist" value="{{opt.idStatus}}">{{opt.desStatus}}</option>
                        </select>                            
                        <div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationError && newPaqueteForm.empresa.$error.required">
                        <spring:message code="required"></spring:message>
                        </div>
                    </div>
                </div>
            </fieldset>
        </form>                    
    </div>
    <hr>
    <div class="pull-right" style="width: 100% !important;">
        <!--<button class="btn btn-primary pull-right glyphicon glyphicon-search" ng-click="searchPaquete(searchPaqueteForm, false);" value='<spring:message code="search"/>'></button>-->
        <button class="btn btn-danger pull-right glyphicon glyphicon-erase" ng-click="resetSearch();"></button>             
        <sec:authorize access="hasAnyRole('ROLE_MASTER','ROLE_ADMINISTRADOR', 'ROLE_OPERADOR')">
            <div ng-class="{'text-center': displayCreateContactButton == true, 'none': displayCreateContactButton == false}">
                <a id="modalShowerAddPaqueteModal"
                   href="#addPaqueteModal"
                   role="button"
                   ng-click="createNewPaquete()"
                   title="<spring:message code='create'/>&nbsp;<spring:message code='ruta'/>"
                   class="btn btn-default pull-left"
                   data-toggle="modal"
                   ng-class="{'text-center': displayCreateContactButton == true, 'none': displayCreateContactButton == false}">
                    <i class="glyphicon-plus"></i>
                    &nbsp;&nbsp;<spring:message code="create"/>
                </a>
                <a id="modalShowerUpdatePaqueteModal"
                   href="#addPaqueteModal"
                   role="button"
                   ng-click="onclickreload()"
                   data-toggle="modal"
                   ng-show="false">
                </a>
                
            </div>
        </sec:authorize>        
    </div>
    <br/><br/>
    <label>Total registros encontrados: {{totalViajesFound}}</label>
    <hr>
    <div class="table-gnkl">
        <table class="table table-striped table-bordered" st-safe-src="leads"  st-reset="isReset" st-table="leadsCollection" st-pipe="serverFilter">
            <thead>
                <tr>
                    <th st-sort="idPaq" >ID</th>
                    <th st-sort="noEmbarque" >No. Embarque</th>
                    <th st-sort="operador">Operador</th>
                    <th>Pto. Origen</th>
                    <th>Pto. Entrega</th>
                    <th st-sort="fechaRecPaq">Fecha Recepción</th>
                    <th st-sort="fechaEntPaq">Fecha Entrega</th>
                    <th>Usuario</th>
                    <th>Estatus</th>
                    <th>Cita</th>
                    <th>Acciones</th>
                </tr>
            </thead>            
            <tbody>
                <tr data-toggle="modal" data-target="#modal-source" ng-repeat="source in leadsCollection" ng-click="rowClick($index);">                    
                    <td ng-class="{'bg-danger':source.cita==1}">{{source.idPaq}}</td>
                    <td ng-class="{'bg-danger':source.cita==1}">{{source.noEmbarque}}</td>
                    <td ng-class="{'bg-danger':source.cita==1}">{{source.idOperador.nombre}}&nbsp;{{source.idOperador.apellidoPat}}&nbsp;{{source.idOperador.apellidoMat}}</td>
                    <td ng-class="{'bg-danger':source.cita==1}"><small><b>{{source.idPuntoorigen.nombrePunto}}</b></small><br><small>{{source.idPuntoorigen.direccion}}</small></td>
                    <td ng-class="{'bg-danger':source.cita==1}"><small><b>{{source.idPuntoe.nombrePunto}}</b></small><br><small>{{source.idPuntoe.direccion}}</small></td>
                    <td ng-class="{'bg-danger':source.cita==1}" ng-bind="source.fechaRecPaq |  date:'dd/MM/yyyy'"></td>
                    <td ng-class="{'bg-danger':source.cita==1}" ng-bind="source.fechaEntPaq |  date:'dd/MM/yyyy'"></td>
                    <td ng-class="{'bg-danger':source.cita==1}">{{source.idUsu.usuario}}</td>
                    <td ng-class="{'bg-danger':source.cita==1}">{{source.idStatus.desStatus}}</td>
                    <td ng-class="{'bg-danger':source.cita==1}">{{source.cita==1?'SI':'NO'}}</td>
                    <td ng-class="{'bg-danger':source.cita==1}">
                        <div>
                            <sec:authorize access="hasAnyRole('ROLE_MASTER', 'ROLE_ADMINISTRADOR', 'ROLE_OPERADOR')">
                            <input type="hidden" value="{{source.idPaq}}"/>
                            <a href="#updatePaqueteModal"
                               ng-click="selectedPaquete(source);"
                               role="button" title="<spring:message code="update"/>&nbsp;<spring:message code="paquete"/>"
                               class="btn btn-default glyphicon glyphicon-pencil" data-toggle="modal">
                            </a>
                            <a ng-show="source.idStatus.desStatus === 'CREACION DE RUTA'" href="#deletePaqueteModal"
                               ng-click="selectedPaquete(source);"
                               role="button"
                               title="<spring:message code="cancel"/>&nbsp;<spring:message code="paquete"/>"
                               class="btn btn-default glyphicon glyphicon-warning-sign" data-toggle="modal">
                            </a>
                            </sec:authorize>
                        </div>
                        <sec:authorize access="hasAnyRole('ROLE_MASTER', 'ROLE_ADMINISTRADOR', 'ROLE_OPERADOR')">
                        <!--<a href="/GnklTracking/protected/paqueteact#/?noembarque={{source.noEmbarque}}"
                           role="button"
                           title="<spring:message code="paquete.actualizarestatus"/>"
                           class="btn btn-default glyphicon glyphicon-edit" data-toggle="modal">
                        </a>-->
                        <a ng-show="source.idStatus.desStatus !== 'CANCELADO'" href="../protected/paqueteact#/?idpaq={{source.idPaq}}"
                           role="button"
                           title="<spring:message code="paquete.actualizarestatus"/>"
                           class="btn btn-default glyphicon glyphicon-edit" data-toggle="modal">
                        </a>                        
                        </sec:authorize>                                                         
                    </td>            
                </tr>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="10">
                        <div class="pull-right" st-pagination="" st-items-by-page="itemsByPage"></div>
                    </td>
                </tr>
            </tfoot>
        </table>
    </div>     
    </div>    
            
    <jsp:include page="dialogs/paqueteDialogs.jsp"/> 
</div>        
<script type="text/javascript" src="${contextPath}/resources/js/pages/paquete.js" /></script>