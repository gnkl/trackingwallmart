<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

                
<div id="showViajeModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 id="deletePaqueteModalLabel" class="displayInLine">
                    Listado de viajes
                </h3>
            </div>
            <button class="btn btn-link" ng-click="exportData('listViajesBystatus','Reporte_Viajes_Estatus')" data-toggle="tooltip" data-placement="top" title="Exportar a Excel">
              <span class="glyphicon glyphicon-share"></span>
            </button>                        
            <div id="listViajesBystatus" class="modal-body" style="height:300px;overflow-y: scroll;">
                    <table st-safe-src="listViajesByStatusCollection" st-table="listViajesByStatus" class="table table-striped">
                            <thead>
                            <tr>
                                <th st-sort="no_embarque">No. Embarque</th>
                                <th st-sort="fecha_rec_paq">Fecha Recepcion</th>
                                <th st-sort="fecha_ent_paq">Fecha Entrega</th>
                                <th>Estatus</th>
                            </tr>                            
                            </thead>
                            <tbody>
                            <tr ng-repeat="row in listViajesByStatus">
                                <td>
                                    <a href="../protected/paqueteact#/?noembarque={{row.no_embarque}}"
                                       role="button"
                                       title="Ver detalle"
                                       data-toggle="modal">
                                       {{row.no_embarque}}
                                    </a>                                     
                                </td>
                                <td ng-bind="row.fecha_rec_paq |  date:'dd/MM/yyyy'"></td>
                                <td ng-bind="row.fecha_ent_paq |  date:'dd/MM/yyyy'"></td>
                                <td>{{row.des_status}}</td>
                            </tr>
                            </tbody>
                            <!--<tfoot>
                                    <tr>
                                            <td colspan="5" class="text-center">
                                                    <div st-pagination="" st-items-by-page="5" st-displayed-pages="7"></div>
                                            </td>
                                    </tr>
                            </tfoot>-->
                    </table>
            </div>
        </div>
    </div>
</div>                
                
                
<div id="showDetallePaquetesModal1" class="modal fade" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 id="deletePaqueteModalLabel" class="displayInLine">
                    Listado de paquetes
                </h3>
            </div>                
            <div class="modal-body" style="height:300px;overflow-y: scroll;">
                <button class="btn btn-link" ng-click="createReporteViajesRealizados('Detalle_Paquetes')" data-toggle="tooltip" data-placement="top" title="Exportar a Excel">
                  <span class="glyphicon glyphicon-share"></span>
                </button>
                <div id="tableDetallePaquetesModal">
                    <table class="table table-striped table-bordered" st-reset="isReset" st-table="leadsDetalleCollection" st-pipe="serverPaquetesDetalle">
                        <thead>
                            <tr>
                                <th>No. Tracking</th>
                                <th>Fecha Recepci&oacute;n</th>
                                <th>Fecha Entrega</th>
                                <th>Punto Origen</th>
                                <th>Punto Destino</th>
                                <th>Operador</th>
                            </tr>
                        </thead>            
                        <tbody ng-show="!isLoading">
                            <tr ng-repeat="row in leadsDetalleCollection">
                                <td>
                                    <a href="../protected/paqueteact#/?noembarque={{row.no_embarque!=undefined?row.no_embarque:row.noEmbarque}}"
                                       role="button"
                                       title="Ver detalle"
                                       data-toggle="modal">
                                       {{row.noEmbarque}}
                                    </a>                                   
                                </td>
                                <td ng-bind="row.fechaRecPaq |  date:'dd/MM/yyyy'"></td>
                                <td ng-bind="row.fechaEntPaq |  date:'dd/MM/yyyy'"></td>
                                <td>{{row.idPuntoorigen.nombrePunto}}</td>
                                <td>{{row.idPuntoe.nombrePunto}}</td>
                                <td>{{row.idOperador.nombre}}&nbsp;{{row.idOperador.apellidoPat}}&nbsp;{{row.idOperador.apellidoMat}}</td>
                            </tr>                        
                        </tbody>
                        <tbody ng-show="isLoading">
                            <tr>
                                    <td colspan="6" class="text-center">Loading ... </td>
                            </tr>
                        </tbody>                        
                        <tfoot>
                            <td colspan="6" class="text-center">
                                <div st-pagination="" st-items-by-page="itemsByPage" st-displayed-pages=""></div>
                            </td>                        
                        </tfoot>
                    </table>
                </div>                    
            </div>
        </div>
    </div>
</div>                

<div id="showDetallePaquetesModal2" class="modal fade" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 id="deletePaqueteModalLabel" class="displayInLine">
                    Listado de paquetes
                </h3>
            </div>                
            <div class="modal-body" style="height:300px;overflow-y: scroll;">
                <button class="btn btn-link" ng-click="createReporteByFase('Detalle_Paquetes')" data-toggle="tooltip" data-placement="top" title="Exportar a Excel">
                  <span class="glyphicon glyphicon-share"></span>
                </button>
                <div id="tableDetallePaquetesModal">
                    <table class="table table-striped table-bordered" st-reset="isReset" st-table="leadsDetalleFaseCollection">
                        <thead>
                            <tr>
                                <th>No. Tracking</th>
                                <th>Fecha Recepci&oacute;n</th>
                                <th>Fecha Entrega</th>
                                <th>Punto Origen</th>
                                <th>Punto Destino</th>
                                <th>Operador</th>
                            </tr>
                        </thead>            
                        <tbody ng-show="!isLoading">
                            <tr ng-repeat="row in leadsDetalleFaseCollection">
                                <td>
                                    <a href="../protected/paqueteact#/?noembarque={{row.no_embarque!=undefined?row.no_embarque:row.noEmbarque}}"
                                       role="button"
                                       title="Ver detalle"
                                       data-toggle="modal">
                                       {{row.noEmbarque}}
                                    </a>                                   
                                </td>
                                <td ng-bind="row.fechaRecPaq |  date:'dd/MM/yyyy'"></td>
                                <td ng-bind="row.fechaEntPaq |  date:'dd/MM/yyyy'"></td>
                                <td>{{row.idPuntoorigen.nombrePunto}}</td>
                                <td>{{row.idPuntoe.nombrePunto}}</td>
                                <td>{{row.idOperador.nombre}}&nbsp;{{row.idOperador.apellidoPat}}&nbsp;{{row.idOperador.apellidoMat}}</td>
                            </tr>                        
                        </tbody>
                        <tbody ng-show="isLoading">
                            <tr>
                                    <td colspan="6" class="text-center">Loading ... </td>
                            </tr>
                        </tbody>                        
                        <tfoot>
                            <td colspan="6" class="text-center">
                                <div st-pagination="" st-items-by-page="itemsByPage" st-displayed-pages=""></div>
                            </td>                        
                        </tfoot>
                    </table>
                </div>                    
            </div>
        </div>
    </div>
</div>                


<div id="showDetallePaquetesModal3" class="modal fade" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 id="deletePaqueteModalLabel" class="displayInLine">
                    Listado de paquetes
                </h3>
            </div>                
            <div class="modal-body" style="height:300px;overflow-y: scroll;">
                <!--<button class="btn btn-link" ng-click="createReporteViajesRealizados('Detalle_Paquetes')" data-toggle="tooltip" data-placement="top" title="Exportar a Excel">
                  <span class="glyphicon glyphicon-share"></span>
                </button>-->
                <button class="btn btn-link" ng-click="exportData('tableDetallePaquetesModalGrupo','Reporte_Detalle_Grupo')" data-toggle="tooltip" data-placement="top" title="Exportar a Excel">
                    <span class="glyphicon glyphicon-share"></span>
                </button>                
                <div id="tableDetallePaquetesModalGrupo">
                    <table class="table table-striped table-bordered" st-reset="isReset" st-table="leadsDetalleGrupoCollection">
                        <thead>
                            <tr>
                                <th>No. Tracking</th>
                                <th>Fecha Recepci&oacute;n</th>
                                <th>Fecha Entrega</th>
                                <th>Punto Origen</th>
                                <th>Punto Destino</th>
                                <th>Operador</th>
                            </tr>
                        </thead>            
                        <tbody ng-show="!isLoading">
                            <tr ng-repeat="row in leadsDetalleGrupoCollection">
                                <td>
                                    <a href="../protected/paqueteact#/?noembarque={{row.no_embarque!=undefined?row.no_embarque:row.noEmbarque}}"
                                       role="button"
                                       title="Ver detalle"
                                       data-toggle="modal">
                                       {{row.noEmbarque}}
                                    </a>                                   
                                </td>
                                <td ng-bind="row.fechaRecPaq |  date:'dd/MM/yyyy'"></td>
                                <td ng-bind="row.fechaEntPaq |  date:'dd/MM/yyyy'"></td>
                                <td>{{row.idPuntoorigen.nombrePunto}}</td>
                                <td>{{row.idPuntoe.nombrePunto}}</td>
                                <td>{{row.idOperador.nombre}}&nbsp;{{row.idOperador.apellidoPat}}&nbsp;{{row.idOperador.apellidoMat}}</td>
                            </tr>                        
                        </tbody>
                        <tbody ng-show="isLoading">
                            <tr>
                                    <td colspan="6" class="text-center">Loading ... </td>
                            </tr>
                        </tbody>                        
                        <tfoot>
                            <td colspan="6" class="text-center">
                                <div st-pagination="" st-items-by-page="itemsByPage" st-displayed-pages=""></div>
                            </td>                        
                        </tfoot>
                    </table>
                </div>                    
            </div>
        </div>
    </div>
</div>