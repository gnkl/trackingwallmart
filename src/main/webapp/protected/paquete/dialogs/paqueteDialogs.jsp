<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div id="addPaqueteModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style='width:100%;'>
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 id="addPaqueteModalLabel" class="displayInLine">
                    <spring:message code="create"></spring:message>&nbsp;<spring:message code="viaje"></spring:message>
                    </h3>
                </div>        
                <div class="modal-body">
                <form name="newPaqueteForm" novalidate="">
                        <div>
                            <label>Id Bitácora:{{bitacora.id}}</label>
                        </div>                    
                        <div>
                            <label>* <spring:message code="paquete.empresa"></spring:message>:</label>
                            <ui-select theme="selectize" ng-model="paquete.idEmpresa" title="Empresa" name="empresa" ng-required="true">
                                <ui-select-match placeholder="Seleccionar empresa" class="ui-select-match">
                                    <span ng-bind="$select.selected.descripcion"></span>
                                </ui-select-match>
                                <ui-select-choices repeat="obj in empresas | filter: $select.search" class="ui-select-choices">
                                  <span ng-bind-html="obj.id+' | '+obj.descripcion | highlight: $select.search"></span>
                                </ui-select-choices>
                            </ui-select>    
                            <span class="text-danger" ng-show="displayValidationError &amp;&amp; newPaqueteForm.empresa.$error.required">Requerido</span>
                            <span class="text-danger" ng-show="displayValidationError &amp;&amp; newPaqueteForm.empresa.invalid">Invalido</span>                                                                                                                      
                        </div>
                        <div ng-show="!isSecondPaquete">    
                            <input type="checkbox" ng-model="isgnkembarque" name="register" id="checkbox_new_embarque" class="pull-left" ng-change="clickEventCheck(isgnkembarque)"/>
                            <label for="checkbox_new_embarque">Generar No. Embarque?</label>
                        </div>
                        <div>
                            <label>* <spring:message code="paquete.noembarque"></spring:message>:</label>
                            <input class="form-control" required="" autofocus="" ng-model="paquete.noEmbarque" name="noEmbarque" placeholder="&nbsp;<spring:message code='paquete.noembarque'></spring:message>" type="text" ng-disabled="isgnkembarque" ng-readonly="isgnkembarque" ng-blur="searchNoEmbarqueFase()">
                            <span class="text-danger" ng-show="displayValidationError &amp;&amp; newPaqueteForm.noEmbarque.$error.required">Requerido</span>
                            <span class="text-danger" ng-show="displayValidationError &amp;&amp; newPaqueteForm.noEmbarque.invalid">Invalido</span>                                                                                                    
                        </div>
                            <!-- envio, noembarque,fecha entrega-->
                            <!-- autocomplete : origen, destino, operador-->
                            <!-- combobox tipo-->
                        <div>
                            <label>* <spring:message code="paquete.id_puntoorigen"></spring:message>:</label>
                            <ui-select ng-model="paquete.idPuntoorigen" data-ng-disabled="disabled" name="idptoorigen" ng-required="true">
                              <ui-select-match placeholder="Select origen..." class="ui-select-match">
                                   <small ng-bind="$select.selected.nombrePunto+' | '+$select.selected.direccion"></small>
                              </ui-select-match>
                              <ui-select-choices class="ui-select-choices" refresh="getOrigenes($select)" repeat="obj in origenes">
                                <div ng-bind-html="obj.nombrePunto | highlight: $select.search"></div>
                                <small ng-bind-html="obj.direccion | highlight: $select.search"></small>
                              </ui-select-choices>
                            </ui-select>
                            <span class="text-danger" ng-show="displayValidationError &amp;&amp; newPaqueteForm.idptoorigen.$error.required">Requerido</span>
                            <span class="text-danger" ng-show="displayValidationError &amp;&amp; newPaqueteForm.idptoorigen.invalid">Invalido</span>                                                                                                    
                        </div>
                        <div>
                            <label>* <spring:message code="paquete.idpuntoe"></spring:message>:</label>
                            <ui-select ng-model="paquete.idPuntoe" data-ng-disabled="disabled" name="idpuntoe" ng-required="true">
                              <ui-select-match placeholder="Select punto entrega..." class="ui-select-match">
                                   <small ng-bind="$select.selected.nombrePunto+' | '+$select.selected.direccion"></small>
                              </ui-select-match>
                              <ui-select-choices class="ui-select-choices" refresh="getEntrega($select)" repeat="obj in entregas">
                                <div ng-bind-html="obj.nombrePunto | highlight: $select.search"></div>
                                <small ng-bind-html="obj.direccion | highlight: $select.search"></small>
                              </ui-select-choices>
                            </ui-select>  
                            <span class="text-danger" ng-show="displayValidationError &amp;&amp; newPaqueteForm.idpuntoe.$error.required">Requerido</span>
                            <span class="text-danger" ng-show="displayValidationError &amp;&amp; newPaqueteForm.idpuntoe.invalid">Invalido</span>                                                                        
                        </div>    
                        <div>
                            <label>* <spring:message code="paquete.idOperador"></spring:message>:</label>
                            <!--<ui-select ng-model="paquete.idOperador" data-ng-disabled="disabledOperador" name="idoperador" ng-required="true">-->
                            <ui-select ng-model="paquete.idOperador" name="idoperador" ng-required="true" on-select="onSelectOperadorForm($item, $model);">
                              <ui-select-match placeholder="Select operador..." class="ui-select-match">
                                   <span ng-bind="$select.selected.nombre+' | '+$select.selected.nombre+' '+$select.selected.apellidoPat+' '+$select.selected.apellidoMat"></span>
                              </ui-select-match>
                              <ui-select-choices class="ui-select-choices" refresh="getOperador($select)" repeat="obj in operadores">
                                <span ng-bind-html="obj.usuario+' | '+obj.nombre+' '+obj.apellidoPat+' '+obj.apellidoMat | highlight: $select.search"></span>
                              </ui-select-choices>
                            </ui-select>
                            <span class="text-danger" ng-show="displayValidationError &amp;&amp; newPaqueteForm.idoperador.$error.required">Requerido</span>
                            <span class="text-danger" ng-show="displayValidationError &amp;&amp; newPaqueteForm.idoperador.invalid">Invalido</span>                                                                        
                            
                            <!--<div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationError &amp;&amp; newPaqueteForm.idoperador.$error.required">
                                <spring:message code="required"></spring:message>
                            </div>-->
                        </div>   
                        <div>
                            <div class="row">
                                <div class="col-md-6">
                                    <p class="input-group col-md-6">
                                        <label>* <spring:message code="paquete.vehiculo"></spring:message>:</label>
                                        <ui-select theme="bootstrap" ng-model="paquete.idVehiculo" title="Vehiculo" name="vehiculo" ng-required="true">
                                            <ui-select-match placeholder="Seleccionar vehiculo" class="ui-select-match">
                                                <span ng-bind="$select.selected.noEconomico"></span>
                                            </ui-select-match>
                                            <ui-select-choices repeat="obj in vehiculolist | filter: $select.search" class="ui-select-choices">
                                              <span ng-bind-html="obj.noEconomico | highlight: $select.search"></span>
                                              <small ng-bind-html="obj.descripcion | highlight: $select.search"></small>
                                            </ui-select-choices>
                                        </ui-select>                                         
                                        <span class="text-danger" ng-show="displayValidationError &amp;&amp; newPaqueteForm.vehiculo.$error.required">Requerido</span>
                                        <span class="text-danger" ng-show="displayValidationError &amp;&amp; newPaqueteForm.vehiculo.invalid">Invalido</span>                                                                        
                                
                                        <!--<div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationError && newPaqueteForm.vehiculo.$error.required">
                                        <spring:message code="required"></spring:message>
                                        </div>-->
                                    </p>
                                </div>
                                <div class="col-md-4">
                                    <label>* <spring:message code="paquete.notarimas"></spring:message>:</label>
                                    <input type="text" ng-model="paquete.noTarimas" ng-required="true" name="notarimas"/>
                                    <span class="text-danger" ng-show="displayValidationError && newPaqueteForm.notarimas.$error.required">Requerido</span>
                                    <span class="text-danger" ng-show="displayValidationError && newPaqueteForm.notarimas.invalid">Invalido</span>                                                                                                            
                                </div>                                    
                            </div>
                        </div>
                        <div>
                            <label>* <spring:message code="paquete.cajas"></spring:message>:</label>
                            <input type="text" ng-model="paquete.noCajas" ng-required="true" name="nocajas"/>
                            <span class="text-danger" ng-show="displayValidationError && newPaqueteForm.nocajas.$error.required">Requerido</span>
                            <span class="text-danger" ng-show="displayValidationError && newPaqueteForm.nocajas.invalid">Invalido</span>                                                                                                                                                
                        </div>            
                        <div class="row">
                            <label class="col-sm-3">* <spring:message code="paquete.cita"></spring:message>:</label>
                            <label class="col-sm-3">
                                <input type="radio" ng-model="paquete.cita" value="1" name="cita">&nbsp;Si
                            </label>
                            <label class="col-sm-3">
                                <input type="radio" ng-model="paquete.cita" value="0" name="cita">&nbsp;No
                            </label>
                            <div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationError &amp;&amp; newPaqueteForm.cita.$invalid">
                                <spring:message code="required"></spring:message>
                            </div>                            
                        </div>                                        
                        <div>
                            <label>* <spring:message code="paquete.fecrecpaq"></spring:message>:</label>                            
                            <div class="row">
                                <div class="col-md-6">
                                    <p class="input-group " style="margin-top: 32px;">
                                        <input type="text" class="form-control" uib-datepicker-popup="{{format}}" ng-model="paquete.fechaRecPaq" is-open="popup1.opened" datepicker-options="dateOptions" ng-required="true" name="fecreceppaq" close-text="Close" ng-readonly="true"/>
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-default" ng-click="open1()"><i class="glyphicon glyphicon-calendar"></i></button>
                                        </span>
                                    </p>                                   
                                    <span class="text-danger" ng-show="displayValidationError &amp;&amp; newPaqueteForm.fecreceppaq.$error.required">Requerido</span>
                                    <span class="text-danger" ng-show="displayValidationError &amp;&amp; newPaqueteForm.fecreceppaq.invalid">Invalido</span>                                    
                                </div>
                                <div class="col-md-4">
                                    <p class="input-group">
                                    <uib-timepicker ng-model="paquete.fechaRecPaq" hour-step="1" minute-step="1" name="sTime" show-meridian="false" show-spinners="true" readonly-input="false" name="hourfecrec" ng-required="true" ></uib-timepicker>
                                    </p>
                                    <span class="text-danger" ng-show="displayValidationError &amp;&amp; newPaqueteForm.hourfecrec.$error.required">Requerido</span>
                                    <span class="text-danger" ng-show="displayValidationError &amp;&amp; newPaqueteForm.hourfecrec.invalid">Invalido</span>                                                                        
                                </div>
                                
                                <!--<span class="text-danger" ng-show="displayValidationError &amp;&amp; newPaqueteForm.fecreceppaq.$error.required">
                                    <spring:message code="required"></spring:message>
                                </span>-->
                            </div>
                        </div>                                
                        <div>
                            <label>* <spring:message code="paquete.fecenrtregapaq"></spring:message>:</label>
                            <div class="row">
                                <div class="col-md-6">
                                    <p class="input-group" style="margin-top: 32px;">
                                        <input type="text" class="form-control" uib-datepicker-popup="{{format}}" ng-model="paquete.fechaEntPaq" is-open="popup2.opened" datepicker-options="{minDate: paquete.fechaRecPaq}" ng-required="true" close-text="Close" alt-input-formats="altInputFormats" name="fecenrtregapaq" ng-readonly="true"/>
                                        <span class="input-group-btn">
                                          <button type="button" class="btn btn-default" ng-click="open2()"><i class="glyphicon glyphicon-calendar"></i></button>
                                        </span>
                                    </p>
                                    <span class="text-danger" ng-show="displayValidationError &amp;&amp; newPaqueteForm.fecenrtregapaq.$error.required">Requerido</span>
                                    <span class="text-danger" ng-show="displayValidationError &amp;&amp; newPaqueteForm.fecenrtregapaq.invalid">Invalido</span>                                                                        
                                </div>
                                <div class="col-md-4">
                                    <p class="input-group">
                                    <uib-timepicker ng-model="paquete.fechaEntPaq" hour-step="1" minute-step="1" name="sTime2" show-meridian="false" show-spinners="true" readonly-input="false"></uib-timepicker>
                                    </p>
                                </div>                                
                                <!--<div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationError &amp;&amp; newPaqueteForm.fecenrtregapaq.$error.required">
                                    <spring:message code="required"></spring:message>
                                </div>-->
                            </div>
                        </div>                                
                        <div>
                            <label>* <spring:message code="paquete.tipoEnvio"></spring:message>:</label>
                            <ui-select theme="bootstrap" ng-model="envioselected.tipo" ng-disabled="disabled"  title="Tipo Envio" name="tipoenvioform" ng-required="true" on-select="onSelectTipoEnvioCallback($item, $model);">
                                <ui-select-match placeholder="Seleccionar tipo de envio" class="ui-select-match">
                                    <span ng-bind="$select.selected.desEnvio"></span>
                                </ui-select-match>
                                <ui-select-choices repeat="obj in tipoenvio | filter: $select.search" class="ui-select-choices">
                                  <span ng-bind-html="obj.desEnvio | highlight: $select.search"></span>
                                </ui-select-choices>
                             </ui-select> 
                            <span class="text-danger" ng-show="displayValidationError &amp;&amp; newPaqueteForm.tipoenvioform.$error.required">Requerido</span>
                            <span class="text-danger" ng-show="displayValidationError &amp;&amp; newPaqueteForm.tipoenvioform.invalid">Invalido</span>                                                                        
                            
                            <!--<div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationError &amp;&amp; newPaqueteForm.tipoenvioform.$invalid">
                                <spring:message code="required"></spring:message>
                            </div>-->
                        </div>
                </form>
                </div>
                <div class="modal-footer">
                    <input class="form-control" name="${_csrf.parameterName}" value="${_csrf.token}" type="hidden">                                
                    <input class="btn btn-primary" ng-click="createPaquete(newPaqueteForm);" value="<spring:message code="create"></spring:message>" type="submit">
                    <button class="btn btn-default" data-dismiss="modal" ng-click="exit('#addPaqueteModal');" aria-hidden="true">
                        <spring:message code="cancel"></spring:message>
                    </button>
                </div>
        </div>
    </div>
</div>

<div id="updatePaqueteModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 id="updatePaqueteModalLabel" class="displayInLine">
                    <spring:message code="update"></spring:message>&nbsp;<spring:message code="viaje"></spring:message>
                    </h3>
                </div>        
                <div class="modal-body">
                <form name="updatePaqueteForm" novalidate="">
                        <div>
                            <label>* <spring:message code="paquete.empresa"></spring:message>:</label>
                            <select name="empresa" class="form-control" ng-model="paquete.idEmpresa" ng-options="opt.descripcion for opt in empresas" required ></select>
                            <span class="text-danger" ng-show="displayValidationError &amp;&amp; updatePaqueteForm.empresa.$error.required">Requerido</span>
                            <span class="text-danger" ng-show="displayValidationError &amp;&amp; updatePaqueteForm.empresa.invalid">Invalido</span>                                                                                                    
                            <!--<div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationError && newPaqueteForm.empresa.$error.required">
                            <spring:message code="required"></spring:message>
                            </div>-->                             
                        </div>                    
                        <div>
                            <label>* <spring:message code="paquete.noembarque"></spring:message>:</label>
                            <input class="form-control" required="" autofocus="" ng-model="paquete.noEmbarque" name="name" placeholder="&nbsp;<spring:message code='paquete.noembarque'></spring:message>" type="text" ng-readonly="true" ng-required="true">
                            <span class="text-danger" ng-show="displayValidationError &amp;&amp; updatePaqueteForm.name.$error.required">Requerido</span>
                            <span class="text-danger" ng-show="displayValidationError &amp;&amp; updatePaqueteForm.name.invalid">Invalido</span>                                                                                                                                
                            <!--<div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationError &amp;&amp; newPaqueteForm.noEmbarque.$error.required">
                                <spring:message code="required"></spring:message>
                            </div>-->
                        </div>
                            <!--<pre>{{disable}} {{$scope.disable}}</pre>-->
                        <div>
                            <label>* <spring:message code="paquete.id_puntoorigen"></spring:message>:</label>
                            <ui-select ng-model="paquete.idPuntoorigen" data-ng-disabled="disable" ng-required="true" name="idpuntoorigen">
                              <ui-select-match placeholder="Select origen..." class="ui-select-match">
                                   <small ng-bind="$select.selected.nombrePunto+' | '+$select.selected.direccion"></small>
                              </ui-select-match>
                              <ui-select-choices class="ui-select-choices" refresh="getOrigenes($select)" repeat="obj in origenes">
                                <div ng-bind-html="obj.nombrePunto | highlight: $select.search"></div>
                                <small ng-bind-html="obj.direccion | highlight: $select.search"></small>
                              </ui-select-choices>
                            </ui-select>
                            <span class="text-danger" ng-show="displayValidationError &amp;&amp; updatePaqueteForm.idpuntoorigen.$error.required">Requerido</span>
                            <span class="text-danger" ng-show="displayValidationError &amp;&amp; updatePaqueteForm.idpuntoorigen.invalid">Invalido</span>                                                                                                                                
                            <!--<div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationError &amp;&amp; updatePaqueteForm.idpuntoorigen.$invalid">
                                <spring:message code="required"></spring:message>
                            </div>-->
                        </div>
                        <div>
                            <label>* <spring:message code="paquete.idpuntoe"></spring:message>:</label>
                            <ui-select ng-model="paquete.idPuntoe" data-ng-disabled="disable" ng-required="true" name="idpuntoe">
                              <ui-select-match placeholder="Select punto entrega..." class="ui-select-match">
                                   <small ng-bind="$select.selected.nombrePunto+' | '+$select.selected.direccion"></small>
                              </ui-select-match>
                              <ui-select-choices class="ui-select-choices" refresh="getEntrega($select)" repeat="obj in entregas">
                                <div ng-bind-html="obj.nombrePunto | highlight: $select.search"></div>
                                <small ng-bind-html="obj.direccion | highlight: $select.search"></small>
                              </ui-select-choices>
                            </ui-select>
                            <span class="text-danger" ng-show="displayValidationError &amp;&amp; updatePaqueteForm.idpuntoe.$error.required">Requerido</span>
                            <span class="text-danger" ng-show="displayValidationError &amp;&amp; updatePaqueteForm.idpuntoe.invalid">Invalido</span>                                                                                                                                                            
                            <!--<div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationError &amp;&amp; updatePaqueteForm.idpuntoe.$invalid">
                                <spring:message code="required"></spring:message>
                            </div>-->
                        </div>    
                        <div>
                            <label>* <spring:message code="paquete.idOperador"></spring:message>:</label>
                        <!-- <ui-select ng-model="paquete.idOperador" data-ng-disabled="disabledOperador" ng-required="true" name="idoperador"> -->
                            <ui-select ng-model="paquete.idOperador"  ng-required="true" name="idoperador">
                              <ui-select-match placeholder="Select operador..." class="ui-select-match">
                                   <span ng-bind="$select.selected.nombre+' | '+$select.selected.nombre+' '+$select.selected.apellidoPat+' '+$select.selected.apellidoMat"></span>
                              </ui-select-match>
                              <ui-select-choices class="ui-select-choices" refresh="getOperador($select)" repeat="obj in operadores">
                                <span ng-bind-html="obj.usuario+' | '+obj.nombre+' '+obj.apellidoPat+' '+obj.apellidoMat  | highlight: $select.search"></span>
                              </ui-select-choices>
                            </ui-select>
                            <span class="text-danger" ng-show="displayValidationError &amp;&amp; updatePaqueteForm.idoperador.$error.required">Requerido</span>
                            <span class="text-danger" ng-show="displayValidationError &amp;&amp; updatePaqueteForm.idoperador.invalid">Invalido</span>                                                                                                                                                                                    
                            <!--<div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationError &amp;&amp; updatePaqueteForm.idoperador.$invalid">
                                <spring:message code="required"></spring:message>
                            </div>-->
                        </div>
                        <div>
                            <div class="row">
                                <div class="col-md-6">
                                    <p class="input-group col-md-6">
                                        <label>* <spring:message code="paquete.vehiculo"></spring:message>:</label>
                                        <ui-select theme="bootstrap" ng-model="paquete.idVehiculo" title="Vehiculo" name="vehiculo" ng-required="true">
                                            <ui-select-match placeholder="Seleccionar vehiculo" class="ui-select-match">
                                                <span ng-bind="$select.selected.noEconomico"></span>
                                            </ui-select-match>
                                            <ui-select-choices repeat="obj in vehiculolist | filter: $select.search" class="ui-select-choices">
                                              <span ng-bind-html="obj.noEconomico | highlight: $select.search"></span>
                                              <small ng-bind-html="obj.descripcion | highlight: $select.search"></small>
                                            </ui-select-choices>
                                        </ui-select>                                         
                                        <span class="text-danger" ng-show="displayValidationError &amp;&amp; updatePaqueteForm.vehiculo.$error.required">Requerido</span>
                                        <span class="text-danger" ng-show="displayValidationError &amp;&amp; updatePaqueteForm.vehiculo.invalid">Invalido</span>                                                                                                                                                                                                                    
                                        <!--<div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationError && newPaqueteForm.vehiculo.$error.required">
                                        <spring:message code="required"></spring:message>
                                        </div>-->
                                    </p>
                                </div>
                                <div class="col-md-4">
                                    <label>* <spring:message code="paquete.notarimas"></spring:message>:</label>
                                    <input type="text" ng-model="paquete.noTarimas" ng-required="true" name="notarimas"/>
                                    <span class="text-danger" ng-show="displayValidationError && newPaqueteForm.notarimas.$error.required">Requerido</span>
                                    <span class="text-danger" ng-show="displayValidationError && newPaqueteForm.notarimas.invalid">Inválido</span>                                                                                                                                                                                    
                                </div>
                                    
                                <div class="col-md-4">
                                    <label>* <spring:message code="paquete.cajas"></spring:message>:</label>
                                    <input type="text" ng-model="paquete.noCajas" ng-required="true" name="nocajas"/>
                                    <span class="text-danger" ng-show="displayValidationError && newPaqueteForm.nocajas.$error.required">Requerido</span>
                                    <span class="text-danger" ng-show="displayValidationError && newPaqueteForm.nocajas.invalid">Inválido</span>                                                                                                                                                
                                </div>                                     
                            </div>
                        </div> 
                        <div class="row">
                            <label class="col-sm-3">* <spring:message code="paquete.cita"></spring:message>:</label>
                            <label class="col-sm-3">
                                <input type="radio" ng-model="paquete.cita" value="1">&nbsp;Si
                            </label>
                            <label class="col-sm-3">
                                <input type="radio" ng-model="paquete.cita" value="0">&nbsp;No
                            </label>
                        </div>                                        
                        <div>
                            <label>* <spring:message code="paquete.fecrecpaq"></spring:message>:</label>                            
                            <div class="row">
                                <div class="col-md-6">
                                    <p class="input-group " style="margin-top: 32px;">
                                        <input type="text" class="form-control" uib-datepicker-popup="{{format}}" ng-model="paquete.fechaRecPaq" is-open="popup1.opened" datepicker-options="dateOptions" ng-required="true" name="fecreceppaq" close-text="Close" ng-readonly="true"/>
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-default" ng-click="open1()"><i class="glyphicon glyphicon-calendar"></i></button>
                                        </span>
                                    </p>
                                    <span class="text-danger" ng-show="displayValidationError &amp;&amp; updatePaqueteForm.fecreceppaq.$error.required">Requerido</span>
                                    <span class="text-danger" ng-show="displayValidationError &amp;&amp; updatePaqueteForm.fecreceppaq.invalid">Invalido</span>                                                                                                                                                                                                                                                        
                                </div>
                                <div class="col-md-4">
                                    <p class="input-group">
                                    <uib-timepicker ng-model="paquete.fechaRecPaq" hour-step="1" minute-step="1" name="sTime" show-meridian="false" show-spinners="true" readonly-input="false"></uib-timepicker>
                                    </p>
                                </div>
                                <div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationError &amp;&amp; updatePaqueteForm.fecreceppaq.$error.required">
                                    <spring:message code="required"></spring:message>
                                </div>                                
                            </div>
                        </div>                                
                        <div>
                            <label>* <spring:message code="paquete.fecenrtregapaq"></spring:message>:</label>
                            <div class="row">
                                <div class="col-md-6">
                                    <p class="input-group" style="margin-top: 32px;">
                                        <input type="text" class="form-control" uib-datepicker-popup="{{format}}" ng-model="paquete.fechaEntPaq" is-open="popup2.opened" datepicker-options="{minDate: paquete.fechaRecPaq}" ng-required="true" close-text="Close" alt-input-formats="altInputFormats" name="fecenrtregapaq" ng-readonly="true"/>
                                        <span class="input-group-btn">
                                          <button type="button" class="btn btn-default" ng-click="open2()"><i class="glyphicon glyphicon-calendar"></i></button>
                                        </span>
                                    </p>
                                    <span class="text-danger" ng-show="displayValidationError &amp;&amp; updatePaqueteForm.fecenrtregapaq.$error.required">Requerido</span>
                                    <span class="text-danger" ng-show="displayValidationError &amp;&amp; updatePaqueteForm.fecenrtregapaq.invalid">Invalido</span>                                                                                                                                                                                                                                                                                            
                                </div>
                                <div class="col-md-4">
                                    <p class="input-group">
                                    <uib-timepicker ng-model="paquete.fechaEntPaq" hour-step="1" minute-step="1" name="sTime2" show-meridian="false" show-spinners="true" readonly-input="false"></uib-timepicker>
                                    </p>
                                </div>                                
                                <div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationError &amp;&amp; newPaqueteForm.fecenrtregapaq.$error.required">
                                    <spring:message code="required"></spring:message>
                                </div>
                            </div>                                
                        </div>        
                        <div>
                            <label>* <spring:message code="paquete.tipoEnvio"></spring:message>:</label>
                            <ui-select ng-model="envioselected.tipo" ng-disabled="disable"  title="Tipo Envio" name="tipoenvioform" ng-required="true">
                                <ui-select-match placeholder="Seleccionar tipo de envio" class="ui-select-match">
                                    <span ng-bind="$select.selected.desEnvio"></span>
                                </ui-select-match>
                                <ui-select-choices repeat="obj in tipoenvio | filter: $select.search" class="ui-select-choices">
                                  <span ng-bind-html="obj.desEnvio | highlight: $select.search"></span>
                                </ui-select-choices>
                             </ui-select> 
                            <span class="text-danger" ng-show="displayValidationError &amp;&amp; updatePaqueteForm.tipoenvioform.$error.required">Requerido</span>
                            <span class="text-danger" ng-show="displayValidationError &amp;&amp; updatePaqueteForm.tipoenvioform.invalid">Invalido</span>                                                                                                                                                                                                                                                                                                                        
                            <!--<div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationError &amp;&amp; updatePaqueteForm.tipoenvioform.$error.required">
                                <spring:message code="required"></spring:message>
                            </div>-->
                        </div>
                </form>
                </div>
                <div class="modal-footer">
                    <input class="form-control" name="${_csrf.parameterName}" value="${_csrf.token}" type="hidden">                                
                    <input class="btn btn-primary" ng-click="updatePaquete(updatePaqueteForm);" value="<spring:message code="update"></spring:message>" type="submit" data-dismiss="modal">
                    <button class="btn btn-default" data-dismiss="modal" ng-click="exit('#updateContactsModal');" aria-hidden="true">
                        <spring:message code="cancel"></spring:message>
                    </button>
            </div>
        </div>
    </div>
</div>
                                
                
<div id="deletePaqueteModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 id="deletePaqueteModalLabel" class="displayInLine">
                    <spring:message code="cancel"/>&nbsp;<spring:message code="viaje"/>
                </h3>
            </div>                
            <div class="modal-body">
                <form name="deletePaqueteForm" novalidate="">
                    <div>
                        <p><spring:message code="delete.confirm"/>:&nbsp;{{paquete.noEmbarque}}?</p>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <input class="form-control" name="${_csrf.parameterName}" value="${_csrf.token}" type="hidden">                                
                <input type="submit" class="btn btn-primary" ng-click="deletePaquete();" value='<spring:message code="delete"/>' data-dismiss="modal"/>
                <button class="btn btn-default" data-dismiss="modal" ng-click="exit('#deletePaqueteModal');" aria-hidden="true">
                    <spring:message code="cancel"/>
                </button>                 
            </div>
        </div>
    </div>
</div>          
                
<div id="actPaqueteModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 id="deletePaqueteModalLabel" class="displayInLine">
                    <spring:message code="header.paqueteact"/>&nbsp;
                </h3>
            </div>                
            <div class="modal-body">
                <form name="newHistPaqueteForm" novalidate="" enctype="multipart/form-data">
                    <div>
                        <label>Siguiente Estatus: </label>&nbsp;{{nameNextStatus}}
                    </div> 
                    <div>
                     <label>* <spring:message code="paquetehist.observaciones"></spring:message>:</label>
                     <textarea class="form-control" rows="5" required="" autofocus="" name="observacionesHist" ng-model="historico.observaciones"></textarea>
                     <div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationError &amp;&amp; newHistPaqueteForm.observacionesHist.$error.required">
                        <spring:message code="required"></spring:message>
                     </div>
                    </div> 
                    <div> 
                        <label>* Cargar Archivos:</label>
                        <button class="btn btn-default" ngf-select="uploadFiles($files, $invalidFiles)" multiple accept="image/*" ngf-max-size="20MB">
                            Seleccionar archivos
                        </button>
                        <br><br>
                        <label>Archivos:</label>
                        <ul>
                          <li ng-repeat="file in f" style="font:smaller">{{file.name}} {{file.$errorParam}}
                            <span class="progress" ng-show="f.progress >= 0">
                              <div style="width:{{file.progress}}%"  
                                  ng-bind="file.progress + '%'"></div>
                            </span>
                          </li>
                          <li ng-repeat="f in errFiles" style="font:smaller">{{f.name}} {{f.$error}} {{f.$errorParam}}
                          </li> 
                        </ul>
                        {{errorMsg}}                        
                    </div>
                    <div ng-show="nextStatus.numStatus==3 && bitacoraStatus.idStatus.numStatus!=5">
                        <label>* <spring:message code="bitacora.kilometraje"></spring:message>:</label>
                        <input class="form-control" ng-required="true" ng-model="kmActual" name="kilometraje" placeholder="&nbsp;<spring:message code='bitacora.kilometraje'></spring:message>" type="text" only-digits>
                        <span class="text-danger" ng-show="displayValidationError && updateStatusBitacoraForm.kilometraje.$error.required">Requerido</span>
                        <span class="text-danger" ng-show="displayValidationError && updateStatusBitacoraForm.kilometraje.invalid">Inválido</span>                        
                    </div>                      
                </form>
            </div>
            <div class="modal-footer">
                <input class="form-control" name="${_csrf.parameterName}" value="${_csrf.token}" type="hidden">                                
                <input type="submit" class="btn btn-primary" ng-click="showConfirmChangeStatus(newHistPaqueteForm);" value='<spring:message code="update"/>'/>
                <button class="btn btn-default" data-dismiss="modal" ng-click="exit('#newHistPaqueteForm');" aria-hidden="true">
                    <spring:message code="cancel"/>
                </button>                 
            </div>
        </div>
    </div>
</div>
                
<div id="uploadImage" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 id="deletePaqueteModalLabel" class="displayInLine">
                    <spring:message code="header.uploadimages"/>&nbsp;
                </h3>
            </div>                
            <div class="modal-body">
                <form name="newImageHistPaqueteForm" novalidate="" enctype="multipart/form-data">
                    <div> 
                        <label>* Cargar Archivos:</label>
                        <button class="btn btn-default" ngf-select="uploadFiles($files, $invalidFiles)" multiple accept="image/*" ngf-max-size="20MB">
                            Seleccionar archivos
                        </button>
                        <br><br>
                        <label>Archivos:</label>
                        <ul>
                          <li ng-repeat="file in f" style="font:smaller">{{file.name}} {{file.$errorParam}}
                            <span class="progress" ng-show="f.progress >= 0">
                              <div style="width:{{file.progress}}%"  
                                  ng-bind="file.progress + '%'"></div>
                            </span>
                          </li>
                          <li ng-repeat="f in errFiles" style="font:smaller">{{f.name}} {{f.$error}} {{f.$errorParam}}
                          </li> 
                        </ul>
                        {{errorMsg}}                        
                    </div> 
                </form>
            </div>
            <div class="modal-footer">
                <input class="form-control" name="${_csrf.parameterName}" value="${_csrf.token}" type="hidden">                                
                <input type="submit" class="btn btn-primary" ng-click="addImageHistorico(newImageHistPaqueteForm,true);" value='<spring:message code="update"/>' data-dismiss="modal"/>
                <button class="btn btn-default" data-dismiss="modal" ng-click="exit('#newImageHistPaqueteForm');" aria-hidden="true">
                    <spring:message code="cancel"/>
                </button>                 
            </div>
        </div>
    </div>
</div>         