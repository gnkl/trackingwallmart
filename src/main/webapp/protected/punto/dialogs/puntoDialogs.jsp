<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div id="addPuntoModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <form name="newPuntoForm" novalidate="">
                    <div>
                        <label>* <spring:message code="punto.name"></spring:message>:</label>
                        <input class="form-control" required="" autofocus="" ng-model="punto.nombrePunto" name="pdescription" placeholder="&nbsp;<spring:message code='description'></spring:message>" type="text">
                            <div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationOError && newPuntoForm.pdescription.$error.required">
                            <spring:message code="required"></spring:message>
                            </div>
                        </div>
                        <div>
                            <label>* <spring:message code="direction"></spring:message>:</label>
                            <input class="form-control" required="" autofocus="" ng-model="punto.direccion" name="pdirection" placeholder="&nbsp;<spring:message code='direction'></spring:message>" type="text">
                            <div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationOError && newPuntoForm.pdirection.$error.required">
                            <spring:message code="required"></spring:message>
                            </div>
                        </div>
                        <div>
                            <label>* <spring:message code="punto.cp"></spring:message>:</label>
                            <input class="form-control" required="" autofocus="" ng-model="punto.cp" name="ppostalcode" placeholder="&nbsp;<spring:message code='postalcode'></spring:message>" type="text">
                            <div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationOError && newPuntoForm.ppostalcode.$error.required">
                            <spring:message code="required"></spring:message>
                            </div>
                        </div>                            
                        <div>
                            <label>* <spring:message code="estatus"></spring:message>:</label>
                            <select class="form-control" ng-model="punto.estatus" ng-options="opt.descripcion for opt in estatus" name="pestatus" required></select>
                            <div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationOError && newPuntoForm.pestatus.$error.required">
                            <spring:message code="required"></spring:message>
                            </div>                             
                        </div>
                        <div>
                            <label>* <spring:message code="punto.grupo"></spring:message>:</label>
                            <input class="form-control" required="" autofocus="" ng-model="punto.grupo" name="pgrupo" placeholder="&nbsp;<spring:message code='direction'></spring:message>" type="text">
                            <div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationOError && newPuntoForm.pgrupo.$error.required">
                            <spring:message code="required"></spring:message>
                            </div>
                        </div>
                        <!--<div>
                            <label>* <spring:message code="punto.cliente"></spring:message>:</label>
                        <input class="form-control" required="" autofocus="" ng-model="punto.cliente" name="pcliente" placeholder="&nbsp;<spring:message code='direction'></spring:message>" type="text">
                            <div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationOError && newPuntoForm.pcliente.$error.required">
                            <spring:message code="required"></spring:message>
                            </div>
                        </div>-->
                        <div>
                            <label>* <spring:message code="punto.ciudad"></spring:message>:</label>
                            <input class="form-control" required="" autofocus="" ng-model="punto.ciudad" name="pciudad" placeholder="&nbsp;<spring:message code='direction'></spring:message>" type="text">
                            <div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationOError && newPuntoForm.pciudad.$error.required">
                            <spring:message code="required"></spring:message>
                            </div>
                        </div>                                    
                        <div>
                            <label>* <spring:message code="punto.estado"></spring:message>:</label>
                            <input class="form-control" required="" autofocus="" ng-model="punto.estado" name="pestado" placeholder="&nbsp;<spring:message code='direction'></spring:message>" type="text">
                            <div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationOError && newPuntoForm.pestado.$error.required">
                            <spring:message code="required"></spring:message>
                            </div>
                        </div>                                    
                        <!--<div>
                            <label>* <spring:message code="punto.comision"></spring:message>:</label>
                            <input class="form-control" autofocus="" ng-model="punto.comision" name="pcomision" placeholder="&nbsp;<spring:message code='direction'></spring:message>" type="text">
                            <div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationOError && newPuntoForm.pcomision.$error.required">
                            <spring:message code="required"></spring:message>
                            </div>
                        </div>-->
                        <div>
                            <label>* <spring:message code="punto.telefono"></spring:message>:</label>
                            <input class="form-control" autofocus="" ng-model="punto.telefono" name="ptelefono" placeholder="&nbsp;<spring:message code='direction'></spring:message>" type="text">
                            <div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationOError && newPuntoForm.ptelefono.$error.required">
                            <spring:message code="required"></spring:message>
                            </div>
                        </div>                                    
                        <!--<div>
                            <label>* <spring:message code="punto.rfc"></spring:message>:</label>
                            <input class="form-control" autofocus="" ng-model="punto.rfc" name="prfc" placeholder="&nbsp;<spring:message code='direction'></spring:message>" type="text">
                            <div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationOError && newPuntoForm.prfc.$error.required">
                            <spring:message code="required"></spring:message>
                            </div>
                        </div>-->
                            
                        <div class="row">
                            <label class="col-sm-3">* <spring:message code="punto.tipo"></spring:message>:</label>
                            <label class="col-sm-3">
                                <input type="checkbox" ng-model="punto.origen" > Origen
                            </label>
                            <label class="col-sm-3">
                                <input type="checkbox" ng-model="punto.entrega"> Entrega
                            </label>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <input class="form-control" name="${_csrf.parameterName}" value="${_csrf.token}" type="hidden">
                <input class="btn btn-primary" ng-click="createPunto(newPuntoForm);" value="<spring:message code="save"></spring:message>" type="submit" data-dismiss="modal">
                    <button class="btn btn-default" data-dismiss="modal" ng-click="exit('#newPuntoForm');" aria-hidden="true">
                    <spring:message code="cancel"></spring:message>
                </button>
            </div>
        </div>
    </div>
</div>
                
<div id="proccesCsvFilePuntoModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 id="deletePaqueteModalLabel" class="displayInLine">
                    <spring:message code="header.uploadfile"/>&nbsp;
                </h3>
            </div>             
            <div class="modal-body">
                <form name="newFilePuntoForm" novalidate="">
                    <div> 
                        <label>* Cargar Archivo:</label>
                        <button class="btn btn-default" type="file" ngf-select="uploadFiles($file, $invalidFiles)" ngf-pattern="'.xlsx'" accept=".xlsx" >
                            Seleccionar archivo
                        </button>
                        <br><br>
                        <label>Archivo:</label>
                        <div >{{f.name}}
                            <span class="progress" ng-show="f.progress >= 0">
                                <div style="width:{{f.progress}}%"   ng-bind="f.progress + '%'"></div>
                            </span>
                        </div>
                        <div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="errFile.name!==null && errFile.name!==undefined">
                        <p>Error en el tipo de archivo</p>
                        <br>
                        <p>Debe ser un archivo del tipo: {{errFile.$errorParam}}</p>
                        </div>
                        <!--<pre>
                            {{errFile}}
                        </pre>-->
                    </div> 
                </form>                 
            </div>
            <div class="modal-footer">
                <input class="form-control" name="${_csrf.parameterName}" value="${_csrf.token}" type="hidden">                                
                <input type="submit" class="btn btn-primary" ng-click="proccesMultipartFile(newFilePuntoForm,true);" value='<spring:message code="procces.file"/>' data-dismiss="modal" ng-show="errFile.name!==null && (errFile.name===undefined || errFile.name==='')"/>
                <button class="btn btn-default" data-dismiss="modal" ng-click="exit('#newFilePuntoForm');" aria-hidden="true">
                    <spring:message code="cancel"/>
                </button>                 
            </div>                
        </div>
    </div>
</div>    