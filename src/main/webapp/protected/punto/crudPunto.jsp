<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<div class="row" ng-app="punto"  ng-controller="CrudPuntoCtrl as puntocrud">
    <div class="modal-body">
        <label><spring:message code="search"/></label>
        <hr>           
        <div class="form-horizontal">
            <form name="searchPuntoForm" novalidate>
                <fieldset>
                    <div class="form-group row">
                        <label class="col-sm-2 form-control-label" ><spring:message code="punto.nombre"/></label>
                        <div class="col-sm-10">
                            <ui-select ng-model="nombrePunto"  ng-required="true" name="idnombrepunto" on-select="onSelectNombreCallback($item, $model);" theme="selectize">
                              <ui-select-match placeholder="Select Nombre Punto" class="ui-select-match">
                                   <span ng-bind="$select.selected"></span>
                              </ui-select-match>
                              <ui-select-choices class="ui-select-choices" refresh="getStringByNombre($select)" repeat="obj in listnombrepunto">
                                <span ng-bind-html="obj | highlight: $select.search"></span>
                              </ui-select-choices>
                            </ui-select>
                            <div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationError &amp;&amp; updatePaqueteForm.idoperador.$invalid">
                                <spring:message code="required"></spring:message>
                            </div>
                        </div>
                    </div>                    
                    <div class="form-group row">
                        <label class="col-sm-2 form-control-label" ><spring:message code="punto.direccion"/></label>
                        <div class="col-sm-10">
                            <ui-select ng-model="direccion"  ng-required="true" name="iddireccionpunto" on-select="onSelectDireccionCallback($item, $model);" theme="selectize">
                              <ui-select-match placeholder="Seleccionar Direccion Punto" class="ui-select-match">
                                   <span ng-bind="$select.selected"></span>
                              </ui-select-match>
                              <ui-select-choices class="ui-select-choices" refresh="getStringByDireccion($select)" repeat="obj in listdireccionpunto">
                                <span ng-bind-html="obj | highlight: $select.search"></span>
                              </ui-select-choices>
                            </ui-select>
                            <div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationError &amp;&amp; updatePaqueteForm.idoperador.$invalid">
                                <spring:message code="required"></spring:message>
                            </div>                            
                        </div>
                    </div>                                            
                </fieldset>
            </form>     
            <!--<div class="pull-right" style="width: 100% !important;">
                <button class="btn btn-primary pull-right glyphicon glyphicon-search" ng-click="searchPunto(searchPuntoForm, false,0);"/>-->         
                <button class="btn btn-danger pull-right glyphicon glyphicon-erase" ng-click="resetSearch();"></button>    
            <!--</div>-->
        </div>     
        <hr>           
    </div>
    <sec:authorize access="hasAnyRole('ROLE_MASTER','ROLE_ADMINISTRADOR')">
        <div class="row" ng-class="{'text-center': displayCreateContactButton == true, 'none': displayCreateContactButton == false}">
            <a href="#addPuntoModal"
               role="button"
               ng-click="createNewPunto()"
               title="<spring:message code='create'/>&nbsp;<spring:message code='paquete'/>"
               class="btn btn-default pull-left"
               data-toggle="modal"
               ng-class="{'text-center': displayCreateContactButton == true, 'none': displayCreateContactButton == false}">
                <i class="glyphicon-plus"></i>
                &nbsp;&nbsp;<spring:message code="create"/>
            </a>
            <a href="#proccesCsvFilePuntoModal"
               role="button"
               ng-click="resetFiles()"
               title="<spring:message code='upload.file'/>"
               class="btn btn-default pull-left"
               data-toggle="modal"
               ng-class="{'text-center': displayCreateContactButton == true, 'none': displayCreateContactButton == false}">
                <i class="glyphicon-upload"></i>
                &nbsp;&nbsp;<spring:message code="upload.file"/>
            </a>
        </div>
    </sec:authorize>  
    <div id="tabPuntoOrigen" class="table-gnkl">
            <br/><br/>
            <div class="table-responsive pull-right" style="width: 100% !important;">
                <table id="1" class="table table-striped table-bordered" st-reset="isReset" st-table="origenCollection" st-pipe="serverOrigenFilter">
                    <thead>
                        <tr>
                            <th st-sort="id">ID</th>
                            <th st-sort="nombrePunto">Nombre</th>
                            <th st-sort="direccion">Dirección</th>
                            <!--<th st-sort="grupo">Grupo</th>-->
                            <th st-sort="grupo">No. Cliente</th>
                            <th>Tipo</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>            
                    <tbody>
                        <tr data-toggle="modal" data-target="#modal-source" ng-repeat="source in origenCollection">
                            <td>{{source.id}}</td>
                            <td>{{source.nombrePunto}}</td>
                            <td>{{source.direccion}}</td>
                            <!--<td>{{source.grupo}}</td>-->
                            <td>{{source.cliente}}</td>
                            <td>{{source.origen===1?'Origen':''}} {{source.entrega===1?'Entrega':''}}</td>
                            <td>
                                <div>
                                    <input type="hidden" value="{{source.id}}"/>
                                    <a href="#addPuntoModal"
                                       ng-click="selectPunto(source);"
                                       role="button"
                                       title="<spring:message code="delete"/>&nbsp;<spring:message code="contact"/>"
                                    class="btn btn-default glyphicon glyphicon-pencil" data-toggle="modal">
                                </a>
                            </div>   
                        </td>            
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="12">
                            <div class="pull-right" st-pagination="" st-items-by-page="itemsByPage"></div>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>    
    </div>
<jsp:include page="dialogs/puntoDialogs.jsp"/> 
</div>

<script type="text/javascript" src="${contextPath}/resources/js/pages/punto.js" /></script>