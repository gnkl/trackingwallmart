<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<div class="" ng-app="estadistica"  ng-controller="EstadisticaCtrl as estadisticactrl">
    
    <div class="panel panel-info" style="width: 100% !important;">
        <div class="panel-heading" data-toggle="collapse" data-target="#collapse4">
            <h3 class="panel-title">Total por estatus</h3>
        </div>        
        <div id="collapse4" >
            <div class="row ">
                <button class="btn btn-link pull-left" ng-csv="getArrayToTotalesCsv(bitacoraRecoleccionList)" csv-header="['Total','Estatus']" data-toggle="tooltip" data-placement="top" title="Exportar a Excel" filename="Reporte_Recoleccion_Detalle_Estatus.csv">
                  <span class="glyphicon glyphicon-share"></span>&nbsp;Exportar Recolección
                </button>
                <button class="btn btn-link pull-right" ng-csv="getArrayToTotalesCsv(bitacoraEntregaList)" csv-header="['Total','Estatus']" data-toggle="tooltip" data-placement="top" title="Exportar a Excel" filename="Reporte_Entrega_Detalle_Estatus.csv">
                  <span class="glyphicon glyphicon-share"></span>&nbsp;Exportar Entrega
                </button>                
            </div>

            <div id="graphicsBitacora" class="panel-body">
                <div class="col-md-6 col-lg-6" align="center">
                <div style="min-height: 250px;">
                    <h3 class="panel-title">Recolección</h3>
                    <table st-table="bitacorasRecoleccion" class="table table-striped">
                        <thead>
                            <tr>
                                <th>Estatus</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="row in bitacoraRecoleccionList">
                                <td>{{row.des_status}}</td>
                                <td>
                                    <a href="#showDetalleBitacoraClienteModal"
                                       ng-click="searchBitacoraListByStatusEnvio(row,1)"
                                       role="button"
                                       title="Ver detalle"
                                       class="" data-toggle="modal">
                                       {{row.total}}
                                    </a>                                          
                                </td>
                            </tr>
                        </tbody>
                        <tfoot></tfoot>           
                    </table>
                </div>
                    <nvd3 options="optionsbarRecoleccionConfchart" data="bitacoraRecoleccionList"></nvd3>
                </div>
                <div class="col-md-6 col-lg-6 col-sm-6" align="center" >
                    <div style="min-height: 250px;">
                    <h3 class="panel-title">Entrega</h3>
                    <table st-table="bitacoraEntrega" class="table table-striped right">
                        <thead>
                            <tr>
                                <th>Estatus</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr ng-repeat="row in bitacoraEntregaList">
                                <td>{{row.des_status}}</td>
                                <td>
                                    <a href="#showDetalleBitacoraClienteModal"
                                       ng-click="searchBitacoraListByStatusEnvio(row,3)"
                                       role="button"
                                       title="Ver detalle"
                                       class="" data-toggle="modal">
                                       {{row.total}}
                                    </a>                                     
                                </td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="5" class="text-center">
                                    <div st-pagination="" st-items-by-page="itemsByPage" st-displayed-pages="3"></div>
                                </td>
                            </tr>
                        </tfoot>           
                    </table>
                    </div>
                    <nvd3 options="optionsbarEntregaConfchart" data="bitacoraEntregaList"></nvd3>
                </div>
            </div>        
        </div>    
    </div>
    <sec:authorize access="hasAnyRole('ROLE_MASTER', 'ROLE_ADMINISTRADOR', 'ROLE_OPERADOR', 'ROLE_CLIENTE')">
    <div class="panel panel-info" style="width: 100% !important;">
        <div class="panel-heading" data-toggle="collapse" data-target="#collapse9">
            <h3 class="panel-title">Bitácoras realizadas por vehículo - operador</h3>
        </div>        
        <div id="collapse9" >
            <div class="panel-body">               
                <div class="form-group row">
                    <label class="col-sm-2 form-control-label"><spring:message code="bitacora.usuarios"></spring:message>:</label>
                    <div class="col-sm-10">
                        <ui-select theme="selectize" ng-model="bitacora.idOperador" data-ng-disabled="disabled" name="idoperador" on-select="onSelectIdOperadorCallback($item, $model);">
                            <ui-select-match placeholder="Seleccionar operador" class="ui-select-match">
                                <span ng-bind="$select.selected.usuario+' | '+$select.selected.nombre+' '+$select.selected.apellidoPat+' '+$select.selected.apellidoMat"></span>
                            </ui-select-match>
                            <ui-select-choices class="ui-select-choices" repeat="obj in operadores | filter: $select.search">
                                <span ng-bind-html="obj.usuario+' | '+obj.nombre+' '+obj.apellidoPat+' '+obj.apellidoMat | highlight: $select.search"></span>
                            </ui-select-choices>
                        </ui-select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 form-control-label"><spring:message code="bitacora.vehiculo"></spring:message>:</label>
                    <div class="col-sm-10">
                        <ui-select theme="selectize" ng-model="bitacora.idVehiculo" data-ng-disabled="disabled" name="idvehiculo" on-select="onSelectIdVehiculoCallback($item, $model);">
                            <ui-select-match placeholder="Seleccionar vehículo" class="ui-select-match">
                                <span ng-bind="$select.selected.noEconomico+' '+$select.selected.placas+' '+$select.selected.submarca"></span>
                            </ui-select-match>
                            <ui-select-choices class="ui-select-choices" repeat="obj in vehiculos | filter: $select.search">
                                <span ng-bind-html="obj.noEconomico+' '+obj.placas+' '+obj.submarca | highlight: $select.search"></span>
                            </ui-select-choices>
                        </ui-select>  
                    </div>
                </div>
                <div class="modal-footer">
                    <input class="btn btn-primary" ng-click="searchTotalByVehiculoOperador('');" value="<spring:message code="search"></spring:message>" type="submit">
                    <button class="btn btn-default" data-dismiss="modal" ng-click="clearSearchByVehiculoOperador()" aria-hidden="true">
                        Limpiar
                    </button>
                </div>                    
                <div class="form-group row">
                    <label class="col-sm-2 form-control-label">
                        Encontrados: 
                    </label>
                    <a href="#showDetallePaquetesModal"
                       ng-click="searchBitacoraListByVehiculoOperador()"
                       role="button"
                       title="Ver detalle"
                       class="" data-toggle="modal">
                       {{totalEncontrado}}
                    </a>                                             
                </div>                    

            </div>        
        </div>    
    </div>
                    
    </sec:authorize>
    <div class="panel panel-info" style="width: 100% !important;">
        <div class="panel-heading" data-toggle="collapse" data-target="#collapse1">
            <h3 class="panel-title">Bitácoras</h3>
        </div>        
        <div id="collapse1" >
            <div class="panel-body">
               <div class="form-group row">
                    <div class="col-md-6">
                        <label>Fecha inicial:</label>                            
                        <div class="row">
                            <div class="col-md-10">
                                <p class="input-group " style="margin-top: 32px;">
                                    <input type="text" class="form-control" uib-datepicker-popup="{{format}}" ng-model="fechaInicio" is-open="popup1.opened" datepicker-options="dateOptions" name="fechaInicio" close-text="Close" ng-readonly="true"/>
                                    <span class="input-group-btn">
                                        <button type="button" class="btn btn-default" ng-click="open1()"><i class="glyphicon glyphicon-calendar"></i></button>
                                    </span>
                                </p>
                            </div>                              
                        </div>
                    </div>                                
                    <div class="col-md-6">
                        <label>Fecha final:</label>
                        <div class="row">
                            <div class="col-md-10">
                                <p class="input-group" style="margin-top: 32px;">
                                    <input type="text" class="form-control" uib-datepicker-popup="{{format}}" ng-model="fechaFin" is-open="popup2.opened" datepicker-options="{minDate: fechaInicio}" close-text="Close" alt-input-formats="altInputFormats" name="fechaFin" ng-readonly="true"/>
                                    <span class="input-group-btn">
                                      <button type="button" class="btn btn-default" ng-click="open2()"><i class="glyphicon glyphicon-calendar"></i></button>
                                    </span>
                                </p>
                            </div>
                        </div>                                
                    </div>                            
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 form-control-label"><spring:message code="bitacora.usuarios"></spring:message>:</label>
                    <div class="col-sm-10">
                        <ui-select theme="selectize" ng-model="idOperador" data-ng-disabled="disabled" name="idoperador" on-select="onSelectIdOperadorCallback($item, $model);">
                            <ui-select-match placeholder="Seleccionar operador" class="ui-select-match">
                                <span ng-bind="$select.selected.usuario+' | '+$select.selected.nombre+' '+$select.selected.apellidoPat+' '+$select.selected.apellidoMat"></span>
                            </ui-select-match>
                            <ui-select-choices class="ui-select-choices" repeat="obj in operadores | filter: $select.search">
                                <span ng-bind-html="obj.usuario+' | '+obj.nombre+' '+obj.apellidoPat+' '+obj.apellidoMat | highlight: $select.search"></span>
                            </ui-select-choices>
                        </ui-select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 form-control-label"><spring:message code="paquete.tipoEnvio"></spring:message>:</label>
                    <div class="col-sm-10">
                        <ui-select theme="selectize" ng-model="idEnvio" ng-disabled="disabled"  title="Tipo Envio" name="rutaenvio" on-select="onSelectTipoEnvioCallback($item, $model);" >
                            <ui-select-match placeholder="Seleccionar tipo de envio" class="ui-select-match">
                                <span ng-bind="$select.selected.desEnvio"></span>
                            </ui-select-match>
                            <ui-select-choices repeat="obj in listTipoEnvioCollection | filter: $select.search" class="ui-select-choices">
                              <span ng-bind-html="obj.desEnvio | highlight: $select.search"></span>
                            </ui-select-choices>
                         </ui-select>                             
                    </div>
                </div>                    
                <div class="modal-footer">
                    <input class="btn btn-info glyphicon-save-file" ng-click="exportBitacoraReporteParams()" value="Exportar" type="submit">
                    <input class="btn btn-primary" ng-click="searchBitacoraFilter()" value="<spring:message code="search"></spring:message>" type="submit">
                    <button class="btn btn-default" data-dismiss="modal" ng-click="resetBitacoraSearch()" aria-hidden="true">Limpiar</button>
                </div>
                <div class="row">
                    <label>Total registros: 0</label>
                </div>
                <div class="table-gnkl">
                    <table class="table table-bordered" st-reset="isReset" st-table="leadsBitacoraCollection" st-pipe="serverBitacoraSearchFilter">
                        <thead>
                            <tr>
                                <th>No. Tienda</th>
                                <th>Fecha</th>
                                <th>Operador</th>
                                <th>Vehículo</th>
                                <th>Tipo</th>
                                <th>Tiempo</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>            
                        <tbody>
                            <tr data-toggle="modal" data-target="#modal-source" ng-repeat="source in leadsBitacoraCollection" ng-click="rowClick($index);" >
                                <td><b>{{source.nombre}}</b></td>
                                <td>{{source.fecha |  date:'yyyy-MM-dd'}}<br>{{source.fecha_fin |  date:'yyyy-MM-dd'}}</td>
                                <td>{{source.operador}}</td>
                                <td>
                                    <small>{{source.vehiculo}}</small>
                                </td>
                                <td>{{source.des_envio}}</td>
                                <td>{{source.total_time}}</td>
                                <td>
                                    <div class="input-group">
                                        <sec:authorize access="hasAnyRole('ROLE_CLIENTE')">
                                            <a ng-if="source.archivo !== null && source.archivo==1" href="" 
                                               ng-click="getImagen(source);" role="button"  
                                               class="btn btn-info glyphicon glyphicon-save-file"> 
                                            </a>
                                        </sec:authorize>
                                    </div>                                                      
                                </td>            
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="7">
                                    <div class="pull-right" st-pagination="" st-items-by-page="itemsByPage"></div>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                    
                <div class="table-gnkl">
                    <table class="table table-striped table-bordered" st-safe-src="leadsHistorico"  st-reset="isReset" st-table="leadsHistoricoCollection">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Fecha</th>
                                <th>Estatus</th>
                                <th>kilometraje</th>
                                <th>Tiempo(hr)</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>            
                        <tbody>
                            <tr data-toggle="modal" data-target="#modal-source" ng-repeat="source in leadsHistoricoCollection" ng-click="rowClickBitacora($index);">
                                <td>{{source.id}}</td>
                                <td ng-bind="source.fecha |  date:'yyyy-MM-dd HH:mm:ss'"></td>
                                <td>{{source.estatus.desStatus}}</td>
                                <td>{{source.kilometraje | number}}</td>       
                                <td>{{getTimePerRow($index)}}</td>
                                <td>
                                    <sec:authorize access="hasAnyRole('ROLE_MASTER', 'ROLE_ADMINISTRADOR', 'ROLE_OPERADOR')">
                                    <a href="#detailBitacoraStatus" ng-click="selectedStatusBitacora(source);" role="button"
                                       title="<spring:message code="actions"/>&nbsp;"
                                       class="btn btn-default glyphicon glyphicon glyphicon-file" data-toggle="modal">
                                    </a>
                                    </sec:authorize>   
                                    <a ng-if="source.archivos.length !== null && source.archivos.length>0" href="" 
                                       ng-click="getImagenHistorico(source);" role="button"  
                                       class="btn btn-info glyphicon glyphicon-save-file"> 
                                    </a>                               
                                </td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="10">
                                    <div class="pull-right" st-pagination="" st-items-by-page="itemsByPage"></div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="10">
                                    <label class="col-sm-6">TIEMPO TRANSCURRIDO (hr/min):&nbsp;{{totalTiempoMins}}</label>
                                    <label class="col-sm-6">KILOMETRAJE TRANSCURRIDO (km):&nbsp;{{totalKm | number}}</label>      
                                </td>
                            </tr>                    
                        </tfoot>
                    </table>
                </div>                    
            </div>        
        </div>    
    </div>
                    
    <div class="panel panel-info" style="width: 100% !important;">
        <div class="panel-heading" data-toggle="collapse" data-target="#collapse1">
            <h3 class="panel-title">Rutas</h3>
        </div>        
        <div id="collapse1" >
            <div class="panel-body">
                    <div class="form-group row">
                        <label class="col-sm-2 form-control-label">Ruta</label>
                        <div  class="col-sm-10">
                            <div>
                                <ui-select ng-model="rutaselected"  name="idruta" theme="selectize" on-select="onSelectIdRutaCallback($item, $model);">
                                  <ui-select-match placeholder="Seleccionar bitácora" class="ui-select-match">
                                       <span ng-bind="$select.selected.nombre+' '+$select.selected.idEnvio.desEnvio+' '+($select.selected.fechaCreacion |  date:'dd/MM/yyyy')"></span>
                                  </ui-select-match>
                                  <ui-select-choices class="ui-select-choices" refresh="getListRuta($select)" repeat="obj in listIdRuta">
                                    <small ng-bind-html="obj.nombre+' '+obj.idEnvio.desEnvio+' '+(obj.fechaCreacion |  date:'dd/MM/yyyy') | highlight: $select.search"></small>
                                  </ui-select-choices>
                                </ui-select>                        
                            </div>                    
                        </div>
                        <div class="modal-footer">
                            <input class="btn btn-info glyphicon-save-file" ng-click="exportRutaTiempos()" value="Exportar" type="submit">
                            <button class="btn btn-danger glyphicon glyphicon-erase" ng-click="resetSearchRutaTime();"></button>                                     
                        </div>
                    </div>
                <div class="table-gnkl">
                    <table class="table table-bordered" st-reset="isReset" st-safe-src="listRutaItemsSecure" st-table="listRutaItems">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Estatus</th>
                                <th>Tipo</th>
                                <th>Tiempo Neto</th>
                                <th>Interrupción</th>
                                <th>Tiempo total</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>            
                        <tbody>
                            <tr data-toggle="modal" data-target="#modal-source" ng-repeat="source in listRutaItems">
                                <td>{{source.nombre}}</td>
                                <td>{{source.estatus==1?'ACTIVO':'INACTIVO'}}</td>
                                <td>{{source.des_envio}}</td>
                                <td>{{source.tiempo_gral}}</td>
                                <td>{{source.tiempo_interrupcion}}</td>
                                <td>{{source.tiempo_total}}</td>
                                <td>
                                    <sec:authorize access="hasAnyRole('ROLE_MASTER', 'ROLE_ADMINISTRADOR', 'ROLE_OPERADOR')">
                                    <a href="" ng-click="rowRutaClick(source);" role="button"
                                       title="<spring:message code="actions"/>&nbsp;"
                                       class="btn btn-default glyphicon glyphicon glyphicon-check" data-toggle="modal">
                                    </a>
                                    </sec:authorize>
                                    <sec:authorize access="hasAnyRole('ROLE_MASTER', 'ROLE_ADMINISTRADOR', 'ROLE_OPERADOR')">
                                    <a href="" ng-click="rowRutaClickLastBitacora(source);" role="button"
                                       title="<spring:message code="actions"/>&nbsp;"
                                       class="btn btn-default glyphicon glyphicon glyphicon-dashboard" data-toggle="modal">
                                    </a>
                                    </sec:authorize>
                                </td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td colspan="7">
                                    <div class="pull-right" st-pagination="" st-items-by-page="itemsByPage"></div>
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <div class="table-gnkl">
                    <table class="table table-striped table-bordered" st-reset="isReset" st-table="leadsBitacoraLast">
                        <thead>
                            <tr>
                                <th>No. Tienda</th>
                                <th>Operador</th>
                                <th>Vehículo</th>
                                <th>Estatus</th>
                                <th>Fecha</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>            
                        <tbody>
                            <tr ng-repeat="source in leadsBitacoraLast">
                                <td><b>{{source.nombre}}</b></td>
                                <td>{{source.idOperador.nombre}}<br>{{source.idOperador.apellidoPat}}<br>{{source.idOperador.apellidoMat}}</td>
                                <td>{{source.idVehiculo.noEconomico}}<br>{{source.idVehiculo.placas}}</td>
                                <td>{{source.idStatus.desStatus}}</td>
                                <td>{{source.fecha | date:'yyyy-MM-dd HH:mm:ss'}}-{{source.fechaFin | date:'yyyy-MM-dd HH:mm:ss'}}</td>
                                <td>
                                    <a ng-if="source.archivos.length !== null && source.archivos.length>0" href="" 
                                       ng-click="getImagen(source);" role="button"  
                                       class="btn btn-info glyphicon glyphicon-save-file"> 
                                    </a>                                    
                                </td>
                            </tr>                        
                        </tbody>       
                        <tfoot>
                            <td colspan="9" class="text-center">
                                <div st-pagination="" st-items-by-page="itemsByPage" st-displayed-pages=""></div>
                            </td>                        
                        </tfoot>
                    </table>
                    
                </div>                                    
            </div>        
        </div>    
    </div>    
                    
    <jsp:include page="dialogs/rutaEstadisticaDialogs.jsp"/>
</div>

<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/ng-csv/0.3.6/ng-csv.min.js"></script>
<script type="text/javascript" src="${contextPath}/resources/js/pages/rutaEstadistica.js" /></script>