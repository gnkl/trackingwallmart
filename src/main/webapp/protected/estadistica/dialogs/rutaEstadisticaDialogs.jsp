<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
                                
<div id="showDetallePaquetesModal" class="modal fade" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h3 id="deletePaqueteModalLabel" class="displayInLine">
                    Listado de bitácoras
                </h3>
            </div>                
            <div class="modal-body" style="height:300px;overflow-y: scroll;">
                <button class="btn btn-link" ng-csv="getArrayToBeCsv()" csv-header="['Nombre','Operador','Veh&iacute;culo','Estatus','Kilometraje','Fecha','Tipo','Ruta']" filename="Detalle_Bitacoras_Operador_Vehiculo.csv" data-toggle="tooltip" data-placement="top" title="Exportar a Excel">
                    <span class="glyphicon glyphicon-share"></span>&nbsp;Exportar
                </button>    
                <div>
                    <table id="tableDetallePaquetesModal" class="table table-striped table-bordered" st-reset="isReset" st-table="leadsDetalleCollection" st-pipe="serverPaquetesDetalle">
                        <thead>
                            <tr>
                                <th>Nombre</th>
                                <th>Operador</th>
                                <th>Vehículo</th>
                                <th>Estatus</th>
                                <th>Kilometraje</th>
                                <th>Fecha</th>
                                <th>Tipo</th>
                                <th>Ruta</th>
                            </tr>
                        </thead>            
                        <tbody>
                            <tr ng-repeat="source in leadsDetalleCollection">
                                <td><b>{{source.nombre}}</b></td>
                                <td>{{source.idOperador.nombre}}<br>{{source.idOperador.apellidoPat}}<br>{{source.idOperador.apellidoMat}}</td>
                                <td>{{source.idVehiculo.noEconomico}}<br>{{source.idVehiculo.placas}}</td>
                                <td>{{source.idStatus.desStatus}}</td>
                                <td>{{source.kmInicial | number}}-{{source.kmFinal | number}}</td>
                                <td>{{source.fecha | date:'yyyy-MM-dd HH:mm:ss'}}-{{source.fechaFin | date:'yyyy-MM-dd HH:mm:ss'}}</td>                           
                                <td>{{source.idEnvio.desEnvio}}</td>
                                <td>{{source.idRuta.nombre}}</td>
                            </tr>                        
                        </tbody>       
                        <tfoot>
                            <td colspan="9" class="text-center">
                                <div st-pagination="" st-items-by-page="itemsByPage" st-displayed-pages=""></div>
                            </td>                        
                        </tfoot>
                    </table>
                </div>                    
            </div>
            <div class="modal-footer">
                <button class="btn btn-default" data-dismiss="modal" ng-click="exit('#showDetallePaquetesModal');" aria-hidden="true">
                    <spring:message code="cancel"></spring:message>
                </button>
            </div>                        
        </div>
    </div>
</div>                
                
<div id="showDetalleBitacoraClienteModal" class="modal fade" role="dialog" aria-labelledby="exampleModalLabel" style="z-index:1000;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h3 id="deletePaqueteModalLabel" class="displayInLine">
                    Listado de bitácoras
                </h3>
            </div>                
            <div class="modal-body" style="height:300px;overflow-y: scroll;">
                <button class="btn btn-link" ng-click="createReporteBitacoraStatus('Detalle_Bitacoras_Estatus')" data-toggle="tooltip" data-placement="top" title="Exportar a Excel">
                  <span class="glyphicon glyphicon-share"></span>&nbsp;Exportar
                </button>
                <div id="tableDetalleBitacoraModal">
                    <table class="table table-striped table-bordered" st-reset="isReset" st-table="leadsBitacoraColletion" st-safe-src="leadsSafeBitacoraColletion">
                        <thead>
                            <tr>
                                <th>No. Tienda</th>
                                <th>Operador</th>
                                <th>Vehículo</th>
                                <th>Estatus</th>
                                <th>Fecha</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>            
                        <tbody>
                            <tr ng-repeat="source in leadsBitacoraColletion">
                                <td><b>{{source.nombre}}</b></td>
                                <td>{{source.idOperador.nombre}}<br>{{source.idOperador.apellidoPat}}<br>{{source.idOperador.apellidoMat}}</td>
                                <td>{{source.idVehiculo.noEconomico}}<br>{{source.idVehiculo.placas}}</td>
                                <td>{{source.idStatus.desStatus}}</td>
                                <td>{{source.fecha | date:'yyyy-MM-dd HH:mm:ss'}}-{{source.fechaFin | date:'yyyy-MM-dd HH:mm:ss'}}</td>
                                <td>
                                    <a ng-if="source.archivos.length !== null && source.archivos.length>0" href="" 
                                       ng-click="getImagen(source);" role="button"  
                                       class="btn btn-info glyphicon glyphicon-save-file"> 
                                    </a>                                    
                                </td>
                            </tr>                        
                        </tbody>       
                        <tfoot>
                            <td colspan="9" class="text-center">
                                <div st-pagination="" st-items-by-page="itemsByPage" st-displayed-pages=""></div>
                            </td>                        
                        </tfoot>
                    </table>
                </div>                    
            </div>
            <div class="modal-footer">
                <button class="btn btn-default" data-dismiss="modal" ng-click="exit('#showDetallePaquetesModal');" aria-hidden="true">
                    <spring:message code="cancel"></spring:message>
                </button>
            </div>                        
        </div>
    </div>
</div>                                