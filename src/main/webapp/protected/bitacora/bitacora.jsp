<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<div ng-app="bitacora"  ng-controller="BitacoraCtrl as demo">                    
    <div class="left">
        <sec:authorize access="hasAnyRole('ROLE_MASTER', 'ROLE_ADMINISTRADOR','ROLE_CLIENTE')">
        <div class="form-horizontal">
            <form name="searchBitacoraForm" novalidate>
                <fieldset>                
                    <div class="form-group row">
                        <label class="col-sm-2 form-control-label">Bitácora</label>
                        <div  class="col-sm-10">
                            <div>
                                <ui-select ng-model="idbitacora"  name="idbitacora" theme="selectize" on-select="onSelectIdBitacoraCallback($item, $model);">
                                  <ui-select-match placeholder="Seleccionar bitácora" class="ui-select-match">
                                       <span ng-bind="$select.selected.nombre"></span>
                                  </ui-select-match>
                                  <ui-select-choices class="ui-select-choices" refresh="getListBitacora($select)" repeat="obj in listIdBitacora">
                                    <small ng-bind-html="obj.nombre | highlight: $select.search"></small>
                                  </ui-select-choices>
                                </ui-select>                        
                            </div>                    
                        </div>                    
                    </div>
                    <div class="form-group row">
                        <label class="col-sm-2 form-control-label"><spring:message code="paquete.tipoEnvio"></spring:message>:</label>
                        <div class="col-sm-10">
                            <ui-select theme="selectize" ng-model="idEnvio" ng-disabled="disabled"  title="Tipo Envio" name="rutaenvio" on-select="onSelectTipoEnvioCallback($item, $model);" >
                                <ui-select-match placeholder="Seleccionar tipo de envio" class="ui-select-match">
                                    <span ng-bind="$select.selected.desEnvio"></span>
                                </ui-select-match>
                                <ui-select-choices repeat="obj in tipoenvio | filter: $select.search" class="ui-select-choices">
                                  <span ng-bind-html="obj.desEnvio | highlight: $select.search"></span>
                                </ui-select-choices>
                             </ui-select>                             
                            <span class="text-danger" ng-show="displayValidationError &amp;&amp; searchBitacoraForm.rutaenvio.$error.required">Requerido</span>
                            <span class="text-danger" ng-show="displayValidationError &amp;&amp; searchBitacoraForm.rutaenvio.invalid">Invalido</span>                                                                                                    
                        </div>
                    </div>
                </fieldset>
                <div>
                    <button class="btn btn-default glyphicon glyphicon-search" ng-click="searchBitacoraByParameters();"></button>
                    <button class="btn btn-default glyphicon glyphicon-list" ng-click="resetBitacoraList();"></button>
                    <button class="btn btn-danger glyphicon glyphicon-erase" ng-click="resetSearch();"></button>
                </div>
            </form>                    
        </div>
        </sec:authorize>
        <sec:authorize access="hasAnyRole('ROLE_OPERADOR')">
        <div class="form-horizontal">
            <form name="searchTipoForm" novalidate>
                <div class="form-group row">
                    <label class="col-sm-2 form-control-label"><spring:message code="paquete.tipoEnvio"></spring:message>:</label>
                    <div class="col-sm-10">
                        <ui-select theme="selectize" ng-model="idEnvio" ng-disabled="disabled"  title="Tipo Envio" name="rutaenvio" on-select="onSelectTipoEnvioCallback2($item, $model);" >
                            <ui-select-match placeholder="Seleccionar tipo de envio" class="ui-select-match">
                                <span ng-bind="$select.selected.desEnvio"></span>
                            </ui-select-match>
                            <ui-select-choices repeat="obj in tipoenvio | filter: $select.search" class="ui-select-choices">
                              <span ng-bind-html="obj.desEnvio | highlight: $select.search"></span>
                            </ui-select-choices>
                         </ui-select>                                                                                                                               
                    </div>
                </div>
            </form>                    
        </div>
        </sec:authorize>        
        <br>
        <div class="pull-right" style="width: 100% !important;">
            <label>Listado bitacoras</label>
        </div>
        <div class="pull-right" style="width: 100% !important;">
           
            <sec:authorize access="hasAnyRole('ROLE_MASTER','ROLE_ADMINISTRADOR')">
                <!--<div ng-class="{'text-center': displayCreateContactButton == true, 'none': displayCreateContactButton == false}">
                    <a id="modalShowerAddPaqueteModal"
                       href="#addBitacoraModal"
                       role="button"
                       ng-click="createNewBitacora()"
                       title="<spring:message code='create'/>&nbsp;<spring:message code='ruta'/>"
                       class="btn btn-default pull-left"
                       data-toggle="modal"
                       ng-class="{'text-center': displayCreateContactButton == true, 'none': displayCreateContactButton == false}">
                        <i class="glyphicon-plus"></i>
                        &nbsp;&nbsp;<spring:message code="create"/>
                    </a>                
                </div>-->
            </sec:authorize>        
        </div>
        <br/><br/>
        <hr>
        <sec:authorize access="hasAnyRole('ROLE_MASTER','ROLE_ADMINISTRADOR','ROLE_OPERADOR','ROLE_CLIENTE')">
            <div class="row">
                <label>Total registros: {{totalViajesFound}}</label>
            </div>
            <div class="table-gnkl">
                <table class="table table-bordered" st-safe-src="leadsBitacora"  st-reset="isReset" st-table="leadsBitacoraCollection" st-pipe="serverBitacoraFilter">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Fecha</th>
                            <th>Ruta</th>
                            <th>Operador</th>
                            <th>Vehículo</th>
                            <th>Km Inicial&nbsp;/&nbsp;Km Final</th>
                            <th>Tipo</th>
                            <th>Final</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>            
                    <tbody>
                        <tr data-toggle="modal" data-target="#modal-source" ng-repeat="source in leadsBitacoraCollection" ng-click="rowClick($index);" ng-class="getCSSClass($index);">
                            <td>{{source.id}}<br><small><b>{{source.isPausa==1?'PAUSA':''}}</b></small></td>
                            <td ng-bind="source.fecha |  date:'yyyy-MM-dd'"></td>
                            <td><b>{{source.nombre}}</b><br><small>{{source.idRuta.nombre}}</small></td>
                            <!--<td>{{source.ptoOrigen.nombrePunto}}<br>{{source.ptoDestino.nombrePunto}}</td>-->
                            <td>{{source.idOperador.nombre}}<br>{{source.idOperador.apellidoPat}}<br>{{source.idOperador.apellidoMat}}</td>
                            <td>
                                <small>{{source.idVehiculo.noEconomico}}<br>{{source.idVehiculo.placas}}&nbsp;{{source.idVehiculo.marca}}</small>
                            </td>
                            <td>{{source.kmInicial | number}}&nbsp;/&nbsp;{{source.kmFinal | number}}</td>
                            <td>{{source.idEnvio.desEnvio}}</td>
                            <td>{{source.isFinal==1?'SI':'NO'}}</td>
                            <td>
                                <div class="input-group">
                                    <sec:authorize access="hasAnyRole('ROLE_MASTER', 'ROLE_ADMINISTRADOR', 'ROLE_OPERADOR')">
                                        <a ng-show="showButtonUpdateStatus(source)" href="#updateStatusBitacoraModal"
                                           ng-click="selectedBitacoraStatus(source);"
                                           role="button" title="<spring:message code="update"/>&nbsp;<spring:message code="status"/>"
                                           class="btn btn-default glyphicon glyphicon-tasks" data-toggle="modal">
                                        </a>
                                    </sec:authorize>
                                    <sec:authorize access="hasAnyRole('ROLE_MASTER', 'ROLE_ADMINISTRADOR')">
                                        <a ng-show="source.idStatus.numStatus === 1" href="#deleteBitacoraModal"
                                           ng-click="selectedBitacora(source);"
                                           role="button"
                                           title="<spring:message code="cancel"/>&nbsp;<spring:message code="bitacora"/>"
                                           class="btn btn-default glyphicon glyphicon-warning-sign" data-toggle="modal">
                                        </a>
                                    </sec:authorize>
                                    <sec:authorize access="hasAnyRole('ROLE_MASTER', 'ROLE_ADMINISTRADOR', 'ROLE_OPERADOR')">
                                        <a href="#uploadBitacoraImage" ng-click="selectedBitacora(source);" role="button"
                                           title="<spring:message code="actions"/>&nbsp;"
                                           class="btn btn-default glyphicon glyphicon glyphicon-file" data-toggle="modal">
                                        </a>
                                    </sec:authorize>      
                                    <sec:authorize access="hasAnyRole('ROLE_CLIENTE')">
                                        <a ng-if="source.archivos.length !== null && source.archivos.length>0" href="" 
                                           ng-click="getImagen(source);" role="button"  
                                           class="btn btn-info glyphicon glyphicon-save-file"> 
                                        </a>
                                    </sec:authorize>
                                    <sec:authorize access="hasAnyRole('ROLE_MASTER', 'ROLE_ADMINISTRADOR', 'ROLE_OPERADOR')">
                                        <a href="#interruptRutaModal" 
                                           ng-click="clickSelectRuta(source);" 
                                           role="button"  
                                           title="<spring:message code="pause"/>&nbsp;<spring:message code="ruta"/>"
                                           class="btn btn-info glyphicon glyphicon-pause" data-toggle="modal"> 
                                        </a>
                                    </sec:authorize>
                                </div>                                                      
                            </td>            
                        </tr>
                    </tbody>
                    <tfoot>
                        <tr>
                            <td colspan="10">
                                <div class="pull-right" st-pagination="" st-items-by-page="itemsByPage"></div>
                            </td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </sec:authorize>
        <sec:authorize access="hasAnyRole('ROLE_CLIENTE','ROLE_MASTER', 'ROLE_ADMINISTRADOR', 'ROLE_OPERADOR')">
            <div ng-show="bitacora!=null && bitacora!=undefined">
                <div class="panel panel-info" style="width: 100% !important;">
                    <div class="panel-heading collapsed" data-toggle="collapse" data-target="#collapse3">
                        <h3 class="panel-title">Bitácora: {{bitacora.nombre}} | Operador: {{bitacora.idOperador.nombre}} {{bitacora.idOperador.apellidoPat}} {{bitacora.idOperador.apellidoMat}} | Estatus: {{bitacora.idStatus.desStatus}}</h3>
                    </div>
                    <div id="collapse3" class="collapse">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-3 col-lg-3 " align="center">
                                    <img ng-src="{{ imageUrl }}" alt="" class="img-responsive" />
                                </div>

                                <div class=" col-md-9 col-lg-9 "> 
                                    <table class="table table-user-information">
                                        <tbody>
                                            <tr>
                                                <td>Bitácora:</td>
                                                    <td>{{bitacora.nombre}} </td>
                                                </tr>
                                                <tr>
                                                    <td>Fecha Inicial:</td>
                                                    <td ng-bind="bitacora.fecha |  date:'dd/MM/yyyy HH:mm:ss'"></td>                                                    
                                                </tr>
                                                <tr>
                                                    <td>Fecha Final:</td>
                                                    <td ng-bind="bitacora.fechaFin |  date:'dd/MM/yyyy HH:mm:ss'"></td>
                                                </tr>
                                                <tr>
                                                    <td>Usuario:</td>
                                                    <td>{{bitacora.idUsuario.usuario}}&nbsp;-&nbsp;{{bitacora.idUsuario.nombre}}</td>
                                                </tr>
                                                <tr>
                                                    <td><spring:message code="paquete.id_puntoorigen"></spring:message></td>
                                                    <td>{{bitacora.ptoOrigen.nombrePunto}} {{bitacora.ptoOrigen.direccion}}</td>
                                                </tr>
                                                <tr>
                                                    <td><spring:message code="paquete.idpuntoe"></spring:message></td>
                                                    <td>{{bitacora.ptoDestino.nombrePunto}} {{bitacora.ptoDestino.direccion}}</td>
                                                </tr>
                                                <tr>
                                                    <td><spring:message code="paquete.idstatus"></spring:message></td>
                                                    <td>{{bitacora.idStatus.desStatus}}</td>
                                                </tr>    
                                                <tr>
                                                    <td><spring:message code="paquete.idOperador"></spring:message></td>
                                                    <td>{{bitacora.idOperador.nombre}} {{bitacora.idOperador.apellidoPat}} {{bitacora.idOperador.apellidoMat}}</td>
                                                </tr> 
                                                <tr>
                                                    <td>Vehículo:</td>
                                                    <td>{{bitacora.idVehiculo.noEconomico}} {{bitacora.idVehiculo.placas}} {{bitacora.idVehiculo.marca}}</td>
                                                </tr>                                                 
                                                <tr>
                                                    <td>Pausa:</td>
                                                    <td>{{bitacora.isPausa==1?'SI':'NO'}}</td>
                                                </tr>                                                                                                 
                                        </tbody>
                                    </table>
                                    <sec:authorize access="hasAnyRole('ROLE_CLIENTE')">
                                        <a ng-if="bitacora.archivos.length !== null && bitacora.archivos.length>0" href="" 
                                           ng-click="getImagen(bitacora);" role="button"  
                                           class="btn btn-info glyphicon glyphicon-save-file"> 
                                        </a>                                        
                                    </sec:authorize>                                                
                                </div>
                            </div>
                        </div>
                    </div>
                    <sec:authorize access="hasAnyRole('ROLE_MASTER', 'ROLE_ADMINISTRADOR', 'ROLE_OPERADOR')">
                        <div class="panel-footer" ng-show="paquete !== undefined && paquete.idPaq!==undefined">
                                <input type="hidden" value="{{paquete.idPaq}}"/>
                                <a ng-show="paquete.idStatus.desStatus!=='CANCELADO'" href="#actPaqueteModal" role="button" title="<spring:message code="paquete.modifyestatus"/>" class="btn btn-primary glyphicon glyphicon-pencil" data-toggle="modal" ng-click="verifyNextStatus(paquete);"></a>
                                <button class="btn btn-primary glyphicon glyphicon-refresh right" ng-click="searchPaqueteAct(searchPaqueteActForm, false);" title="Recargar Historico"/>        
                        </div>
                    </sec:authorize>
                </div>           
            </div>
        </sec:authorize>
        <div class="table-gnkl">
            <table class="table table-striped table-bordered" st-safe-src="leadsHistorico"  st-reset="isReset" st-table="leadsHistoricoCollection">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Fecha</th>
                        <th>Estatus</th>
                        <th>kilometraje</th>
                        <th>Tiempo(hr)</th>
                        <th>Acciones</th>
                    </tr>
                </thead>            
                <tbody>
                    <tr data-toggle="modal" data-target="#modal-source" ng-repeat="source in leadsHistoricoCollection" ng-click="rowClickBitacora($index);">
                        <td>{{source.id}}</td>
                        <td ng-bind="source.fecha |  date:'yyyy-MM-dd HH:mm:ss'"></td>
                        <td>{{source.estatus.desStatus}}</td>
                        <td>{{source.kilometraje | number}}</td>       
                        <td>{{getTimePerRow($index)}}</td>
                        <td>
                            <sec:authorize access="hasAnyRole('ROLE_MASTER', 'ROLE_ADMINISTRADOR', 'ROLE_OPERADOR')">
                            <a href="#detailBitacoraStatus" ng-click="selectedStatusBitacora(source);" role="button"
                               title="<spring:message code="actions"/>&nbsp;"
                               class="btn btn-default glyphicon glyphicon glyphicon-file" data-toggle="modal">
                            </a>
                            </sec:authorize>   
                            <a ng-if="source.archivos.length !== null && source.archivos.length>0" href="" 
                               ng-click="getImagenHistorico(source);" role="button"  
                               class="btn btn-info glyphicon glyphicon-save-file"> 
                            </a>                               
                        </td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="10">
                            <div class="pull-right" st-pagination="" st-items-by-page="itemsByPage"></div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="10">
                            <label class="col-sm-6">TIEMPO TRANSCURRIDO (hr/min):&nbsp;{{totalTiempoMins}}</label>
                            <label class="col-sm-6">KILOMETRAJE TRANSCURRIDO (km):&nbsp;{{totalKm | number}}</label>      
                        </td>
                    </tr>                    
                </tfoot>
            </table>
        </div>         

    </div>    
            
    <jsp:include page="dialogs/bitacoraDialogs.jsp"/> 
</div>        
<script type="text/javascript" src="${contextPath}/resources/js/pages/bitacora.js" /></script>