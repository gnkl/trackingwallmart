<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div id="addPermissionModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style='width:100%;'>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 id="addPaqueteModalLabel" class="displayInLine">
                    <spring:message code="manage"></spring:message>&nbsp;<spring:message code="permisos"></spring:message>
                    </h3>
                </div>        
                <div class="modal-body">
                    <form name="newPermissionForm" novalidate="">
                        <label ng-repeat="role in roles">
                            <input type="checkbox" data-checklist-model="user.roles" data-checklist-value="role" style="width:100%"> {{role.desRol}}
                        </label>
                    </form>
                </div>
                <div class="modal-footer">
                    <input class="form-control" name="${_csrf.parameterName}" value="${_csrf.token}" type="hidden">                                
                <input class="btn btn-primary" ng-click="savePermission(newPermissionForm);" value="<spring:message code="save"></spring:message>" type="submit" data-dismiss="modal">
                    <button class="btn btn-default" data-dismiss="modal" ng-click="exit('#newPermissionForm');" aria-hidden="true">
                        <spring:message code="cancel"></spring:message>
                    </button>
            </div>
        </div>
    </div>
</div>
                    
<div id="changePasswordModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style='width:100%;'>
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 id="addPaqueteModalLabel" class="displayInLine">
                    <spring:message code="manage"></spring:message>&nbsp;<spring:message code="password"></spring:message>
                    </h3>
                </div>        
                <div class="modal-body">
                    <form name="newPasswordForm" novalidate="">
                        <!--<div>
                            <label>* <spring:message code="usuario.passwordanterior"></spring:message>:</label>
                            <input class="form-control" required="" autofocus="" ng-model="usuario.passwordant" name="passwordant" placeholder="&nbsp;<spring:message code='usuario.passwordanterior'></spring:message>" type="text">
                            <div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationError &amp;&amp; newPasswordForm.passwordant.$error.required">
                                <spring:message code="required"></spring:message>
                            </div>
                        </div>-->              
                        <div>
                            <div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="error_password_match">
                                Error!! Las constraseñas no son las mísmas
                            </div>
                            <div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="error_pattern_match">
                                Error!! Password no cumple con lo mínimo necesario (6-8 caracteres 1 letra mayúscula, 1 número)
                            </div>                            
                            <label>* <spring:message code="usuario.passwordnvo"></spring:message>:</label>
                            <input class="form-control" required="" autofocus="" ng-model="usuario.passwordnvo" name="nvoPassword" placeholder="&nbsp;<spring:message code='usuario.passwordnvo'></spring:message>" type="password" ng-change="analizepassword()" >
                            <div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationError &amp;&amp; newPasswordForm.nvoPassword.$error.required">
                                <spring:message code="required"></spring:message>
                            </div>
                        </div>
                        <div>
                            <label>* <spring:message code="usuario.passwordrep"></spring:message>:</label>
                            <input class="form-control" required="" autofocus="" ng-model="usuario.passwordnvorep" name="nvoPassRep" placeholder="&nbsp;<spring:message code='usuario.passwordrep'></spring:message>" type="password" ng-change="verify(usuario.passwordnvorep)">
                            <div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationError &amp;&amp; newPasswordForm.nvoPassRep.$error.required">
                                <spring:message code="required"></spring:message>
                            </div>
                        </div>                            
                    </form>
                </div>
                <div class="modal-footer">
                    <input class="form-control" name="${_csrf.parameterName}" value="${_csrf.token}" type="hidden">                                
                    <input class="btn btn-primary"  ng-if="error_pattern_match==false && error_password_match==false" ng-click="changePasswordUser(newPasswordForm);" value="<spring:message code="save.password"></spring:message>" type="submit" data-dismiss="modal">
                    <button class="btn btn-default" data-dismiss="modal" ng-click="exit('#newPasswordForm');" aria-hidden="true">
                        <spring:message code="cancel"></spring:message>
                    </button>
            </div>
        </div>
    </div>
</div>                    
                    
<div id="createOperador" class="modal fade" tabindex="0" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <form name="createOperadorForm" novalidate="">
                    <div>
                        <center>
                        <img ng-src="{{ imageUrl }}" alt="" class="img-responsive" style="max-width: 100px;max-height: 100px;"/>
                        </center>
                        <label>* <spring:message code="usuario"></spring:message>:</label>
                        <input class="form-control" required="" autofocus="" ng-model="usuario.usuario" name="username" placeholder="&nbsp;<spring:message code='username'></spring:message>" type="text" ng-readonly="updateuser">
                            <div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationError &amp;&amp; createOperadorForm.username.$error.required">
                            <spring:message code="required"></spring:message>
                            </div>
                        </div>
                        <div>    
                            <label>* <spring:message code="nombre"></spring:message>:</label>
                        <input class="form-control" required="" autofocus="" ng-model="usuario.nombre" name="nombre" placeholder="&nbsp;<spring:message code='description'></spring:message>" type="text">
                            <div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationError &amp;&amp; createOperadorForm.nombre.$error.required">
                            <spring:message code="required"></spring:message>
                            </div>
                        </div>
                        <div>
                            <label>* <spring:message code="apellidopat"></spring:message>:</label>
                        <input class="form-control" required="" autofocus="" ng-model="usuario.apellidoPat" name="apellidopat" placeholder="&nbsp;<spring:message code='apellidopat'></spring:message>" type="text">
                            <div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationError &amp;&amp; createOperadorForm.apellidopat.$error.required">
                            <spring:message code="required"></spring:message>
                            </div>
                        </div>
                        <div>
                            <label>* <spring:message code="apellidomat"></spring:message>:</label>
                            <input class="form-control" required="" autofocus="" ng-model="usuario.apellidoMat" name="apellidopat" placeholder="&nbsp;<spring:message code='apellidopat'></spring:message>" type="text">
                            <div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationError &amp;&amp; createOperadorForm.apellidopat.$error.required">
                            <spring:message code="required"></spring:message>
                            </div>
                        </div>
                        <div>
                            <label>* <spring:message code="telefono"></spring:message>:</label>
                            <input type='text' class="form-control" required="" ng-model='usuario.telefono' mask="99 9999 9999" clean="true" name="telefono"/>                        
                            <!--<pre>{{usuario.telefono}}</pre>-->
                            <div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationError &amp;&amp; createOperadorForm.telefono.$error.required">
                            <spring:message code="required"></spring:message>
                            </div>
                        </div>
                        <!--<div>
                            <label for="inputPhone">*Phone :</label>
                            <input type="number" class="form-control" ng-pattern="ph_numbr"  id="inputPhone" name="phone" placeholder="Phone" ng-model="usuario.telefono" ng-required="true">
                            <div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="(createOperadorForm.phone.$invalid || createOperadorForm.phone.$pristine) && (usuario.telefono!=null)">
                            <spring:message code="required"></spring:message>
                            </div>                            
                        </div>-->
                        
                        <div>
                            <label>* <spring:message code="contacts.email"></spring:message>:</label>
                            <input class="form-control" ng-model="usuario.email" name="email" placeholder="&nbsp;<spring:message code='contacts.email'></spring:message>" type="email" required>
                            <div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationError && createOperadorForm.email.$error.required">
                            <spring:message code="required"></spring:message>
                            </div>
                            <!--<pre>{{createOperadorForm.email.$valid}}</pre>
                            <pre>{{createOperadorForm.email.$error.required}}</pre>-->
                            <div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="createOperadorForm.email.$error.required==null && !createOperadorForm.email.$valid">
                            <spring:message code="invalid"></spring:message>
                            </div>                            
                        </div>                            
                        <div>
                            <label>* <spring:message code="estatus"></spring:message>:</label>
                            <select class="form-control" ng-model="usuario.estatusid" ng-options="opt.descripcion for opt in estatus"></select>
                            <div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationError &amp;&amp; newPuntoForm.tipoenvioform.$error.required">
                            <spring:message code="required"></spring:message>
                            </div>                             
                        </div>
                        <div> 
                            <label>* Cargar Imagen:</label>
                            <button class="btn btn-default" type="file" ngf-select="uploadFiles($file, $invalidFiles)" accept="image/*">
                                Seleccionar archivo
                            </button>
                            <br><br>
                            <label>Archivo:</label>
                            <div >{{f.name}} {{errFile.name}} {{errFile.$error}} {{errFile.$errorParam}}
                                <span class="progress" ng-show="f.progress >= 0">
                                    <div style="width:{{f.progress}}%"   ng-bind="f.progress + '%'"></div>
                                </span>
                            </div>     
                            {{errorMsg}}
                        </div>                                            
                    </form>
                </div>
                <div class="modal-footer">
                    <input class="form-control" name="${_csrf.parameterName}" value="${_csrf.token}" type="hidden">
                <input class="btn btn-primary" ng-click="createUsuario(createOperadorForm);" value="<spring:message code="saveUsuario"></spring:message>" type="submit" data-dismiss="modal">
                    <button class="btn btn-default" data-dismiss="modal" ng-click="resetUsuario()" aria-hidden="true">
                    <spring:message code="cancel"></spring:message>
                </button>
            </div>
        </div>
    </div>                           
</div>
                
<div id="proccesCsvFileUserModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 id="deletePaqueteModalLabel" class="displayInLine">
                    <spring:message code="header.uploadfile"/>&nbsp;
                </h3>
            </div>             
            <div class="modal-body">
                <form name="newFileUserForm" novalidate="">
                    <div> 
                        <label>* Cargar Imagen:</label>
                        <button class="btn btn-default" type="file" ngf-select="uploadFiles($file, $invalidFiles)" ngf-pattern="'.xlsx'" accept=".xlsx" >
                            Seleccionar archivo
                        </button>
                        <br><br>
                        <label>Archivo:</label>
                        <div >{{f.name}}
                            <span class="progress" ng-show="f.progress >= 0">
                                <div style="width:{{f.progress}}%"   ng-bind="f.progress + '%'"></div>
                            </span>
                        </div>
                        <div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="errFile.name!==null && errFile.name!==undefined">
                        <p>Error en el tipo de archivo</p>
                        <br>
                        <p>Debe ser un archivo del tipo: {{errFile.$errorParam}}</p>
                        </div>
                        <!--<pre>
                            {{errFile}}
                        </pre>-->
                    </div> 
                </form>                 
            </div>
            <div class="modal-footer">
                <input class="form-control" name="${_csrf.parameterName}" value="${_csrf.token}" type="hidden">                                
                <input type="submit" class="btn btn-primary" ng-click="proccesMultipartFile(newFileUserForm,true);" value='<spring:message code="procces.file"/>' data-dismiss="modal" ng-show="errFile.name!==null && (errFile.name===undefined || errFile.name==='')"/>
                <button class="btn btn-default" data-dismiss="modal" ng-click="exit('#newFilePuntoForm');" aria-hidden="true">
                    <spring:message code="cancel"/>
                </button>                 
            </div>                
        </div>
    </div>
</div>