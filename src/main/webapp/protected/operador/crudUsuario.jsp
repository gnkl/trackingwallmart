<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<div class="row" ng-app="crudoperador"  ng-controller="CrudOperadorCtrl as usuariocrud">                      
    <!--<div id="loadingModal" class="modal hide fade in centering" role="dialog" aria-labelledby="deletePaqueteModalLabel" aria-hidden="true">
        <div id="divLoadingIcon" class="text-center">
            <div class="icon-align-center loading"></div>
        </div>
    </div>-->
    
    <div class="container">
        <div class="modal-body">
            <form name="searchUsuarioForm" novalidate>
                <!--<label><spring:message code="search.for.usuario"/></label>
                <div>
                    <input type="text" class="form-control" autofocus required ng-model="searchUser" name="searchFor" placeholder="<spring:message code='usuario'/>&nbsp;<spring:message code='nombre'/> "/>                            
                    <div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationError && searchUsuarioForm.searchFor.$error.required">
                        <spring:message code="required"></spring:message>
                    </div>                                   
                </div>-->
                <label><spring:message code="search.for.usuario"/></label>
                <div>
                    <div>
                        <ui-select ng-model="searchUserObj"  ng-required="true" name="idoperador" on-select="onSelectCallback($item, $model);" theme="selectize">
                          <ui-select-match placeholder="Select Usuario..." class="ui-select-match">
                               <span ng-bind="$select.selected.usuario+' | '+$select.selected.nombre+' '+$select.selected.apellidoPat+' '+$select.selected.apellidoMat"></span>
                          </ui-select-match>
                          <ui-select-choices class="ui-select-choices" refresh="getUserByName($select)" repeat="obj in listusers">
                            <span ng-bind-html="obj.usuario+' | '+obj.nombre+' '+obj.apellidoPat+' '+obj.apellidoMat | highlight: $select.search"></span>
                          </ui-select-choices>
                        </ui-select>
                        <div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationError &amp;&amp; updatePaqueteForm.idoperador.$invalid">
                            <spring:message code="required"></spring:message>
                        </div>                            
                    </div>                    
                    <div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationError && searchContactForm.searchFor.$error.required">
                        <spring:message code="required"></spring:message>
                    </div>                                   
                </div>                        
            </form>   
            <button class="btn btn-danger pull-right glyphicon glyphicon-erase" ng-click="resetSearchUser();"></button>
        </div>    
    </div>    
    <div id="crudOperador" class="container">
        <sec:authorize access="hasAnyRole('ROLE_MASTER','ROLE_ADMINISTRADOR','ROLE_OPERADOR')">
            <div ng-class="{'text-center': displayCreateContactButton == true, 'none': displayCreateContactButton == false}">
                <a href="#createOperador"
                   role="button"
                   ng-click="resetUsuario();"
                   title="<spring:message code='create'/>&nbsp;<spring:message code='usuario'/>"
                   class="btn btn-default pull-left"
                   data-toggle="modal"
                   ng-class="{'text-center': displayCreateContactButton == true, 'none': displayCreateContactButton == false}">
                    <i class="glyphicon-plus"></i>
                    &nbsp;&nbsp;<spring:message code="create"/>
                </a>
                <!--<sec:authorize access="hasAnyRole('ROLE_MASTER', 'ROLE_ADMINISTRADOR')">
                <a href="#proccesCsvFileUserModal"
                   role="button"
                   ng-click="resetFiles()"
                   title="<spring:message code='upload.file'/>"
                   class="btn btn-default pull-left"
                   data-toggle="modal"
                   ng-class="{'text-center': displayCreateContactButton == true, 'none': displayCreateContactButton == false}">
                    <i class="glyphicon-upload"></i>
                    &nbsp;&nbsp;<spring:message code="upload.file"/>
                </a>
                </sec:authorize>-->
            </div>
        </sec:authorize>         
            <br/><br/>
            <div class="table-responsive pull-right" style="width: 100% !important;">
                <table id="1" class="table table-striped table-bordered" st-reset="isReset" st-table="usuarioCollection" st-pipe="serverUsuarioFilter">
                    <thead>
                        <tr>
                            <th st-sort="idUsu">ID</th>
                            <th st-sort="usuario">Usuario</th>
                            <th>Nombre</th>
                            <th st-sort="telefono">Telefono</th>
                            <th st-sort="status">Estatus</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>            
                    <tbody>
                        <tr data-toggle="modal" data-target="#modal-source" ng-repeat="source in usuarioCollection">
                            <td>{{source.idUsu}}</td>
                            <td>{{source.usuario}}</td>
                            <td>{{source.nombre}} {{source.apellidoPat}} {{source.apellidoMat}}</td>
                            <td>{{source.telefono}}</td>
                            <td>{{source.status}}</td>
                            <td>
                                <div>
                                    <input type="hidden" value="{{source.idUsu}}"/>
                                    <a href="#createOperador" ng-click="selectUsuario(source);" role="button" title="<spring:message code="update"/>&nbsp;<spring:message code="usuario"/>"
                                    class="btn btn-default glyphicon glyphicon-pencil" data-toggle="modal">
                                    </a>
                                    <a href="#addPermissionModal" ng-click="showManagePermission(source);" role="button" title="<spring:message code="usuario.permisos"/>&nbsp;"
                                    class="btn btn-default glyphicon glyphicon-flag" data-toggle="modal">
                                    </a>                                    
                                    <input type="hidden" value="{{source.idUsu}}"/>
                                    <a href="#changePasswordModal" ng-click="modificarPassword(source);" role="button" title="<spring:message code="usuario.password"/>&nbsp;"
                                    class="btn btn-default glyphicon glyphicon-lock" data-toggle="modal">
                                    </a>                                    
                                </div>   
                            </td>            
                        </tr>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="8">
                            <div class="pull-right" st-pagination="" st-items-by-page="itemsByPage"></div>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>    
    </div>
    <jsp:include page="dialogs/usuarioDialogs.jsp"/> 

</div>        
<script type="text/javascript" src="${contextPath}/resources/js/pages/operador.js" /></script>