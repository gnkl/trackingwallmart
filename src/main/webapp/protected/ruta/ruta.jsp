<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<div ng-app="ruta">
    <div class="left" ng-controller="RutaCtrl" >
        <label><spring:message code="search"/></label>
        <hr>    
        <div class="form-horizontal">
            <form name="searchRutaForm" novalidate>
                <fieldset>
                    <div class="form-group row">
                        <label class="col-sm-2 form-control-label">Ruta</label>
                        <div  class="col-sm-10">
                            <div>
                                <ui-select ng-model="rutaselected"  name="idruta" theme="selectize" on-select="onSelectIdRutaCallback($item, $model);">
                                  <ui-select-match placeholder="Seleccionar bitácora" class="ui-select-match">
                                       <span ng-bind="$select.selected.nombre+' '+$select.selected.idEnvio.desEnvio+' '+($select.selected.fechaCreacion |  date:'dd/MM/yyyy')"></span>
                                  </ui-select-match>
                                  <ui-select-choices class="ui-select-choices" refresh="getListRuta($select)" repeat="obj in listIdRuta">
                                    <small ng-bind-html="obj.nombre+' '+obj.idEnvio.desEnvio+' '+(obj.fechaCreacion |  date:'dd/MM/yyyy') | highlight: $select.search"></small>
                                  </ui-select-choices>
                                </ui-select>                        
                            </div>                    
                        </div>                    
                    </div> 
                    <div class="form-group row">
                            <label class="col-sm-2 form-control-label">* <spring:message code="estatus"></spring:message>:</label>
                            <div class="col-sm-10">
                                <select class="form-control" ng-model="rutaestatus" ng-options="opt.descripcion for opt in estatus" name="rutaestatus" required></select>
                                <div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="displayValidationOError && searchRutaForm.rutanombre.$error.required">
                                <spring:message code="required"></spring:message>
                                </div>                             
                            </div>
                    </div>
                </fieldset>
            </form>
            <div class="pull-right" style="width: 100% !important;">
                <button class="btn btn-primary pull-right glyphicon glyphicon-search" ng-click="searchRutaByParameters()" value='<spring:message code="search"/>'></button>
                <button class="btn btn-danger pull-right glyphicon glyphicon-erase" ng-click="resetSearch();"></button>                                     
            </div>    
        </div>
        <hr>
        <div class="pull-right" style="width: 100% !important;">
            <sec:authorize access="hasAnyRole('ROLE_MASTER','ROLE_ADMINISTRADOR')">
                <div ng-class="{'text-center': displayCreateContactButton == true, 'none': displayCreateContactButton == false}">
                    <a href="#addRutaModal"
                       role="button"
                       ng-click="createNewRuta()"
                       title="<spring:message code='create'/>&nbsp;<spring:message code='ruta'/>"
                       class="btn btn-default pull-left"
                       data-toggle="modal"
                       ng-class="{'text-center': displayCreateContactButton == true, 'none': displayCreateContactButton == false}">
                        <i class="glyphicon-plus"></i>
                        &nbsp;&nbsp;<spring:message code="create"/>
                    </a>
                </div>
                <a href="#proccesFileRutaModal"
                   role="button"
                   ng-click="resetFiles()"
                   title="<spring:message code='upload.file'/>"
                   class="btn btn-default pull-left"
                   data-toggle="modal"
                   ng-class="{'text-center': displayCreateContactButton == true, 'none': displayCreateContactButton == false}">
                    <i class="glyphicon-upload"></i>
                    &nbsp;&nbsp;<spring:message code="upload.file"/>
                </a>
                    
            </sec:authorize>        
        </div>
        <br/>
        <br/>
        <hr>
        <div class="table-responsive pull-right" style="width: 100% !important;">
            <table class="table table-striped table-bordered" st-reset="isReset" st-table="leadsCollection" st-pipe="serverFilter"> 
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Estatus</th>
                        <th>Fecha Creación</th>
                        <th>Tipo</th>
                        <th>Pausa</th>
                        <th>Acciones</th>
                    </tr>
                </thead>            
                <tbody>
                    <tr data-toggle="modal" data-target="#modal-source" ng-repeat="source in leadsCollection" ng-click="rowClick($index);">
                        <td>{{source.id}}</td>
                        <td>{{source.nombre}}</td>
                        <td>{{source.estatus==1?'Activo':'Inactivo'}}</td>
                        <td ng-bind="source.fechaCreacion |  date:'dd / MM / yyyy HH:mm:ss'"></td>
                        <td><small>{{source.idEnvio.desEnvio}}</small><br></td>
                        <td>{{source.isPausa==1?'SI':'NO'}}</td>
                        <td>
                            <input type="hidden" value="{{source.idPaq}}"/>
                            <div>

                                <sec:authorize access="hasAnyRole('ROLE_MASTER', 'ROLE_ADMINISTRADOR')">
                                    <a href="#addRutaModal"
                                       ng-if="source.estatus === 1 || source.estatus === 2"
                                       ng-click="selectedRuta(source);"
                                       role="button" title="<spring:message code="update"/>&nbsp;<spring:message code="ruta"/>"
                                       class="btn btn-default glyphicon glyphicon-pencil" data-toggle="modal">
                                    </a>
                                    <a  ng-if="source.estatus === 1" href="#deleteRutaModal"
                                        ng-if="source.estatus === 1"
                                        ng-click="selectedRuta(source);"
                                        role="button"
                                        title="<spring:message code="cancel"/>&nbsp;<spring:message code="ruta"/>"
                                        class="btn btn-default glyphicon glyphicon-warning-sign" data-toggle="modal">
                                    </a>
                                </sec:authorize>
                                <sec:authorize access="hasAnyRole('ROLE_MASTER', 'ROLE_ADMINISTRADOR', 'ROLE_OPERADOR')">
                                    <a  href="#addBitacoraModal"
                                        ng-click="createNewBitacora();"
                                        role="button"
                                        title="<spring:message code="create"/>&nbsp;<spring:message code="bitacora"/>"
                                        class="btn btn-default glyphicon glyphicon-plus" data-toggle="modal">
                                    </a>
                                </sec:authorize>
                                <sec:authorize access="hasAnyRole('ROLE_MASTER', 'ROLE_ADMINISTRADOR', 'ROLE_OPERADOR')">
                                    <a href="#interruptRutaModal" 
                                       ng-click="clickSelectRuta(source);" 
                                       role="button"  
                                       title="<spring:message code="pause"/>&nbsp;<spring:message code="bitacora"/>"
                                       class="btn btn-info glyphicon glyphicon-pause" data-toggle="modal"> 
                                    </a>
                                </sec:authorize>
                            </div>

                        </td>            
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="10">
                            <div class="pull-right" st-pagination="" st-items-by-page="itemsByPage"></div>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
        <div>
            <label>Ruta seleccionada: {{ruta.id}}</label>  
        </div>
        <div class="table-gnkl">
            <table class="table table-bordered" st-safe-src="leadsBitacora"  st-reset="isReset" st-table="leadsBitacoraCollection" st-pipe="serverBitacoraFilter">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Fecha</th>
                        <th>Ruta</th>
                        <th>Operador</th>
                        <th>Vehículo</th>
                        <th>Km Inicial&nbsp;/&nbsp;Km Final</th>
                        <th>Final</th>
                        <th>Acciones</th>
                    </tr>
                </thead>            
                <tbody>
                    <tr data-toggle="modal" data-target="#modal-source" ng-repeat="source in leadsBitacoraCollection" ng-class="getCSSClass($index);">
                        <th>{{source.id}}</th>
                        <td ng-bind="source.fecha |  date:'yyyy-MM-dd HH:mm:ss'"></td>
                        <td>{{source.nombre}}</td>
                        <!--<td>{{source.ptoOrigen.nombrePunto}}<br>{{source.ptoDestino.nombrePunto}}</td>-->
                        <td>{{source.idOperador.nombre}}&nbsp;{{source.idOperador.apellidoPat}}&nbsp;{{source.idOperador.apellidoMat}}</td>
                        <td>
                            <small>{{source.idVehiculo.noEconomico}}<br>{{source.idVehiculo.placas}}&nbsp;{{source.idVehiculo.marca}}</small>
                        </td>
                        <td>{{source.kmInicial}}&nbsp;/&nbsp;{{source.kmFinal}}</td>
                        <td>{{source.isFinal==1?'SI':'NO'}}</td>
                        <td>
                            <div class="input-group">
                                <a ng-show="source.idStatus.numStatus !== 6 && source.idStatus.numStatus !== 5" href="#addBitacoraModal"
                                   ng-click="selectedBitacora(source);"
                                   role="button" title="<spring:message code="update"/>&nbsp;<spring:message code="paquete"/>"
                                   class="btn btn-default glyphicon glyphicon-pencil" data-toggle="modal">
                                </a>
                                <a href="../protected/bitacoras#/?idbitacora={{source.id}}&idruta={{source.idRuta.id}}"
                                   role="button"
                                   title="<spring:message code="paquete.actualizarestatus"/>"
                                   class="btn btn-default glyphicon glyphicon-new-window" data-toggle="modal">
                                </a>                                    
                            </div>                                                      
                        </td>            
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="10">
                            <div class="pull-right" st-pagination="" st-items-by-page="itemsByPage"></div>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
        <jsp:include page="dialogs/rutaDialogs.jsp"/> 
        <jsp:include page="dialogs/bitacoraDialogs.jsp"/>
    </div>
</div>
    
<script type="text/javascript" src="${contextPath}/resources/js/pages/ruta.js" /></script>