<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div id="addRutaModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style='width:100%;'>
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 id="addPaqueteModalLabel" class="displayInLine">
                    <spring:message code="create"></spring:message>&nbsp;<spring:message code="ruta"></spring:message>
                    </h3>
                </div>        
                <div class="modal-body">
                <form name="newRutaForm" novalidate>
                        <div>
                            <label>* <spring:message code="ruta.nombre"></spring:message>:</label>
                            <input class="form-control" required="" autofocus="" ng-model="ruta.nombre" name="rutanombre" placeholder="&nbsp;<spring:message code='ruta.nombre'></spring:message>" type="text">
                            <span class="text-danger" ng-show="displayValidationError &amp;&amp; newRutaForm.rutanombre.$error.required">Requerido</span>
                            <span class="text-danger" ng-show="displayValidationError &amp;&amp; newRutaForm.rutanombre.invalid">Invalido</span>                                                                        
                        </div>
                        <div>
                            <label>* <spring:message code="estatus"></spring:message>:</label>
                            <select class="form-control" ng-model="selected.estatus" ng-options="opt.descripcion for opt in estatus" name="rutaestatus" required></select>
                            <span class="text-danger" ng-show="displayValidationError &amp;&amp; newRutaForm.rutaestatus.$error.required">Requerido</span>
                            <span class="text-danger" ng-show="displayValidationError &amp;&amp; newRutaForm.rutaestatus.invalid">Invalido</span>                                                                        
                        </div>
                        <div>
                            <label>* <spring:message code="paquete.tipoEnvio"></spring:message>:</label>
                            <ui-select theme="bootstrap" ng-model="ruta.idEnvio" ng-disabled="disabled"  title="Tipo Envio" name="rutaenvio" ng-required="true">
                                <ui-select-match placeholder="Seleccionar tipo de envio" class="ui-select-match">
                                    <span ng-bind="$select.selected.desEnvio"></span>
                                </ui-select-match>
                                <ui-select-choices repeat="obj in tipoenvio | filter: $select.search" class="ui-select-choices">
                                  <span ng-bind-html="obj.desEnvio | highlight: $select.search"></span>
                                </ui-select-choices>
                             </ui-select> 
                            <span class="text-danger" ng-show="displayValidationError &amp;&amp; newRutaForm.rutaenvio.$error.required">Requerido</span>
                            <span class="text-danger" ng-show="displayValidationError &amp;&amp; newRutaForm.rutaenvio.invalid">Invalido</span>                                                                        
                        </div>                            
                </form>
                </div>
                <div class="modal-footer">
                    <input class="btn btn-primary" ng-click="createRuta(newRutaForm);" value="<spring:message code="create"></spring:message>" type="submit" data-dismiss="modal">
                    <button class="btn btn-default" data-dismiss="modal" ng-click="exit('#addRutaModal');" aria-hidden="true">
                        <spring:message code="cancel"></spring:message>
                    </button>
            </div>
        </div>
    </div>
</div>
                    
<div id="deleteRutaModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 id="deleteRutaModalLabel" class="displayInLine">
                    <spring:message code="cancel"/>&nbsp;<spring:message code="ruta"/>
                </h3>
            </div>                
            <div class="modal-body">
                <form name="deleteRutaForm" novalidate="">
                    <div>
                        <p><spring:message code="delete.confirm"/>:&nbsp;{{ruta.nombre}}?</p>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <input class="form-control" name="${_csrf.parameterName}" value="${_csrf.token}" type="hidden">                                
                <input type="submit" class="btn btn-primary" ng-click="deleteRuta(deleteRutaForm);" value='<spring:message code="delete"/>' data-dismiss="modal"/>
                <button class="btn btn-default" data-dismiss="modal" ng-click="exit('#deleteRutaModal');" aria-hidden="true">
                    <spring:message code="cancel"/>
                </button>                 
            </div>
        </div>
    </div>
</div>  
                
<div id="proccesFileRutaModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 id="deletePaqueteModalLabel" class="displayInLine">
                    <spring:message code="header.uploadfile"/>&nbsp;
                </h3>
            </div>             
            <div class="modal-body">
                <form name="newFileRutaForm" novalidate="">
                    <div> 
                        <label>* Cargar Archivo:</label>
                        <button class="btn btn-default" type="file" ngf-select="uploadFiles($file, $invalidFiles)"  multiple accept="image/*" ngf-max-size="20MB">
                            Seleccionar archivo
                        </button>
                        <br><br>
                        <label>Archivo:</label>
                        <div >{{f.name}}
                            <span class="progress" ng-show="f.progress >= 0">
                                <div style="width:{{f.progress}}%"   ng-bind="f.progress + '%'"></div>
                            </span>
                        </div>
                        <div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="errFile.name!==null && errFile.name!==undefined">
                        <p>Error en el tipo de archivo</p>
                        <br>
                        <p>Debe ser un archivo del tipo: {{errFile.$errorParam}}</p>
                        </div>
                        <!--<pre>
                            {{errFile}}
                        </pre>-->
                    </div>
                    <div>
                        <label>* <spring:message code="paquete.tipoEnvio"></spring:message>:</label>
                        <ui-select theme="bootstrap" ng-model="tipoEnvio" ng-disabled="disabled"  title="Tipo Envio" name="rutaenvio" ng-required="true" on-select="onSelectTipoEnvioCallback($item, $model);">
                            <ui-select-match placeholder="Seleccionar tipo de envio" class="ui-select-match">
                                <span ng-bind="$select.selected.desEnvio"></span>
                            </ui-select-match>
                            <ui-select-choices repeat="obj in tipoenvio | filter: $select.search" class="ui-select-choices">
                              <span ng-bind-html="obj.desEnvio | highlight: $select.search"></span>
                            </ui-select-choices>
                         </ui-select> 
                        <span class="text-danger" ng-show="displayValidationError &amp;&amp; newRutaForm.rutaenvio.$error.required">Requerido</span>
                        <span class="text-danger" ng-show="displayValidationError &amp;&amp; newRutaForm.rutaenvio.invalid">Invalido</span>                                                                        
                    </div>                    
                </form>   
                
            </div>
            <div class="modal-footer">                             
                <input type="submit" class="btn btn-primary" ng-click="proccesMultipartFile(newFileRutaForm,true);" value='<spring:message code="procces.file"/>' ng-show="errFile.name!==null && (errFile.name===undefined || errFile.name==='')"/>
                <button class="btn btn-default" data-dismiss="modal" ng-click="exit('#newFileRutaForm');" aria-hidden="true">
                    <spring:message code="cancel"/>
                </button>                 
            </div>                
        </div>
    </div>
</div>
                
<div id="interruptRutaModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 id="deletePaqueteModalLabel" class="displayInLine">
                    <spring:message code="header.interrupt.ruta"/>&nbsp;
                </h3>
            </div>                
            <div class="modal-body">
                <form name="updateInterruptRutaForm" novalidate="">                    
                    <div ng-show="ruta.isPausa == 1">
                        <label>Quitar pausa de la ruta</label>
                    </div>
                    <div>
                        <div ng-show="ruta.isPausa == 0">
                            <label>* <spring:message code="ruta.pausa.observaciones"></spring:message>:</label>
                            <textarea class="form-control" rows="5" ng-required="true" autofocus="" name="observaciones" ng-model="comentario"></textarea>
                            <span class="text-danger" ng-show="displayValidationError && updateInterruptRutaForm.observaciones.$error.required">Requerido</span>
                            <span class="text-danger" ng-show="displayValidationError && updateInterruptRutaForm.observaciones.invalid">Inválido</span>
                        </div>
                        <div> 
                            <label>* Cargar Imagen:</label>
                            <!--<button class="btn btn-default" type="file" ngf-select="uploadFiles($file, $invalidFiles)" ngf-pattern="image/*" accept="image/*"  ngf-max-size="20MB">
                                Seleccionar imagen
                            </button>-->
                            <button class="btn btn-default" type="file" ngf-select="uploadFiles($file, $invalidFiles)"  multiple accept="image/*" ngf-max-size="20MB">
                                Seleccionar archivo
                            </button>                            
                            <br><br>
                            <label>Archivo:</label>
                            <div >{{f.name}}
                                <span class="progress" ng-show="f.progress >= 0">
                                    <div style="width:{{f.progress}}%"   ng-bind="f.progress + '%'"></div>
                                </span>
                            </div>
                            <div class="alert alert-danger alert-warning alert-dismissible" role="alert" ng-show="errFile.name!==null && errFile.name!==undefined">
                            <p>Error en el tipo de archivo</p>
                            <br>
                            <p>Debe ser un archivo del tipo: {{errFile.$errorParam}}</p>
                            </div>
                        </div>                        
                    </div>    
                </form>
            </div>
            <div class="modal-footer">                              
                <input type="submit" class="btn btn-primary" ng-click="showConfirmRutaInterrupcion(updateInterruptRutaForm);" value='<spring:message code="update"/>'/>
                <button class="btn btn-default" data-dismiss="modal" ng-click="exit('#updateInterruptRutaForm');" aria-hidden="true">
                    <spring:message code="cancel"/>
                </button>                 
            </div>
        </div>
    </div>
</div>
                