<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div id="addBitacoraModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" style='width:100%;'>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 id="addBitacoraModalLabel" class="displayInLine">
                    <spring:message code="create"></spring:message>&nbsp;<spring:message code="bitacora"></spring:message>
                    </h3>
                </div>        
                <div class="modal-body">
                <form name="newBitacoraForm" novalidate="">
                        <div>
                            <label>* <spring:message code="ruta"></spring:message>:{{ruta.id}}</label>
                        </div>
                        <div>
                            <label>* <spring:message code="bitacora.idOperador"></spring:message>:</label>
                            <ui-select ng-model="bitacora.idOperador" name="idoperador" ng-required="true">
                              <ui-select-match placeholder="Select operador..." class="ui-select-match">
                                   <span ng-bind="$select.selected.nombre+' | '+$select.selected.nombre+' '+$select.selected.apellidoPat+' '+$select.selected.apellidoMat"></span>
                              </ui-select-match>
                              <ui-select-choices class="ui-select-choices" refresh="getOperador($select)" repeat="obj in operadores">
                                <span ng-bind-html="obj.usuario+' | '+obj.nombre+' '+obj.apellidoPat+' '+obj.apellidoMat | highlight: $select.search"></span>
                              </ui-select-choices>
                            </ui-select>
                            <span class="text-danger" ng-show="displayValidationError &amp;&amp; newBitacoraForm.idoperador.$error.required">Requerido</span>
                            <span class="text-danger" ng-show="displayValidationError &amp;&amp; newBitacoraForm.idoperador.invalid">Invalido</span>                                                                        
                        </div>   
                        <div>
                            <div class="row">
                                <div class="col-md-6">
                                    <p class="input-group col-md-6">
                                        <label>* <spring:message code="bitacora.vehiculo"></spring:message>:</label>
                                        <ui-select theme="bootstrap" ng-model="bitacora.idVehiculo" title="Vehiculo" name="vehiculo" ng-required="true">
                                            <ui-select-match placeholder="Seleccionar vehiculo" class="ui-select-match">
                                                <span ng-bind="$select.selected.noEconomico+' '+ $select.selected.placas+' '+$select.selected.marca"></span>
                                            </ui-select-match>
                                            <ui-select-choices repeat="obj in vehiculolist | filter: $select.search" class="ui-select-choices">
                                              <span ng-bind-html="obj.noEconomico+' '+obj.placas+' '+obj.marca | highlight: $select.search"></span>
                                              <small ng-bind-html="obj.descripcion | highlight: $select.search"></small>
                                            </ui-select-choices>
                                        </ui-select>                                         
                                        <span class="text-danger" ng-show="displayValidationError &amp;&amp; newBitacoraForm.vehiculo.$error.required">Requerido</span>
                                        <span class="text-danger" ng-show="displayValidationError &amp;&amp; newBitacoraForm.vehiculo.invalid">Invalido</span>                                                                        
                                    </p>
                                </div>
                            </div>
                        </div>                
                        <div>
                            <label>* <spring:message code="bitacora.ptoorigen"></spring:message>:</label>
                            <ui-select ng-model="bitacora.ptoOrigen" data-ng-disabled="disabled" name="idptoorigen" ng-required="true">
                              <ui-select-match placeholder="Select origen..." class="ui-select-match">
                                   <small ng-bind="$select.selected.nombrePunto+' | '+$select.selected.direccion"></small>
                              </ui-select-match>
                              <ui-select-choices class="ui-select-choices" refresh="getOrigenes($select)" repeat="obj in origenes">
                                <div ng-bind-html="obj.nombrePunto | highlight: $select.search"></div>
                                <small ng-bind-html="obj.direccion | highlight: $select.search"></small>
                              </ui-select-choices>
                            </ui-select>
                            <span class="text-danger" ng-show="displayValidationError &amp;&amp; newBitacoraForm.idptoorigen.$error.required">Requerido</span>
                            <span class="text-danger" ng-show="displayValidationError &amp;&amp; newBitacoraForm.idptoorigen.invalid">Invalido</span>                                                                                                                               
                        </div>
                        <div>
                            <label>* <spring:message code="bitacora.ptodestino"></spring:message>:</label>
                            <ui-select ng-model="bitacora.ptoDestino" data-ng-disabled="disabled" name="idpuntoe" ng-required="true">
                              <ui-select-match placeholder="Select punto entrega..." class="ui-select-match">
                                   <small ng-bind="$select.selected.nombrePunto+' | '+$select.selected.direccion"></small>
                              </ui-select-match>
                              <ui-select-choices class="ui-select-choices" refresh="getEntrega($select)" repeat="obj in entregas">
                                <div ng-bind-html="obj.nombrePunto | highlight: $select.search"></div>
                                <small ng-bind-html="obj.direccion | highlight: $select.search"></small>
                              </ui-select-choices>
                            </ui-select>  
                            <span class="text-danger" ng-show="displayValidationError &amp;&amp; newBitacoraForm.idpuntoe.$error.required">Requerido</span>
                            <span class="text-danger" ng-show="displayValidationError &amp;&amp; newBitacoraForm.idpuntoe.invalid">Invalido</span>                                                                        
                        </div>    
                        <div>
                            <label>* <spring:message code="bitacora.fecha"></spring:message>:</label>                            
                            <div class="row">
                                <div class="col-md-6">
                                    <p class="input-group " style="margin-top: 32px;">
                                        <input type="text" class="form-control" uib-datepicker-popup="{{format}}" ng-model="bitacora.fecha" is-open="popup1.opened" datepicker-options="dateOptions" ng-required="true" name="fecha" close-text="Close" ng-readonly="true"/>
                                        <span class="input-group-btn">
                                            <button type="button" class="btn btn-default" ng-click="open1()"><i class="glyphicon glyphicon-calendar"></i></button>
                                        </span>
                                    </p>                                   
                                    <span class="text-danger" ng-show="displayValidationError &amp;&amp; newBitacoraForm.fecha.$error.required">Requerido</span>
                                    <span class="text-danger" ng-show="displayValidationError &amp;&amp; newBitacoraForm.fecha.invalid">Invalido</span>                                    
                                </div>
                                <div class="col-md-4">
                                    <p class="input-group">
                                    <uib-timepicker ng-model="bitacora.fecha" hour-step="1" minute-step="1" name="sTime" show-meridian="false" show-spinners="true" readonly-input="false" name="hour" ng-required="true" ></uib-timepicker>
                                    </p>
                                    <span class="text-danger" ng-show="displayValidationError &amp;&amp; newBitacoraForm.hour.$error.required">Requerido</span>
                                    <span class="text-danger" ng-show="displayValidationError &amp;&amp; newBitacoraForm.hour.invalid">Invalido</span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <label class="col-sm-3">* Es bitácora final:</label>
                            <label class="col-sm-3">
                                <input type="radio" ng-model="bitacora.isFinal" value="1" name="isFinal">&nbsp;Si
                            </label>
                            <label class="col-sm-3">
                                <input type="radio" ng-model="bitacora.isFinal" value="0" name="isFinal">&nbsp;No
                            </label>
                            <span class="text-danger" ng-show="displayValidationError &amp;&amp; newBitacoraForm.isFinal.$error.required">Requerido</span>
                            <span class="text-danger" ng-show="displayValidationError &amp;&amp; newBitacoraForm.isFinal.invalid">Inválido</span>
                        </div>                             
                        <div class="row">
                            <label>*Tipo envio:&nbsp;{{ruta.idEnvio.desEnvio}}</label>    
                        </div>                            
                </form>
                </div>
                <div class="modal-footer">
                    <input class="btn btn-primary" ng-click="createBitacora(newBitacoraForm);" value="<spring:message code="create"></spring:message>" type="submit" data-dismiss="modal">
                    <button class="btn btn-default" data-dismiss="modal" ng-click="exit('#addBitacoraModal');" aria-hidden="true">
                        <spring:message code="cancel"></spring:message>
                    </button>
            </div>
        </div>
    </div>
</div>
                
<div id="deleteBitacoraModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 id="deletePaqueteModalLabel" class="displayInLine">
                    <spring:message code="cancel"/>&nbsp;<spring:message code="bitacora"/>
                </h3>
            </div>                
            <div class="modal-body">
                <form name="deleteBitacoraForm" novalidate="">
                    <div>
                        <div>                        
                            <label><spring:message code="bitacora.delete.confirm"/></label>
                        </div>                        
                        <div>                        
                            <label><spring:message code="bitacora.idBitacora"></spring:message>:</label>
                            <small>{{bitacora.id}}</small>
                        </div>
                        <div>                        
                            <label><spring:message code="bitacora.fecha"></spring:message>:</label>
                            <small>{{bitacora.fecha |  date:'dd/MM/yyyy'}} </small>
                        </div>
                        <div>                        
                            <label><spring:message code="bitacora.idOperador"></spring:message>:</label>
                            <small>{{bitacora.idOperador.nombre}}&nbsp;{{bitacora.idOperador.apellidoPat}}&nbsp;{{bitacora.idOperador.apellidoMat}}</small>
                        </div>
                        <div>                        
                            <label><spring:message code="bitacora.vehiculo"></spring:message>:</label>
                            <small>{{bitacora.idVehiculo.noEconomico}}&nbsp;{{bitacora.idVehiculo.descripcion}}</small>
                        </div>
                        <div>                        
                            <label><spring:message code="bitacora.ruta"></spring:message>:</label>
                            <small>{{bitacora.ptoOrigen.nombrePunto}}&nbsp;-&nbsp;{{bitacora.ptoDestino.nombrePunto}}</small>
                        </div>                    
                        
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <input type="submit" class="btn btn-primary" ng-click="deleteBitacora();" value='<spring:message code="delete"/>' data-dismiss="modal"/>
                <button class="btn btn-default" data-dismiss="modal" ng-click="exit('#deleteBitacoraForm');" aria-hidden="true">
                    <spring:message code="cancel"/>
                </button>                 
            </div>
        </div>
    </div>
</div>
                
<div id="updateStatusBitacoraModal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 id="deletePaqueteModalLabel" class="displayInLine">
                    <spring:message code="header.actualizarstatus"/>&nbsp;
                </h3>
            </div>                
            <div class="modal-body">
                <form name="updateStatusBitacoraForm" novalidate="">                    
                    <div>
                        <label>Siguiente Estatus: </label>&nbsp;{{nameNextStatus}}
                    </div>
                    <div>                        
                        <label><spring:message code="bitacora.idBitacora"></spring:message>:</label>
                        <small>{{bitacora.id}}</small>
                    </div>
                    <div>                        
                        <label><spring:message code="bitacora.fecha"></spring:message>:</label>
                        <small>{{bitacora.fecha | date:'dd/MM/yyyy'}}</small>
                    </div>
                    <div>                        
                        <label><spring:message code="bitacora.idOperador"></spring:message>:</label>
                        <small>{{bitacora.idOperador.nombre}}&nbsp;{{bitacora.idOperador.apellidoPat}}&nbsp;{{bitacora.idOperador.apellidoMat}}</small>
                    </div>
                    <div>                        
                        <label><spring:message code="bitacora.vehiculo"></spring:message>:</label>
                        <small>{{bitacora.idVehiculo.noEconomico}}&nbsp;{{bitacora.idVehiculo.placas}}&nbsp;{{bitacora.idVehiculo.marca}}</small>
                    </div>
                    <div>                        
                        <label><spring:message code="bitacora.ruta"></spring:message>:</label>
                        <small>{{bitacora.ptoOrigen.nombrePunto}}&nbsp;-&nbsp;{{bitacora.ptoDestino.nombrePunto}}</small>
                    </div>                    
                    <div>                        
                        <label>* <spring:message code="bitacora.kilometraje"></spring:message>:</label>
                        <input class="form-control" ng-required="true" ng-model="kmActual" name="kilometraje" placeholder="&nbsp;<spring:message code='bitacora.kilometraje'></spring:message>" type="text" only-digits>
                        <span class="text-danger" ng-show="displayValidationError && updateStatusBitacoraForm.kilometraje.$error.required">Requerido</span>
                        <span class="text-danger" ng-show="displayValidationError && updateStatusBitacoraForm.kilometraje.invalid">Inválido</span>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <input class="form-control" name="${_csrf.parameterName}" value="${_csrf.token}" type="hidden">                                
                <input type="submit" class="btn btn-primary" ng-click="showConfirmChangeStatus(updateStatusBitacoraForm);" value='<spring:message code="update"/>' data-dismiss="modal"/>
                <button class="btn btn-default" data-dismiss="modal" ng-click="exit('#updateStatusBitacoraForm');" aria-hidden="true">
                    <spring:message code="cancel"/>
                </button>                 
            </div>
        </div>
    </div>
</div>
                
<div id="uploadBitacoraImage" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 id="deletePaqueteModalLabel" class="displayInLine">
                    <spring:message code="header.uploadimages"/>&nbsp;
                </h3>
            </div>                
            <div class="modal-body">
                <form name="newImageBitacoraPaqueteForm" novalidate="" enctype="multipart/form-data">
                    <div> 
                        <label>* Cargar Archivos:</label>
                        <button class="btn btn-default" ngf-select="uploadFiles($files, $invalidFiles)" multiple accept="image/*" ngf-max-size="20MB">
                            Seleccionar archivos
                        </button>
                        <br><br>
                        <label>Archivos:</label>
                        <ul>
                          <li ng-repeat="file in f" style="font:smaller">{{file.name}} {{file.$errorParam}}
                            <span class="progress" ng-show="f.progress >= 0">
                              <div style="width:{{file.progress}}%"  
                                  ng-bind="file.progress + '%'"></div>
                            </span>
                          </li>
                          <li ng-repeat="f in errFiles" style="font:smaller">{{f.name}} {{f.$error}} {{f.$errorParam}}
                          </li> 
                        </ul>
                        {{errorMsg}}                        
                    </div> 
                </form>
            </div>
            <div class="modal-footer">
                <input type="submit" class="btn btn-primary" ng-click="addImageBitacora(newImageBitacoraPaqueteForm,true);" value='<spring:message code="update"/>' data-dismiss="modal"/>
                <button class="btn btn-default" data-dismiss="modal" ng-click="exit('#newImageBitacoraPaqueteForm');" aria-hidden="true">
                    <spring:message code="cancel"/>
                </button>                 
            </div>
        </div>
    </div>
</div>