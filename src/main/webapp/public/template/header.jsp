<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>


<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<div class="masthead" ng-controller="HeaderCtrl" id="principalheader">
    <h3 class="muted">
        <spring:message code='header.message'/>
    </h3>
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#">GNKL</a>
                </div>
                <ul class="nav navbar-nav">
                    <sec:authorize access="hasAnyRole('ROLE_MASTER', 'ROLE_ADMINISTRADOR', 'ROLE_OPERADOR')">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Menu principal<span class="caret"></span></a>
                        <sec:authorize access="hasAnyRole('ROLE_MASTER', 'ROLE_ADMINISTRADOR')">
                            <ul class="dropdown-menu">
                                <li ng-class="{'gray': activeURL == 'puntos', '': activeURL != 'puntos'}"><a title='Crud Punto' href="<c:url value='/protected/punto'/>"><p><spring:message code="header.points"/></p></a></li>
                                <li ng-class="{'gray': activeURL == 'usuarios', '': activeURL != 'usuarios'}"><a title='Crud usuario' href="<c:url value='/protected/usuario'/>"><p><spring:message code="header.users"/></p></a></li>
                                <li ng-class="{'gray': activeURL == 'ruta', '': activeURL != 'ruta'}"><a title='Crud Rutas' href="<c:url value='/protected/rutas'/>"><p><spring:message code="header.rutas"/></p></a></li>
                                <li ng-class="{'gray': activeURL == 'estadistica', '': activeURL != 'estadistica'}"><a title='Estadísticas' href="<c:url value='/protected/statistics'/>"><p><spring:message code="header.statistics"/></p></a></li>
                            </ul>
                        </sec:authorize>
                        <sec:authorize access="hasRole('ROLE_OPERADOR')">
                            <ul class="dropdown-menu">
                                <li ng-class="{'gray': activeURL == 'puntos', '': activeURL != 'puntos'}"><a title='Crud Punto' href="<c:url value='/protected/punto'/>"><p><spring:message code="header.points"/></p></a></li>                                
                            </ul>           
                        </sec:authorize>
                    </li>
                    </sec:authorize>
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">Bitácoras<span class="caret"></span></a>
                        <sec:authorize access="hasAnyRole('ROLE_MASTER', 'ROLE_ADMINISTRADOR')">
                            <ul class="dropdown-menu">
                                <li ng-class="{'gray': activeURL == 'bitacoras', '': activeURL != 'bitacoras'}"><a title='Bitacoras' href="<c:url value='/protected/bitacoras'/>"><p><spring:message code="header.bitacoras"/></p></a></li>
                            </ul>
                        </sec:authorize>
                        <sec:authorize access="hasRole('ROLE_OPERADOR')">
                            <ul class="dropdown-menu">
                                <li ng-class="{'gray': activeURL == 'bitacoras', '': activeURL != 'bitacoras'}"><a title='Bitacoras' href="<c:url value='/protected/bitacoras'/>"><p><spring:message code="header.bitacoras"/></p></a></li>
                            </ul>           
                        </sec:authorize>                        
                        <sec:authorize access="hasRole('ROLE_CLIENTE')">
                            <ul class="dropdown-menu">
                                <li ng-class="{'gray': activeURL == 'bitacoras', '': activeURL != 'bitacoras'}"><a title='Bitacoras' href="<c:url value='/protected/bitacoras'/>"><p><spring:message code="header.bitacoras"/></p></a></li>
                                <li ng-class="{'gray': activeURL == 'estadistica', '': activeURL != 'estadistica'}"><a title='Estadísticas' href="<c:url value='/protected/statistics'/>"><p><spring:message code="header.statistics"/></p></a></li>
                            </ul>           
                        </sec:authorize>                                                
                    </li>                    
                </ul>
                        
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#"><span class="glyphicon glyphicon-user"></span>&nbsp;{{user.usuario}}</a></li>
                    <li><a href="<c:url value="/logout" />"><span class="glyphicon glyphicon-log-out"></span></a></li>
                </ul>         
            </div>        
        </nav>
</div>
<script src="<c:url value='/resources/js/pages/header.js' />"></script>