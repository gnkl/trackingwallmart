<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<c:set var="contextPath" value="${pageContext.request.contextPath}"/>

<!doctype html>
<!--<html lang="pt-BR" id="ng-app" ng-app="app">-->
<html lang="pt-BR" id="ng-app">
<head>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />

        <title><spring:message  code="project.title" /></title>
        
        <!-- styles-->
        <link rel="stylesheet" href="<c:url value='/resources/css/bootstrap.min.css'/>">
        <link rel="stylesheet" href="<c:url value='/resources/css/bootstrap-theme.min.css'/>">
        <link rel="stylesheet" href="<c:url value='/resources/css/jquery.dataTables.min.css'/>">
        <link rel="stylesheet" href="<c:url value='/resources/css/select.min.css'/>">
        <link rel="stylesheet" href="<c:url value='/resources/css/sweetalert.css'/>"/>
        <link rel="stylesheet" href="<c:url value='/resources/css/simple.css'/>"/>
        <link rel="stylesheet" href="<c:url value='/resources/css/select2.css'/>">
        <link rel="stylesheet" href="<c:url value='/resources/css/selectize.default.css'/>">
        <link rel="stylesheet" href="<c:url value='/resources/css/project_style.css'  />"/>
        
        <!--<link href="<c:url value='/resources/css/bootstrap-modal-bs3patch.css'/>" rel="stylesheet" />-->
        <link href="<c:url value='/resources/css/bootstrap-modal.css'  />" rel="stylesheet" />
        
        <!-- js libraries -->
        <script src="<c:url value='/resources/js/jquery-2.1.4.min.js'/>"></script>
        <script src="<c:url value='/resources/js/angular.js'/>"></script>
        <script src="<c:url value='/resources/js/bootstrap.min.js'/>"></script>
        
        <script src="<c:url value='/resources/js/angular-route.js'/>"></script>
        <script src="<c:url value='/resources/js/angular-touch.js'/>"></script>
        <script src="<c:url value='/resources/js/angular-animate.js'/>"></script>

        <script src="<c:url value='/resources/js/smart-table.js'/>"></script>
        <script src="<c:url value='/resources/js/ui-bootstrap-tpls.min.js'/>"></script>
        <script src="<c:url value='/resources/js/angular-sanitize.js'  />"></script>
        <script src="<c:url value='/resources/js/pages/select.js'  />"></script>
        <script src="<c:url value='/resources/js/sweetalert.min.js'  />"></script>
        <script src="<c:url value='/resources/js/ngSweetAlert.js'  />"></script>
        <script src="<c:url value='/resources/js/ng-file-upload-all.min.js'/>"></script>
        <script src="<c:url value='/resources/js/ng-file-upload-shim.min.js'/>"></script>
        <script src="<c:url value='/resources/js/d3.v3.min.js'/>" charset="utf-8"></script>
        <script src="<c:url value='/resources/js/nv.d3.min.js'/>" charset="utf-8"></script>
        <script src="<c:url value='/resources/js/angular-nvd3.min.js'/>"></script>
        <script src="<c:url value='/resources/js/angular-locale_es-mx.js'/>"></script>
        <script src="<c:url value='/resources/js/angular-drag-and-drop-lists.js'/>"></script>
        <script src="<c:url value='/resources/js/checklist-model.js'/>"></script>
        <script src="<c:url value='/resources/js/ngMask.min.js'/>"></script>
        <script src="<c:url value='/resources/js/FileSaver.js'/>"></script>
        
        <script src="<c:url value='/resources/js/bootstrap-modalmanager.js'/>"></script>
        <script src="<c:url value='/resources/js/bootstrap-modal.js'/>"></script>
       
        
	<meta name="_csrf" content="${_csrf.token}"/>
	<!-- default header name is X-CSRF-TOKEN -->
	<meta name="_csrf_header" content="${_csrf.headerName}"/>
	<!-- ... -->
                
    </head>
    <body>
        <div id="loadingModal" class="modal fade in centering" aria-labelledby="" aria-hidden="true" tabindex="-1">
            <img src="<c:url value='/resources/img/ajax-loader.gif' />"/>
        </div>
        <!--<div id="loadingModal" class="modal fade" tabindex="-1" style="display: block;"><img src="<c:url value='/resources/img/ajax-loader.gif' />"/></div>        -->           
        <div class="container">
            <tiles:insertAttribute name="header" />

            <tiles:insertAttribute name="body" />
        </div>
        <tiles:insertAttribute name="footer" />
    </body>
</html>