var app = angular.module('estadistica', ['smart-table', 'ngAnimate', 'ui.bootstrap', 'ngSanitize', 'ui.select', 'hSweetAlert', 'nvd3','ngCsv']);

app.factory("sessionInjector", ['$log', 'sweet', function($log,sweet){
    return {
        request: function(config) {return config;},
        response: function(response) {
            if (typeof response.data === "string" && response.data.indexOf("forma-login") > -1) {
                location.reload();
            }
            return response;
        }
    };
}]);

app.config(function ($httpProvider) {
    $httpProvider.interceptors.push("sessionInjector");
    $httpProvider.defaults.headers.post = {"X-Requested-With": "XMLHttpRequest"};
    $httpProvider.defaults.headers.get = {"X-Requested-With": "XMLHttpRequest"};
    $httpProvider.defaults.headers.put = {"X-Requested-With": "XMLHttpRequest"};
}); 

app.controller('EstadisticaCtrl', ['$scope', '$http', '$location','$q', 'sweet', '$rootScope', '$timeout','$filter', function($scope, $http, $location,$q,  sweet,$rootScope,$timeout,$filter) {

    $scope.url = "../protected/statistics/";
    $scope.url_ruta = "../protected/rutas/";
    $scope.url_bitacora = "../protected/bitacoras/";
    $scope.url_rutas = "../protected/rutas/"; 

    $scope.totalViajes=0;
    $scope.detalleViajes=[];
    $scope.viajesCrossdock=[];
    $scope.viajesDirecto=[];
    $scope.itemsByPage = "10";
    $scope.tablestate=null;
    $scope.databarCrossDockChart = [];
    $scope.databarDirectoChart = [];
    
    $scope.listViajesByStatusCollection=[];
    $scope.listViajesByStatus=[];
    $scope.listViajesFaltantes=[];
    $scope.tablestateFaltante=null;
    $scope.tablestateIncompleto=null;
    $scope.tablestateCamion=null;
    $scope.tablestateDetalle=null;
    $scope.tablestateViajesCompletos=null;
    $scope.tablestateGrupo=null;
    $scope.leadsFaltante = [];
    $scope.leadsFaltanteCollection = [];    
    $scope.leadsIncompleto = [];
    $scope.leadsIncompletoCollection = [];
    $scope.totalViajesFaltantes=0;
    $scope.totalViajesIncompletos=0;
    $scope.leadsVehiculo = [];
    $scope.leadsVehiculoCollection = [];
    $scope.totalViajeCamion=0;
    $scope.itemsByPage=10;
    $scope.totalViajesCrossdock=0;
    $scope.totalViajesDestino=0;
    $scope.totalVecesEncontrado=0;
    $scope.selected={};
    $scope.origenes = [];
    $scope.entregas = [];
    $scope.leadsDetalle=[];
    $scope.leadsDetalleCollection=[];
    $scope.totalDetalleFound=0;
    $scope.leadsViajeCompleto=[];
    $scope.leadsViajeCompletoCollection=[];
    $scope.listGrupo=[];
    $scope.gruposelected=null;
    $scope.totalVecesGrupo=0;
    $scope.reportByFaseEstatus=true;
    $scope.currentFase=null;
    $scope.currentStatus=null;
    $scope.isLoading=false;
    $scope.leadsDetalleFase=[];
    $scope.leadsDetalleFaseCollection=[];
    $scope.leadsDetalleGrupo=[];
    $scope.leadsDetalleGrupoCollection=[];
    
    $scope.ruta={};
    $scope.vehiculo={};
    $scope.bitacora={};
    $scope.operadores = [];
    $scope.totalEncontrado=null;
    $scope.leadsDetalleCollection=[];
    
    $scope.leadsSafeBitacoraColletion=[];
    $scope.leadsBitacoraColletion=[];
    
    $scope.listSafeTipoEnvioCollection=[];
    $scope.listTipoEnvioCollection=[];
    
    $scope.estatusRow=null;
    $scope.tipoEnvio=null;
    $scope.idEnvio=null;
    $scope.url_paquete = "../protected/paquetes/";
    $scope.tablestate=null;
    
    $scope.leadsBitacoraParamsCollection=[];
    $scope.leadsBitacoraCollection=[];
    $scope.leadsHistorico=[];
    $scope.url_bitacora = "../protected/bitacoras/";
    $scope.listRutaItems=[];
    $scope.listRutaItemsSecure=[];
    //$scope.listGralEstatus=[];


    $scope.getTotalStatusRecoleccion=function(){
        var url = $scope.url+"/countbystatus";
        $scope.startDialogAjaxRequest();

        var config = {headers: { 'Content-Type': "application/json; charset=utf-8"},params:{'tipoEnvio':1}};

        $http.get(url, config)
            .success(function (data) {
                $scope.bitacoraRecoleccionList=data;
//                if(data!=null){
//                    angular.forEach(data, function (item) {
//                        $scope.listGralEstatus.push(item);
//                    });                    
//                }
                $scope.finishAjaxCallOnSuccess(data, '', false);                
            })
            .error(function(data, status, headers, config) {
                $scope.handleErrorInDialogs(status);
                sweet.show('Error', 'Error en el proceso de busqueda', 'error');
            });
    };

    $scope.getTotalStatusEntrega=function(){
        var url = $scope.url+"/countbystatus";
        $scope.startDialogAjaxRequest();

        var config = {headers: { 'Content-Type': "application/json; charset=utf-8"},params:{'tipoEnvio':3}};

        $http.get(url, config)
            .success(function (data) {
                $scope.bitacoraEntregaList=data;
//                if(data!=null){
//                    angular.forEach(data, function (item) {
//                        $scope.listGralEstatus.push(item);
//                    });                    
//                }
                $scope.finishAjaxCallOnSuccess(data, '', false);
            })
            .error(function(data, status, headers, config) {
                $scope.handleErrorInDialogs(status);
                sweet.show('Error', 'Error en el proceso de busqueda', 'error');
            });
    };
        

    $scope.getNumberOfPages = function(totaldata){
        return Math.ceil(totaldata / $scope.itemsByPage);
    };    
        
    $scope.startDialogAjaxRequest = function () {
        $scope.displayValidationError = false;
        $("#loadingModal").modal('show');
        $scope.previousState = $scope.state;
        $scope.state = 'busy';
    };
    
    $scope.finishAjaxCallOnSuccess = function (data, modalId, isPagination) {
        //$scope.tableParams.reload();
        //$("#loadingModal").modal('hide');
        if(!isPagination){
            if(modalId){
                $scope.exit(modalId);
            }
        }
        $scope.lastAction = '';
        $("#loadingModal").modal('hide');
    };
    
    $scope.exit = function (modalId) {
        $(modalId).modal('hide');
        $("#loadingModal").modal('hide');
    };
    
    $scope.handleErrorInDialogs = function (status) {
        //$("#loadingModal").modal('hide');
        angular.element(document.querySelector('#loadingModal')).modal('hide');
        $scope.state = $scope.previousState;
        // illegal access
        if(status === 403 || status === 401 || status === 302){
            $scope.errorIllegalAccess = true;
            $scope.state = 'error';
            window.location = "../welcome";
            return;
        }

        $scope.errorOnSubmit = true;
        $scope.lastAction = '';
    };
    
    $scope.optionsbarRecoleccionConfchart = {
            chart: {
                type: 'pieChart',
                height: 250,
                x: function(d){return d.des_status;},
                y: function(d){return d.total;},
                showLabels: false,
                duration: 500,
                labelThreshold: 0.01,
                labelSunbeamLayout: true,
                legend: {
                    margin: {
                        top: 5,
                        right: 35,
                        bottom: 5,
                        left: 0
                    }
                }
            }
    };

    $scope.optionsbarEntregaConfchart = {
            chart: {
                type: 'pieChart',
                height: 250,
                x: function(d){return d.des_status;},
                y: function(d){return d.total;},
                showLabels: false,
                duration: 500,
                labelThreshold: 0.01,
                labelSunbeamLayout: true,
                legend: {
                    margin: {
                        top: 5,
                        right: 35,
                        bottom: 5,
                        left: 0
                    }
                }
            }
    };
    

        
    
    $scope.getNumberOfPages = function(totaldata){
        return Math.ceil(totaldata / $scope.itemsByPage);
    };    

    $scope.getOperadores = function() {
          var completeurl = $scope.url_ruta+'/getoperador';
          var config = {headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}};
          $http.get(completeurl, config)
          .then(function(response) {
              console.log(response);
              $scope.operadores = response.data;
          });    
    };  
  
    $scope.getVehiculos = function() {
          var completeurl = $scope.url_ruta+'/getvehiculo';
          var config = {headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}};
          $http.get(completeurl, config)
          .then(function(response) {
              console.log(response);
              $scope.vehiculos = response.data;
          });          
    }

    $scope.onSelectIdOperadorCallback = function (item, model){
        if(item!==null && item!==undefined && item!==''){
          //console.log(item);
          $scope.idOperador=item;
        }
    };

    $scope.onSelectIdVehiculoCallback = function (item, model){
        if(item!==null && item!==undefined && item!==''){
          //console.log(item);
          //$scope.ruta.idOperador=item;
        }
    };        

    $scope.searchTotalByVehiculoOperador=function(form){

        console.log($scope.bitacora);
        
          var config = {headers: { 'Content-Type': "application/json; charset=utf-8"},params:{
              'idVehiculo':$scope.bitacora.idVehiculo!=null?$scope.bitacora.idVehiculo.id:'',
              'idOperador':$scope.bitacora.idOperador!=null?$scope.bitacora.idOperador.idUsu:'',
          }};   

          console.log(config);

          var url = $scope.url+'countByOperadorVehiculo';
          $http.get(url, config)
              .success(function (response) {
                  console.log(response);
                  $scope.totalEncontrado=response;
              })
              .error(function(data, status, headers, config) {
                  $scope.handleErrorInDialogs(status);
                  sweet.show('Error', 'Error en el proceso de busqueda', 'error');
              });
    }
    
    $scope.searchBitacoraListByVehiculoOperador=function(){

        if( ( $scope.bitacora.idVehiculo==null || $scope.bitacora.idVehiculo==undefined ) && ( $scope.bitacora.idOperador==null || $scope.bitacora.idOperador==undefined )){
            sweet.show('Warning', 'La búsqueda no puede procesarce se necesita seleccionar vehículo u operador', 'warning');
            return;
        }
        
          var config = {headers: { 'Content-Type': "application/json; charset=utf-8"},params:{
              'idVehiculo':$scope.bitacora.idVehiculo!=null?$scope.bitacora.idVehiculo.id:null,
              'idOperador':$scope.bitacora.idOperador!=null?$scope.bitacora.idOperador.idUsu:null,
          }};   

          console.log(config);

          var url = $scope.url+'/getByOperadorVehiculo';
          $http.get(url, config)
              .success(function (response) {
                console.log(response);
                $scope.leadsDetalleCollection=response;
              })
              .error(function(data, status, headers, config) {
                  $scope.handleErrorInDialogs(status);
                  sweet.show('Error', 'Error en el proceso de busqueda', 'error');
              });
    }    
  
    $scope.clearSearchByVehiculoOperador=function() {
        $scope.bitacora={};
        $scope.totalEncontrado=null;
    }
    
    $scope.getArrayToBeCsv=function(){
        var resultListObj=[];
        //return $scope.leadsDetalleCollection;
        angular.forEach($scope.leadsDetalleCollection, function (item) {
            //console.log(item);

            var objDetalle ={
                'nombre':item.nombre,
                'operador':item.idOperador.nombre+' '+item.idOperador.apellidoPat+' '+item.idOperador.apellidoMat,
                'vehiculo':item.idVehiculo.noEconomico+' '+item.idVehiculo.placas,
                'status':item.idStatus.desStatus,
                'kilometraje':$filter('number')(item.kmInicial)+'-'+$filter('number')(item.kmFinal),
                'fecha':$filter('date')(item.fecha,'yyyy-MM-dd HH:mm:ss')+'-'+$filter('date')(item.fechaFin,'yyyy-MM-dd HH:mm:ss'),
                'tipo':item.idEnvio.desEnvio,
                'ruta':item.idRuta.nombre
            };
            resultListObj.push(objDetalle);
        }); 
        return resultListObj;
    }
    
    $scope.getArrayToTotalesCsv = function(listTotales){
        var resultListObj=[];
        angular.forEach(listTotales, function (item) {
            var objDetalle ={
                'total':item.total,
                'des_status':item.des_status
            };
            resultListObj.push(objDetalle);
        }); 
        
//        angular.forEach(listEntrega, function (item) {
//            var objDetalle ={
//                'total':item.total,
//                'des_status':item.des_status
//            };
//            resultListObj.push(objDetalle);
//        }); 
        
        return resultListObj;        
    }
    
    $scope.exportData = function (tableId,excelName) {
        console.log(tableId);
        console.log(excelName);
    
        
//        var uri = 'data:application/vnd.ms-excel;base64,',
//            template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
//            base64 = function (s) {
//                return window.btoa(decodeURI(encodeURIComponent(s)));
//            },
//
//            format = function (s, c) {
//                return s.replace(/{(\w+)}/g, function (m, p) {
//                    return c[p];
//                });
//            };
//        // get the table data
//        var table = document.getElementById(tableId);
//        var ctx = {
//            worksheet: 'worksheet',
//            table: table.innerHTML
//        };
//        // if browser is IE then save the file as blob, tested on IE10 and IE11
//        var browser = window.navigator.appVersion;
//        if ((browser.indexOf('Trident') !== -1 && browser.indexOf('rv:11') !== -1) || (browser.indexOf('MSIE 10') !== -1)) {
//            var builder = new MSBlobBuilder(); 
//            builder.append(uri + format(template, ctx));
//            var blob = builder.getBlob('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); 
//            window.navigator.msSaveBlob(blob, excelName + '.xls'); 
//        } else {
//            //var element = document.getElementById(linkElement);
//            var a = document.createElement('a');
//            a.href = uri + base64(format(template, ctx));
//            a.target = '_blank';
//            a.setAttribute('download', excelName + '.xls');
//            document.body.appendChild(a);
//            a.click();
//        }        
//        var blob = new Blob([document.getElementById(tableId).innerHTML], {
//            type: "application/vnd.ms-excel;base64"
//        });
//        var now = $filter('date')(new Date(),'yyyy-MM-dd');
//        saveAs(blob, excelName+"_"+now+".xlsx");
//        var uri = 'data:application/vnd.ms-excel;base64,',
//            template = '<html xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:x="urn:schemas-microsoft-com:office:excel" xmlns="http://www.w3.org/TR/REC-html40"><head><!--[if gte mso 9]><xml><x:ExcelWorkbook><x:ExcelWorksheets><x:ExcelWorksheet><x:Name>{worksheet}</x:Name><x:WorksheetOptions><x:DisplayGridlines/></x:WorksheetOptions></x:ExcelWorksheet></x:ExcelWorksheets></x:ExcelWorkbook></xml><![endif]--></head><body><table>{table}</table></body></html>',
//            base64 = function (s) {
//                return window.btoa(decodeURI(encodeURIComponent(s)));
//            },
//
//            format = function (s, c) {
//                return s.replace(/{(\w+)}/g, function (m, p) {
//                    return c[p];
//                });
//            };
//        // get the table data
//        var table = document.getElementById(tableId);
//        var ctx = {
//            worksheet: excelName,
//            table: table.innerHTML
//        };
//        // if browser is IE then save the file as blob, tested on IE10 and IE11
//        var browser = window.navigator.appVersion;
//        if ((browser.indexOf('Trident') !== -1 && browser.indexOf('rv:11') !== -1) ||
//            (browser.indexOf('MSIE 10') !== -1)) {
//            var builder = new MSBlobBuilder(); 
//            builder.append(uri + format(template, ctx));
//            var blob = builder.getBlob('application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'); 
//            window.navigator.msSaveBlob(blob, excelName + '.xlsx'); 
//        } else {
//            var a = document.createElement('a');
//            a.href = uri + base64(format(template, ctx));
//            a.target = '_blank';
//            a.setAttribute('download', excelName + '.xlsx');
//            document.body.appendChild(a);
//            a.click();
//        }
    };    
      
//      $scope.exportData=function(tableId,excelName){ // ex: '#my-table'
//            var exportHref=Excel.tableToExcel(tableId,'sheet name');
//            $timeout(function(){location.href=exportHref;},100); // trigger download
//        }      

//      $scope.exportDataGral=function(){
//        $scope.listGralEstatus.push($scope.bitacoraRecoleccionList);
//        $scope.listGralEstatus.push($scope.bitacoraEntregaList);
//        return $scope.listGralEstatus;
//      }             
      
    $scope.open1 = function() {
      $scope.popup1.opened = true;
    };

    $scope.open2 = function() {
      $scope.popup2.opened = true;
    };
    
    $scope.popup1 = {
      opened: false
    };

    $scope.popup2 = {
      opened: false
    };    
        
    $scope.searchBitacoraListByStatusEnvio=function(row, tipoEnvio){
        $scope.isLoadingByParameters=true;
        $scope.leadsSafeBitacoraColletion=[];
        $scope.leadsBitacoraColletion=[];
        
        
        var listStatus = [];
        
        if(row.num_status==5){
            listStatus = [5,7];
        }else{
            listStatus = [row.num_status];
        }
        
        var config = {headers: { 'Content-Type': "application/json; charset=utf-8"},params:{
            'tipoEnvio':tipoEnvio,
            'listStatus':listStatus
        }};
    
        $scope.estatusRow=listStatus;
        $scope.tipoEnvio=tipoEnvio;

        $scope.startDialogAjaxRequest();
        
          var url = $scope.url+'getBitacoraDetalle';
          $http.get(url, config)
              .success(function (response) {
                console.log(response);
                $scope.leadsSafeBitacoraColletion=response;
                $scope.leadsBitacoraColletion=response;
                $scope.finishAjaxCallOnSuccess(response, '', false);
              })
              .error(function(data, status, headers, config) {
                  $scope.handleErrorInDialogs(status);
                  $scope.finishAjaxCallOnSuccess(data, '', false);
                  sweet.show('Error', 'Error en el proceso de busqueda', 'error');
              });    
    }
    
    $scope.createReporteBitacoraStatus = function(fileName){
        $scope.startDialogAjaxRequest();
        var now =  $filter('date')(new Date(),'yyyy-MM-dd');
        var namefile = fileName+now+".zip";
        var typefile='application/octet-stream';
        var url=''; 
        
        if($scope.estatusRow!=null && $scope.estatusRow!=undefined && $scope.tipoEnvio!=null && $scope.tipoEnvio!=undefined){
            $scope.startDialogAjaxRequest();
            var url = $scope.url+'getBitacoraDetalleReporte';

            $http.get(url, {responseType: 'arraybuffer',params:{'tipoEnvio':$scope.tipoEnvio,'listStatus':$scope.estatusRow, 'filename':fileName}})
              .success(function (data) {
                $timeout(function(){  
                var file = new Blob([data], {type: typefile});
                var fileURL = URL.createObjectURL(file);
                var a         = document.createElement('a');
                a.href        = fileURL; 
                a.target      = '_blank';
                a.download    = namefile;
                document.body.appendChild(a);
                a.click();
                });
                $scope.finishAjaxCallOnSuccess(data, '', false);
              }).error(function(data){
                  $scope.finishAjaxCallOnSuccess(data, '', false);
              });
        }        
        
    };         
    
    $scope.onSelectTipoEnvioCallback = function(item, model){
        if(item!==null && item!==undefined && item!==''){
            $scope.idEnvio=item;
        }
    }    
    
    $scope.getTipoEnvio = function () {
        var completeurl = $scope.url_paquete + 'searchenvio/';
        var config = {headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}};
        $http.get(completeurl, config)
        .then(function (response) {
            $scope.listTipoEnvioCollection=response.data.data;
        });            
    };    
    
    $scope.resetBitacoraSearch = function() {
        $scope.fechaInicio=null;
        $scope.fechaFin=null;
        $scope.idOperador=null;
        $scope.idEnvio!=null;
    }
    
    $scope.searchBitacoraFilter = function(){
        if($scope.tablestate!=null){
            $scope.serverBitacoraSearchFilter($scope.tablestate);
        }
        
    }
    
    $scope.serverBitacoraSearchFilter = function(tablestate) {
        $scope.tablestate=tablestate;        
        var date1 = null;
        var date2 = null;
        var idOperador=null;
        var idEnvio=null;
        
        if($scope.fechaInicio!=null && $scope.fechaInicio!=undefined && $scope.fechaFin!=null && $scope.fechaFin!=undefined){   
            date1 = $filter('date')($scope.fechaInicio, 'yyyy-MM-dd');
            date2 = $filter('date')($scope.fechaFin, 'yyyy-MM-dd');            
        }
        
        if($scope.idOperador!=null && $scope.idOperador!=undefined){
            idOperador = $scope.idOperador.idUsu;
        }

        if($scope.idEnvio!=null && $scope.idEnvio!=undefined){
            idEnvio = $scope.idEnvio.idEnvio;
        }
        
        var complete_url = $scope.url+"searchBitacoraDetalle";  
        var pagination = tablestate.pagination;
        var start = pagination.start || 0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
        var number = pagination.number || 10;  // Number of entries showed per page.    
        var config = {params: {page: Math.floor(start / number) + 1,'fechaInicio':date1,'fechaFin':date2,'idOperador':idOperador,'idEnvio':idEnvio}, headers: { 'Content-Type': 'application/json'}};
        //$scope.startDialogAjaxRequest();
        $http.get(complete_url,config).success(function(response){
            $scope.leadsBitacoraCollection=response.data;
            $scope.totalViajesFound=response.totalData;
            $scope.tablestate.pagination.numberOfPages = $scope.getNumberOfPages(response.totalData);            
            $scope.finishAjaxCallOnSuccess(response, "null", false);
        });        
    }
    
    $scope.getListHistoricoBitacoraByBitacoraId=function(value){
        if(value!=null && value!=undefined){
            var complete_url = $scope.url_bitacora+"gethistoricobitacora";  
            var config = {params: {'idbitacora':value.id}, headers: { 'Content-Type': 'application/json'}};
            $scope.startDialogAjaxRequest();
            $http.get(complete_url,config).success(function(response){
                $scope.leadsHistorico = response;
                $scope.leadsHistoricoCollection = [].concat($scope.leadsHistorico);
                $scope.finishAjaxCallOnSuccess(response.data, "null", false);
                $scope.getTotalTime();
            });                    
        }
    }    
    
    $scope.rowClick = function (idx){
        var value = $scope.leadsBitacoraCollection[idx];
        console.log(value);
        //$scope.bitacora=value;
        $scope.getListHistoricoBitacoraByBitacoraId(value);
    };
    
    $scope.getTotalTime=function(){
        $scope.totalKm=0;
        $scope.totalTiempo=0;
        $scope.totalTiempoMins=0;
        if($scope.leadsHistorico!=null && $scope.leadsHistorico!=undefined && $scope.leadsHistorico.length>0){
            var kmInicial = 0;
            var kmFinal = 0;
            
            var tiempoInicial = 0;
            var tiempoFinal = 0;
            
            kmInicial = $scope.leadsHistorico[0].kilometraje;
            kmFinal = $scope.leadsHistorico[$scope.leadsHistorico.length-1].kilometraje;
            $scope.totalKm = kmFinal - kmInicial;

            tiempoInicial = $scope.leadsHistorico[0].fecha;
            tiempoFinal = $scope.leadsHistorico[$scope.leadsHistorico.length-1].fecha;
            $scope.totalTiempo = tiempoFinal - tiempoInicial;
            var seconds = $scope.totalTiempo/1000;
            var minutos = seconds/60;  
            
            var hours = Math.trunc(minutos/60);
            var minutes = Math.round(minutos % 60);
            $scope.totalTiempoMins=$scope.zeroPad(hours,2) +":"+ $scope.zeroPad(minutes,2);
        }
    }
    
    $scope.getTimePerRow=function(index){
        var time=0;
        if(index>0){
            var tiempoInicial = $scope.leadsHistorico[index-1].fecha;
            var tiempoFinal = $scope.leadsHistorico[index].fecha;
            var tiempoTotal = tiempoFinal - tiempoInicial;
            var seconds = tiempoTotal/1000;
            var minutos = seconds/60;            
            var hours = Math.trunc(minutos/60);
            var minutes = Math.round(minutos % 60);
            time = $scope.zeroPad(hours,2) +":"+ $scope.zeroPad(minutes,2);
        }
        return time;
    }
    
    $scope.zeroPad=function(num, places) {
      var zero = places - num.toString().length + 1;
      return Array(+(zero > 0 && zero)).join("0") + num;
    }    
    
    
    $scope.exportBitacoraReporteParams=function() {
        var date1 = null;
        var date2 = null;
        var idOperador=null;
        var idEnvio=null;
        var typefile='application/octet-stream';
        
        if($scope.fechaInicio!=null && $scope.fechaInicio!=undefined && $scope.fechaFin!=null && $scope.fechaFin!=undefined){   
            date1 = $filter('date')($scope.fechaInicio, 'yyyy-MM-dd');
            date2 = $filter('date')($scope.fechaFin, 'yyyy-MM-dd');            
        }
        
        if($scope.idOperador!=null && $scope.idOperador!=undefined){
            idOperador = $scope.idOperador.idUsu;
        }

        if($scope.idEnvio!=null && $scope.idEnvio!=undefined){
            idEnvio = $scope.idEnvio.idEnvio;
        }
        
        var complete_url = $scope.url+"reporteBitacoraDetalle2";  
        $scope.startDialogAjaxRequest();

            var params = {'fechaInicio':date1,'fechaFin':date2,'idOperador':idOperador,'idEnvio':idEnvio};
            console.log(params);
            
            $http.get(complete_url, {responseType: 'arraybuffer',params: {'fechaInicio':date1,'fechaFin':date2,'idOperador':idOperador,'idEnvio':idEnvio}})
              .success(function (data) {
                $timeout(function(){  
                var file = new Blob([data], {type: typefile});
                var fileURL = URL.createObjectURL(file);
                var a         = document.createElement('a');
                a.href        = fileURL; 
                a.target      = '_blank';
                a.download    = "Detalle_Reporte_Parametros.zip";
                document.body.appendChild(a);
                a.click();
                });
                $scope.finishAjaxCallOnSuccess(data, '', false);
              }).error(function(data){
                  $scope.finishAjaxCallOnSuccess(data, '', false);
              });
    }
    
    $scope.getImagen = function (bitacora) {
        $scope.startDialogAjaxRequest();
        var typefile='application/zip';
        $http.get($scope.url_bitacora+'getimage', {responseType: 'arraybuffer',params:{'idbitacora':bitacora.id}})
          .success(function (data) {
               $timeout(function(){
                var file = new Blob([data], {type: typefile});
                var fileURL = URL.createObjectURL(file);
                var a         = document.createElement('a');
                a.href        = fileURL; 
                a.target      = '_blank';
                a.download    = new Date().getTime()+'.zip';
                document.body.appendChild(a);
                a.click();                               
                $scope.finishAjaxCallOnSuccess(data, null, true);
               });
          }).error(function(data){
              $scope.finishAjaxCallOnSuccess(data, null, true);
          });
    };    
        
        
    $scope.getListRuta = function($select) {
      if($select.search!==null && $select.search !== undefined && $select.search!==''){
          var completeurl = $scope.url_ruta+'getrutas/';
          var config = {params:{'idRuta':$select.search},headers: { 'Content-Type': 'application/json', 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}};
          $http.get(completeurl, config)
              .then(function(response) {
                $scope.listIdRuta = response.data;
                console.log(response);
              }, function(error) {
                console.log(error);
            });  
      }
    };        

  $scope.onSelectIdRutaCallback = function (item, model){
      if(item!==null && item!==undefined && item!==''){
        console.log(item);
        $scope.rutaselected = item;
        $scope.searchByRuta();
      }
  }; 
  
  $scope.searchByRuta=function(){
          var completeurl = $scope.url_ruta+'getRutaData/';
          var config = {params:{'idruta':($scope.rutaselected!=null && $scope.rutaselected!=undefined)?$scope.rutaselected.id:null},headers: { 'Content-Type': 'application/json', 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}};
          $http.get(completeurl, config)
              .then(function(response) {
                console.log(response);
                $scope.listRutaItems=response.data;
                $scope.listRutaItemsSecure=response.data;
              }, function(error) {
                console.log(error);
            });
  }
  
  $scope.resetSearchRutaTime=function(){
      $scope.rutaselected=null
      $scope.searchByRuta();
  }
  
    $scope.exportRutaTiempos=function() {
        var typefile='application/octet-stream';
        
        var complete_url = $scope.url_ruta+"reporteRutaTiemposExport";  
        $scope.startDialogAjaxRequest();
            
            $http.get(complete_url, {responseType: 'arraybuffer',params: {'idruta':null}})
              .success(function (data) {
                $timeout(function(){  
                var file = new Blob([data], {type: typefile});
                var fileURL = URL.createObjectURL(file);
                var a         = document.createElement('a');
                a.href        = fileURL; 
                a.target      = '_blank';
                a.download    = "Ruta_Reporte.zip";
                document.body.appendChild(a);
                a.click();
                });
                $scope.finishAjaxCallOnSuccess(data, '', false);
              }).error(function(data){
                  $scope.finishAjaxCallOnSuccess(data, '', false);
              });
    }  
    
    $scope.rowRutaClick=function(source){
        var selectedRuta = source;
        console.log(selectedRuta);
        var complete_url = $scope.url_bitacora+'searchBitacoraByRuta';
        var config = {params:{'idruta':selectedRuta.id},headers: { 'Content-Type': 'application/json', 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}};        
        $scope.startDialogAjaxRequest();
        
        $http.get(complete_url,config).success(function(response){
            //$scope.leadsBitacoraColletion=response;
            //$scope.leadsSafeBitacoraColletion=response;
            $scope.leadsBitacoraLast=response;
            $scope.finishAjaxCallOnSuccess(response, '', false);
        }).error(function(response){
            $scope.finishAjaxCallOnSuccess(null, '', false);
        });     
    }
    
    $scope.rowRutaClickLastBitacora=function(source){
        $scope.leadsBitacoraColletion=[];
        $scope.leadsSafeBitacoraColletion=[];
        $scope.leadsBitacoraLast=[];
        var selectedRuta = source;
        var config = {params:{'idruta':selectedRuta.id},headers: { 'Content-Type': 'application/json', 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}};        
        var test = $scope.url_rutas+'getCurrentBitacoraRuta';
        $http.get(test,config).success(function(response){
//            console.log(response);
            //$scope.leadsBitacoraColletion=response;
            //$scope.leadsSafeBitacoraColletion=response;
            $scope.leadsBitacoraLast=response;
            $scope.finishAjaxCallOnSuccess(response, '', false);
        }).error(function(response){
            $scope.finishAjaxCallOnSuccess(null, '', false);
        });             
    }
        
    angular.element(document).ready(function () {
        $scope.getTotalStatusRecoleccion();
        $scope.getTotalStatusEntrega();
        $scope.getOperadores();
        $scope.getVehiculos();
        $scope.getTipoEnvio();
    });

}]);