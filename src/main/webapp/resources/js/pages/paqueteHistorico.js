var hist = angular.module('historico', ['smart-table', 'ngAnimate', 'ui.bootstrap', 'ngSanitize', 'ui.select', 'hSweetAlert']);

hist.config(function ($httpProvider) {
    $httpProvider.defaults.headers.get = {"X-Requested-With": "XMLHttpRequest"};
    $httpProvider.defaults.headers.put = {"X-Requested-With": "XMLHttpRequest"};
}); 

hist.controller('PaqueteHistoricoCtrl', ['$scope', '$http', '$location','$q', 'sweet',function($scope, $http, $location,$q, sweet) {

    $scope.errorOnSubmit = false;
    $scope.errorIllegalAccess = false;
    $scope.displayMessageToUser = false;
    $scope.displayValidationError = false;
    $scope.displaySearchMessage = false;
    $scope.displaySearchButton = false;
    $scope.displayCreatePaqueteButton = false;
    $scope.paquete = {};
    $scope.ptoorigen=null;
    $scope.noEmbarque=null;
    $scope.idPuntoe=null;
    $scope.idOperador=null;
    $scope.fechaEntPaq=null;
    $scope.url = "/GnklTracking/protected/historico/";
    $scope.disabled = undefined;
    $scope.searchEnabled = undefined;    
    $scope.origenes = [];    
    $scope.entregas = [];
    $scope.operadores = [];
    
    $scope.leads = [];
    $scope.leadsCollection = [].concat($scope.leads);
    $scope.itemsByPage = "10";    
    $scope.tablestate={};
    
        
    $scope.searchHistorico = function (searchPaqueteForm, isPagination) {
        if (!($scope.searchHist) && (!searchPaqueteForm.$valid)) {
            $scope.displayValidationError = true;
            return;
        }

        $scope.lastAction = 'search';
        var url = $scope.url +  "search/"+$scope.searchHist;
        $scope.startDialogAjaxRequest();
        var pagination = $scope.tablestate.pagination;
        var start = pagination.start || 0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
        var number = pagination.number || 10;  // Number of entries showed per page.           
        var config = {params: {page: Math.floor(start / number) + 1}, headers: { 'Content-Type': 'application/json'}};
        $http.get(url, config)
            .success(function (data) {
                $scope.finishAjaxCallOnSuccess(data, null, isPagination);
                $scope.displaySearchMessage = true;
                            
                $scope.leads = data.data;
                $scope.leadsCollection = [].concat($scope.leads);
                if($scope.tablestate!=='undefined'){
                    $scope.tablestate.pagination.start = 0;
                    $scope.tablestate.pagination.numberOfPages = $scope.getNumberOfPages(data.totalData);
                }
                
            })
            .error(function(data, status, headers, config) {
                $scope.handleErrorInDialogs(status);
                sweet.show('Error', 'Error en el proceso de búsqueda', 'error');
            });
    };   

    $scope.resetSearch = function(){
        $scope.searchFor = "";
        $scope.pageToGet = 0;
        $scope.displaySearchMessage = false;
        $scope.tablestate.sort.predicate = undefined;
        $scope.tablestate.search.predicateObject={};

        $scope.leads = {};
        $scope.leadsCollection = [];
        if($scope.tablestate!=='undefined'){
            $scope.tablestate.pagination.start = 0
            $scope.tablestate.pagination.numberOfPages = 0;
        }        
    }    
        
    $scope.addSearchParametersIfNeeded = function(config, isPagination) {
        if(!config.params){
            config.params = {};
        }

        config.params.page = $scope.pageToGet;

        if($scope.searchFor){
            config.params.searchFor = $scope.searchFor;
        }
    }
    
    
    $scope.selectedPaquete = function (contact) {
        var selectedPaquete = angular.copy(contact);
        $scope.paquete = selectedPaquete;
    }    
   
    $scope.getImagen = function (paquetehist) {
        $scope.startDialogAjaxRequest();
        var array = paquetehist.dirArchivo.split('/');
        var sizearray = array.length;
        var namefile = array[sizearray-1];
        var typefile = "";
        if(namefile.indexOf("jpg") !== -1  || namefile.indexOf("jpeg") !== -1){
            typefile='application/jpg';
        }else if(namefile.indexOf("pdf") !== -1){
            typefile='application/pdf';
        }else if(namefile.indexOf("png") !== -1){
            typefile='application/png';
        }else{
            typefile='application/octet-stream';
        }
        
        $http.get($scope.url+'getimage/'+paquetehist.idHist, {responseType: 'arraybuffer'})
          .success(function (data) {
              
            var file = new Blob([data], {type: typefile});
            var fileURL = URL.createObjectURL(file);
            var a         = document.createElement('a');
            a.href        = fileURL; 
            a.target      = '_blank';
            a.download    = namefile;
            document.body.appendChild(a);
            a.click();            
            $scope.finishAjaxCallOnSuccess(data, null, isPagination);
          }).error(function(data){
              $scope.finishAjaxCallOnSuccess(data, null, isPagination);
          });
    } 
    
    $scope.handleErrorInDialogs = function (status) {
        $("#loadingModal").modal('hide');
        $scope.state = $scope.previousState;
        // illegal access
        if(status == 403 || status == 401){
            $scope.errorIllegalAccess = true;
            $scope.state = 'error';
            //window.location.reload();
            //  window.location = "/GnklTracking/accessdenied";
            //$location.path('/login');
            //return $q.reject(response);
            
            return;
        }

        $scope.errorOnSubmit = true;
        $scope.lastAction = '';
    }    
    
    $scope.startDialogAjaxRequest = function () {
        $scope.displayValidationError = false;
        $("#loadingModal").modal('show');
        $scope.previousState = $scope.state;
        $scope.state = 'busy';
    }    
    
    $scope.finishAjaxCallOnSuccess = function (data, modalId, isPagination) {
        //$scope.tableParams.reload();
        $("#loadingModal").modal('hide');

        if(!isPagination){
            if(modalId){
                $scope.exit(modalId);
            }
        }

        $scope.lastAction = '';
    }    
    
    $scope.exit = function (modalId) {
        console.log("hide "+modalId);
        $(modalId).modal('hide');

        $scope.paquete = {};
        $scope.errorOnSubmit = false;
        $scope.errorIllegalAccess = false;
        $scope.displayValidationError = false;
    }

   
    

    $scope.rowClick = function (idx)
    {
        $scope.editor = $scope.leads[idx];
    }

    $scope.serverFilter = function(tablestate){
        $scope.tablestate=tablestate;
        console.log($scope.tablestate);
        if($scope.searchFor!==undefined && $scope.searchFor!=='' && $scope.searchFor!=='undefined'){
            
            var pagination = tablestate.pagination;
            var start = pagination.start || 0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
            var number = pagination.number || 10;  // Number of entries showed per page.    
            var config = {params: {page: Math.floor(start / number) + 1}, headers: { 'Content-Type': 'application/json'}};
            var complete_url = $scope.url+"search/"+$scope.searchFor;  
            $scope.startDialogAjaxRequest();
            $http.get(complete_url,config).success(function(response){
                $scope.leads = response.data;
                $scope.leadsCollection = [].concat($scope.leads);
                //$scope.tablestate.pagination.numberOfPages = response.totalData;
                $scope.tablestate.pagination.numberOfPages = $scope.getNumberOfPages(response.totalData);
                $scope.finishAjaxCallOnSuccess(response.data, "null", false);
            }).error(function(response){
                sweet.show('Error', 'Error en el proceso de obtención históricos', 'error');
            });            
        }
    }

    $scope.getNumberOfPages = function(totaldata){
        return Math.ceil(totaldata / $scope.itemsByPage);
    }        
        
}]);

hist.filter('propsFilter', function() {
  return function(items, props) {
    var out = [];

    if (angular.isArray(items)) {
      items.forEach(function(item) {
        var itemMatches = false;

        var keys = Object.keys(props);
        for (var i = 0; i < keys.length; i++) {
          var prop = keys[i];
          var text = props[prop].toLowerCase();
          if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
            itemMatches = true;
            break;
          }
        }

        if (itemMatches) {
          out.push(item);
        }
      });
    } else {
      // Let the output be the input untouched
      out = items;
    }

    return out;
  };
});
