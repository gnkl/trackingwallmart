var app = angular.module('tabs', ['smart-table', 'ngAnimate', 'ui.bootstrap', 'ngSanitize', 'ui.select', 'hSweetAlert']);

app.config(function ($httpProvider) {
    $httpProvider.defaults.headers.post = {"X-Requested-With": "XMLHttpRequest"};
    $httpProvider.defaults.headers.get = {"X-Requested-With": "XMLHttpRequest"};
    $httpProvider.defaults.headers.put = {"X-Requested-With": "XMLHttpRequest"};
});

app.factory('dataShare',function($rootScope){
  var service = {};
  service.data = false;
  service.sendData = function(data){
      this.data = data;
      $rootScope.$broadcast('data_shared');
  };
  service.getData = function(){
    return this.data;
  };
  return service;
});

app.controller('TabCtrl', ['$scope', '$http', '$location','$q', 'sweet', '$rootScope', 'dataShare', function($scope, $http, $location,$q,  sweet,$rootScope, dataShare) {
        $scope.showTab=function(){
        };
        
        $scope.showTabRuta=function(){
            dataShare.sendData(null);
        };
}]);

app.controller('RutaCtrl', ['$scope', '$http', '$location','$q', 'sweet', '$rootScope', 'dataShare', function($scope, $http, $location,$q,  sweet,$rootScope, dataShare) {

    $scope.errorOnSubmit = false;
    $scope.errorIllegalAccess = false;
    $scope.displayMessageToUser = false;
    $scope.displayValidationError = false;
    $scope.displaySearchMessage = false;
    $scope.displaySearchButton = false;
    $scope.displayCreatePaqueteButton = false;
    $scope.ruta = {};
    $scope.ptoorigen=null;
    $scope.noEmbarque=null;
    $scope.idPuntoe=null;
    $scope.idOperador=null;
    $scope.fechaEntPaq=null;
    $scope.fechaRecPaq=null;
    $scope.url = "/GnklTracking/protected/rutas/";
    $scope.disabled = undefined;
    $scope.searchEnabled = undefined;    
    $scope.origenes = [];    
    $scope.entregas = [];
    $scope.operadores = [];
    $scope.tipoenvio = [];
    $scope.envioselected={};
    $scope.disabledOperador=false;
    $scope.leads = [];
    $scope.leadsCollection = [].concat($scope.leads);
    $scope.itemsByPage = "10";
    $scope.tablestate={};
    $scope.isgnkembarque=true;
    $scope.minDate= new Date();
    $scope.searchformorigen=[];
    $scope.searchformentrega=[];
    $scope.searchformoperador=[];
    $scope.selected={};
    $scope.estatus=[{'status':1,'descripcion':'Activo'}, {'status':3,'descripcion':'Inactivo'}];
    $scope.idruta=null;
    $scope.firsttab=false;
    
    
    
    $scope.createNewRuta=function(){
        $scope.ruta = {};
        $scope.selected={};
    };
    
    
    $scope.deleteRuta = function () {
        $scope.lastAction = 'delete';
        var url = $scope.url + 'delete/' +$scope.ruta.id;
        $scope.startDialogAjaxRequest();
        $http({
            method: 'POST',
            url: url
        }).success(function (data) {
                $scope.serverFilter($scope.tablestate);
                $scope.finishAjaxCallOnSuccess(data, "#deleteRutaModal", false);
                sweet.show('Ok', 'Paquete eliminado correctamente', 'success');
            }).error(function(data, status, headers, config) {
                $scope.handleErrorInDialogs(status);
                sweet.show('Error', 'Error en el proceso de eliminación', 'error');
            });
    };    

    
    $scope.createRuta = function (newRutaForm) {
        if (!newRutaForm.$valid) {
            $scope.displayValidationError = true;
            return;
        }
        $scope.lastAction = 'create';
        var url = $scope.url+'create/';
        $scope.startDialogAjaxRequest();      
        

        var rutatest ={
            id:$scope.ruta.id,
            nombre:$scope.ruta.nombre,
            estatus:$scope.selected.estatus.status,
            fechaCreacion:$scope.ruta.fechaCreacion,
            fechaModificacion:$scope.ruta.fechaModificacion,
            idOrigen:$scope.ruta.idOrigen,
            idDestino:$scope.ruta.idDestino            
        }    
        

        var config = {headers: { 'Content-Type': "application/json; charset=utf-8"}};
        $http.post(url, rutatest, config)
            .success(function (data) {
                $scope.ruta={};
                $scope.serverFilter($scope.tablestate);
                $scope.finishAjaxCallOnSuccess(data, "#addRutaModal", false);
                sweet.show('Ok', 'Datos guardados correctamente', 'success');
            })
            .error(function(data, status, headers, config) {
                $scope.ruta={};
                $scope.handleErrorInDialogs(status);
                $scope.finishAjaxCallOnSuccess(data, "#addPaqueteModal", false);
                sweet.show('Ok', 'Error en el proceso de creación '+data.actionMessage, 'error');
            });
    };    
    
    
    $scope.selectedRuta = function (ruta) {
        var selectedRuta = angular.copy(ruta);
        $scope.ruta = selectedRuta;
        if($scope.ruta!==undefined && $scope.ruta.estatus!==undefined){
            for(i=0;i<$scope.estatus.length;i++){
                if($scope.estatus[i].status===$scope.ruta.estatus){
                   $scope.selected.estatus=$scope.estatus[i];
                   break;
                }
            }
        }
    };
    
    $scope.sendRuta = function(ruta){
        dataShare.sendData(ruta);
        var myElements = angular.element('#paquetetab');
        var as = myElements.children().find("a");
        if (as.length > 0){
           var myEl = angular.element(myElements[0].querySelector('.nav-link'));
           myEl.trigger('click');
        }      
    };
        
    $scope.handleErrorInDialogs = function (status) {
        $("#loadingModal").modal('hide');
        $scope.state = $scope.previousState;
        // illegal access
        if(status === 403 || status === 401 || status === 302){
            $scope.errorIllegalAccess = true;
            $scope.state = 'error';
            window.location = "/GnklTracking/welcome";
            return;
        }

        $scope.errorOnSubmit = true;
        $scope.lastAction = '';
    }    
    
    $scope.startDialogAjaxRequest = function () {
        $scope.displayValidationError = false;
        $("#loadingModal").modal('show');
        $scope.previousState = $scope.state;
        $scope.state = 'busy';
    }    
    
    $scope.finishAjaxCallOnSuccess = function (data, modalId, isPagination) {
        //$scope.tableParams.reload();
        $("#loadingModal").modal('hide');

        if(!isPagination){
            if(modalId){
                $scope.exit(modalId);
            }
        }

        $scope.lastAction = '';
    }    
    
    $scope.exit = function (modalId) {
        $(modalId).modal('hide');
        $("#loadingModal").modal('hide');
        $scope.ruta = {};
        $scope.selected={};
        $scope.errorOnSubmit = false;
        $scope.errorIllegalAccess = false;
        $scope.displayValidationError = false;
    }

    $scope.serverFilter = function(tablestate)
    {
        var complete_url = $scope.url+"listrutas";  
        $scope.tablestate=tablestate;
        var pagination = tablestate.pagination;
        var start = pagination.start || 0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
        var number = pagination.number || 10;  // Number of entries showed per page.    
        var config = {params: {page: Math.floor(start / number) + 1}, headers: { 'Content-Type': 'application/json'}};
        $scope.startDialogAjaxRequest();
        $http.get(complete_url,config).success(function(response){
            $scope.leads = response.data;
            $scope.leadsCollection = [].concat($scope.leads);
            $scope.tablestate.pagination.numberOfPages = $scope.getNumberOfPages(response.totalData);
            $scope.finishAjaxCallOnSuccess(response.data, "null", false);
        });
    };

    $scope.getNumberOfPages = function(totaldata){
        return Math.ceil(totaldata / $scope.itemsByPage);
    };
    
  $scope.getOrigenes = function($select) {
    if($select.search!=='' && $select.search.length>3){
        var url = "/GnklTracking/protected/paquetes/";
        var completeurl = url+'searchorigen/'+$select.search;
        var config = {headers: { 'Content-Type': 'application/json', 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}};
        $http.get(completeurl, config)
            .then(function(response) {
              $scope.origenes = response.data.data;
            });    
        
    }  
  };  

  $scope.getEntrega = function($select) {
    if($select.search!=='' && $select.search.length>3){
        var url = "/GnklTracking/protected/paquetes/";
        var completeurl = url+'searchentrega/'+$select.search;
        var config = {headers: { 'Content-Type': 'application/json', 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}};
        $http.get(completeurl, config)
            .then(function(response) {
              $scope.entregas = response.data.data;
            });         
    }  
  };    
    
    
    
  // datepicket to select date
  
  $scope.today = function() {
    $scope.dt = new Date();
  };
  $scope.today();

  $scope.clear = function() {
    $scope.dt = null;
  };

  $scope.inlineOptions = {
    customClass: getDayClass,
    minDate: new Date(),
    showWeeks: true
  };

  $scope.dateOptions = {
    dateDisabled: disabled,
    formatYear: 'YYYY',
    //maxDate: new Date(2020, 5, 22),
    minDate: new Date(),
    startingDay: 1
  };

  /*$scope.dateOptions2 = {
    dateDisabled: disabled,
    formatYear: 'YYYY',
    minDate: $scope.paquete.fechaRecPaq,
    startingDay: 1
  };*/
        
  // Disable weekend selection
  function disabled(data) {
    var date = data.date,
      mode = data.mode;
    //return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
    return mode === 'day' && (date.getDay() > 6);
  }
  
  $scope.open1 = function() {
    $scope.popup1.opened = true;
    $scope.paquete.fechaEntPaq=null;
  };

  $scope.open2 = function() {
    $scope.popup2.opened = true;
  };

  $scope.setDate = function(year, month, day) {
    $scope.dt = new Date(year, month, day);
  };

  $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate','dd/MM/yyyy'];
  $scope.format = $scope.formats[4];
  $scope.altInputFormats = ['M!/d!/yyyy'];

  $scope.popup1 = {
    opened: false
  };

  $scope.popup2 = {
    opened: false
  };

  var tomorrow = new Date();
  tomorrow.setDate(tomorrow.getDate() + 1);
  var afterTomorrow = new Date();
  afterTomorrow.setDate(tomorrow.getDate() + 1);
  $scope.events = [
    {
      date: tomorrow,
      status: 'full'
    },
    {
      date: afterTomorrow,
      status: 'partially'
    }
  ];

  function getDayClass(data) {
    var date = data.date,
      mode = data.mode;
    if (mode === 'day') {
      var dayToCheck = new Date(date).setHours(0,0,0,0);

      for (var i = 0; i < $scope.events.length; i++) {
        var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);

        if (dayToCheck === currentDay) {
          return $scope.events[i].status;
        }
      }
    }
  }
      
    $scope.clickEventCheck = function(event) { 
        if(event==true){
            $scope.paquete.noEmbarque=$scope.randomString(2)+new Date().getTime();
        }else{
            $scope.paquete.noEmbarque='';
        }
     };      
     
    $scope.randomString = function ( n ) {
      var r="";
      while(n--)r+=String.fromCharCode((r=Math.random()*62|0,r+=r>9?(r<36?55:61):48));
      return r.toUpperCase();
    };  
        
}]);

app.controller('PaqueteCtrl', ['$scope', '$http', '$location','$q', 'sweet', '$rootScope',  'dataShare', '$timeout',function($scope, $http, $location,$q,  sweet,$rootScope,  dataShare, $timeout) {

    $scope.errorOnSubmit = false;
    $scope.errorIllegalAccess = false;
    $scope.displayMessageToUser = false;
    $scope.displayValidationError = false;
    $scope.displaySearchMessage = false;
    $scope.displaySearchButton = false;
    $scope.displayCreatePaqueteButton = false;
    $scope.paquete = {};
    $scope.ptoorigen=null;
    $scope.noEmbarque=null;
    $scope.idPuntoe=null;
    $scope.idOperador=null;
    $scope.fechaEntPaq=null;
    $scope.fechaRecPaq=null;
    $scope.url = "/GnklTracking/protected/paquetes/";
    $scope.disabled = undefined;
    $scope.searchEnabled = undefined;    
    $scope.origenes = [];    
    $scope.entregas = [];
    $scope.operadores = [];
    $scope.tipoenvio = [];
    $scope.envioselected={};
    $scope.disabledOperador=false;
    $scope.leads = [];
    $scope.leadsCollection = [].concat($scope.leads);
    $scope.itemsByPage = "10";
    $scope.tablestate={};
    $scope.isgnkembarque=true;
    $scope.minDate= new Date();
    $scope.searchformorigen=[];
    $scope.searchformentrega=[];
    $scope.searchformoperador=[];
    $scope.selected={};
    $scope.paquete.fechaRecPaq=new Date();
    $scope.paquete.fechaEntPaq=new Date();
    $scope.ruta=null;
    $scope.secondtab=true;
    $scope.showdata=false;
    $scope.is_data=false;
    
    $scope.$on('data_shared',function(){
        $scope.showdata=true;
        $scope.is_data=true;        
        var data =  dataShare.getData();    
        $scope.ruta=data;
        if($scope.ruta!==null && $scope.ruta!==undefined){
            sweet.show('Ok', 'Carga de detalle completada', 'success');
            $scope.serverFilter($scope.tablestate);
            $timeout(function() {   
                angular.element('#paquetetab').find( "a" ).triggerHandler('click');
            }, 0);            
        }else{
            $scope.showdata=false;
            $scope.is_data=false;
        }
    });

    $scope.resetPaquete = function(){
        $scope.paquete = {};
        //var timestamp = new Date().getTime();
        if($scope.paquete.noEmbarque==null || $scope.paquete.noEmbarque==undefined){
            $scope.paquete.noEmbarque=$scope.randomString(2)+new Date().getTime();
        }
        
        console.log($scope.paquete.noEmbarque);        
    };
    
    $scope.deletePaquete = function () {
        $scope.lastAction = 'delete';
        var url = $scope.url + 'delete/' +$scope.paquete.idPaq;
        $scope.startDialogAjaxRequest();
        $http({
            method: 'POST',
            url: url
        }).success(function (data) {
                $scope.leads = data.data;
                $scope.leadsCollection = [].concat($scope.leads);
                if($scope.tablestate!='undefined'){
                    $scope.tablestate.pagination.start = 0
                    //$scope.tablestate.pagination.numberOfPages = data.totalData;
                    $scope.tablestate.pagination.numberOfPages = $scope.getNumberOfPages(data.totalData);
                }            
                $scope.resetPaquete();
                $scope.finishAjaxCallOnSuccess(data, "#deletePaqueteModal", false);
                sweet.show('Ok', 'Paquete eliminado correctamente', 'success');
            }).error(function(data, status, headers, config) {
                $scope.handleErrorInDialogs(status);
                sweet.show('Error', 'Error en el proceso de eliminación', 'error');
            });
    };    

    $scope.updatePaquete = function (updatePaqueteForm) {
        if (!updatePaqueteForm.$valid) {
            $scope.displayValidationError = true;
            return;
        }

        $scope.lastAction = 'update';

        var url = $scope.url + 'update/' +$scope.paquete.idPaq;

        $scope.startDialogAjaxRequest();

        var config = {headers: {'Content-Type': 'application/json; charset=UTF-8'}};
        var statustemp = {idEnvio:$scope.envioselected.tipo};        
        var paquete2 ={
            idPaq:$scope.paquete.idPaq,
            noEmbarque:$scope.paquete.noEmbarque,
            fecCargaPaq:$scope.paquete.fecCargaPaq,
            fechaEntPaq:$scope.paquete.fechaEntPaq,
            fechaRecPaq:$scope.paquete.fechaRecPaq,
            idOperador:$scope.paquete.idOperador,
            idPuntoorigen:$scope.paquete.idPuntoorigen,
            idStatus:statustemp,
            idPuntoe:$scope.paquete.idPuntoe,
            idUsu:$scope.paquete.idUsu,
            idRuta:$scope.ruta
        }          

        $http.post(url, paquete2, config)
            .success(function (data) {
                $scope.leads = data.data;
                $scope.leadsCollection = [].concat($scope.leads);
                if($scope.tablestate!='undefined'){
                    $scope.tablestate.pagination.start = 0;
                    $scope.tablestate.pagination.numberOfPages = $scope.getNumberOfPages(data.totalData);
                }                
                $scope.finishAjaxCallOnSuccess(data, "#updatePaqueteModal", false);
                $scope.resetPaquete();
                sweet.show('Ok', 'Datos actualizados correctamente', 'success');
            })
            .error(function(data, status, headers, config) {
                $scope.handleErrorInDialogs(status);
                sweet.show('Error', 'Error en el proceso de actualización', 'error');
            });
    };
    
    $scope.createPaquete = function (newPaqueteForm) {
        if (!newPaqueteForm.$valid) {
            $scope.displayValidationError = true;
            return;
        }
        if($scope.ruta===null || $scope.ruta===undefined){
            sweet.show('Error', 'Error no existe ruta seleccionada', 'error');
            return;
        }
        
        $scope.lastAction = 'create';
        var url = $scope.url+'create/'+$scope.ruta.id;
        $scope.startDialogAjaxRequest();      
        
        var statustemp = {idEnvio:$scope.envioselected.tipo};
        var paquetetest ={
            noEmbarque:$scope.paquete.noEmbarque,
            fecCargaPaq:'',
            fechaEntPaq:$scope.paquete.fechaEntPaq,
            fechaRecPaq:$scope.paquete.fechaRecPaq,
            idOperador:$scope.paquete.idOperador,
            idPuntoorigen:$scope.paquete.idPuntoorigen,
            idUsu:$scope.paquete.idUsu,
            idStatus:statustemp,
            idPuntoe:$scope.paquete.idPuntoe  
        }    
        //console.log(paquetetest);        
        var config = {headers: { 'Content-Type': "application/json; charset=utf-8"}};
        $http.post(url, paquetetest, config)
            .success(function (data) {
                $scope.paquete={};
                $scope.leads = data.data;
                $scope.leadsCollection = [].concat($scope.leads);
                if($scope.tablestate!='undefined'){
                    $scope.tablestate.pagination.start = 0
                    $scope.tablestate.pagination.numberOfPages = $scope.getNumberOfPages(data.totalData);
                }                        
                $scope.finishAjaxCallOnSuccess(data, "#addPaqueteModal", false);
                sweet.show('Ok', 'Datos guardados correctamente', 'success');
            })
            .error(function(data, status, headers, config) {
                $scope.paquete={};
                $scope.handleErrorInDialogs(status);
                $scope.finishAjaxCallOnSuccess(data, "#addPaqueteModal", false);
                sweet.show('Ok', 'Error en el proceso de creación '+data.actionMessage, 'error');
            });
    };    
    
    $scope.searchPaquete = function (searchPaqueteForm, isPagination) {
        if ((!searchPaqueteForm.$valid)) {
            $scope.displayValidationError = true;
            return;
        }

        $scope.lastAction = 'search';
        var url = $scope.url +  "search";
        $scope.startDialogAjaxRequest();
        
        console.log($scope.selected);
        
//        var datasearch = {
//            'noembarque':$scope.searchFor,
//            'idoperador':($scope.selected.operadorid!==null && $scope.selected.operadorid!==undefined)?$scope.selected.operadorid.idUsu:undefined,
//            'puntoorigen':($scope.selected.puntoo!==null && $scope.selected.puntoo!==undefined)?$scope.selected.puntoo.idPuntoe:undefined,
//            'puntoentrega':($scope.selected.puntoe!==null && $scope.selected.puntoe!==undefined)?$scope.selected.puntoe.idPuntoe:undefined,
//        };
        var config = {headers: { 'Content-Type': "application/json; charset=utf-8"},params:{
            'noembarque':($scope.searchFor!==null && $scope.searchFor!==undefined)?$scope.searchFor:undefined,
            'idoperador':($scope.selected.operadorid!==null && $scope.selected.operadorid!==undefined)?$scope.selected.operadorid.idUsu:undefined,
            'puntoorigen':($scope.selected.puntoo!==null && $scope.selected.puntoo!==undefined)?$scope.selected.puntoo.idPuntoe:undefined,
            'puntoentrega':($scope.selected.puntoe!==null && $scope.selected.puntoe!==undefined)?$scope.selected.puntoe.idPuntoe:undefined,
        }};
        console.log(config);
        console.log(url);
        $http.get(url, config)
            .success(function (data) {
                $scope.displaySearchMessage = true;
                $scope.leads = data.data;
                $scope.leadsCollection = [].concat($scope.leads);
                if($scope.tablestate!='undefined'){
                    $scope.tablestate.pagination.start = 0
                    $scope.tablestate.pagination.numberOfPages = $scope.getNumberOfPages(data.totalData);
                }
                $scope.finishAjaxCallOnSuccess(data, "#searchContactsModal", isPagination);
            })
            .error(function(data, status, headers, config) {
                $scope.handleErrorInDialogs(status);
                sweet.show('Error', 'Error en el proceso de busqueda', 'error');
            });
    };   

    $scope.resetSearch = function(){
        $scope.searchFor = "";
        $scope.pageToGet = 0;
        $scope.displaySearchMessage = false;
        $scope.tablestate.sort.predicate = undefined;
        $scope.tablestate.search.predicateObject={};
        $scope.serverFilter($scope.tablestate);
        $scope.selected={};
//        $scope.selected.operadorid={};
//        $scope.selected.puntoo={};
//        $scope.selected.puntoe={};
    };
        
    $scope.addSearchParametersIfNeeded = function(config, isPagination) {
        if(!config.params){
            config.params = {};
        }

        config.params.page = $scope.pageToGet;

        if($scope.searchFor){
            config.params.searchFor = $scope.searchFor;
        }
    }
    
    
    $scope.selectedPaquete = function (contact) {
        $scope.disabledOperador=false;
        var selectedPaquete = angular.copy(contact);
        $scope.paquete = selectedPaquete;
        console.log($scope.paquete);
//        $scope.paquete.idOperador=$scope.paquete.idUsu;
        if($scope.paquete!==undefined && $scope.paquete.idStatus!==undefined){
            $scope.envioselected.tipo = $scope.paquete.idStatus.idEnvio;
        }
        var result = $scope.checkIfUserIsOperador($scope.paquete.idOperador.roles);
        $scope.disabledOperador=result;
    }    
        
    $scope.checkIfUserIsOperador = function(roles){
        var founded=false;
        angular.forEach(roles, function(value, key) {
          var obj = value;
          if(obj.desRol=='ROLE_OPERADOR'){
              founded=true;
          }
        });
        return founded;
    };
        
    $scope.handleErrorInDialogs = function (status) {
        $("#loadingModal").modal('hide');
        $scope.state = $scope.previousState;
        // illegal access
        if(status === 403 || status === 401 || status === 302){
            $scope.errorIllegalAccess = true;
            $scope.state = 'error';
            console.log(status);
            //window.location.reload();
            window.location = "/GnklTracking/welcome";
            //$location.path('/login');
            //return $q.reject(response);
            
            return;
        }

        $scope.errorOnSubmit = true;
        $scope.lastAction = '';
    }    
    
    $scope.startDialogAjaxRequest = function () {
        $scope.displayValidationError = false;
        $("#loadingModal").modal('show');
        $scope.previousState = $scope.state;
        $scope.state = 'busy';
    }    
    
    $scope.finishAjaxCallOnSuccess = function (data, modalId, isPagination) {
        //$scope.tableParams.reload();
        $("#loadingModal").modal('hide');

        if(!isPagination){
            if(modalId){
                $scope.exit(modalId);
            }
        }

        $scope.lastAction = '';
    }    
    
    $scope.exit = function (modalId) {
        $(modalId).modal('hide');
        $("#loadingModal").modal('hide');
        $scope.paquete = {};
        $scope.errorOnSubmit = false;
        $scope.errorIllegalAccess = false;
        $scope.displayValidationError = false;
    }
    
    $scope.populateTable = function (data) {
        if (data.pagesCount > 0) {
            $scope.state = 'list';

            $scope.page = {currentPage: $scope.pageToGet, pagesCount: data.pagesCount, totalContacts : data.totalPaquetes};
            $scope.displayCreatePaqueteButton = true;
            $scope.displaySearchButton = true;
        } else {
            $scope.state = 'noresult';
            $scope.displayCreatePaqueteButton = true;

            if(!$scope.searchFor){
                $scope.displaySearchButton = false;
            }
        }

        if (data.actionMessage || data.searchMessage) {
            $scope.displayMessageToUser = $scope.lastAction != 'search';

            $scope.page.actionMessage = data.actionMessage;
            $scope.page.searchMessage = data.searchMessage;
        } else {
            $scope.displayMessageToUser = false;
        }
    };    
    
    $scope.createNewPaquete=function(){
        $scope.resetPaquete();
        if($scope.paquete!==undefined && $scope.paquete.idOperador===undefined ){
        var url='/GnklTracking/protected/usuario/getusuario/';
        var config = {headers: { 'Content-Type': "application/json; charset=utf-8"}};
        $http.get(url, config)
            .success(function (data) {
                //console.log(data);
                var roles = data.roles;
                var result = $scope.checkIfUserIsOperador(roles);
                $scope.disabledOperador=result;
                console.log(result);
                console.log($scope.disabledOperador);
                if(result){
                    $scope.paquete.idOperador=data;
                }
            })
            .error(function(data, status, headers, config) {

            });   
            
        }
    };

    $scope.rowClick = function (idx)
    {
        $scope.editor = $scope.leads[idx];
    };

    $scope.serverFilter = function(tablestate)
    {
        $scope.tablestate=tablestate;
        if($scope.ruta!==null && $scope.ruta!==undefined){
            var complete_url = $scope.url+"listrutapaquete";  
//            $scope.tablestate=tablestate;
            var pagination = tablestate.pagination;
            var start = pagination.start || 0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
            var number = pagination.number || 10;  // Number of entries showed per page.    
            var config = {params: {page: Math.floor(start / number) + 1, idruta:$scope.ruta.id}, headers: { 'Content-Type': 'application/json'}};
            $scope.startDialogAjaxRequest();
            $http.get(complete_url,config).success(function(response){
                $scope.leads = response.data;
                $scope.leadsCollection = [].concat($scope.leads);
                $scope.tablestate.pagination.numberOfPages = $scope.getNumberOfPages(response.totalData);
                $scope.finishAjaxCallOnSuccess(response.data, "null", false);
            });            
        }
    };

    $scope.getNumberOfPages = function(totaldata){
        return Math.ceil(totaldata / $scope.itemsByPage);
    };
    
    $scope.getTipoEnvio = function () {
        var completeurl = $scope.url + 'searchenvio/';
        var config = {headers: {'Content-Type': 'application/json', 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}};
        $http.get(completeurl, config)
                .then(function (response) {
                    $scope.tipoenvio = response.data.data;
                    console.log($scope.tipoenvio);
                });            
    };
  
  // Any function returning a promise object can be used to load values asynchronously
  $scope.getOrigenes = function($select) {
    if($select.search!=='' && $select.search.length>3){
        var completeurl = $scope.url+'searchorigen/'+$select.search;
        var config = {headers: { 'Content-Type': 'application/json', 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}};
        $http.get(completeurl, config)
            .then(function(response) {
              $scope.origenes = response.data.data;
            });    
        
    }  
  };  

//  $scope.getOrigenesSearch = function() {
//    var completeurl = $scope.url+'searchorigen/';
//    var config = {headers: { 'Content-Type': 'application/json', 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}};
//    $http.get(completeurl, config)
//    .then(function(response) {
//      $scope.searchformorigen = response.data.data;
//    });    
//  }; 

  // Any function returning a promise object can be used to load values asynchronously
  $scope.getEntrega = function($select) {
    if($select.search!=='' && $select.search.length>3){
    var completeurl = $scope.url+'searchentrega/'+$select.search;
    var config = {headers: { 'Content-Type': 'application/json', 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}};
//    var result = [];
//    var deferred =  $q.defer();
        $http.get(completeurl, config)
            .then(function(response) {
              $scope.entregas = response.data.data;
            });         
    }  
  };  

//  $scope.getEntregaSearch = function() {
//    var completeurl = $scope.url+'searchentrega/';
//    var config = {headers: { 'Content-Type': 'application/json', 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}};
//    $http.get(completeurl, config)
//        .then(function(response) {
//          //$scope.entregas = response.data.data;
//          $scope.searchformentrega = response.data.data;
//        });         
//  };  

  // Any function returning a promise object can be used to load values asynchronously
  $scope.getOperador = function($select) {
    if($select.search!=''){
        var completeurl = $scope.url+'searchoperador/'+$select.search;
        var config = {headers: { 'Content-Type': 'application/json', 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}};
        $http.get(completeurl, config)
            .then(function(response) {
              $scope.operadores = response.data.data;
              console.log($scope.operadores);
            });  
    }
  };  

  $scope.getOperadorSearch = function() {
    var completeurl = $scope.url+'searchoperador/';
    var config = {headers: { 'Content-Type': 'application/json', 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}};
    $http.get(completeurl, config)
        .then(function(response) {
          $scope.searchformoperador = response.data.data;
        });  
  };  
        
  // datepicket to select date
  
  $scope.today = function() {
    $scope.dt = new Date();
  };
  $scope.today();

  $scope.clear = function() {
    $scope.dt = null;
  };

  $scope.inlineOptions = {
    customClass: getDayClass,
    minDate: new Date(),
    showWeeks: true
  };

  $scope.dateOptions = {
    dateDisabled: disabled,
    formatYear: 'YYYY',
    //maxDate: new Date(2020, 5, 22),
    minDate: new Date(),
    startingDay: 1
  };

  /*$scope.dateOptions2 = {
    dateDisabled: disabled,
    formatYear: 'YYYY',
    minDate: $scope.paquete.fechaRecPaq,
    startingDay: 1
  };*/
        
  // Disable weekend selection
  function disabled(data) {
    var date = data.date,
      mode = data.mode;
    //return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
    return mode === 'day' && (date.getDay() > 6);
  }
  
  $scope.open1 = function() {
    $scope.popup1.opened = true;
    $scope.paquete.fechaEntPaq=null;
  };

  $scope.open2 = function() {
    $scope.popup2.opened = true;
  };

  $scope.setDate = function(year, month, day) {
    $scope.dt = new Date(year, month, day);
  };

  $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate','dd/MM/yyyy'];
  $scope.format = $scope.formats[4];
  $scope.altInputFormats = ['M!/d!/yyyy'];

  $scope.popup1 = {
    opened: false
  };

  $scope.popup2 = {
    opened: false
  };

  var tomorrow = new Date();
  tomorrow.setDate(tomorrow.getDate() + 1);
  var afterTomorrow = new Date();
  afterTomorrow.setDate(tomorrow.getDate() + 1);
  $scope.events = [
    {
      date: tomorrow,
      status: 'full'
    },
    {
      date: afterTomorrow,
      status: 'partially'
    }
  ];

  function getDayClass(data) {
    var date = data.date,
      mode = data.mode;
    if (mode === 'day') {
      var dayToCheck = new Date(date).setHours(0,0,0,0);

      for (var i = 0; i < $scope.events.length; i++) {
        var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);

        if (dayToCheck === currentDay) {
          return $scope.events[i].status;
        }
      }
    }
  }
      
    $scope.clickEventCheck = function(event) { 
        if(event==true){
            $scope.paquete.noEmbarque=$scope.randomString(2)+new Date().getTime();
        }else{
            $scope.paquete.noEmbarque='';
        }
     };      
     
    $scope.randomString = function ( n ) {
      var r="";
      while(n--)r+=String.fromCharCode((r=Math.random()*62|0,r+=r>9?(r<36?55:61):48));
      return r.toUpperCase();
    };  
        
    angular.element(document).ready(function () {
        $scope.getTipoEnvio();
        $scope.getOperadorSearch();
        //$scope.getEntregaSearch();
        //$scope.getOrigenesSearch();
    });            

}]);


app.filter('propsFilter', function() {
  return function(items, props) {
    var out = [];

    if (angular.isArray(items)) {
      items.forEach(function(item) {
        var itemMatches = false;

        var keys = Object.keys(props);
        for (var i = 0; i < keys.length; i++) {
          var prop = keys[i];
          var text = props[prop].toLowerCase();
          if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
            itemMatches = true;
            break;
          }
        }

        if (itemMatches) {
          out.push(item);
        }
      });
    } else {
      // Let the output be the input untouched
      out = items;
    }

    return out;
  };
});