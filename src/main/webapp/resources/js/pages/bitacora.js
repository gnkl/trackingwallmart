var app = angular.module('bitacora', ['smart-table', 'ngAnimate', 'ui.bootstrap', 'ngSanitize', 'ui.select', 'hSweetAlert', 'ngFileUpload']);

app.factory("sessionInjector", ['$log', 'sweet', function($log,sweet){
    return {
        request: function(config) {return config;},
        response: function(response) {
            if (typeof response.data === "string" && response.data.indexOf("forma-login") > -1) {
                location.reload();
            }
            return response;
        }
    };
}]);

app.config(function ($httpProvider) {
    $httpProvider.interceptors.push("sessionInjector");
    $httpProvider.defaults.headers.post = {"X-Requested-With": "XMLHttpRequest"};
    $httpProvider.defaults.headers.get = {"X-Requested-With": "XMLHttpRequest"};
    $httpProvider.defaults.headers.put = {"X-Requested-With": "XMLHttpRequest"};
}); 


app.controller('BitacoraCtrl', ['$scope', '$http', '$location','$q', 'sweet', '$rootScope', '$filter', '$timeout','Upload', '$window',function($scope, $http, $location,$q,  sweet,$rootScope, $filter, $timeout, Upload, $window) {

    $scope.errorOnSubmit = false;
    $scope.errorIllegalAccess = false;
    $scope.displayMessageToUser = false;
    $scope.displayValidationError = false;
    $scope.displaySearchMessage = false;
    $scope.displaySearchButton = false;
    $scope.displayCreatePaqueteButton = false;
    $scope.bitacora = {};
    $scope.ptoorigen=null;
    $scope.noEmbarque=null;
    $scope.idPuntoe=null;
    $scope.idOperador=null;
    $scope.fechaEntPaq=null;
    $scope.fechaRecPaq=null;
    $scope.url_paquete = "../protected/paquetes/";
    $scope.url = "../protected/bitacoras/";
    $scope.url_rutas = "../protected/rutas/"; 
    $scope.url_usuario = "../protected/usuario/"; 
    $scope.urlact = "../protected/paqueteact/";

    $scope.disabled = undefined;
    $scope.searchEnabled = undefined;    
    $scope.origenes = [];    
    $scope.entregas = [];
    $scope.operadores = [];
    $scope.tipoenvio = [];
    $scope.envioselected={};
    $scope.disabledOperador=false;
    $scope.leads = [];
    $scope.leadsCollection = [].concat($scope.leads);
    $scope.leadsHistorico = [];
    $scope.leadsHistoricoCollection = [].concat($scope.leadsHistorico);
    $scope.itemsByPage = "10";
    $scope.tablestate={};
    $scope.isgnkembarque=true;
    $scope.minDate= new Date();
    $scope.searchformorigen=[];
    $scope.searchformentrega=[];
    $scope.searchformoperador=[];
    $scope.selected={};
    $scope.bitacora.fecha=new Date();
    $scope.disable=false;
    $scope.listnotracking=[];
    $scope.selected={};
    $scope.secondPaquete={};
    $scope.statuslist=[];
    $scope.vehiculolist=[];
    $scope.leadsBitacora=[];
    $scope.leadsBitacoraCollection=[];
    $scope.kmActual=0;
    $scope.searchBitacora={};
    $scope.fechaRec1=null;
    $scope.fechaRec2=null;
    $scope.isSearching=false;
    $scope.idbitacora=null;
    $scope.listIdBitacora=[];
    $scope.notracking=null;
    $scope.idRuta=null;
    $scope.idBitacora=null;
    $scope.ruta=null;
    $scope.listRutas = [];
    $scope.observaciones=null;
    $scope.comentario=null;
    $scope.imageUrl='';
    $scope.tipoEnvio=null;
    $scope.statusBitacora = null;
    $scope.loggedUser = null;
    $scope.usuarioLogueado=null;
    

    $scope.resetBitacora = function(){   
        $scope.bitacora = {};        
    };
    
    $scope.deleteBitacora = function () {
        $scope.lastAction = 'delete';
        var url = $scope.url + 'delete/';
        $scope.startDialogAjaxRequest();
        $http({
            method: 'POST',
            url: url,
            params:{'idbitacora':$scope.bitacora.id}
        }).success(function (data) {      
                $scope.resetBitacora();
                $scope.finishAjaxCallOnSuccess(data, "#deleteBitacoraModal", false);
                sweet.show('Ok', 'Paquete eliminado correctamente', 'success');
                $scope.reloadTableBitacoras();
            }).error(function(data, status, headers, config) {
                $scope.handleErrorInDialogs(status);
                sweet.show('Error', 'Error en el proceso de eliminación', 'error');
            });
    };    

    
    $scope.createBitacora = function (newBitacoraForm) {
        if (!newBitacoraForm.$valid) {
            $scope.displayValidationError = true;
            return;
        }

        $scope.lastAction = 'create';
        var url = $scope.url+'create/';
        var bitacoraObj = {};
//        $scope.startDialogAjaxRequest();      

        var bitacoratest ={
            id:$scope.bitacora.id,
            fecha:$scope.bitacora.fecha,
            ptoDestino:$scope.bitacora.ptoDestino,
            ptoOrigen:$scope.bitacora.ptoOrigen,
            idVehiculo:$scope.bitacora.idVehiculo,
            idOperador:$scope.bitacora.idOperador,
            idEnvio:$scope.bitacora.idEnvio,
            observaciones:$scope.comentario,
            isFinal:$scope.bitacora.isFinal
        }
        
        var config = {headers: { 'Content-Type': "application/json; charset=utf-8"}};
        $http.post(url, bitacoratest, config)
            .success(function (data) {
                $scope.leadsBitacora = [];
                $scope.leadsBitacoraCollection = [].concat($scope.leadsBitacora);
                
                bitacoraObj = $scope.paquete;
                $scope.bitacora={};
                $scope.leadsBitacora = data.data;
                $scope.leadsBitacoraCollection = [].concat($scope.leadsBitacora);
                if($scope.tablestate!=='undefined'){
                    $scope.tablestate.pagination.start = 0
                    $scope.tablestate.pagination.numberOfPages = $scope.getNumberOfPages(data.totalData);
                }                        
                $scope.finishAjaxCallOnSuccess(data, "#addBitacoraModal", false);
                sweet.show('Ok', 'Datos guardados correctamente', 'success');
            })
            .error(function(data, status, headers, config) {
                $scope.paquete={};
                $scope.handleErrorInDialogs(status);
                $scope.finishAjaxCallOnSuccess(data, "#addPaqueteModal", false);
                sweet.show('Ok', 'Error en el proceso de creación '+data.actionMessage, 'error');
            });
    };
    

    $scope.updateBitacoraObservaciones = function (updateStatusObservacionesForm) {
        if (!updateStatusObservacionesForm.$valid) {
            $scope.displayValidationError = true;
            return;
        }

        $scope.lastAction = 'update';
        var url = $scope.url+'updateObservaciones/';        
        var config = {headers: { 'Content-Type': "application/json; charset=utf-8"},params:{'historico':$scope.statusBitacora.id,'observaciones':$scope.statusBitacora.observaciones}};
        $http.get(url, config)
            .success(function (data) {
                if(data!=null || data!=undefined){
                    $scope.statusBitacora = data;
                    var params = {'id':data.idBitacora.id,'idStatus':data.estatus};
                    $scope.addMoreImagesToHistorico(params, true);
                }
                $scope.finishAjaxCallOnSuccess(data, "#detailBitacoraStatus", false);
                sweet.show('Ok', 'Datos guardados correctamente', 'success');
            })
            .error(function(data, status, headers, config) {
                $scope.paquete={};
                $scope.handleErrorInDialogs(status);
                $scope.finishAjaxCallOnSuccess(data, "#addPaqueteModal", false);
                sweet.show('Ok', 'Error en el proceso de creación '+data.actionMessage, 'error');
            });
    };


    $scope.showConfirmChangeStatus = function(updateBitacoraForm){
        if (!updateBitacoraForm.$valid) {
            $scope.displayValidationError = true;
            return;
        }        
        sweet.show({
            title: 'Confirmar',
            text: 'Está seguro de actualizar el estatus de la bitácora?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Si, seguro!',
            closeOnConfirm: true,
            closeOnCancel: false
        }, function(isConfirm) {
            if (isConfirm) {
                $scope.exit('#updateStatusBitacoraModal');
                $scope.actualizarEstatusBitacora(updateBitacoraForm);
            }else{
                $scope.exit('#updateStatusBitacoraModal');
                sweet.show('Advertencia', 'El proceso ha sido cancelado', 'warning');
            }
        });        
        
    }
    
    $scope.actualizarEstatusBitacora = function (updateStatusBitacoraForm) {
        if (!updateStatusBitacoraForm.$valid) {
            $scope.displayValidationError = true;
            return;
        }
        $scope.lastAction = 'update';
        var url = $scope.url + 'updatestatus';

        if(parseInt($scope.kmActual) < parseInt($scope.bitacora.kmInicial) || parseInt($scope.kmActual) < parseInt($scope.maxkm) ){
            sweet.show('Error', 'el kilometraje no puede ser menor al kilometraje capturado actualmente', 'error');
            return;
        }
        
        $scope.startDialogAjaxRequest();

        var kilometraje = $scope.kmActual!=null && $scope.kmActual!='' && $scope.kmActual!=undefined?$scope.kmActual:'0';
        var observaciones = $scope.comentario!=null && $scope.comentario!=undefined && $scope.comentario!=''?$scope.comentario:'SIN COMENTARIOS'
        var config = {headers: {'Content-Type': 'application/json; charset=UTF-8'},params:{'idbitacora':parseInt($scope.bitacora.id),'kmActual':parseInt(kilometraje),'observaciones':observaciones}};
//        console.log(config);
        $http.get(url, config)
            .success(function (data) {
                sweet.show('Ok', 'Datos guardados correctamente', 'success');
                $scope.addMoreImagesToHistorico(data, true);
                $scope.searchBitacoraByParameters();
                $scope.getListHistoricoBitacoraByBitacoraId($scope.bitacora);                
                $scope.finishAjaxCallOnSuccess(data, "null", false);
//                console.log(data);
                if(data!=null && data.idStatus.numStatus==4 && data.idEnvio.idEnvio==1 && $scope.loggedUser!=null && $scope.loggedUser!=undefined){
//                    console.log('data');
//                    console.log(data);
                    $window.open('http://52.43.26.76:8080/walmartCaptura/indexParametros.jsp?noTienda='+data.ptoDestino.nombrePunto+'&usuario='+$scope.loggedUser.username);
                }
            })
            .error(function(data, status, headers, config) {
                $scope.paquete={};
                $scope.handleErrorInDialogs(status);
                $scope.finishAjaxCallOnSuccess(data, "#addPaqueteModal", false);
                sweet.show('Ok', 'Error en el proceso de creación '+data.actionMessage, 'error');
            });        
    };        
        
    
    $scope.searchBitacoraByParameters = function(){        
        var start = 0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
        var number = 10;  // Number of entries showed per page.
        var page=0;        
        //$scope.searchBitacora=true;
        
        if($scope.tablestate!==null && $scope.tablestate.pagination!==undefined){
            var pagination = $scope.tablestate.pagination;
            start = pagination.start || 0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
            number = pagination.number || 10;  // Number of entries showed per page.            
            page=Math.floor(start / number);
        }        
        
        var config = {headers: { 'Content-Type': "application/json; charset=utf-8"},params:{
            'fechaInicial':$filter('date')($scope.fechaRec1,'yyyy-MM-dd'),
            'fechaFinal':$filter('date')($scope.fechaRec2,'yyyy-MM-dd'),
            'idBitacora':$scope.idbitacora!=null?$scope.idbitacora.id:'',
            'idEnvio':$scope.idEnvio!=null?$scope.idEnvio.idEnvio:'',
            //'noTracking':$scope.notracking,
            'page':page
        }};        
    
        var url = $scope.url+'searchBitacora';
        $http.get(url, config)
            .success(function (response) {
                $scope.displaySearchMessage = true;
                $scope.leadsBitacora = response.data;
                $scope.leadsBitacoraCollection = [].concat($scope.leadsBitacora);
                $scope.totalViajesFound=response.totalData;
                if($scope.tablestate.pagination!=null && $scope.tablestate.pagination!=undefined){
                    $scope.tablestate.pagination.numberOfPages = $scope.getNumberOfPages(response.totalData);
                }
                if($scope.leadsBitacora!=null && $scope.leadsBitacora.length==1){
                    $scope.bitacora=$scope.leadsBitacora[0];
                }
                $scope.finishAjaxCallOnSuccess(response.data, "null", false);
            })
            .error(function(data, status, headers, config) {
                $scope.handleErrorInDialogs(status);
                sweet.show('Error', 'Error en el proceso de busqueda', 'error');
            });        
//        $scope.idbitacora = null;
//        $scope.bitacora = null;            
        
    }
    
    
    $scope.resetSearch = function(){
        $scope.idBitacora=null;
        $scope.idbitacora=null;
        $scope.isSearching=false;
        $scope.searchFor = "";
        //$scope.pageToGet = 0;
        $scope.displaySearchMessage = false;
        $scope.tablestate.sort.predicate = undefined;
        $scope.tablestate.search.predicateObject={};
        $scope.serverBitacoraFilter($scope.tablestate);
        $scope.selected={};
        $scope.statusselected=null;
        $scope.tipoEnvioSelected=null;
        $scope.totalViajesFound=0;
        
        $scope.fechaRec1=null;
        $scope.fechaRec2=null;
        $scope.isSearching=false;
        $scope.idbitacora=null;
        $scope.listIdBitacora=[];
        $scope.notracking=null;        
        $scope.leadsHistorico = [];
        $scope.leadsHistoricoCollection = [].concat($scope.leadsHistorico);
        //$scope.idEnvio=null;
        $scope.idbitacora = null;
        $scope.bitacora = null;         
    };

    $scope.resetBitacoraList = function(){
        $scope.idBitacora=null;
        $scope.leadsBitacora = [];
        $scope.leadsBitacoraCollection = [].concat($scope.leadsBitacora);
        $scope.totalViajesFound=null;        
        $scope.tablestate.pagination.start=0;
        $scope.tablestate.pagination.number=10;
        $scope.tablestate.pagination.numberOfPages = 0;
        console.log($scope.tablestate);
        $scope.bitacora=null;
        $scope.serverBitacoraFilter($scope.tablestate);
    }        
        
    $scope.printPdfReport = function(source){
        console.log(source);
        var url = $scope.url+'generateReport';
        
        var config = {responseType: 'arraybuffer', headers: { 'Content-Type': "application/json; charset=utf-8"},params:{'id':source.id}};
        console.log(config);
        var typefile='application/octet-stream';
    
        $http.get(url, config)
            .success(function (response) {
                console.log(response);
               $timeout(function(){
                var file = new Blob([response], {type: typefile});
                var fileURL = URL.createObjectURL(file);
                var a         = document.createElement('a');
                a.href        = fileURL; 
                a.target      = '_blank';
                a.download    = 'BitacoraViajeRecepción_'+source.id+'_'+new Date().getTime()+'.xlsx';
                document.body.appendChild(a);
                a.click();                               
                $scope.finishAjaxCallOnSuccess(response, null, true);
               });        
        
            })
            .error(function(data, status, headers, config) {
                $scope.handleErrorInDialogs(status);
                sweet.show('Error', 'Error en el proceso de busqueda', 'error');
            });         
    }
        
    $scope.addSearchParametersIfNeeded = function(config, isPagination) {
        if(!config.params){
            config.params = {};
        }

        config.params.page = $scope.pageToGet;

        if($scope.searchFor){
            config.params.searchFor = $scope.searchFor;
        }
    }
    
    
    $scope.selectedBitacora = function (bitacora) {
        $scope.bitacora=bitacora;
        console.log($scope.bitacora);
    }    

    $scope.selectedBitacoraStatus = function (bitacora) {
        $scope.bitacora=bitacora;
        $scope.nameNextStatus=null;
        $scope.kmActual=null;
        $scope.comentario=null;
        $scope.nextStatus=null;
        $scope.isRequiredKm=false;
        $scope.maxkm=null;
        
        $scope.bitacora = bitacora;
        var complete_url = $scope.url+"nextstatus";
        var config = {params: {'status':$scope.bitacora.idStatus.numStatus,'tipoEnvio':$scope.bitacora.idEnvio.idEnvio, 'isFinal':$scope.bitacora.isFinal}, headers: { 'Content-Type': 'application/json'}};
        $http.get(complete_url,config).success(function(response){
            $scope.nameNextStatus=response.desStatus;
            $scope.comentario = response.comentario;
            $scope.nextStatus = response;
//            console.log($scope.nextStatus);
            if($scope.nextStatus.numStatus!=4 && $scope.nextStatus.numStatus!=5 && $scope.nextStatus.numStatus!=6){
                $scope.isRequiredKm=true;
            }else{
                $scope.isRequiredKm=false;
            }
//            console.log($scope.comentario);
            if($scope.bitacora.idStatus.numStatus==response.numStatus){
                $scope.exit('#updateStatusBitacoraModal');
                sweet.show('Warning', 'Actualización completa de estatus', 'warning');                
            }
        });

        console.log($scope.bitacora);
        var url_km = $scope.url+"currentkm";
        var config = {params: {'idBitacora':$scope.bitacora.id}, headers: { 'Content-Type': 'application/json'}};
        $http.get(url_km,config).success(function(response){
            $scope.maxkm = response;
        });         
    }        
        
        
    $scope.checkIfUserIsOperador = function(roles){
        var founded=false;
        angular.forEach(roles, function(value, key) {
          var obj = value;
          if(obj.desRol=='ROLE_OPERADOR'){
              founded=true;
          }
        });
        return founded;
    };
        
    $scope.handleErrorInDialogs = function (status) {
        $("#loadingModal").modal('hide');
        $scope.state = $scope.previousState;
        // illegal access
        if(status === 403 || status === 401 || status === 302){
            $scope.errorIllegalAccess = true;
            $scope.state = 'error';
            window.location = "../welcome";
            return;
        }

        $scope.errorOnSubmit = true;
        $scope.lastAction = '';
    }    
    
    $scope.startDialogAjaxRequest = function () {
        $scope.displayValidationError = false;
        $("#loadingModal").modal('show');
        $scope.previousState = $scope.state;
        $scope.state = 'busy';
    }    
    
    $scope.finishAjaxCallOnSuccess = function (data, modalId, isPagination) {
        $("#loadingModal").modal('hide');

        if(!isPagination){
            if(modalId){
                $scope.exit(modalId);
            }
        }

        $scope.lastAction = '';
    }    
    
    $scope.exit = function (modalId) {
        $(modalId).modal('hide');
        $("#loadingModal").modal('hide');
        $scope.paquete = {};
        $scope.errorOnSubmit = false;
        $scope.errorIllegalAccess = false;
        $scope.displayValidationError = false;
        $scope.f=null;
    }
        
    $scope.createNewBitacora=function(){
        $scope.resetBitacora();
        if($scope.bitacora!==undefined && $scope.bitacora.idOperador===undefined ){
            var url='../protected/usuario/getusuario/';
            var config = {headers: { 'Content-Type': "application/json; charset=utf-8"}};
            $http.get(url, config)
            .success(function (data) {
                //console.log(data);
                var roles = data.roles;
                var result = $scope.checkIfUserIsOperador(roles);
                $scope.disabledOperador=result;
                if(result){
                    $scope.bitacora.idOperador=data;
                }
            })
            .error(function(data, status, headers, config) {
            });
        }

        var url=$scope.url+'getdefaultpunto';
        var config = {headers: { 'Content-Type': "application/json; charset=utf-8"}};
        $http.get(url, config)
        .success(function (data) {
            if(data!=null && data!=undefined){
                $scope.bitacora.ptoOrigen=data[0].origen;
                $scope.bitacora.ptoDestino=data[0].destino;
            }
            console.log($scope.bitacora);
        })
        .error(function(data, status, headers, config) {
        });

        $scope.bitacora.fecha = new Date();
        $scope.bitacora.isFalse = 0;        
    };
    
    $scope.createNoEmbarque=function(){
//        var prefijo = ($scope.prefijoEmpresa!==null && $scope.prefijoEmpresa!=='' && $scope.prefijoEmpresa!==undefined?$scope.prefijoEmpresa:$scope.randomString(2))
//        $scope.paquete.noEmbarque= prefijo+'-'+$scope.consecutivo;
    }

    $scope.rowClick = function (idx)
    {
        var value = $scope.leadsBitacora[idx];
        $scope.bitacora=value;
        $scope.getListHistoricoBitacoraByBitacoraId(value);
    };
    
/*    $scope.rowClickBitacora = function(index){
        var value = $scope.leadsHistoricoCollection[index];
        console.log(value)
    }*/
    
    $scope.getListHistoricoBitacoraByBitacoraId=function(value){
        if(value!=null && value!=undefined){
            var complete_url = $scope.url+"gethistoricobitacora";  
            var config = {params: {'idbitacora':value.id}, headers: { 'Content-Type': 'application/json'}};
            $scope.startDialogAjaxRequest();
            $http.get(complete_url,config).success(function(response){
                $scope.leadsHistorico = response;
                $scope.leadsHistoricoCollection = [].concat($scope.leadsHistorico);
                $scope.finishAjaxCallOnSuccess(response.data, "null", false);
                $scope.getTotalTime();
            });                    
        }
    }
    
    $scope.getTotalTime=function(){
        $scope.totalKm=0;
        $scope.totalTiempo=0;
        $scope.totalTiempoMins=0;
        if($scope.leadsHistorico!=null && $scope.leadsHistorico!=undefined && $scope.leadsHistorico.length>0){
            var kmInicial = 0;
            var kmFinal = 0;
            
            var tiempoInicial = 0;
            var tiempoFinal = 0;
            
            kmInicial = $scope.leadsHistorico[0].kilometraje;
            kmFinal = $scope.leadsHistorico[$scope.leadsHistorico.length-1].kilometraje;
            $scope.totalKm = kmFinal - kmInicial;

            tiempoInicial = $scope.leadsHistorico[0].fecha;
            tiempoFinal = $scope.leadsHistorico[$scope.leadsHistorico.length-1].fecha;
            $scope.totalTiempo = tiempoFinal - tiempoInicial;
            var seconds = $scope.totalTiempo/1000;
            var minutos = seconds/60;  
            
            var hours = Math.trunc(minutos/60);
            var minutes = Math.round(minutos % 60);
            $scope.totalTiempoMins=$scope.zeroPad(hours,2) +":"+ $scope.zeroPad(minutes,2);
        }
    }
    
    $scope.getTimePerRow=function(index){
        var time=0;
        if(index>0){
//            var dateInicial = new Date($scope.leadsHistorico[index-1].fecha);
//            var dateActual = new Date($scope.leadsHistorico[index].fecha);
//            var minuteInicial = dateInicial.getMinutes(); 
//            var minuteActual = dateActual.getMinutes(); 
//            time = minuteActual - minuteInicial;
//            console.log(time);

            var tiempoInicial = $scope.leadsHistorico[index-1].fecha;
            var tiempoFinal = $scope.leadsHistorico[index].fecha;
            var tiempoTotal = tiempoFinal - tiempoInicial;
            var seconds = tiempoTotal/1000;
            var minutos = seconds/60;            
            
            //time = Math.round(seconds/60);  
            var hours = Math.trunc(minutos/60);
            var minutes = Math.round(minutos % 60);

            time = $scope.zeroPad(hours,2) +":"+ $scope.zeroPad(minutes,2);
        }
        return time;
    }
    
    $scope.zeroPad=function(num, places) {
      var zero = places - num.toString().length + 1;
      return Array(+(zero > 0 && zero)).join("0") + num;
    }    
    
    $scope.selectedStatusBitacora = function (status) {
        $scope.statusBitacora = status;
        console.log($scope.statusBitacora);
    }      
    
    $scope.orderByColumnFilter=function(params){
        var filtered = [];
        if (params.sort.predicate) {
            filtered = $filter('orderBy')($scope.leads, params.sort.predicate, params.sort.reverse);
        }
        return filtered;
    }

    $scope.serverBitacoraFilter = function(tablestate)
    {
        $scope.tablestate=tablestate;
        
        var resultFilters = $scope.orderByColumnFilter(tablestate);        
        if(resultFilters.length>0){
            $scope.leadsCollection=[].concat(resultFilters);
        }else {
            if($scope.idBitacora!=null){
            }else{
                var complete_url = $scope.url+"listbitacoras";  
                var pagination = tablestate.pagination;
                var start = pagination.start || 0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
                var number = pagination.number || 10;  // Number of entries showed per page.    
                console.log($scope.idEnvio);
                var config = {params: {page: Math.floor(start / number) + 1,tipoEnvio:$scope.idEnvio!=null && $scope.idEnvio!=undefined?$scope.idEnvio.idEnvio:null}, headers: { 'Content-Type': 'application/json'}};
                console.log(config);
                $scope.startDialogAjaxRequest();
                $http.get(complete_url,config).success(function(response){
                    $scope.leadsBitacora = response.data;
                    $scope.leadsBitacoraCollection = [].concat($scope.leadsBitacora);
                    $scope.totalViajesFound=response.totalData;
                    $scope.tablestate.pagination.numberOfPages = $scope.getNumberOfPages(response.totalData);
                    $scope.finishAjaxCallOnSuccess(response.data, "null", false);
                });
            }
        }
    };
    
    $scope.reloadTableBitacoras=function(){
        if($scope.tablestate!=null && $scope.tablestate!=undefined){
            $scope.serverBitacoraFilter($scope.tablestate);
//            var complete_url = $scope.url+"listbitacoras";  
//            var pagination = $scope.tablestate.pagination;
//            var start = pagination.start || 0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
//            var number = pagination.number || 10;  // Number of entries showed per page.    
//            var config = {params: {page: Math.floor(start / number) + 1}, headers: { 'Content-Type': 'application/json'}};
//            //$scope.startDialogAjaxRequest();
//            $http.get(complete_url,config).success(function(response){
//                $scope.leadsBitacora = response.data;
//                $scope.leadsBitacoraCollection = [].concat($scope.leadsBitacora);
//                $scope.totalViajesFound=response.totalData;
//                $scope.tablestate.pagination.numberOfPages = $scope.getNumberOfPages(response.totalData);
//            //    $scope.finishAjaxCallOnSuccess(response.data, "null", false);
//            });                                                
        }
    }
    
    $scope.getCSSClass = function(index){
        var item = $scope.leadsBitacoraCollection[index];
        var style='';
        if(item.idStatus.numStatus==1){
            style='bg-danger';
        }else{
            switch(item.isFinal){
                case 1:
                    if(item.idStatus.numStatus==7){
                        style='bg-info';
                    }else{
                        style='bg-warning';
                    }
                break;
                case 0:
                    if(item.idStatus.numStatus==5){
                        style='bg-info';
                    }else{
                        style='bg-warning';
                    }                
                break;
                default:
                break;
            }            
        }
        if(item.isPausa==1){
            style='bg-primary';
        }
        return style;
    }

    $scope.showButtonUpdateStatus = function(source){
//       console.log(source);
//        console.log(source.isPausa);
//        console.log(source.activo);
        // si esta activa significa que todavia anda
        if(source.isPausa==1){
           return false; 
        }        
        if(source.activo==0){
           return false; 
        }        
        if(source.isFinal==1 && source.idStatus.numStatus==7){
            return false;
        }else if (source.isFinal==0 && source.idStatus.numStatus==5){
            return false;
        }else{
            return true;
        }
    }

    $scope.getNumberOfPages = function(totaldata){
        return Math.ceil(totaldata / $scope.itemsByPage);
    };
    
    $scope.getTipoEnvio = function () {
        var completeurl = $scope.url_paquete + 'searchenvio/';
        var config = {headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}};
        $http.get(completeurl, config)
        .then(function (response) {
            $scope.tipoenvio = response.data.data;
            $scope.loadDataByUserOperador();
        });            
    };
    
    $scope.loadDataByUserOperador=function(){
        var url='../protected/usuario/getusuario/';
        var config = {headers: { 'Content-Type': "application/json; charset=utf-8"}};
        $http.get(url, config)
        .success(function (data) {
            $scope.usuarioLogueado=data;
            var roles = data.roles;
            var result = $scope.checkIfUserIsOperador(roles);
            if(result){
                for(var i=0; i<$scope.tipoenvio.length;i++){
                    if($scope.tipoenvio[i].desEnvio==='ENTREGA'){
                        $scope.idEnvio=$scope.tipoenvio[i];
                        console.log('load data by operador');
                        console.log($scope.idEnvio);
                        break;
                    }
                }
            }            
        })
        .error(function(data, status, headers, config) {
        });
    }
  
  
  // Any function returning a promise object can be used to load values asynchronously
  $scope.getOrigenes = function($select) {
    if($select.search!=='' && $select.search.length>3){
        var completeurl = $scope.url_paquete+'searchorigen/'+$select.search;
        var config = {headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}};
        $http.get(completeurl, config)
            .then(function(response) {
              $scope.origenes = response.data.data;
            });    
        
    }  
  };  

  // Any function returning a promise object can be used to load values asynchronously
  $scope.getEntrega = function($select) {
    if($select.search!=='' && $select.search.length>3){
    var completeurl = $scope.url_paquete+'searchentrega/'+$select.search;
    var config = {headers: { 'Content-Type': 'application/json', 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}};
        $http.get(completeurl, config)
            .then(function(response) {
              $scope.entregas = response.data.data;
            });         
    }  
  };  

  // Any function returning a promise object can be used to load values asynchronously
  $scope.getOperador = function($select) {
    if($select.search!==''){
        var completeurl = $scope.url_paquete+'operadorbyname';
        var config = {params:{'name':$select.search},headers: { 'Content-Type': 'application/json', 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}};
        $http.get(completeurl, config)
            .then(function(response) {
              $scope.operadores = response.data.data;
            });  
    }
  };  

  $scope.getOperadorSearch = function() {
    var completeurl = $scope.url+'searchoperador/';
    var config = {headers: { 'Content-Type': 'application/json', 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}};
    $http.get(completeurl, config)
        .then(function(response) {
          $scope.searchformoperador = response.data.data;
        });  
  };  
                
  $scope.getVehiculos = function() {
    var completeurl = $scope.url_paquete+'getvehiculos/';
    var config = {headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}};
    $http.get(completeurl, config)
        .then(function(response) {
          console.log(response);
          $scope.vehiculolist=response.data;
        });  
  };        
        
  $scope.getNoEmbarqueSearch = function() {
    var completeurl = $scope.url+'searchoperador/';
    var config = {headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}};
    $http.get(completeurl, config)
        .then(function(response) {
          $scope.searchformoperador = response.data.data;
        });  
  };          
  
  
  $scope.today = function() {
    $scope.dt = new Date();
  };
  $scope.today();

  $scope.clear = function() {
    $scope.dt = null;
  };

  $scope.inlineOptions = {
    customClass: getDayClass,
    //minDate: new Date(),
    showWeeks: true
  };

  $scope.dateOptions = {
    //dateDisabled: disabled,
    formatYear: 'YYYY',
    //maxDate: new Date(2020, 5, 22),
    //minDate: new Date(),
    startingDay: 1
  };

  /*$scope.dateOptions2 = {
    dateDisabled: disabled,
    formatYear: 'YYYY',
    minDate: $scope.paquete.fechaRecPaq,
    startingDay: 1
  };*/
        
  // Disable weekend selection
  function disabled(data) {
    var date = data.date,
      mode = data.mode;
    return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
    //return mode === 'day' && (date.getDay() > 6);
  }
  
  $scope.open1 = function() {
    $scope.popup1.opened = true;
    $scope.paquete.fechaEntPaq=null;
  };

  $scope.open2 = function() {
    $scope.popup2.opened = true;
  };

  $scope.open3 = function() {
    $scope.popup3.opened = true;
  };

  $scope.setDate = function(year, month, day) {
    $scope.dt = new Date(year, month, day);
  };

  $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate','dd/MM/yyyy'];
  $scope.format = $scope.formats[4];
  $scope.altInputFormats = ['M!/d!/yyyy'];

  $scope.popup1 = {
    opened: false
  };

  $scope.popup2 = {
    opened: false
  };

  $scope.popup3 = {
    opened: false
  };
  
  var tomorrow = new Date();
  tomorrow.setDate(tomorrow.getDate() + 1);
  var afterTomorrow = new Date();
  afterTomorrow.setDate(tomorrow.getDate() + 1);
  $scope.events = [
    {
      date: tomorrow,
      status: 'full'
    },
    {
      date: afterTomorrow,
      status: 'partially'
    }
  ];

  function getDayClass(data) {
    var date = data.date,
      mode = data.mode;
    if (mode === 'day') {
      var dayToCheck = new Date(date).setHours(0,0,0,0);

      for (var i = 0; i < $scope.events.length; i++) {
        var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);

        if (dayToCheck === currentDay) {
          return $scope.events[i].status;
        }
      }
    }
  }
      
    $scope.clickEventCheck = function(event) { 
        if(event===true){
            $scope.createNoEmbarque();
        }else{
            $scope.paquete.noEmbarque='';
        }
     };      
     
    $scope.selectEmpresa=function(){
        if($scope.isSecondPaquete!==true){
            var prefijo = $scope.paquete.idEmpresa!==null?$scope.paquete.idEmpresa.descripcion:'';
            $scope.prefijoEmpresa = prefijo!==''?prefijo.substring(0,3).toUpperCase():'';
            $scope.createNoEmbarque();            
        }
    };
     
    $scope.randomString = function ( n ) {
      var r="";
      while(n--)r+=String.fromCharCode((r=Math.random()*62|0,r+=r>9?(r<36?55:61):48));
      return r.toUpperCase();
    };  
    
    $scope.getNoTracking = function($select) {
      if($select.search!==null && $select.search !== undefined && $select.search!=='' && $select.search.length>=4){
          var completeurl = $scope.urlact+'notracking/';
          var config = {params:{'name':$select.search},headers: { 'Content-Type': 'application/json', 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}};
          $http.get(completeurl, config)
              .then(function(response) {
                $scope.listnotracking = response.data;
                console.log(response);
              }, function(error) {
                console.log(error);
            });  
      }
    };     
        
  $scope.onSelectNoTrackingCallback = function (item, model){
      if(item!==null && item!==undefined && item!==''){
        console.log(item);
        $scope.notracking=item;
      }
  };          

  $scope.onSelectIdBitacoraCallback = function (item, model){
      if(item!==null && item!==undefined && item!==''){
        console.log(item);
        $scope.idbitacora = item;
        $scope.bitacora = item;
        $scope.searchBitacoraByParameters();
        //$scope.bitacora=value;
        $scope.getListHistoricoBitacoraByBitacoraId(item);
        //$scope.url = "../protected/paqueteact/";
        $scope.imageUrl="../protected/paqueteact/getimage/"+item.idOperador.idUsu;
      }
  };
  
    $scope.onSelectTipoEnvioCallback = function(item, model){
        if(item!==null && item!==undefined && item!==''){
            $scope.idEnvio=item;
            $scope.searchBitacoraByParameters();
        }
    }
    
    $scope.onSelectTipoEnvioCallback2 = function(item, model){
        if(item!==null && item!==undefined && item!==''){
            $scope.idEnvio = item;
            $scope.reloadTableBitacoras();
        }
    }
    

  $scope.onSelectOperadorCallback = function (item, model){
      if(item!==null && item!==undefined && item!==''){
        $scope.searchBitacora.idOperador=item;
      }
  };          

  $scope.onSelectPtoOrigenCallback = function (item, model){
      if(item!==null && item!==undefined && item!==''){
          $scope.searchBitacora.ptoOrigen=item;
            console.log(item);
      }
  };          

  $scope.onSelectPtoEntregaCallback = function (item, model){
      if(item!==null && item!==undefined && item!==''){
          $scope.searchBitacora.ptoDestino=item;
      }
  };
  
  $scope.onSelectIdRutaCallback = function (item, model){
      if(item!==null && item!==undefined && item!==''){
          $scope.ruta=item;
      }
  };
  
  
//    $scope.getListBitacora = function($select) {
//        //&& $select.search.length>=4
//      if($select.search!==null && $select.search !== undefined && $select.search!==''){
//          var completeurl = $scope.url+'getidbitacora/';
//          var config = {params:{'nombre':$select.search},headers: { 'Content-Type': 'application/json', 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}};
//          $http.get(completeurl, config)
//              .then(function(response) {
//                $scope.listIdBitacora = response.data;
//                console.log(response);
//              }, function(error) {
//                console.log(error);
//            });  
//      }
//    };  

    $scope.getListBitacora = function($select) {
        //&& $select.search.length>=4
      if($select.search!==null && $select.search !== undefined && $select.search!=='' && $select.search.length>=4){
          var completeurl = $scope.url+'getidbitacora/';
          var config = {params:{'nombre':$select.search},headers: { 'Content-Type': 'application/json', 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}};
          $http.get(completeurl, config)
              .then(function(response) {
                $scope.listIdBitacora = response.data;
                console.log(response);
              }, function(error) {
                console.log(error);
            });  
      }
    };  

    $scope.getListByIdBitacora = function(idBitacora) {
        //&& $select.search.length>=4
      if(idBitacora!==null && idBitacora !== undefined && idBitacora!==''){
          var completeurl = $scope.url+'getidbitacora/';
          var config = {params:{'idBitacora':idBitacora},headers: { 'Content-Type': 'application/json'}};
          $http.get(completeurl, config)
              .then(function(response) {
                if(response!=null && response!=undefined && response.data!=null && response.data!=undefined && response.data.length>0){
                    $scope.idbitacora=response.data[0];
                    console.log($scope.idbitacora);
                    $scope.searchBitacoraByParameters();
                }
              }, function(error) {
                console.log(error);
            });  
      }
    };
    
    $scope.getListRuta = function($select) {
        //&& $select.search.length>=4
        console.log($select);
      if($select.search!==null && $select.search !== undefined && $select.search!==''){
          var completeurl = $scope.url_rutas+'getListRuta/';
          var config = {params:{'idRuta':$select.search},headers: { 'Content-Type': 'application/json', 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}};
          $http.get(completeurl, config)
              .then(function(response) {
                $scope.listRutas = response.data;
                console.log(response);
              }, function(error) {
                console.log(error);
            });  
      }
    };  
        
    $scope.exportToExcel=function(tableId){ // ex: '#my-table'
          var exportHref=Excel.tableToExcel(tableId,'WireWorkbenchDataExport');
          $timeout(function(){location.href=exportHref;},100); // trigger download
    }
    
    $scope.uploadFiles = function(files, errFiles) {
        $scope.f = files;
        $scope.errFile = errFiles && errFiles[0];
        if (files) {
        }   
    }    
    
    $scope.addImageBitacora = function(newImageBitacoraPaqueteForm, flag){
        if ((!newImageBitacoraPaqueteForm.$valid)) {
            $scope.displayValidationError = true;
            return;
        }
        $scope.addMoreImagesToBitacora(flag);  
    };
    
//    $scope.addImageHistorico = function(newImageHistoricoForm, flag){
//        if ((!newImageHistoricoForm.$valid)) {
//            $scope.displayValidationError = true;
//            return;
//        }
//        $scope.addMoreImagesToHistorico(flag);  
//    };    
    
    $scope.addMoreImagesToBitacora = function(flag){
        var urlimages = $scope.url + 'addimage';
        if($scope.f!=null && $scope.f!=undefined){
            for(var i=0;i<$scope.f.length;i++){

                var formData=new FormData();
                formData.append('file',$scope.f[i]);

                var request = {
                    method: 'POST',
                    url: urlimages,
                    headers: {'Content-Type': undefined},
                    params: {'idbitacora':$scope.bitacora.id},
                    data:formData,
                    transformRequest: function(data, headersGetterFunction) {
                                    return data;
                    }    
                };

                $http(request).success(function (data) {
                        if(flag!==null && flag===true){
                            sweet.show('Correcto', 'Carga completa de la imagen', 'success');
                        }
                        //$scope.reloadTableBitacoras();
                    })
                    .error(function(data, status, headers, config) {
                            $scope.handleErrorInDialogs(status);
                        if(flag!==null && flag===true)
                            sweet.show('Error', 'Error en la carga de imagenes', 'error');
                    });            
            }
        }
        $scope.f=null;
    };
    
    $scope.addMoreImagesToHistorico = function(data, flag){
        var urlimages = $scope.url + 'addhistoricoimage';
        if($scope.f!=null && $scope.f!=undefined && data!=null && data!=undefined){
            console.log(data);
            for(var i=0;i<$scope.f.length;i++){

                var formData=new FormData();
                formData.append('file',$scope.f[i]);

                var request = {
                    method: 'POST',
                    url: urlimages,
                    headers: {'Content-Type': undefined},
                    params: {'idBitacora':data.id, 'status':data.idStatus.id},
                    data:formData,
                    transformRequest: function(data, headersGetterFunction) {
                                    return data;
                    }    
                };
                
                console.log(request);

                $http(request).success(function (data) {
                        if(flag!==null && flag===true){
                            sweet.show('Correcto', 'Carga completa de la imagen', 'success');
                        }
                        //$scope.reloadTableBitacoras();
                    })
                    .error(function(data, status, headers, config) {
                            $scope.handleErrorInDialogs(status);
                        if(flag!==null && flag===true)
                            sweet.show('Error', 'Error en la carga de imagenes', 'error');
                    });            
            }
        }
        $scope.f=null;
    };    
    
    $scope.getImagen = function (bitacora) {
        $scope.startDialogAjaxRequest();
        var typefile='application/zip';
        $http.get($scope.url+'getimage', {responseType: 'arraybuffer',params:{'idbitacora':bitacora.id}})
          .success(function (data) {
               $timeout(function(){
                var file = new Blob([data], {type: typefile});
                var fileURL = URL.createObjectURL(file);
                var a         = document.createElement('a');
                a.href        = fileURL; 
                a.target      = '_blank';
                a.download    = new Date().getTime()+'.zip';
                document.body.appendChild(a);
                a.click();                               
                $scope.finishAjaxCallOnSuccess(data, null, true);
               });
          }).error(function(data){
              $scope.finishAjaxCallOnSuccess(data, null, true);
          });
    };

    $scope.getImagenHistorico = function (bitacoraHistorico) {
        $scope.startDialogAjaxRequest();
        var typefile='application/zip';
        $http.get($scope.url+'getimagehistorico', {responseType: 'arraybuffer',params:{'historico':bitacoraHistorico.id}})
          .success(function (data) {
               $timeout(function(){
                var file = new Blob([data], {type: typefile});
                var fileURL = URL.createObjectURL(file);
                var a         = document.createElement('a');
                a.href        = fileURL; 
                a.target      = '_blank';
                a.download    = new Date().getTime()+'.zip';
                document.body.appendChild(a);
                a.click();                               
                $scope.finishAjaxCallOnSuccess(data, null, true);
               });
          }).error(function(data){
              $scope.finishAjaxCallOnSuccess(data, null, true);
          });
    };
        
    $scope.deleteBitacoraArchivos = function (bitacora){
        $scope.startDialogAjaxRequest();
        var config = {params: {'idbitacora':bitacora.id}, headers: { 'Content-Type': 'application/json'}};
        $http.get($scope.url+'deleteFiles', config)
          .success(function (data) {
              $scope.reloadTableBitacoras();
              $scope.finishAjaxCallOnSuccess(data, null, true);
              $scope.exit('#uploadBitacoraImage');
          }).error(function(data){
              $scope.finishAjaxCallOnSuccess(data, null, true);
          });
    }
    
    
    
    $scope.getLoggedUser = function() {
        var completeurl = $scope.url_usuario+'getloggeduser/';
        var config = {headers: { 'Content-Type': 'application/json', 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}};
        $http.get(completeurl, config)
            .then(function(response) {
                if(response!=null && response!=undefined && response.data!=null && response.data!=undefined){
                    $scope.loggedUser=response.data;
                }
            }, function(error) {
              console.log(error);
          });  
    };      
                
//    angular.element(document).ready(function () {
//        $scope.getTipoEnvio();
////        $scope.getOperadorSearch();
//        $scope.getVehiculos();
//    });  

//    $scope.showConfirmBitacoraInterrupcion = function(source){
//        console.log(source.isPausa);
//        var comentario="";
//        if(source.isPausa==0){
//            comentario=" poner en pausa la bitacora seleccionada?"
//        }else{
//            comentario=" quitar la pausa la bitacora seleccionada?"
//        }
//        
//        var sourceData = source;
//        sweet.show({
//            title: 'Confirmar',
//            text: 'Está seguro de '+comentario,
//            type: 'warning',
//            showCancelButton: true,
//            confirmButtonColor: '#DD6B55',
//            confirmButtonText: 'Si, seguro!',
//            closeOnConfirm: true,
//            closeOnCancel: false
//        }, function(isConfirm) {
//            if (isConfirm) {
//                $scope.putOrRemoveInterrupcion(sourceData);
//            }else{
//                sweet.show('Advertencia', 'El proceso ha sido cancelado', 'warning');
//            }
//        });        
//        
//    }
//    
//    $scope.putOrRemoveInterrupcion = function(source){
//        console.log(source.isPausa);
//        var completeurl = $scope.url+'interrumpirBitacora/';
//        $scope.startDialogAjaxRequest();
//        var config = {params: {'idbitacora':source.id}, headers: { 'Content-Type': 'application/json'}};
//        $http.get(completeurl, config)
//          .success(function (data) {
//              console.log(data);
//              sweet.show('Success', 'Actualizaciòn de interrupciòn', 'success');
//              $scope.reloadTableBitacoras();
//              $scope.finishAjaxCallOnSuccess(data, null, true);
//              //$scope.exit('#uploadBitacoraImage');
//          }).error(function(data){
//              $scope.finishAjaxCallOnSuccess(data, null, true);
//          }); 
//        
//    }

    $scope.clickSelectRuta = function (source) {
        $scope.comentario=null;
        $scope.resetFiles();
        var selectedBitacora = angular.copy(source);
        //console.log(selectedBitacora);
        if(selectedBitacora!=null && selectedBitacora!=undefined && selectedBitacora.idRuta!=null && selectedBitacora.idRuta!=undefined){
            $scope.ruta = selectedBitacora.idRuta;
            if($scope.ruta.isPausa==1){
                $scope.exit('#interruptRutaModal');
                $scope.comentario="N/A";
            }            
        }
    };
    
    $scope.resetFiles = function() {
        $scope.f = null;
        $scope.errFile = null;
    };
    
    $scope.showConfirmRutaInterrupcion = function(form){
        if (!form.$valid) {
            $scope.displayValidationError = true;
            return;
        }        
        var source =  $scope.ruta;
        
        var comentario="";
        if(source.isPausa==0){
            comentario=" poner en pausa la ruta seleccionada?"
        }else{
            comentario=" quitar la pausa la ruta seleccionada?"
        }
        
        var sourceData = source;
        sweet.show({
            title: 'Confirmar',
            text: 'Está seguro de '+comentario,
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Si, seguro!',
            closeOnConfirm: true,
            closeOnCancel: false
        }, function(isConfirm) {
            if (isConfirm) {
                $scope.putOrRemoveInterrupcion(sourceData);
            }else{
                sweet.show('Advertencia', 'El proceso ha sido cancelado', 'warning');
            }
        });        
        
    }
    
    $scope.putOrRemoveInterrupcion = function(source){
        var completeurl = $scope.url_rutas+'interrumpirRuta/';
        $scope.startDialogAjaxRequest();
        var config = {params: {'idruta':source.id,'comentario':$scope.comentario}, headers: { 'Content-Type': 'application/json'}};
        $http.get(completeurl, config)
          .success(function (data) {
              sweet.show('Ok', 'Actualización de interrupción', 'success');
               $scope.reloadTableBitacoras();
               $scope.exit('#interruptRutaModal');
               $scope.addMoreImagesToRuta(source, true);
               $scope.finishAjaxCallOnSuccess(data, null, true);
          }).error(function(data, status, headers, config) {
                $scope.f=null;
                $scope.handleErrorInDialogs(status);
                sweet.show('Error', 'Error en el proceso de creación '+data.actionMessage, 'error');
                $scope.finishAjaxCallOnSuccess(data, "#addPaqueteModal", false);
            });
        
    }    
    
    $scope.addMoreImagesToRuta = function(data, flag){
        var urlimages = $scope.url + 'addrutaimage';
        if($scope.f!=null && $scope.f!=undefined && data!=null && data!=undefined){
            //for(var i=0;i<$scope.f.length;i++){

                var formData=new FormData();
                formData.append('file',$scope.f);

                console.log(formData);

                var request = {
                    method: 'POST',
                    url: urlimages,
                    headers: {'Content-Type': undefined},
                    params: {'idruta':data.id},
                    data:formData,
                    transformRequest: function(data, headersGetterFunction) {
                                    return data;
                    }    
                };
                
                console.log(request);
                //$scope.startDialogAjaxRequest();
                $http(request).success(function (data) {
                        if(flag!==null && flag===true){
                            sweet.show('Correcto', 'Carga completa de la imagen', 'success');
                        }
                        $scope.finishAjaxCallOnSuccess(data, null, true);
                    })
                    .error(function(data, status, headers, config) {
                            $scope.handleErrorInDialogs(status);
                        if(flag!==null && flag===true)
                            sweet.show('Error', 'Error en la carga de imagenes', 'error');
                    });            
            //}
        }
        $scope.f=null;
    };    

    angular.element(document).ready(function () {
        var params = $location.search();
        if(params!=null && params!=undefined){
            $scope.idRuta=params.idruta;
            $scope.idBitacora=params.idbitacora;
            //$scope.idbitacora=params.idbitacora;
            var data = {search:$scope.idBitacora};
            if($scope.idBitacora!=null && $scope.idBitacora!=undefined && $scope.idBitacora!=''){
                //$scope.getIdBitacora(data);
                //$scope.searchBitacoraByParameters();   
                $scope.getListByIdBitacora($scope.idBitacora);
            }
        }
        $scope.getTipoEnvio();
        $scope.getLoggedUser();
        
    });

}]);

app.directive('onlyDigits', function () {
    return {
      require: 'ngModel',
      restrict: 'A',
      link: function (scope, element, attr, ctrl) {
        function inputValue(val) {
          if (val) {
            var digits = val.replace(/[^0-9]/g, '');

            if (digits !== val) {
              ctrl.$setViewValue(digits);
              ctrl.$render();
            }
            return parseInt(digits,10);
          }
          return undefined;
        }            
        ctrl.$parsers.push(inputValue);
      }
    }
});

