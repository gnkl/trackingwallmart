var chart = angular.module('nvd3chart', ['nvd3']);

chart.config(function ($httpProvider) {
    $httpProvider.defaults.headers.get = {"X-Requested-With": "XMLHttpRequest"};
    $httpProvider.defaults.headers.post = {"X-Requested-With": "XMLHttpRequest"};
    $httpProvider.defaults.headers.put = {"X-Requested-With": "XMLHttpRequest"};
}); 

chart.controller('GenericChartCtrl', ['$scope', '$http', '$location',function($scope, $http, $location){
    $scope.dataline = [];
    $scope.databarterrestrechart = [];
    $scope.databaraereochart = [];
    $scope.urlpiechart = '/GnklTracking/protected/paquetechart/countbystatus';    
    var config = {headers: {'Content-Type': 'application/json; charset=UTF-8'}};
    // aereo
    $http.get($scope.urlpiechart+'/2', config)
        .success(function (data) {
            var databarchart=[];
            data.forEach(function(data){
              //$scope.dataline.push({ key: data.des_status, y:data.total});
              databarchart.push({ label: data.des_status, value:data.total});
            });                        
            $scope.databaraereochart =[{key: "Estatus paquete aéreos",values:databarchart}];
        })
        .error(function(data, status, headers, config) {
            console.log('error');
        });

// terrestre
    $http.get($scope.urlpiechart+'/1', config)
        .success(function (data) {
            var databarchart=[];
            data.forEach(function(data){

              databarchart.push({ label: data.des_status, value:data.total});
            });                        
            $scope.databarterrestrechart =[{key: "Estatus paquetes terrestres",values:databarchart}];
        })
        .error(function(data, status, headers, config) {
            console.log('error');
        });
        
        
    $scope.optionsbarTConfchart = {
        chart: {
            type: 'discreteBarChart',
            height: 450,
            margin : {
                top: 20,
                right: 20,
                bottom: 50,
                left: 55
            },
            x: function(d){return d.label;},
            y: function(d){return d.value;},
            showValues: true,
            valueFormat: function(d){
                return d;
            },
            duration: 500,
            xAxis: {
                axisLabel: 'Estatus'
            },
            yAxis: {
                axisLabel: 'No. Paquetes Terrestres',
                axisLabelDistance: -10
            }
        }
    };

    $scope.optionsbarAConfchart = {
        chart: {
            type: 'discreteBarChart',
            height: 450,
            margin : {
                top: 20,
                right: 20,
                bottom: 50,
                left: 55
            },
            x: function(d){return d.label;},
            y: function(d){return d.value;},
            showValues: true,
            valueFormat: function(d){
                return d;
            },
            duration: 500,
            xAxis: {
                axisLabel: 'Estatus'
            },
            yAxis: {
                axisLabel: 'No. Paquetes Aéreos',
                axisLabelDistance: -10
            }
        }
    };
        
}]);