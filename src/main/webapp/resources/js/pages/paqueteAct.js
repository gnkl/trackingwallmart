var act = angular.module('paqueteact', ['smart-table', 'ngAnimate', 'ui.bootstrap', 'ngSanitize', 'ui.select','hSweetAlert','ngFileUpload']);

act.factory("sessionInjector", ['$log', 'sweet', function($log,sweet){
    return {
        request: function(config) {return config;},
        response: function(response) {
            if (typeof response.data === "string" && response.data.indexOf("forma-login") > -1) {
                location.reload();
            }
            return response;
        }
    };
}]);

act.config(function ($httpProvider) {
    $httpProvider.interceptors.push("sessionInjector");
    $httpProvider.defaults.headers.get = {"X-Requested-With": "XMLHttpRequest"};
    $httpProvider.defaults.headers.put = {"X-Requested-With": "XMLHttpRequest"};
}); 

act.controller('PaqueteActCtrl', ['$scope', '$http', '$location','$q', 'sweet', 'Upload','$timeout','$filter', function($scope, $http, $location,$q, sweet, Upload, $timeout, $filter) {

    $scope.errorOnSubmit = false;
    $scope.errorIllegalAccess = false;
    $scope.displayMessageToUser = false;
    $scope.displayValidationError = false;
    $scope.displaySearchMessage = false;
    $scope.displaySearchButton = false;
    $scope.displayCreatePaqueteButton = false;
    $scope.disabledForm=true;
    $scope.displayInformationPaquete=false;
    $scope.paquete = {};
//    $scope.url = "/GnklTracking/protected/paqueteact/";
//    $scope.urlhistorico = "/GnklTracking/protected/historico/";
    $scope.url = "../protected/paqueteact/";
    $scope.urlhistorico = "../protected/historico/";
    $scope.disabled = undefined;
    $scope.searchEnabled = undefined;    
    $scope.historico = {};
    $scope.imageoperador=null;
    
    $scope.leads = [];
    $scope.leadsCollection = [].concat($scope.leads);
    
    $scope.paquetes = [];
    $scope.paquetesCollection = [].concat($scope.paquetes);

    //$scope.noTrackingLead = ['1','2','3','4','5','6','7'];
    $scope.noTrackingLead = [];
    $scope.noTrackingLeadCollection = [].concat($scope.noTrackingLead);
        
    $scope.itemsByPage = 5;    
    $scope.tablestate={};    
    $scope.searchHist='';
    
    $scope.imageUrl='';
    $scope.histpaquete={};
    $scope.searchNoEmbarque='';
    $scope.searchIdPaq='';
    $scope.searchFormStatus=true;
    $scope.tablestateNoEmbarque={};
    $scope.searchFor='';
    $scope.listnotracking = [];
    $scope.nameNextStatus='';
    $scope.commentNextStatus='';
    $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate','dd/MM/yyyy'];
    $scope.format = $scope.formats[4];
    $scope.altInputFormats = ['M!/d!/yyyy'];
    $scope.fechaRecPaq1=null;
    $scope.fechaRecPaq2=null;
    $scope.fechaRecepcionConsulta=null;
    $scope.fechaEntregaConsulta=null;
    $scope.tablestatePeriod=null;
    $scope.nextStatus=null;
    $scope.kmActual=0;
    $scope.bitacoraStatus=null;
    
    
    
    $scope.showConfirmChangeStatus = function(updatePaqueteForm){
        sweet.show({
            title: 'Confirmar',
            text: 'Está seguro de cambiar el estatus?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Si, seguro!',
            closeOnConfirm: false,
            closeOnCancel: false
        }, function(isConfirm) {
            if (isConfirm) {
                $scope.actualizarEstatusPaquete(updatePaqueteForm);
            }else{
                $scope.exit('#actPaqueteModal');
                sweet.show('Advertencia', 'El proceso ha sido cancelado', 'warning');
            }
        });        
        
    }    
    
    $scope.actualizarEstatusPaquete = function (updatePaqueteForm) {
        if (!updatePaqueteForm.$valid) {
            $scope.displayValidationError = true;
            return;
        }
        $scope.lastAction = 'update';
        var url = $scope.url + 'updatestatus/'+$scope.paquete.idPaq;
        $scope.startDialogAjaxRequest();

        var config = {headers: {'Content-Type': 'application/json; charset=UTF-8'}};
        var formData=new FormData();
        var km = ($scope.kmActual!=null && $scope.kmActual!=undefined && $scope.kmActual!='')?$scope.kmActual:0;
        formData.append('observaciones',$scope.historico.observaciones); 

        var request = {
            method: 'POST',
            url: url,
            headers: {'Content-Type': undefined},
            data:formData,
            params:{'observaciones':$scope.historico.observaciones,'kilometraje':km},
            transformRequest: function(data, headersGetterFunction) {
                            return data;
             }    
        };

        $http(request)
            .success(function (data) {
                if(data.data!== null && data.data.idPaq!==null && data.data.idPaq!==undefined){
                    $scope.paquete=data.data.idPaq;
                }
                //$scope.resetSearch();
                $scope.displayInformationPaquete=true;
                $scope.historico.observaciones='';
                sweet.show('Correcto', 'Datos del histórico actualizados correctamente', 'success');
                if($scope.f!==null && $scope.f!==undefined && $scope.f.length>0){
                    $scope.finishAjaxCallOnSuccess(data, "#actPaqueteModal", false);
                    console.log(data);
                    $scope.addMoreImagesToHistorico(data.data);

                }else{
                    $scope.f=null;
                    $scope.finishAjaxCallOnSuccess(data, "#actPaqueteModal", false);
                }
                $scope.reloadTableData($scope.searchHist);                
            })
            .error(function(data, status, headers, config) {
                $scope.handleErrorInDialogs(status);
                $scope.f=null;
                $scope.historico.observaciones='';
                sweet.show('Error', 'Error en el proceso de actualización', 'error');
            });
    };    
    
    $scope. addMoreImagesToHistorico = function(data, flag){
        var urlimages = $scope.url + 'addimage/'+data.idHist;
        for(var i=0;i<$scope.f.length;i++){
            
            var formData=new FormData();
            formData.append('file',$scope.f[i]);

            var request = {
                method: 'POST',
                url: urlimages,
                headers: {'Content-Type': undefined},
                data:formData,
                transformRequest: function(data, headersGetterFunction) {
                                return data;
                }    
            };
            
            $http(request).success(function (data) {
                    if(flag!==null && flag===true){
                        sweet.show('Correcto', 'Carga completa de la imagen', 'success');
                    }
                    console.log($scope.searchHist);
                    $scope.reloadTableData($scope.searchHist);
                    
                })
                .error(function(data, status, headers, config) {
                    console.log(data);
            console.log(status);
                    $scope.handleErrorInDialogs(status);
                    if(flag!==null && flag===true)
                        sweet.show('Error', 'Error en la carga de imagenes', 'error');
                });            
        }
        $scope.f=null;
    };

    $scope.reloadTableData=function(search){
        var pagination = $scope.tablestate.pagination;        
        var start = pagination.start || 0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
        var number = pagination.number || 10;  // Number of entries showed per page.    
        var config = {params: {page: Math.floor(start / number) + 1}, headers: { 'Content-Type': 'application/json'}}        
        var config = {params: {page: 0}, headers: { 'Content-Type': 'application/json'}};
        var url = $scope.urlhistorico +  "searchbyidpaq/"+search;
        $scope.leads = [];
        $scope.leadsCollection = [].concat($scope.leads);
        $scope.tablestate.pagination.start = 0;
        $scope.tablestate.pagination.numberOfPages =0;

        $http.get(url,config).success(function(response){
                $scope.displaySearchMessage = true;                            
                $scope.leads = response.data;
                $scope.leadsCollection = [].concat($scope.leads);
                if($scope.tablestate!=='undefined'){
                    $scope.tablestate.pagination.start = 0;
                    $scope.tablestate.pagination.numberOfPages = $scope.getNumberOfPages(response.totalData);
                }
        }).error(function(response){
            sweet.show('Error', 'Error en el proceso de obtención históricos', 'error');
        });        
    };
        
    $scope.addImageHistorico = function(imgPaqueteForm, flag){
        if ((!imgPaqueteForm.$valid)) {
            $scope.displayValidationError = true;
            return;
        }
        var paquete = {};
        paquete.idHist=$scope.histpaquete.idHist;
        $scope.addMoreImagesToHistorico(paquete, flag);
        
    };
    
    $scope.searchPaquetesByIdPaq = function (idPaq) {
        if(idPaq!==null && idPaq!==undefined && idPaq!==''){
            $scope.lastAction = 'search';
            var url = $scope.url +  "searchlist/"+idPaq;
            $scope.startDialogAjaxRequest();

            var config = {headers: { 'Content-Type': 'application/json'}};
            $http.get(url, config)
                .success(function (data) {
                    if(data.data===null || data.data===undefined){
                        sweet.show('Atención', 'Paquete no encontrado en sistema', 'warning');
                        $scope.handleErrorInDialogs(status);
                        $scope.displayInformationPaquete=false;
                        $scope.imageUrl='';
                        $scope.finishAjaxCallOnSuccess(data, null, false);                    
                    }else{
                        $scope.imageUrl=$scope.url+'getimage/'+data.data.idOperador.idUsu;
                        $scope.displaySearchMessage = true;
                        $scope.selectedPaquete(data.data);
                        $scope.displayInformationPaquete=true;
                    }
                })
                .error(function(data, status, headers, config) {
                    $scope.handleErrorInDialogs(status);
                    $scope.displayInformationPaquete=false;
                    $scope.finishAjaxCallOnSuccess(data, null, isPagination);
                    $scope.imageUrl='';
                    sweet.show('Error', 'Error en el proceso de búsqueda', 'error');
                });            
        }

    };   
    
        
    $scope.searchPaqueteAct = function (searchPaqueteForm, isPagination) {
        //if (!($scope.searchNoEmbarque) && (!searchPaqueteForm.$valid)) {
        if ($scope.searchNoEmbarque===null || $scope.searchNoEmbarque===undefined) {
            $scope.displayValidationError = true;
            return;
        }

        if($scope.searchNoEmbarque.length<4){
            $scope.displayValidationError = true;
            return;
        }
        
        $scope.lastAction = 'search';
        //var url = '';
        var value = '';
        if($scope.searchNoEmbarque!==null && $scope.searchNoEmbarque!==undefined && $scope.searchNoEmbarque!==''){
           value=$scope.searchNoEmbarque;
        }else{
           value=$scope.searchHist;
        }
        
        
        $scope.startDialogAjaxRequest();
        
        console.log($scope.tablestateNoEmbarque.pagination);
        var start = 0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
        var number = 10;  // Number of entries showed per page.                               
        if($scope.tablestateNoEmbarque!==null && $scope.tablestateNoEmbarque.pagination!==undefined){
            var pagination = $scope.tablestateNoEmbarque.pagination;
            start = pagination.start || 0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
            number = pagination.number || 10;  // Number of entries showed per page.                       
        }
//        var config = {params: {page: Math.floor(start / number) + 1}, headers: { 'Content-Type': 'application/json'}};
        

        var config = {params:{'name':value, 'page':Math.floor(start / number)},headers: { 'Content-Type': "application/json; charset=utf-8"}};
        console.log(config);
        $http.get($scope.url+'search/', config)
            .success(function (data) {
                console.log(data);
                $scope.leads = [];
                $scope.leadsCollection = [].concat($scope.leads);
                $scope.displayInformationPaquete=false;
                $scope.paquete={};
                if(data.data===null || data.data===undefined){
                    sweet.show('Advertencia', 'No existe ruta asignada', 'warning');
                     $scope.finishAjaxCallOnSuccess($scope.paquetes, '', false);
                }else{
                    if(data.data.length<=0){
                        sweet.show('Advertencia', 'No existe ruta asignada', 'warning');
                    }
                    $scope.paquetes = data.data;
                    $scope.paquetesCollection = [].concat($scope.paquetes);
                    $scope.tablestateNoEmbarque.pagination.numberOfPages = $scope.getNumberOfPages(data.totalData);
                    $scope.finishAjaxCallOnSuccess($scope.paquetes, '', false);
                }
            })
            .error(function(data, status, headers, config) {
                $scope.leads = [];
                $scope.leadsCollection = [].concat($scope.leads);        
                $scope.handleErrorInDialogs(status);
                $scope.displayInformationPaquete=false;
                $scope.finishAjaxCallOnSuccess(data, null, isPagination);
                $scope.imageUrl='';
                sweet.show('Error', 'Error en el proceso de búsqueda', 'error');
            });
    };   

    $scope.resetSearch = function(){
        $scope.searchFor = "";
        $scope.searchNoEmbarque=null;
        $scope.pageToGet = 0;
        $scope.displaySearchMessage = false;
        $scope.paquete={};
        $scope.displayInformationPaquete=false;
    };    
    
    
    $scope.selectedPaquete = function (contact) {
        var selectedPaquete = angular.copy(contact);
        $scope.paquete = selectedPaquete;
    }    
    
    $scope.selectPaqueteAct = function(source){
     if(source!==null && source!==undefined){
            var selectedPaquete = angular.copy(source);
            $scope.paquete = selectedPaquete;
            $scope.displayInformationPaquete=true;
            $scope.searchHist=source.idPaq;
            $scope.searchHistorico('searchPaqueteHistForm', false);
            $scope.finishAjaxCallOnSuccess(source, null, false);                 
        }   
    };
        
        
    $scope.handleErrorInDialogs = function (status) {
        $("#loadingModal").modal('hide');
        $scope.state = $scope.previousState;
        // illegal access
        if(status == 403 || status == 401){
            $scope.errorIllegalAccess = true;
            $scope.state = 'error';  
            return;
        }

        $scope.errorOnSubmit = true;
        $scope.lastAction = '';
    }    
    
    $scope.startDialogAjaxRequest = function () {
        $scope.displayValidationError = false;
        $("#loadingModal").modal('show');
        $scope.previousState = $scope.state;
        $scope.state = 'busy';
    }    
    
    $scope.finishAjaxCallOnSuccess = function (data, modalId, isPagination) {
        //$scope.tableParams.reload();
        $("#loadingModal").modal('hide');

        if(!isPagination){
            if(modalId){
                $scope.exit(modalId);
            }
        }

        $scope.lastAction = '';
    }    
    
    $scope.exit = function (modalId) {
        console.log("hide "+modalId);
        $(modalId).modal('hide');
        $scope.paquete = {};
        $scope.errorOnSubmit = false;
        $scope.errorIllegalAccess = false;
        $scope.displayValidationError = false;
        $scope.displayInformationPaquete = false;
    }
    
    $scope.populateTable = function (data) {
        if (data.pagesCount > 0) {
            $scope.state = 'list';
            $scope.page = {currentPage: $scope.pageToGet, pagesCount: data.pagesCount, totalContacts : data.totalPaquetes};
            $scope.displayCreatePaqueteButton = true;
            $scope.displaySearchButton = true;
        } else {
            $scope.state = 'noresult';
            $scope.displayCreatePaqueteButton = true;

            if(!$scope.searchFor){
                $scope.displaySearchButton = false;
            }
        }

        if (data.actionMessage || data.searchMessage) {
            $scope.displayMessageToUser = $scope.lastAction != 'search';

            $scope.page.actionMessage = data.actionMessage;
            $scope.page.searchMessage = data.searchMessage;
        } else {
            $scope.displayMessageToUser = false;
        }
    }
    
    $scope.uploadFiles = function(files, errFiles) {
        $scope.f = files;
        $scope.errFile = errFiles && errFiles[0];
        if (files) {
        }   
    }
    
/* PARA LA PARTE DE HISTORICO */    
    $scope.searchHistorico = function (searchPaqueteForm, isPagination) {
        if (!($scope.searchHist) && (!searchPaqueteForm.$valid)) {
            $scope.displayValidationError = true;
            return;
        }
        console.log('search historico');
        $scope.lastAction = 'search';
        var url = $scope.urlhistorico +  "searchbyidpaq/"+$scope.searchHist;
        $scope.startDialogAjaxRequest();
        var pagination = $scope.tablestate.pagination;
        var start = pagination.start || 0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
        var number = pagination.number || 10;  // Number of entries showed per page.           
        var config = {params: {page: Math.floor(start / number) + 1}, headers: { 'Content-Type': 'application/json'}};
        $http.get(url, config)
            .success(function (data) {
                //console.log(data.data[0].archivos.length);
                $scope.displaySearchMessage = true;
                            
                $scope.leads = data.data;
                $scope.leadsCollection = [].concat($scope.leads);
                if($scope.tablestate!='undefined'){
                    $scope.tablestate.pagination.start = 0
                    $scope.tablestate.pagination.numberOfPages = $scope.getNumberOfPages(data.totalData);
                }
                $scope.finishAjaxCallOnSuccess(data, null, isPagination);
            })
            .error(function(data, status, headers, config) {
                $scope.finishAjaxCallOnSuccess(data, null, isPagination);
                $scope.handleErrorInDialogs(status);
                sweet.show('Error', 'Error en el proceso de búsqueda', 'error');
            });
    };   

    $scope.resetSearchHistorico = function(){
        $scope.searchHist = "";
        $scope.pageToGet = 0;
        $scope.displaySearchMessage = false;
        $scope.tablestate.sort.predicate = undefined;
        $scope.tablestate.search.predicateObject={};

        $scope.leads = {};
        $scope.leadsCollection = [];
        if($scope.tablestate!=='undefined'){
            $scope.tablestate.pagination.start = 0;
            $scope.tablestate.pagination.numberOfPages = 0;
        }        
    };
        
//    $scope.addSearchParametersIfNeeded = function(config, isPagination) {
//        if(!config.params){
//            config.params = {};
//        }
//
//        config.params.page = $scope.pageToGet;
//
//        if($scope.searchFor){
//            config.params.searchFor = $scope.searchFor;
//        }
//    };
    
    
    $scope.selectedPaquete = function (contact) {
//        console.log(contact);
        var selectedPaquete = angular.copy(contact);
        $scope.paquete = selectedPaquete;
//        if($scope.paquete!==null){
//            $scope.paquete.idOperador=$scope.paquete.idOperador;
//        }
    };

    $scope.selectedHistPaquete = function (histpaq) {
        var histpaquete = angular.copy(histpaq);
        $scope.histpaquete = histpaquete;
    };
        
    $scope.getImagen = function (paquetehist) {
        $scope.startDialogAjaxRequest();
        var typefile='application/zip';
        $http.get($scope.urlhistorico+'getimage/'+paquetehist.idHist, {responseType: 'arraybuffer'})
          .success(function (data) {
               $timeout(function(){
                var file = new Blob([data], {type: typefile});
                var fileURL = URL.createObjectURL(file);
                var a         = document.createElement('a');
                a.href        = fileURL; 
                a.target      = '_blank';
                a.download    = new Date().getTime()+'.zip';
                document.body.appendChild(a);
                a.click();                               
                $scope.finishAjaxCallOnSuccess(data, null, true);
               });
          }).error(function(data){
              $scope.finishAjaxCallOnSuccess(data, null, true);
          });
    }; 
    
    $scope.handleErrorInDialogs = function (status) {
        $("#loadingModal").modal('hide');
        $scope.state = $scope.previousState;
        // illegal access
        if(status === 403 || status === 401 || status === 302){
            $scope.errorIllegalAccess = true;
            $scope.state = 'error';
            //window.location.reload();
            window.location = "../welcome";
            //$location.path('/login');
            //return $q.reject(response);
            
            return;
        }

        $scope.errorOnSubmit = true;
        $scope.lastAction = '';
    };    
    
    $scope.startDialogAjaxRequest = function () {
        $scope.displayValidationError = false;
        $("#loadingModal").modal('show');
        $scope.previousState = $scope.state;
        $scope.state = 'busy';
    };    
    
    $scope.finishAjaxCallOnSuccess = function (data, modalId, isPagination) {
        $("#loadingModal").modal('hide');

        if(!isPagination){
            if(modalId){
                $scope.exit(modalId);
            }
        }

        $scope.lastAction = '';
    };    
    
    $scope.exit = function (modalId) {
        $(modalId).modal('hide');
        //TODO verificar si la linea que resetea a null el objeto es necesaria
        $scope.errorOnSubmit = false;
        $scope.errorIllegalAccess = false;
        $scope.displayValidationError = false;
    };

    $scope.rowClick = function (idx){
        $scope.editor = $scope.leads[idx];
    };
    
    $scope.paqueteFilter = function(tablestate){
        $scope.tablestateNoEmbarque=tablestate;
    }

    $scope.serverFilter = function(tablestate){
        $scope.tablestate=tablestate;
        if($scope.searchHist!==undefined && $scope.searchHist!=='' && $scope.searchHist!=='undefined'){
            var pagination = tablestate.pagination;
            var start = pagination.start || 0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
            var number = pagination.number || 10;  // Number of entries showed per page.    
            var config = {params: {page: Math.floor(start / number) + 1}, headers: { 'Content-Type': 'application/json'}};
            var complete_url = $scope.urlhistorico+"searchbyidpaq/"+$scope.searchHist;  
            $scope.startDialogAjaxRequest();
            $http.get(complete_url,config).success(function(response){
                $scope.leads = response.data;
                $scope.leadsCollection = [].concat($scope.leads);
                $scope.tablestate.pagination.numberOfPages = $scope.getNumberOfPages(response.totalData);
                $scope.finishAjaxCallOnSuccess(response.data, '', false);
            }).error(function(response){
                sweet.show('Error', 'Error en el proceso de obtención históricos', 'error');
            });            
        }
    };

    $scope.getNumberOfPages = function(totaldata){
        return Math.ceil(totaldata / $scope.itemsByPage);
    };
    
    $scope.getImageOperador = function(idoperador){
        //${contextPath}/protected/paqueteact/getimage/{{paquete.idOperador.id}}
        $scope.url+'getimage/'+idoperador;
    };
    
    $scope.showConfirmHist = function(source){
        sweet.show({
            title: 'Confirmar',
            text: 'Borrar estos archivos?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Si, borrar!',
            closeOnConfirm: false,
            closeOnCancel: false
        }, function(isConfirm) {
            console.log(isConfirm);
            if (isConfirm) {
                $scope.startDialogAjaxRequest();
                var urldelete=$scope.url+"deleteimages/"+source.idHist;
                var config = {headers: { 'Content-Type': 'application/json'}};
                $http.post(urldelete,config)
                .success(function(response){
                    $scope.finishAjaxCallOnSuccess(response.data, "null", false);
                    $scope.reloadTableData($scope.searchHist);
                    sweet.show('Borrado!', 'El archivo fue borrado.', 'success');
                }).error(function(response){
                    sweet.show('Error', 'Error en el proceso de eliminacion de imagenes', 'error');
                }); 
                
            }else{
                sweet.show('Cancelado', '', 'warning');
            }
        });        
        
    }
    
    $scope.getNoTracking = function($select) {
      if($select.search!==null && $select.search !== undefined && $select.search!=='' && $select.search.length>=4){
          var completeurl = $scope.url+'notracking/';
          var config = {params:{'name':$select.search},headers: { 'Content-Type': 'application/json', 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}};
          $http.get(completeurl, config)
              .then(function(response) {
                $scope.listnotracking = response.data;
                console.log(response);
              });  
      }
    }; 
    
  $scope.onSelectCallback = function (item, model){
      if(item!==null && item!==undefined && item!==''){
          $scope.searchNoEmbarque=item;
          $scope.searchPaqueteAct('searchPaqueteActForm', false);
      }
  };    

    $scope.verifyNextStatus = function(paquete){
        if(paquete!=null && paquete!=undefined){
            $scope.nextStatus=null;
            $scope.bitacoraStatus=null;
            var url=$scope.url+"getnextstatus";
            var config = {headers: { 'Content-Type': 'application/json'},params:{'idpaq':paquete.idPaq}};
            $http.get(url,config)
            .success(function(response){
                if(response!=null && response!=undefined){
                    $scope.nextStatus=response;
                    $scope.nameNextStatus=response.desStatus;
                    $scope.commentNextStatus=response.comentario;
                    $scope.historico.observaciones=$scope.commentNextStatus;
                    //if(paquete.idStatus.numStatus==5 && response.numStatus==5){
                    if(paquete.idStatus.numStatus==response.numStatus){
                        angular.element(document.querySelector('#actPaqueteModal')).modal('hide');
                        sweet.show('Warning', 'El proceso de actualización ha terminado para esta fase', 'warning');
                    }
                }        
            }).error(function(response){
                sweet.show('Error', 'Error en el proceso de eliminacion de imagenes', 'error');
            });             
            //getBitacoraStatus
            var url=$scope.url+"getBitacoraStatus";
            var config = {headers: { 'Content-Type': 'application/json'},params:{'idpaq':paquete.idPaq}};
            $http.get(url,config)
            .success(function(response){
                if(response!=null && response!=undefined){
                    $scope.bitacoraStatus=response;
                }else{
                    $scope.bitacoraStatus={};
                }
                $scope.kmActual=0;
            }).error(function(response){
                sweet.show('Error', 'Error en el proceso de eliminacion de imagenes', 'error');
            });             
            
        }
    }
    
    $scope.serverPeriod = function(tablestate){
        $scope.tablestatePeriod=tablestate;
        var pagination = tablestate.pagination;
        var start = pagination.start || 0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
        var number = pagination.number || 10;  // Number of entries showed per page.       
        var resultFilters = $scope.searchByColumnFilter(tablestate);
        $scope.noTrackingLeadCollection = resultFilters;
    }
    
    $scope.searchByColumnFilter=function(params){
        var filtered = [];
//        if (params.sort.predicate) {
//            filtered = $filter('orderBy')($scope.leads, params.sort.predicate, params.sort.reverse);
//        }
        filtered = params.search.predicateObject ? $filter('filter')($scope.noTrackingLead, params.search.predicateObject) : $scope.noTrackingLead;
        return filtered;
    }    

    $scope.open3 = function() {
      $scope.popup3.opened = true;
    };

    $scope.open4 = function() {
      $scope.popup4.opened = true;
    };
     
    $scope.open1 = function() {
      $scope.popup1.opened = true;
      $scope.paquete.fechaEntPaq=null;
    };

    $scope.open2 = function() {
      $scope.popup2.opened = true;
    };

    $scope.setDate = function(year, month, day) {
      $scope.dt = new Date(year, month, day);
    };

    $scope.popup1 = {
      opened: false
    };

    $scope.popup2 = {
      opened: false
    };

    $scope.popup3 = {
      opened: false
    };

    $scope.popup4 = {
      opened: false
    };
    
    var tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);
    var afterTomorrow = new Date();
    afterTomorrow.setDate(tomorrow.getDate() + 1);
    $scope.events = [
      {
        date: tomorrow,
        status: 'full'
      },
      {
        date: afterTomorrow,
        status: 'partially'
      }
    ];

    function getDayClass(data) {
      var date = data.date,
        mode = data.mode;
      if (mode === 'day') {
        var dayToCheck = new Date(date).setHours(0,0,0,0);

        for (var i = 0; i < $scope.events.length; i++) {
          var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);

          if (dayToCheck === currentDay) {
            return $scope.events[i].status;
          }
        }
      }
    }
        
    $scope.searchPaquetesPeriod=function(){
       if($scope.fechaRecPaq2!=null && $scope.fechaRecPaq1!=null){
            $scope.fechaRecepcionConsulta=null;
            $scope.fechaEntregaConsulta=null;

            var date1 = $filter('date')($scope.fechaRecPaq1, 'dd/MM/yyyy');
            var date2 = $filter('date')($scope.fechaRecPaq2, 'dd/MM/yyyy');
           
            $scope.startDialogAjaxRequest();
            var url=$scope.url+"searchByDate";
            var config = {headers: { 'Content-Type': 'application/json'},params:{'date1':date1,'date2':date2}};
            $http.get(url,config).success(function(response){
                $scope.noTrackingLead = response;
                $scope.noTrackingLeadCollection = [].concat($scope.noTrackingLead);
                $scope.finishAjaxCallOnSuccess(response, '', false);
            }).error(function(response){
                sweet.show('Error', 'Error en el proceso de eliminacion de imagenes', 'error');
            });           
       }
    }
    
    $scope.selectNoEmbarque=function(source){
        $scope.searchNoEmbarque=source;
        $scope.searchPaqueteAct('searchPaqueteActForm', false);
    }
    
    $scope.clearSearchPaquetes=function(){
        $scope.noTrackingLead = [];
        $scope.noTrackingLeadCollection = [].concat($scope.noTrackingLead);
        $scope.fechaRecPaq2=null;
        $scope.fechaRecPaq1=null;
        $scope.fechaRecepcionConsulta=null;
        $scope.fechaEntregaConsulta=null;
    }
    
    $scope.searchPaquetes = function(fecha,tipo){
        $scope.fechaRecPaq2=null;
        $scope.fechaRecPaq1=null;

        switch(tipo){
            case 1:
                $scope.fechaEntregaConsulta=null;        
                break;
            case 2:
                $scope.fechaRecepcionConsulta=null;
                break;
        }
        
        if(fecha!=null && fecha!=undefined){            
            var date1 = $filter('date')(fecha, 'dd/MM/yyyy');
            $scope.startDialogAjaxRequest();
            var url=$scope.url+"searchByFechaRecepcionEntrega";
            var config = {headers: { 'Content-Type': 'application/json'},params:{'fecha':date1,'tipo':tipo}};
            $http.get(url,config).success(function(response){
                $scope.noTrackingLead = response;
                $scope.noTrackingLeadCollection = [].concat($scope.noTrackingLead);
                $scope.finishAjaxCallOnSuccess(response, '', false);                
            }).error(function(response){
                sweet.show('Error', 'Error en el proceso de eliminacion de imagenes', 'error');
            });              
        }
    }

    angular.element(document).ready(function () {
        var params = $location.search();
        if(params!==undefined && params.noembarque!==null){
            $scope.searchIdPaq=$location.search().idpaq;
            if($location.search().idpaq!==null && $location.search().idpaq!==undefined){
                $scope.searchFormStatus=false;
                $scope.searchHist=$location.search().idpaq;
                $scope.searchPaquetesByIdPaq($location.search().idpaq);
            }
            if($location.search().noembarque!==null && $location.search().noembarque!==undefined){
                console.log($location.search().noembarque);
                $scope.searchFormStatus=true;
                $scope.searchNoEmbarque=$location.search().noembarque;
                $scope.searchPaqueteAct('searchPaqueteActForm', false);
            }            
        }
    });

  

}]);

act.directive('onlyDigits', function () {
    return {
      require: 'ngModel',
      restrict: 'A',
      link: function (scope, element, attr, ctrl) {
        function inputValue(val) {
          if (val) {
            var digits = val.replace(/[^0-9]/g, '');

            if (digits !== val) {
              ctrl.$setViewValue(digits);
              ctrl.$render();
            }
            return parseInt(digits,10);
          }
          return undefined;
        }            
        ctrl.$parsers.push(inputValue);
      }
    }
});

//act.filter('propsFilter', function() {
//  return function(items, props) {
//    var out = [];
//
//    if (angular.isArray(items)) {
//      items.forEach(function(item) {
//        var itemMatches = false;
//
//        var keys = Object.keys(props);
//        for (var i = 0; i < keys.length; i++) {
//          var prop = keys[i];
//          var text = props[prop].toLowerCase();
//          if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
//            itemMatches = true;
//            break;
//          }
//        }
//
//        if (itemMatches) {
//          out.push(item);
//        }
//      });
//    } else {
//      // Let the output be the input untouched
//      out = items;
//    }
//
//    return out;
//  };
//});
