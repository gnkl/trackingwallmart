var headerapp = angular.module('headerapp', []);

headerapp.config(function ($httpProvider) {
    $httpProvider.defaults.headers.get = {"X-Requested-With": "XMLHttpRequest"};
    $httpProvider.defaults.headers.put = {"X-Requested-With": "XMLHttpRequest"};
}); 

headerapp.controller('HeaderCtrl', ['$scope', '$http', '$location',function($scope, $http, $location) {
    $scope.username='TEST';
    $scope.user={};
    //$scope.url='/GnklTracking/protected/usuario/getloggeduser';
    if($location.$$absUrl.lastIndexOf('/contacts') > 0){
        $scope.activeURL = 'contacts';
    } else{
        $scope.activeURL = 'home';
    }

    $scope.getLoggedUser = function(){
       var config = {headers: { 'Content-Type': "application/json; charset=utf-8"}};
       $http.get($scope.url, config)
       .success(function (data,  status, headers) {
           $scope.username=data.username;
           $scope.user=data;
       })
       .error(function(data, status, headers, config) {
           $scope.username='';
       });        
    };
        
    angular.element(document).ready(function () {
        var path = $location.absUrl(); 
        var pathArray = path.split('/');
        var appContext = pathArray[3];
        $scope.appContext=appContext;      
        //$scope.url = "/"+appContext+"/protected/usuario/";
        
        $scope.url='/'+appContext+'/protected/usuario/getloggeduser';
        $scope.getLoggedUser();
    });
    
}]);

angular.bootstrap(document.getElementById("principalheader"), ['headerapp']);
