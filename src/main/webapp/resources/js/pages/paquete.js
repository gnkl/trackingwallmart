//var app = angular.module('paquete', ['ngTable']);

var app = angular.module('paquete', ['smart-table', 'ngAnimate', 'ui.bootstrap', 'ngSanitize', 'ui.select', 'hSweetAlert']);

app.factory("sessionInjector", ['$log', 'sweet', function($log,sweet){
    return {
        request: function(config) {return config;},
        response: function(response) {
            if (typeof response.data === "string" && response.data.indexOf("forma-login") > -1) {
                location.reload();
            }
            return response;
        }
    };
}]);

app.config(function ($httpProvider) {
    $httpProvider.interceptors.push("sessionInjector");
    $httpProvider.defaults.headers.post = {"X-Requested-With": "XMLHttpRequest"};
    $httpProvider.defaults.headers.get = {"X-Requested-With": "XMLHttpRequest"};
    $httpProvider.defaults.headers.put = {"X-Requested-With": "XMLHttpRequest"};
}); 


app.controller('PaqueteCtrl', ['$scope', '$http', '$location','$q', 'sweet', '$rootScope', '$filter', '$timeout',function($scope, $http, $location,$q,  sweet,$rootScope, $filter, $timeout) {

    $scope.errorOnSubmit = false;
    $scope.errorIllegalAccess = false;
    $scope.displayMessageToUser = false;
    $scope.displayValidationError = false;
    $scope.displaySearchMessage = false;
    $scope.displaySearchButton = false;
    $scope.displayCreatePaqueteButton = false;
    $scope.paquete = {};
    $scope.ptoorigen=null;
    $scope.noEmbarque=null;
    $scope.idPuntoe=null;
    $scope.idOperador=null;
    $scope.fechaEntPaq=null;
    $scope.fechaRecPaq=null;
    $scope.url = "../protected/paquetes/";
    $scope.urlact = "../protected/paqueteact/";
    $scope.url_bitacora = "../protected/bitacoras/";

    $scope.disabled = undefined;
    $scope.searchEnabled = undefined;    
    $scope.origenes = [];    
    $scope.entregas = [];
    $scope.operadores = [];
    $scope.tipoenvio = [];
    $scope.envioselected={};
    $scope.disabledOperador=false;
    $scope.leads = [];
    $scope.leadsCollection = [].concat($scope.leads);
    $scope.itemsByPage = "10";
    $scope.tablestate={};
    $scope.isgnkembarque=true;
    $scope.minDate= new Date();
    $scope.searchformorigen=[];
    $scope.searchformentrega=[];
    $scope.searchformoperador=[];
    $scope.selected={};
    $scope.paquete.fechaRecPaq=new Date();
    $scope.paquete.fechaEntPaq=new Date();
    //$scope.empresas=[{'id':0,'descripcion':'Novartis'}, {'id':2,'descripcion':'Jaloma'}];
    $scope.empresas=[{'id':2,'descripcion':'Jaloma'}];
    //$scope.empresas=[{'id':2,'descripcion':'Farmasa'}];
    $scope.consecutivo=0;
    $scope.prefijoEmpresa='';
    $scope.disable=false;
    $scope.listnotracking=[];
    $scope.selected={};
    $scope.secondPaquete={};
    $scope.isSecondPaquete=false;
    $scope.statuslist=[];
    $scope.vehiculolist=[];
    $scope.statusselected=null;
    $scope.tipoEnvioSelected=null;
    $scope.isSearching=false;
    $scope.totalViajesFound=0;
    $scope.statusNotCompleted=null;
    $scope.noTarimas=0;
    $scope.noCajas=0;
    $scope.fase=null;
    $scope.bitacora=null;
    $scope.kmActual=null;
    $scope.isSegundaFase=null;
    

    $scope.resetPaquete = function(){   
        $scope.paquete = {};
        $scope.paquete.noTarimas=0;
        if($scope.paquete.noEmbarque === null || $scope.paquete.noEmbarque === undefined){
            $scope.createNoEmbarque();
        }
    };

    $scope.resetBitacora = function(){   
        $scope.bitacora=null;
        $scope.kmActual=null;
        $scope.isSegundaFase=null;

    };
        
    $scope.deletePaquete = function () {
        $scope.lastAction = 'delete';
        var url = $scope.url + 'delete/' +$scope.paquete.idPaq;
        $scope.startDialogAjaxRequest();
        $http({
            method: 'POST',
            url: url
        }).success(function (data) {
                $scope.leads = data.data;
                $scope.leadsCollection = [].concat($scope.leads);
                if($scope.tablestate!=='undefined'){
                    $scope.tablestate.pagination.start = 0
                    //$scope.tablestate.pagination.numberOfPages = data.totalData;
                    $scope.tablestate.pagination.numberOfPages = $scope.getNumberOfPages(data.totalData);
                }            
                $scope.resetPaquete();
                $scope.finishAjaxCallOnSuccess(data, "#deletePaqueteModal", false);
                sweet.show('Ok', 'Paquete eliminado correctamente', 'success');
            }).error(function(data, status, headers, config) {
                $scope.handleErrorInDialogs(status);
                sweet.show('Error', 'Error en el proceso de eliminación', 'error');
            });
    };    

    $scope.updatePaquete = function (updatePaqueteForm) {
        if (!updatePaqueteForm.$valid) {
            $scope.displayValidationError = true;
            return;
        }

        $scope.lastAction = 'update';

        var url = $scope.url + 'update/' +$scope.paquete.idPaq;

        $scope.startDialogAjaxRequest();

        var config = {headers: {'Content-Type': 'application/json; charset=UTF-8'}};
        var statustemp = {idEnvio:$scope.envioselected.tipo};        
        var paquete2 ={
            idPaq:$scope.paquete.idPaq,
            noEmbarque:$scope.paquete.noEmbarque,
            fecCargaPaq:$scope.paquete.fecCargaPaq,
            fechaEntPaq:$scope.paquete.fechaEntPaq,
            fechaRecPaq:$scope.paquete.fechaRecPaq,
            idOperador:$scope.paquete.idOperador,
            idPuntoorigen:$scope.paquete.idPuntoorigen,
            idStatus:statustemp,
            idPuntoe:$scope.paquete.idPuntoe,
            idUsu:$scope.paquete.idUsu,
            idEmpresa:$scope.paquete.idEmpresa.id,
            idVehiculo:$scope.paquete.idVehiculo,
            noTarimas:$scope.paquete.noTarimas,
            cita:$scope.paquete.cita,
            noCajas:$scope.paquete.noCajas
        };
        console.log(paquete2);

        $http.post(url, paquete2, config)
            .success(function (data) {
                $scope.leads = data.data;
                $scope.leadsCollection = [].concat($scope.leads);
                if($scope.tablestate!='undefined'){
                    $scope.tablestate.pagination.start = 0;
                    $scope.tablestate.pagination.numberOfPages = $scope.getNumberOfPages(data.totalData);
                }                
                $scope.finishAjaxCallOnSuccess(data, "#updatePaqueteModal", false);
                $scope.resetPaquete();
                sweet.show('Ok', 'Datos actualizados correctamente', 'success');
            })
            .error(function(data, status, headers, config) {
                $scope.handleErrorInDialogs(status);
                sweet.show('Error', 'Error en el proceso de actualización', 'error');
            });
    };
    
    $scope.createPaquete = function (newPaqueteForm) {
        if (!newPaqueteForm.$valid) {
            $scope.displayValidationError = true;
            return;
        }
        
        if($scope.isSegundaFase==true){
            $scope.bitacora=null;
        }else if($scope.isSegundaFase==false && ($scope.bitacora==null || $scope.bitacora==undefined || $scope.bitacora=='')){
            $scope.displayValidationError = true;
            $scope.finishAjaxCallOnSuccess(null, "#addPaqueteModal", false);
            $scope.resetBitacora();
            $scope.resetPaquete();
            sweet.show('Error', 'No se tiene registrada una bitácora disponible para este viaje, se debe crear la bitacora antes de seguir con la creación de la fase', 'warning');                                                        
            return;
        }
        
        $scope.lastAction = 'create';
        var url = $scope.url+'create/';
        var paqueteObj = {};
        $scope.startDialogAjaxRequest();      
        
        var statustemp = {idEnvio:$scope.envioselected.tipo};
        var paquetetest ={
            noEmbarque:$scope.paquete.noEmbarque,
            fecCargaPaq:'',
            fechaEntPaq:$scope.paquete.fechaEntPaq,
            fechaRecPaq:$scope.paquete.fechaRecPaq,
            idOperador:$scope.paquete.idOperador,
            idPuntoorigen:$scope.paquete.idPuntoorigen,
            idUsu:$scope.paquete.idUsu,
            idStatus:statustemp,
            idPuntoe:$scope.paquete.idPuntoe,
            idEmpresa:$scope.paquete.idEmpresa.id,
            idVehiculo:$scope.paquete.idVehiculo,
            noTarimas:$scope.paquete.noTarimas,
            cita:$scope.paquete.cita,
            noCajas:$scope.paquete.noCajas
        }

        console.log($scope.bitacora);
        var idBitacora = ($scope.bitacora!=null && $scope.bitacora!=undefined && $scope.bitacora.id!=null && $scope.bitacora.id!=undefined)?$scope.bitacora.id:0;
        var config = {headers: { 'Content-Type': "application/json; charset=utf-8"},params:{'idBitacora':idBitacora}};
        $http.post(url, paquetetest, config)
            .success(function (data) {
                console.log(data);
                paqueteObj = $scope.paquete;
                $scope.paquete={};
                $scope.leads = data.data;
                $scope.leadsCollection = [].concat($scope.leads);
                if($scope.tablestate!=='undefined'){
                    $scope.tablestate.pagination.start = 0
                    $scope.tablestate.pagination.numberOfPages = $scope.getNumberOfPages(data.totalData);
                }                        
                $scope.finishAjaxCallOnSuccess(data, "#addPaqueteModal", false);
                sweet.show('Ok', 'Datos guardados correctamente', 'success');
                
                //$scope.updatePaqueteForm.$setPristine();
                //todo como saber si es segundo o primer viaje?
                console.log(statustemp);
                console.log(paquetetest);
//                if(data.extraParam!=null && data.extraParam!=undefined && statustemp.idEnvio!=3){
//                    if(data.extraParam<2){
//                        $scope.showConfirmCreateNextViaje(paqueteObj);
//                    }
//                }else{
//                    $scope.resetSecondPaquete();
//                }
                if(data.extraParam!=null && data.extraParam!=undefined && statustemp.idEnvio.idEnvio!=3){
                    if(data.extraParam<2){
                        $scope.showConfirmCreateNextViaje(paqueteObj);
                    }
                }else{
                    $scope.resetSecondPaquete();
                }
            })
            .error(function(data, status, headers, config) {
                $scope.paquete={};
                $scope.handleErrorInDialogs(status);
                $scope.finishAjaxCallOnSuccess(data, "#addPaqueteModal", false);
                sweet.show('Ok', 'Error en el proceso de creación '+data.actionMessage, 'error');
            });
    };
    
    $scope.showConfirmCreateNextViaje = function(paqueteobj){
        sweet.show({
            title: 'Confirmar',
            text: 'Está seguro de crear el siguiente viaje?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Si, seguro!',
            closeOnConfirm: false,
            closeOnCancel: false
        }, function(isConfirm) {
            $scope.finishAjaxCallOnSuccess(null, "#addPaqueteModal", false);
            sweet.show('Ok', 'Datos guardados correctamente', 'success');            
            if (isConfirm) {
                $scope.resetSecondPaquete();
                $scope.reloadModalWindow(paqueteobj);
                $scope.searchNoEmbarqueFase();
                $scope.bitacora=null;
            }else{
                $scope.resetSecondPaquete();
            }
        });        
    };
    
    $scope.onclickreload=function(){
        $scope.paquete = {};

        $scope.disabledOperador=false;
        $scope.disable=false;
        var selectedPaquete = angular.copy($scope.secondPaquete);
        $scope.secondPaquete={};
        $scope.paquete = selectedPaquete;
        //console.log($scope.paquete);
    //        $scope.paquete.idOperador=$scope.paquete.idUsu;
        if($scope.paquete!==undefined && $scope.paquete.idStatus!==undefined){
            $scope.envioselected.tipo = $scope.paquete.idStatus.idEnvio;
        }

        for(var i=0;i<$scope.empresas.length;i++){
          if(selectedPaquete.idEmpresa===$scope.empresas[i].id){
              $scope.paquete.idEmpresa=$scope.empresas[i];
          }  
        }

        $scope.paquete.idPuntoorigen=null;
        $scope.paquete.idPuntoe=null;
        $scope.paquete.idOperador=null;
        $scope.paquete.fechaEntPaq=null;
        $scope.paquete.fechaRecPaq=null;
        $scope.isgnkembarque=false;

    }
    
    $scope.resetSecondPaquete = function(){
        $scope.isSecondPaquete=false;
        $scope.isgnkembarque=true;
        $scope.secondPaquete={};        
    };
    
    $scope.reloadModalWindow = function(paqueteobj){
        $scope.secondPaquete = angular.copy(paqueteobj);
        $scope.newPaqueteForm.$dirty = false;
        $scope.newPaqueteForm.$pristine = true;
        $scope.newPaqueteForm.$submitted = false;
        $scope.isSecondPaquete=true;
        $scope.isgnkembarque=false;
        var myElement= document.getElementById('modalShowerUpdatePaqueteModal');
        angular.element(myElement).trigger('click');        
    };
    
    $scope.searchPaquete = function (searchPaqueteForm, isPagination) {
//        if ((!searchPaqueteForm.$valid)) {
//            $scope.displayValidationError = true;
//            return;
//        }
        
        $scope.isSearching=true;
        $scope.lastAction = 'search';
        var url = $scope.url +  "search";
        var start = 0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
        var number = 10;  // Number of entries showed per page.
        var page=0;

        $scope.startDialogAjaxRequest();
        
        if($scope.tablestate!==null && $scope.tablestate.pagination!==undefined){
            var pagination = $scope.tablestate.pagination;
            start = pagination.start || 0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
            number = pagination.number || 10;  // Number of entries showed per page.            
            page=Math.floor(start / number);
            //                $scope.tablestate.pagination.numberOfPages = $scope.getNumberOfPages(response.totalData);        
        }
                
        var config = {headers: { 'Content-Type': "application/json; charset=utf-8"},params:{
            'noembarque':($scope.selected.notracking!==null && $scope.selected.notracking!==undefined)?$scope.selected.notracking:undefined,
            'idoperador':($scope.selected.operadorid!==null && $scope.selected.operadorid!==undefined)?$scope.selected.operadorid.idUsu:undefined,
            'puntoorigen':($scope.selected.puntoo!==null && $scope.selected.puntoo!==undefined)?$scope.selected.puntoo.id:undefined,
            'puntoentrega':($scope.selected.puntoe!==null && $scope.selected.puntoe!==undefined)?$scope.selected.puntoe.id:undefined,
            'status':($scope.statusselected!==null && $scope.statusselected!==undefined)?$scope.statusselected:undefined,
            'page':page
        }};
        
        console.log(config);

        $http.get(url, config)
            .success(function (data) {
                $scope.displaySearchMessage = true;
                $scope.leads = data.data;
                $scope.leadsCollection = [].concat($scope.leads);
                if($scope.tablestate!=='undefined'){
//                    $scope.tablestate.pagination.start = page;
//                    $scope.tablestate.pagination.currentPage = page;
//                    $scope.tablestate.pagination.page = page;
//                    $scope.currentPage=page;
//                    $scope.page=page;
//                    $scope.tablestate.pagination.totalItemCount = data.totalData;
                    $scope.totalViajesFound=data.totalData;
                    $scope.tablestate.pagination.numberOfPages = $scope.getNumberOfPages(data.totalData);
                    console.log($scope.tablestate.pagination);
                }
                $scope.finishAjaxCallOnSuccess(data, "#searchContactsModal", isPagination);
            })
            .error(function(data, status, headers, config) {
                $scope.handleErrorInDialogs(status);
                sweet.show('Error', 'Error en el proceso de busqueda', 'error');
            });
    };
    
    $scope.searchNoEmbarqueFase = function () {
        if($scope.paquete!=null && $scope.paquete!=undefined && $scope.paquete.noEmbarque!=undefined && $scope.paquete.noEmbarque!=''){
//            $scope.startDialogAjaxRequest();
            var config = {headers: { 'Content-Type': "application/json; charset=utf-8"},params:{'name':$scope.paquete.noEmbarque}};
            var url = $scope.url +  "searchExacly";
            
            $http.get(url, config)
                .success(function (data) {
                    if(data!=null && data.length>0){
                        var result = data[0];
                        $scope.paquete.idPuntoorigen=result.origen;
                        $scope.paquete.idPuntoe=result.destino;
                        if(result.no_embarque==null && result.fase==null){
                            $scope.fase=1;
                        }else{
                            $scope.fase=result.fase;
                            switch(result.fase){
                                case 0:
                                case 2:    
                                    //el numero de embarque ya fue completado
                                    angular.element(document.querySelector('#addPaqueteModal')).modal('hide');
                                    angular.element(document.querySelector('#updatePaqueteModal')).modal('hide');                                    
                                    sweet.show('Warning', 'Las fases para este numero de embarque ya fueron completadas', 'warning');
                                    break;
                                case 1:
                                    //Al numero de embarque le falta la segunda fase
                                    break;
                                default:
                                    //ocurrio un error, reportar con del depto de sistemas
                                    sweet.show('Error', 'Ocurrio un error, reportar con el departamento de sistemas', 'error');
                                    break;
                            }
                        }
                    }
//                    $scope.finishAjaxCallOnSuccess(data, "#searchContactsModal", true);
                })
                .error(function(data, status, headers, config) {
                    $scope.handleErrorInDialogs(status);
                    sweet.show('Error', 'Error en el proceso de busqueda', 'error');
                });        
                
                //verificar en que fase estoy para saber si agrego o no agrego la bitacora, para segunda fase de cross dock no se necesita agregar la bitacora
                // cuando se cambie el tipo de envio verificar si estamos en primera o segunda fase
                console.log($scope.envioselected.idEnvio);
                if($scope.envioselected!=null && $scope.envioselected!=undefined && $scope.envioselected.idEnvio!=undefined && $scope.envioselected.idEnvio!=null){
                    var fase_url = $scope.url+'getFaseByEmbarque';    
                    config = {headers: { 'Content-Type': "application/json; charset=utf-8"},params:{'notracking':$scope.paquete.noEmbarque,'tipoenvio':$scope.envioselected.tipo}};
                    $http.get(fase_url, config)
                        .success(function (data) {
                            console.log('getting fase');
                            console.log(data);
                        })
                        .error(function(data, status, headers, config) {
                            $scope.handleErrorInDialogs(status);
                            sweet.show('Error', 'Error en el proceso de busqueda', 'error');
                        });                      
                }

        }
    };
    
    $scope.resetSearch = function(){
        $scope.isSearching=false;
        $scope.searchFor = "";
        //$scope.pageToGet = 0;
        $scope.displaySearchMessage = false;
        $scope.tablestate.sort.predicate = undefined;
        $scope.tablestate.search.predicateObject={};
        $scope.serverFilter($scope.tablestate);
        $scope.selected={};
        $scope.statusselected=null;
        $scope.tipoEnvioSelected=null;
        $scope.totalViajesFound=0;
    };
        
    $scope.addSearchParametersIfNeeded = function(config, isPagination) {
        if(!config.params){
            config.params = {};
        }

        config.params.page = $scope.pageToGet;

        if($scope.searchFor){
            config.params.searchFor = $scope.searchFor;
        }
    }
    
    
    $scope.selectedPaquete = function (contact) {
        $scope.disabledOperador=false;
        $scope.disable=false;
        var selectedPaquete = angular.copy(contact);
        $scope.paquete = selectedPaquete;
      
//        $scope.paquete.idOperador=$scope.paquete.idUsu;
        if($scope.paquete!==undefined && $scope.paquete.idStatus!==undefined){
            $scope.envioselected.tipo = $scope.paquete.idStatus.idEnvio;
        }
//        var result = $scope.checkIfUserIsOperador($scope.paquete.idOperador.roles);
//        $scope.disabledOperador=result;
        if($scope.paquete.idStatus!==null && $scope.paquete.idStatus!==undefined){
                console.log($scope.paquete.idStatus.numStatus);
                if($scope.paquete.idStatus.numStatus>1){
                    $scope.disable=true;
                }

        }
        
        for(var i=0;i<$scope.empresas.length;i++){
          if(selectedPaquete.idEmpresa===$scope.empresas[i].id){
              $scope.paquete.idEmpresa=$scope.empresas[i];
              break;
          }  
        }
    }    
        
    $scope.checkIfUserIsOperador = function(roles){
        var founded=false;
        angular.forEach(roles, function(value, key) {
          var obj = value;
          if(obj.desRol=='ROLE_OPERADOR'){
              founded=true;
          }
        });
        return founded;
    };
        
    $scope.handleErrorInDialogs = function (status) {
        $("#loadingModal").modal('hide');
        $scope.state = $scope.previousState;
        // illegal access
        if(status === 403 || status === 401 || status === 302){
            $scope.errorIllegalAccess = true;
            $scope.state = 'error';
            window.location = "../welcome";
            return;
        }

        $scope.errorOnSubmit = true;
        $scope.lastAction = '';
    }    
    
    $scope.startDialogAjaxRequest = function () {
        $scope.displayValidationError = false;
        $("#loadingModal").modal('show');
        $scope.previousState = $scope.state;
        $scope.state = 'busy';
    }    
    
    $scope.finishAjaxCallOnSuccess = function (data, modalId, isPagination) {
        //$scope.tableParams.reload();
        $("#loadingModal").modal('hide');

        if(!isPagination){
            if(modalId){
                $scope.exit(modalId);
            }
        }

        $scope.lastAction = '';
    }    
    
    $scope.exit = function (modalId) {
        $(modalId).modal('hide');
        $("#loadingModal").modal('hide');
        $scope.paquete = {};
        $scope.errorOnSubmit = false;
        $scope.errorIllegalAccess = false;
        $scope.displayValidationError = false;
    }
        
    $scope.createNewPaquete=function(){
        $scope.resetPaquete();
        if($scope.paquete!==undefined && $scope.paquete.idOperador===undefined ){
            var url='../protected/usuario/getusuario/';
            var config = {headers: { 'Content-Type': "application/json; charset=utf-8"}};
            $http.get(url, config)
            .success(function (data) {
                //console.log(data);
                var roles = data.roles;
                var result = $scope.checkIfUserIsOperador(roles);
                $scope.disabledOperador=result;
                if(result){
                    $scope.paquete.idOperador=data;
                    //$scope.getBitacoraByOperador($scope.paquete.idOperador.idUsu);
                }
            })
            .error(function(data, status, headers, config) {

            });
    
            if($scope.paquete!==undefined && $scope.empresas.length==1){
                for(var i=0;i<$scope.empresas.length;i++){
                    $scope.paquete.idEmpresa=$scope.empresas[i];
                }
            }        
        }
        
        var url='../protected/paquetes/folios/1';
        var config = {headers: { 'Content-Type': "application/json; charset=utf-8"}};
        $http.get(url, config)
            .success(function (data) {
                $scope.consecutivo=data;
                $scope.createNoEmbarque();
            })
            .error(function(data, status, headers, config) {
                console.log('error');
            });
        
        $scope.paquete.cita=0;
        $scope.envioselected={};
        $scope.bitacora={};
        $scope.kmActual=null;        
    };
    
    $scope.getBitacoraByOperador=function(idOperador, tipoEnvio){
        console.log('getBitacoraByOperador');
        console.log(tipoEnvio);
        console.log(idOperador);
        console.log($scope.isSegundaFase)
        if(idOperador!=null && idOperador!=undefined && tipoEnvio!=null && tipoEnvio!=undefined){
            var url=$scope.url_bitacora+'gettodaybitacora';
            var config = {headers: { 'Content-Type': "application/json; charset=utf-8"},params:{'idOperador':idOperador,'tipoenvio':tipoEnvio}};
            $http.get(url, config).success(function (data) {
                    console.log(data);
                    if(data!=null && data!=undefined){
                        $scope.bitacora=data;
                    }else{
                        $scope.finishAjaxCallOnSuccess(data, "#addPaqueteModal", false);
                        sweet.show('Error', 'No se tiene creada una bitácora correspondiente para la fecha de hoy y el operador seleccionado, se debe crear la bitacora antes de seguir con la creación de la fase', 'warning');                                            
                    }
                })
                .error(function(data, status, headers, config) {
                    console.log('error');
                    //$scope.exit('#addPaqueteModal');
                    $scope.finishAjaxCallOnSuccess(data, "#addPaqueteModal", false);
                    sweet.show('Error', 'Ocurrió un error en el sistema, favor de reportar', 'warning');            
                });                
        }else{
            //sweet.show('Warning', 'No se tiene seleccionado operador o el tipo de envio, es necesario seleccionar ambos para que el sistema asigne una bitácora', 'warning');
        }
    }
    
    $scope.createNoEmbarque=function(){
        var prefijo = ($scope.prefijoEmpresa!==null && $scope.prefijoEmpresa!=='' && $scope.prefijoEmpresa!==undefined?$scope.prefijoEmpresa:$scope.randomString(2))
        $scope.paquete.noEmbarque= prefijo+'/'+$scope.consecutivo;
    }

    $scope.rowClick = function (idx)
    {
        $scope.editor = $scope.leads[idx];
    };
    
    $scope.orderByColumnFilter=function(params){
        var filtered = [];
        if (params.sort.predicate) {
            filtered = $filter('orderBy')($scope.leads, params.sort.predicate, params.sort.reverse);
        }
        return filtered;
    }

    $scope.serverFilter = function(tablestate)
    {
        $scope.tablestate=tablestate;
        
        var resultFilters = $scope.orderByColumnFilter(tablestate);
        
        if(resultFilters.length>0){
            $scope.leadsCollection=[].concat(resultFilters);
        }else {
            if($scope.isSearching===true){
                 $scope.searchPaquete('searchPaqueteForm', false);
            }else{
                var complete_url = $scope.url+"listpaquetes";  
                var pagination = tablestate.pagination;
                var start = pagination.start || 0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
                var number = pagination.number || 10;  // Number of entries showed per page.    
                var config = {params: {page: Math.floor(start / number) + 1}, headers: { 'Content-Type': 'application/json'}};
                $scope.startDialogAjaxRequest();
                $http.get(complete_url,config).success(function(response){
                    $scope.leads = response.data;
                    $scope.leadsCollection = [].concat($scope.leads);
                    $scope.totalViajesFound=response.totalData;
                    $scope.tablestate.pagination.numberOfPages = $scope.getNumberOfPages(response.totalData);
                    $scope.finishAjaxCallOnSuccess(response.data, "null", false);
                });            
            }                        
        }
        

    };

    $scope.getNumberOfPages = function(totaldata){
        return Math.ceil(totaldata / $scope.itemsByPage);
    };
    
    $scope.getTipoEnvio = function () {
        var completeurl = $scope.url + 'searchenvio/';
        var config = {headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}};
        $http.get(completeurl, config)
                .then(function (response) {
                    $scope.tipoenvio = response.data.data;
                    console.log($scope.tipoenvio);
                });            
    };
  
  // Any function returning a promise object can be used to load values asynchronously
  $scope.getOrigenes = function($select) {
    if($select.search!=='' && $select.search.length>3){
        var completeurl = $scope.url+'searchorigen/'+$select.search;
        var config = {headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}};
        $http.get(completeurl, config)
            .then(function(response) {
              $scope.origenes = response.data.data;
            });    
        
    }  
  };  

  // Any function returning a promise object can be used to load values asynchronously
  $scope.getEntrega = function($select) {
    if($select.search!=='' && $select.search.length>3){
    var completeurl = $scope.url+'searchentrega/'+$select.search;
    var config = {headers: { 'Content-Type': 'application/json', 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}};
//    var result = [];
//    var deferred =  $q.defer();
        $http.get(completeurl, config)
            .then(function(response) {
              $scope.entregas = response.data.data;
            });         
    }  
  };  

  // Any function returning a promise object can be used to load values asynchronously
  $scope.getOperador = function($select) {
    if($select.search!==''){
        var completeurl = $scope.url+'operadorbyname';
        var config = {params:{'name':$select.search},headers: { 'Content-Type': 'application/json', 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}};
        $http.get(completeurl, config)
            .then(function(response) {
              $scope.operadores = response.data.data;
              console.log($scope.operadores);
            });  
    }
  };  

  $scope.getOperadorSearch = function() {
    var completeurl = $scope.url+'searchoperador/';
    var config = {headers: { 'Content-Type': 'application/json', 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}};
    $http.get(completeurl, config)
        .then(function(response) {
          $scope.searchformoperador = response.data.data;
        });  
  };  
        
  $scope.getStatusSearch = function(tipoEnvio) {
    var completeurl = $scope.url+'getstatus/';
    var config = {headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'},params:{'empresa':2,'page':0,'tipoenvio':tipoEnvio}};
    $http.get(completeurl, config)
        .then(function(response) {
          $scope.statuslist=response.data;
          //$scope.leadsCollection = [].concat($scope.leads);
          var id='';
          for(i=0;i<$scope.statuslist.length-2 ;i++){
             if(i+1===$scope.statuslist.length-2){
                id+=$scope.statuslist[i].idStatus;
             }else{
                id+=$scope.statuslist[i].idStatus+',';
             } 
          }
//          console.log($scope.statuslist);
//          console.log(id);
          $scope.statuslist.push({'idStatus':id,'idEmpresa':2,'desStatus':'MOSTRAR TODOS LOS INCOMPLETOS','numStatus':0});
//          console.log($scope.statuslist);
        });  
  };         
        
  $scope.getVehiculos = function() {
    var completeurl = $scope.url+'getvehiculos/';
    var config = {headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}};
    $http.get(completeurl, config)
        .then(function(response) {
          console.log(response);
          $scope.vehiculolist=response.data;
        });  
  };        
        
  $scope.getNoEmbarqueSearch = function() {
    var completeurl = $scope.url+'searchoperador/';
    var config = {headers: { 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}};
    $http.get(completeurl, config)
        .then(function(response) {
          $scope.searchformoperador = response.data.data;
        });  
  };          
  
  $scope.onChangeTipoEnvio = function(){
      //console.log($scope.tipoEnvioSelected);
      if($scope.tipoEnvioSelected!==null && $scope.tipoEnvioSelected!==undefined){
        $scope.statusNotCompleted=null;
        $scope.getStatusSearch($scope.tipoEnvioSelected);    
      }
  };
  // datepicket to select date
  $scope.onChangeStatus=function(){
      //console.log($scope.statusselected);
      $scope.searchPaquete('searchPaqueteForm', false);
  };
  
  $scope.today = function() {
    $scope.dt = new Date();
  };
  $scope.today();

  $scope.clear = function() {
    $scope.dt = null;
  };

  $scope.inlineOptions = {
    customClass: getDayClass,
    //minDate: new Date(),
    showWeeks: true
  };

  $scope.dateOptions = {
    //dateDisabled: disabled,
    formatYear: 'YYYY',
    //maxDate: new Date(2020, 5, 22),
    //minDate: new Date(),
    startingDay: 1
  };

  /*$scope.dateOptions2 = {
    dateDisabled: disabled,
    formatYear: 'YYYY',
    minDate: $scope.paquete.fechaRecPaq,
    startingDay: 1
  };*/
        
  // Disable weekend selection
  function disabled(data) {
    var date = data.date,
      mode = data.mode;
    return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
    //return mode === 'day' && (date.getDay() > 6);
  }
  
  $scope.open1 = function() {
    $scope.popup1.opened = true;
    $scope.paquete.fechaEntPaq=null;
  };

  $scope.open2 = function() {
    $scope.popup2.opened = true;
  };

  $scope.setDate = function(year, month, day) {
    $scope.dt = new Date(year, month, day);
  };

  $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate','dd/MM/yyyy'];
  $scope.format = $scope.formats[4];
  $scope.altInputFormats = ['M!/d!/yyyy'];

  $scope.popup1 = {
    opened: false
  };

  $scope.popup2 = {
    opened: false
  };

  var tomorrow = new Date();
  tomorrow.setDate(tomorrow.getDate() + 1);
  var afterTomorrow = new Date();
  afterTomorrow.setDate(tomorrow.getDate() + 1);
  $scope.events = [
    {
      date: tomorrow,
      status: 'full'
    },
    {
      date: afterTomorrow,
      status: 'partially'
    }
  ];

  function getDayClass(data) {
    var date = data.date,
      mode = data.mode;
    if (mode === 'day') {
      var dayToCheck = new Date(date).setHours(0,0,0,0);

      for (var i = 0; i < $scope.events.length; i++) {
        var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);

        if (dayToCheck === currentDay) {
          return $scope.events[i].status;
        }
      }
    }
  }
      
    $scope.clickEventCheck = function(event) { 
        if(event===true){
            $scope.createNoEmbarque();
        }else{
            $scope.paquete.noEmbarque='';
        }
     };      
     
    $scope.selectEmpresa=function(){
        if($scope.isSecondPaquete!==true){
            var prefijo = $scope.paquete.idEmpresa!==null?$scope.paquete.idEmpresa.descripcion:'';
            $scope.prefijoEmpresa = prefijo!==''?prefijo.substring(0,3).toUpperCase():'';
            $scope.createNoEmbarque();            
        }
    };
     
    $scope.randomString = function ( n ) {
      var r="";
      while(n--)r+=String.fromCharCode((r=Math.random()*62|0,r+=r>9?(r<36?55:61):48));
      return r.toUpperCase();
    };  
    
    $scope.getNoTracking = function($select) {
      if($select.search!==null && $select.search !== undefined && $select.search!=='' && $select.search.length>=4){
          var completeurl = $scope.urlact+'notracking/';
          var config = {params:{'name':$select.search},headers: { 'Content-Type': 'application/json', 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}};
          $http.get(completeurl, config)
              .then(function(response) {
                $scope.listnotracking = response.data;
                console.log(response);
              }, function(error) {
                console.log(error);
            });  
      }
    };     
        
  $scope.onSelectNoTrackingCallback = function (item, model){
      if(item!==null && item!==undefined && item!==''){
        console.log(item);
        $scope.selected.notracking=item;
        //$scope.nombrePunto=item;
        //$scope.searchPunto('searchPuntoForm', false,0);                    
        $scope.searchPaquete('searchPaqueteForm', false);
      }
  };          

  $scope.onSelectOperadorCallback = function (item, model){
      if(item!==null && item!==undefined && item!==''){
        console.log(item);
        $scope.selected.operadorid=item;
        //$scope.nombrePunto=item;
        //$scope.searchPunto('searchPuntoForm', false,0);
        $scope.searchPaquete('searchPaqueteForm', false);
      }
  };          

  $scope.onSelectOperadorForm = function (item, model){
      if(item!==null && item!==undefined && item!==''){
        console.log(item);
//        $scope.getBitacoraByOperador(item.idUsu, )
      }
  };          


  $scope.onSelectPtoOrigenCallback = function (item, model){
      if(item!==null && item!==undefined && item!==''){
        console.log(item);
        $scope.selected.puntoo=item;
        //$scope.nombrePunto=item;
        //$scope.searchPunto('searchPuntoForm', false,0);
        $scope.searchPaquete('searchPaqueteForm', false);
      }
  };          

  $scope.onSelectPtoEntregaCallback = function (item, model){
      if(item!==null && item!==undefined && item!==''){
        $scope.selected.puntoe=item;
        $scope.searchPaquete('searchPaqueteForm', false);
      }
  };

        
    $scope.onSelectTipoEnvioCallback = function (item, model){
        if(item!==null && item!==undefined && item!==''){
            if($scope.envioselected!=null && $scope.envioselected!=undefined){
                var fase_url = $scope.url+'getFaseByEmbarque';    
                config = {headers: { 'Content-Type': "application/json; charset=utf-8"},params:{'notracking':$scope.paquete.noEmbarque,'tipoenvio':item.idEnvio}};
                $http.get(fase_url, config)
                    .success(function (data) {
                        console.log('check segunda fase');
                        console.log(data);
                        if(data!=null && data!=undefined){
                            if(data==1 || data==0){
                                $scope.isSegundaFase=false;
                                $scope.getBitacoraByOperador($scope.paquete.idOperador.idUsu, item.idEnvio);
                            }else if(data==2){
                                $scope.isSegundaFase=true;
                            }
                        }else{
                            $scope.isSegundaFase=false;
                        }
                    })
                    .error(function(data, status, headers, config) {
                        $scope.handleErrorInDialogs(status);
                        sweet.show('Error', 'Error en el proceso de busqueda', 'error');
                    });                      
            }            
        }
    };
    
    $scope.exportToExcel=function(tableId){ // ex: '#my-table'
          var exportHref=Excel.tableToExcel(tableId,'WireWorkbenchDataExport');
          $timeout(function(){location.href=exportHref;},100); // trigger download
    }  
        
    angular.element(document).ready(function () {
        $scope.getTipoEnvio();
        $scope.getOperadorSearch();
        $scope.getVehiculos();
    });            

}]);

//app.filter('propsFilter', function() {
//  return function(items, props) {
//    var out = [];
//
//    if (angular.isArray(items)) {
//      items.forEach(function(item) {
//        var itemMatches = false;
//
//        var keys = Object.keys(props);
//        for (var i = 0; i < keys.length; i++) {
//          var prop = keys[i];
//          var text = props[prop].toLowerCase();
//          if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
//            itemMatches = true;
//            break;
//          }
//        }
//
//        if (itemMatches) {
//          out.push(item);
//        }
//      });
//    } else {
//      // Let the output be the input untouched
//      out = items;
//    }
//
//    return out;
//  };
//});