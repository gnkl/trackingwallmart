var app = angular.module('ruta', ['smart-table', 'ngAnimate', 'ui.bootstrap', 'ngSanitize', 'ui.select', 'hSweetAlert','ngFileUpload']);

app.config(function ($httpProvider) {
    $httpProvider.defaults.headers.post = {"X-Requested-With": "XMLHttpRequest"};
    $httpProvider.defaults.headers.get = {"X-Requested-With": "XMLHttpRequest"};
    $httpProvider.defaults.headers.put = {"X-Requested-With": "XMLHttpRequest"};
}); 

app.controller('RutaCtrl', ['$scope', '$http', '$location','$q', 'sweet', '$rootScope', 'Upload','$timeout', function($scope, $http, $location,$q,  sweet,$rootScope, Upload, $timeout) {

    $scope.errorOnSubmit = false;
    $scope.errorIllegalAccess = false;
    $scope.displayMessageToUser = false;
    $scope.displayValidationError = false;
    $scope.displaySearchMessage = false;
    $scope.displaySearchButton = false;
    $scope.displayCreatePaqueteButton = false;
    $scope.ruta = {};
    $scope.ptoorigen=null;
    $scope.noEmbarque=null;
    $scope.idPuntoe=null;
    $scope.idOperador=null;
    $scope.fechaEntPaq=null;
    $scope.fechaRecPaq=null;
    $scope.url = "../protected/rutas/";
    $scope.url_paquete = "../protected/paquetes/";
    $scope.url_bitacora = "../protected/bitacoras/";
    $scope.disabled = undefined;
    $scope.searchEnabled = undefined;    
    $scope.origenes = [];    
    $scope.entregas = [];
    $scope.operadores = [];
    $scope.tipoenvio = [];
    $scope.envioselected={};
    $scope.disabledOperador=false;
    $scope.leads = [];
    $scope.leadsCollection = [].concat($scope.leads);
    $scope.itemsByPage = "10";
    $scope.tablestate={};
    $scope.tablestateBitacora={};
    $scope.isgnkembarque=true;
    $scope.minDate= new Date();
    $scope.searchformorigen=[];
    $scope.searchformentrega=[];
    $scope.searchformoperador=[];
    $scope.selected={};
    $scope.estatus=[{'status':1,'descripcion':'Activo'}, {'status':3,'descripcion':'Inactivo'}];
    $scope.idruta=null;
    $scope.firsttab=false;
    $scope.leadsBitacora=[];
    $scope.leadsBitacoraCollection=[];
    $scope.ruta=null;
    $scope.tipoEnvio=null;
    $scope.rutaselected=null;
    $scope.listIdRuta=[];
    $scope.comentario=null;
    
    
    
    $scope.createNewRuta=function(){
        $scope.ruta = {};
        $scope.selected={};
    };
    
    
    $scope.deleteRuta = function () {
        $scope.lastAction = 'delete';
        var url = $scope.url + 'delete/' +$scope.ruta.id;
        $scope.startDialogAjaxRequest();
        $http({
            method: 'POST',
            url: url
        }).success(function (data) {
                $scope.serverFilter($scope.tablestate);
                $scope.finishAjaxCallOnSuccess(data, "#deleteRutaModal", false);
                sweet.show('Ok', 'Paquete eliminado correctamente', 'success');
            }).error(function(data, status, headers, config) {
                $scope.handleErrorInDialogs(status);
                sweet.show('Error', 'Error en el proceso de eliminación', 'error');
            });
    };    

    
    $scope.createRuta = function (newRutaForm) {
        if (!newRutaForm.$valid) {
            $scope.displayValidationError = true;
            return;
        }
        $scope.lastAction = 'create';
        var url = $scope.url+'create/';
        $scope.startDialogAjaxRequest();      
        

        var rutatest ={
            id:$scope.ruta.id,
            nombre:$scope.ruta.nombre,
            estatus:$scope.selected.estatus.status,
            fechaCreacion:$scope.ruta.fechaCreacion,
            fechaModificacion:$scope.ruta.fechaModificacion,
            idEnvio:$scope.ruta.idEnvio
        }    
        
        console.log(rutatest);
        
        var config = {headers: { 'Content-Type': "application/json; charset=utf-8"}};
        $http.post(url, rutatest, config)
            .success(function (data) {
                $scope.ruta={};
                $scope.serverFilter($scope.tablestate);
                $scope.finishAjaxCallOnSuccess(data, "#addRutaModal", false);
                sweet.show('Ok', 'Datos guardados correctamente', 'success');
            })
            .error(function(data, status, headers, config) {
                $scope.ruta={};
                $scope.handleErrorInDialogs(status);
                $scope.finishAjaxCallOnSuccess(data, "#addPaqueteModal", false);
                sweet.show('Ok', 'Error en el proceso de creación '+data.actionMessage, 'error');
            });
    };    
    
    
    $scope.selectedRuta = function (ruta) {
        var selectedRuta = angular.copy(ruta);
        $scope.ruta = selectedRuta;
        if($scope.ruta!==undefined && $scope.ruta.estatus!==undefined){
            for(i=0;i<$scope.estatus.length;i++){
                if($scope.estatus[i].status===$scope.ruta.estatus){
                   $scope.selected.estatus=$scope.estatus[i];
                   break;
                }
            }
        }
    };
    
    $scope.clickSelectRuta = function (ruta) {
        $scope.comentario=null;
        $scope.resetFiles();
        var selectedRuta = angular.copy(ruta);
        $scope.ruta = selectedRuta;
        if($scope.ruta.isPausa==1){
            $scope.exit('#interruptRutaModal');
            $scope.comentario="N/A";
        }
    };    
    
        
    $scope.handleErrorInDialogs = function (status) {
        $("#loadingModal").modal('hide');
        $scope.state = $scope.previousState;
        // illegal access
        if(status === 403 || status === 401 || status === 302){
            $scope.errorIllegalAccess = true;
            $scope.state = 'error';
            window.location = "/GnklTracking/welcome";
            return;
        }

        $scope.errorOnSubmit = true;
        $scope.lastAction = '';
    }    
    
    $scope.startDialogAjaxRequest = function () {
        $scope.displayValidationError = false;
        $("#loadingModal").modal('show');
        $scope.previousState = $scope.state;
        $scope.state = 'busy';
    }    
    
    $scope.finishAjaxCallOnSuccess = function (data, modalId, isPagination) {
        //$scope.tableParams.reload();
        $("#loadingModal").modal('hide');

//        if(!isPagination){
//            if(modalId){
//                $scope.exit(modalId);
//            }
//        }

        $scope.lastAction = '';
    }    
    
    $scope.exit = function (modalId) {
        $(modalId).modal('hide');
        $("#loadingModal").modal('hide');
//        $scope.ruta = {};
        $scope.selected={};
        $scope.errorOnSubmit = false;
        $scope.errorIllegalAccess = false;
        $scope.displayValidationError = false;
    }

    $scope.serverFilter = function(tablestate)
    {
        var complete_url = $scope.url+"listrutas";  
        $scope.tablestate=tablestate;
        var pagination = tablestate.pagination;
        var start = pagination.start || 0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
        var number = pagination.number || 10;  // Number of entries showed per page.    
        var config = {params: {page: Math.floor(start / number) + 1}, headers: { 'Content-Type': 'application/json'}};
        $scope.startDialogAjaxRequest();
        $http.get(complete_url,config).success(function(response){
            console.log(response);
            $scope.leads = response.data;
            $scope.leadsCollection = [].concat($scope.leads);
            $scope.tablestate.pagination.numberOfPages = $scope.getNumberOfPages(response.total);
            $scope.finishAjaxCallOnSuccess(response.data, "null", false);
        });
    };

    $scope.serverBitacoraFilter = function(tablestate){
        var complete_url = $scope.url+"getBitacorasByRuta";  
        $scope.tablestateBitacora=tablestate;
        if($scope.ruta!=null && $scope.ruta.id!=null && $scope.ruta.id!=undefined){
            var pagination = tablestate.pagination;
            var start = pagination.start || 0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
            var number = pagination.number || 10;  // Number of entries showed per page.    
            var config = {params: {'idruta':$scope.ruta.id}, headers: { 'Content-Type': 'application/json'}};
            $scope.startDialogAjaxRequest();
            $http.get(complete_url,config).success(function(response){
                console.log(response);
                $scope.leadsBitacora = response;
                $scope.leadsBitacoraCollection = [].concat($scope.leadsBitacora);
                //$scope.tablestate.pagination.numberOfPages = $scope.getNumberOfPages(response.totalData);
                $scope.finishAjaxCallOnSuccess(response.data, "null", false);
            });                    
        }
    }
    
    $scope.getCSSClass = function(index){
        var item = $scope.leadsBitacoraCollection[index];
        var style='';
        if(item.idStatus.numStatus==1){
            style='bg-danger';
        }else{
            switch(item.isFinal){
                case 1:
                    if(item.idStatus.numStatus==7){
                        style='bg-info';
                    }else{
                        style='bg-warning';
                    }
                break;
                case 0:
                    if(item.idStatus.numStatus==5){
                        style='bg-info';
                    }else{
                        style='bg-warning';
                    }                
                break;
                default:
                break;
            }            
        }
        return style;
    }

    $scope.showButtonUpdateStatus = function(source){
        if(source.isFinal==1 && source.idStatus.numStatus==7){
            return false;
        }else if (source.isFinal==0 && source.idStatus.numStatus==5){
            return false;
        }else{
            return true;
        }
    }
    

    $scope.getListBitacoraByRuta=function(source){
        console.log('test');
        console.log(source);
        //if(source!=null && source!=undefined){
//            $scope.ruta=source;
            $scope.serverBitacoraFilter($scope.tablestateBitacora);
            
        //}
    }
    
    $scope.getListRuta = function($select) {
      if($select.search!==null && $select.search !== undefined && $select.search!==''){
          var completeurl = $scope.url+'getrutas/';
          var config = {params:{'idRuta':$select.search},headers: { 'Content-Type': 'application/json', 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}};
          $http.get(completeurl, config)
              .then(function(response) {
                $scope.listIdRuta = response.data;
                console.log(response);
              }, function(error) {
                console.log(error);
            });  
      }
    };    

    $scope.getNumberOfPages = function(totaldata){
        return Math.ceil(totaldata / $scope.itemsByPage);
    };
    
  $scope.getOrigenes = function($select) {
    if($select.search!=='' && $select.search.length>3){
        var completeurl = $scope.url_paquete+'searchorigen/'+$select.search;
        var config = {headers: { 'Content-Type': 'application/json', 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}};
        $http.get(completeurl, config)
            .then(function(response) {
              $scope.origenes = response.data.data;
            });    
        
    }  
  };  

  $scope.getEntrega = function($select) {
    if($select.search!=='' && $select.search.length>3){
        var completeurl = $scope.url_paquete+'searchentrega/'+$select.search;
        var config = {headers: { 'Content-Type': 'application/json', 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}};
        $http.get(completeurl, config)
            .then(function(response) {
              $scope.entregas = response.data.data;
            });         
    }  
  };    
  
    $scope.getTipoEnvio = function () {
        var completeurl = $scope.url_paquete + 'searchenvio/';
        var config = {headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}};
        $http.get(completeurl, config)
                .then(function (response) {
                    $scope.tipoenvio = response.data.data;
                    console.log($scope.tipoenvio);
                });            
    };  
    
    
    
  // datepicket to select date
  
  $scope.today = function() {
    $scope.dt = new Date();
  };
  $scope.today();

  $scope.clear = function() {
    $scope.dt = null;
  };

  $scope.inlineOptions = {
    customClass: getDayClass,
    minDate: new Date(),
    showWeeks: true
  };

  $scope.dateOptions = {
    dateDisabled: disabled,
    formatYear: 'YYYY',
    //maxDate: new Date(2020, 5, 22),
    minDate: new Date(),
    startingDay: 1
  };

  /*$scope.dateOptions2 = {
    dateDisabled: disabled,
    formatYear: 'YYYY',
    minDate: $scope.paquete.fechaRecPaq,
    startingDay: 1
  };*/
        
  // Disable weekend selection
  function disabled(data) {
    var date = data.date,
      mode = data.mode;
    //return mode === 'day' && (date.getDay() === 0 || date.getDay() === 6);
    return mode === 'day' && (date.getDay() > 6);
  }
  
  $scope.open1 = function() {
    $scope.popup1.opened = true;
    $scope.paquete.fechaEntPaq=null;
  };

  $scope.open2 = function() {
    $scope.popup2.opened = true;
  };

  $scope.setDate = function(year, month, day) {
    $scope.dt = new Date(year, month, day);
  };

  $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate','dd/MM/yyyy'];
  $scope.format = $scope.formats[4];
  $scope.altInputFormats = ['M!/d!/yyyy'];

  $scope.popup1 = {
    opened: false
  };

  $scope.popup2 = {
    opened: false
  };

  var tomorrow = new Date();
  tomorrow.setDate(tomorrow.getDate() + 1);
  var afterTomorrow = new Date();
  afterTomorrow.setDate(tomorrow.getDate() + 1);
  $scope.events = [
    {
      date: tomorrow,
      status: 'full'
    },
    {
      date: afterTomorrow,
      status: 'partially'
    }
  ];

  function getDayClass(data) {
    var date = data.date,
      mode = data.mode;
    if (mode === 'day') {
      var dayToCheck = new Date(date).setHours(0,0,0,0);

      for (var i = 0; i < $scope.events.length; i++) {
        var currentDay = new Date($scope.events[i].date).setHours(0,0,0,0);

        if (dayToCheck === currentDay) {
          return $scope.events[i].status;
        }
      }
    }
  }
      
    $scope.clickEventCheck = function(event) { 
        if(event==true){
            $scope.paquete.noEmbarque=$scope.randomString(2)+new Date().getTime();
        }else{
            $scope.paquete.noEmbarque='';
        }
     };      
     
    $scope.randomString = function ( n ) {
      var r="";
      while(n--)r+=String.fromCharCode((r=Math.random()*62|0,r+=r>9?(r<36?55:61):48));
      return r.toUpperCase();
    };  
    
    $scope.rowClick = function (idx) {
        var value = $scope.leads[idx];
        $scope.ruta=value;
        console.log($scope.ruta);
        $scope.getListBitacoraByRuta($scope.ruta);
    };    

    
    $scope.uploadFiles = function(files, errFiles) {
        $scope.f = files;
        $scope.errFile = errFiles && errFiles[0];
    };    
    
    $scope.resetFiles = function() {
        $scope.f = null;
        $scope.errFile = null;
    };    
    
    $scope.proccesMultipartFile = function(imgPaqueteForm, flag){
        if ((!imgPaqueteForm.$valid)) {
            $scope.displayValidationError = true;
            return;
        }
        
        console.log(imgPaqueteForm.$valid);
        
        if($scope.f===null){
            sweet.show('Error', 'Se debe cargar un archivo antes de procesarce', 'error');
            $scope.displayValidationError = true;
            return;
        }
        console.log($scope.f.type);
        if($scope.f.type!=='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'){
            sweet.show('Error', 'Debe ser un archivo de tipo CSV', 'error');
            $scope.displayValidationError = true;
            return;            
        }
        
        $scope.exit('#proccesFileRutaModal');
        
        var urlimages = $scope.url + 'uploadfile';
            
        var formData=new FormData();
        formData.append('file',$scope.f);

        var request = {
            method: 'POST',
            url: urlimages,
            headers: {'Content-Type': undefined},
            params:{'tipoEnvio':$scope.tipoEnvio.idEnvio},
            data:formData,
            transformRequest: function(data, headersGetterFunction) {
                            return data;
            }    
        };
        console.log('request ');
        console.log(request);
        
        $scope.startDialogAjaxRequest();    
        $http(request).success(function (data) {            
            $scope.reloadTableRutas();
            $scope.finishAjaxCallOnSuccess(data, "null", false);
            sweet.show('OK', 'El archivo fue procesado correctamente', 'success');
            $scope.exit('#proccesFileRutaModal');
        })
        .error(function(data, status, headers, config) {
            $scope.handleErrorInDialogs(status);
            if(flag!==null && flag===true)
                sweet.show('Error', 'Error en la carga del archivo', 'error');
        });     
        $scope.f=null;        
    };
        
    
/////////////////////////////////////////////////////////////////////////////////////////////////////////////BITACORAS
    $scope.createNewBitacora=function(){
        console.log($scope.ruta);
        $scope.resetBitacora();
        if($scope.bitacora!==undefined && $scope.bitacora.idOperador===undefined ){
            var url='../protected/usuario/getusuario/';
            var config = {headers: { 'Content-Type': "application/json; charset=utf-8"}};
            $http.get(url, config)
            .success(function (data) {
                //console.log(data);
                var roles = data.roles;
                var result = $scope.checkIfUserIsOperador(roles);
                $scope.disabledOperador=result;
                if(result){
                    $scope.bitacora.idOperador=data;
                }
            })
            .error(function(data, status, headers, config) {
            });
        }

//        var url=$scope.url_bitacora+'getdefaultpunto';
//        var config = {headers: { 'Content-Type': "application/json; charset=utf-8"}};
//        $http.get(url, config)
//        .success(function (data) {
//            if(data!=null && data!=undefined){
//                $scope.bitacora.ptoOrigen=data[0].origen;
//                $scope.bitacora.ptoDestino=data[0].destino;
//            }
//            console.log($scope.bitacora);
//        })
//        .error(function(data, status, headers, config) {
//        });

        $scope.bitacora.fecha = new Date();
        $scope.bitacora.isFinal = 0;
        console.log('create new bitacora');
        console.log($scope.bitacora);
        
    };
    
    $scope.resetBitacora = function(){   
        $scope.bitacora = {};        
    };    
    
        
    $scope.checkIfUserIsOperador = function(roles){
        var founded=false;
        angular.forEach(roles, function(value, key) {
          var obj = value;
          if(obj.desRol=='ROLE_OPERADOR'){
              founded=true;
          }
        });
        return founded;
    };
    
    $scope.createBitacora = function (newBitacoraForm) {
        if (!newBitacoraForm.$valid) {
            $scope.displayValidationError = true;
            return;
        }

        $scope.lastAction = 'create';
        var url = $scope.url_bitacora+'create/';
        var bitacoraObj = {};
        $scope.startDialogAjaxRequest();      

        var bitacoratest ={
            id:$scope.bitacora.id,
            fecha:$scope.bitacora.fecha,
            ptoDestino:$scope.bitacora.ptoDestino,
            ptoOrigen:$scope.bitacora.ptoOrigen,
            idVehiculo:$scope.bitacora.idVehiculo,
            idOperador:$scope.bitacora.idOperador,
            idEnvio:$scope.ruta.idEnvio,
            idRuta:$scope.ruta,
            isFinal:$scope.bitacora.isFinal
        }
        
        console.log(bitacoratest);
        
        var config = {headers: { 'Content-Type': "application/json; charset=utf-8"}};
        $http.post(url, bitacoratest, config)
            .success(function (data) {
                console.log(data);
                $scope.getListBitacoraByRuta($scope.ruta);
//                bitacoraObj = $scope.paquete;
                $scope.bitacora={};
//                $scope.leadsBitacora = data.data;
//                $scope.leadsBitacoraCollection = [].concat($scope.leadsBitacora);
//                if($scope.tablestate!=='undefined'){
//                    $scope.tablestate.pagination.start = 0
//                    $scope.tablestate.pagination.numberOfPages = $scope.getNumberOfPages(data.totalData);
//                }                        
                $scope.finishAjaxCallOnSuccess(data, "#addBitacoraModal", false);
                sweet.show('Ok', 'Datos guardados correctamente', 'success');
            })
            .error(function(data, status, headers, config) {
                $scope.paquete={};
                $scope.handleErrorInDialogs(status);
                $scope.finishAjaxCallOnSuccess(data, "#addBitacoraModal", false);
                sweet.show('Ok', 'Error en el proceso de creación '+data.actionMessage, 'error');
            });
    };    

  // Any function returning a promise object can be used to load values asynchronously
  $scope.getOperador = function($select) {
    if($select.search!==''){
        var completeurl = $scope.url_paquete+'operadorbyname';
        var config = {params:{'name':$select.search},headers: { 'Content-Type': 'application/json', 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}};
        $http.get(completeurl, config)
            .then(function(response) {
              $scope.operadores = response.data.data;
            });  
    }
  };
  
    $scope.getVehiculos = function() {
    var completeurl = $scope.url_paquete+'getvehiculos/';
    var config = {headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}};
    $http.get(completeurl, config)
        .then(function(response) {
          console.log(response);
          $scope.vehiculolist=response.data;
        });  
  }; 
  
    $scope.selectedBitacora = function (bitacora) {
        $scope.bitacora=bitacora;
        console.log($scope.bitacora);
    }  
        
  $scope.onSelectTipoEnvioCallback = function (item, model){
      if(item!==null && item!==undefined && item!==''){
          $scope.tipoEnvio=item;
          console.log($scope.tipoEnvio);
      }
  };
  
  $scope.onSelectIdRutaCallback = function (item, model){
      if(item!==null && item!==undefined && item!==''){
        console.log(item);
        $scope.rutaselected = item;
      }
  };  
  
    $scope.reloadTableRutas=function(){
        if($scope.tablestate!=null && $scope.tablestate!=undefined){
            $scope.serverFilter($scope.tablestate);        
        }
    }  
    
    $scope.searchRutaByParameters = function(){        
        var start = 0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
        var number = 10;  // Number of entries showed per page.
        var page=0;        

        
        if($scope.tablestate!==null && $scope.tablestate.pagination!==undefined){
            var pagination = $scope.tablestate.pagination;
            start = pagination.start || 0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
            number = pagination.number || 10;  // Number of entries showed per page.            
            page=Math.floor(start / number);
        }        

        var config = {headers: { 'Content-Type': "application/json; charset=utf-8"},params:{
            'idRuta':$scope.rutaselected!=null?$scope.rutaselected.nombre:'',
            'status':$scope.rutaestatus!=null?$scope.rutaestatus.status:'',
            'page':page
        }};   
    
    
        var url = $scope.url+'searchRuta';
        $http.get(url, config)
            .success(function (response) {
                $scope.displaySearchMessage = true;
                $scope.leads = response;
                $scope.leadsCollection = [].concat($scope.leads);
                $scope.tablestate.pagination.numberOfPages = $scope.getNumberOfPages(response.length);
                $scope.finishAjaxCallOnSuccess(response.data, "null", false);
            })
            .error(function(data, status, headers, config) {
                $scope.handleErrorInDialogs(status);
                sweet.show('Error', 'Error en el proceso de busqueda', 'error');
            });        
//        $scope.idbitacora = null;
//        $scope.bitacora = null;            
        
    }
    
    
    $scope.resetSearch = function(){
        $scope.idBitacora=null;
        $scope.idbitacora=null;
        $scope.isSearching=false;
        $scope.searchFor = "";
        //$scope.pageToGet = 0;
        $scope.displaySearchMessage = false;
        $scope.tablestate.sort.predicate = undefined;
        $scope.tablestate.search.predicateObject={};
        $scope.serverBitacoraFilter($scope.tablestate);
        $scope.selected={};
        $scope.statusselected=null;
        $scope.tipoEnvioSelected=null;
        $scope.totalViajesFound=0;
        
        $scope.fechaRec1=null;
        $scope.fechaRec2=null;
        $scope.isSearching=false;
        $scope.idbitacora=null;
        $scope.listIdBitacora=[];
        $scope.notracking=null;        
        $scope.leadsHistorico = [];
        $scope.leadsHistoricoCollection = [].concat($scope.leadsHistorico);
        $scope.idEnvio=null;
        $scope.idbitacora = null;
        $scope.bitacora = null;  
        $scope.rutaselected=null;
        $scope.rutaestatus=null;
        
        $scope.reloadTableRutas();
    };
    
    
    $scope.showConfirmRutaInterrupcion = function(form){
        if (!form.$valid) {
            $scope.displayValidationError = true;
            return;
        }        
        var source =  $scope.ruta;
//        console.log(source);
//        console.log(source.isPausa);
//        console.log($scope.comentario);
        
        var comentario="";
        if(source.isPausa==0){
            comentario=" poner en pausa la ruta seleccionada?"
        }else{
            comentario=" quitar la pausa la ruta seleccionada?"
        }
        
        var sourceData = source;
        sweet.show({
            title: 'Confirmar',
            text: 'Está seguro de '+comentario,
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#DD6B55',
            confirmButtonText: 'Si, seguro!',
            closeOnConfirm: true,
            closeOnCancel: false
        }, function(isConfirm) {
            if (isConfirm) {
                $scope.putOrRemoveInterrupcion(sourceData);
            }else{
                sweet.show('Advertencia', 'El proceso ha sido cancelado', 'warning');
            }
        });        
        
    }
    
    $scope.putOrRemoveInterrupcion = function(source){
        var completeurl = $scope.url+'interrumpirRuta/';
        $scope.startDialogAjaxRequest();
        var config = {params: {'idruta':source.id,'comentario':$scope.comentario}, headers: { 'Content-Type': 'application/json'}};
        $http.get(completeurl, config)
          .success(function (data) {
              sweet.show('Success', 'Actualización de interrupción', 'success');
               $scope.reloadTableRutas();
               $scope.exit('#interruptRutaModal');
               $scope.addMoreImagesToRuta(source, true);
               $scope.finishAjaxCallOnSuccess(data, null, true);
          }).error(function(data, status, headers, config) {
                $scope.f=null;
                $scope.handleErrorInDialogs(status);
                sweet.show('Ok', 'Error en el proceso de creación '+data.actionMessage, 'error');
                $scope.finishAjaxCallOnSuccess(data, "#addPaqueteModal", false);
            });
        
    }    
    
    $scope.addMoreImagesToRuta = function(data, flag){
        var urlimages = $scope.url + 'addrutaimage';
        if($scope.f!=null && $scope.f!=undefined && data!=null && data!=undefined){
//            console.log(data);
//            console.log($scope.f);
//            console.log($scope.f.length);
            //for(var i=0;i<$scope.f.length;i++){

                var formData=new FormData();
                formData.append('file',$scope.f);

                console.log(formData);

                var request = {
                    method: 'POST',
                    url: urlimages,
                    headers: {'Content-Type': undefined},
                    params: {'idruta':data.id},
                    data:formData,
                    transformRequest: function(data, headersGetterFunction) {
                                    return data;
                    }    
                };
                
                console.log(request);
                //$scope.startDialogAjaxRequest();
                $http(request).success(function (data) {
                        if(flag!==null && flag===true){
                            sweet.show('Correcto', 'Carga completa de la imagen', 'success');
                        }
                        $scope.finishAjaxCallOnSuccess(data, null, true);
                    })
                    .error(function(data, status, headers, config) {
                            $scope.handleErrorInDialogs(status);
                        if(flag!==null && flag===true)
                            sweet.show('Error', 'Error en la carga de imagenes', 'error');
                    });            
            //}
        }
        $scope.f=null;
    };    
    
    angular.element(document).ready(function () {
        $scope.getTipoEnvio();
        $scope.getVehiculos();
    });     
    
        
}]);


app.filter('propsFilter', function() {
  return function(items, props) {
    var out = [];

    if (angular.isArray(items)) {
      items.forEach(function(item) {
        var itemMatches = false;

        var keys = Object.keys(props);
        for (var i = 0; i < keys.length; i++) {
          var prop = keys[i];
          var text = props[prop].toLowerCase();
          if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
            itemMatches = true;
            break;
          }
        }

        if (itemMatches) {
          out.push(item);
        }
      });
    } else {
      // Let the output be the input untouched
      out = items;
    }

    return out;
  };
});