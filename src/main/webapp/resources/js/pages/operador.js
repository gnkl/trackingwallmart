var ope = angular.module('crudoperador', ['smart-table', 'ngAnimate', 'ui.bootstrap', 'ngSanitize', 'ui.select','hSweetAlert','ngFileUpload', "checklist-model","ngMask"]);

ope.factory("sessionInjector", ['$log', 'sweet', function($log,sweet){
    return {
        request: function(config) {return config;},
        response: function(response) {
            if (typeof response.data === "string" && response.data.indexOf("forma-login") > -1) {
                location.reload();
            }
            return response;
        }
    };
}]);

ope.config(function ($httpProvider) {
    $httpProvider.interceptors.push("sessionInjector");
    $httpProvider.defaults.headers.get = {"X-Requested-With": "XMLHttpRequest"};
    $httpProvider.defaults.headers.put = {"X-Requested-With": "XMLHttpRequest"};
}); 

ope.controller('CrudOperadorCtrl', ['$scope', '$http', '$location','$q', 'sweet', 'Upload','$timeout',function($scope, $http, $location,$q, sweet, Upload, $timeout) {

    $scope.displayValidationError = false;
    $scope.url = "../protected/usuario/";
    $scope.usuarios = [];
    $scope.usuarioCollection = [].concat($scope.usuarios);
        
    $scope.itemsByPage = "10";    
    $scope.tablestateUsuario={};
    
    $scope.usuario={};
    $scope.estatus=[{'status':'A','descripcion':'Activo'}, {'status':'I','descripcion':'Inactivo'}];
    $scope.updateuser = false;
    $scope.user={};
    $scope.isButtonDisabled = false;
    $scope.button_clicked = false;
    $scope.imageUrl='';
    $scope.searchUser='';
    $scope.urlpaq = "../protected/paquetes/";
    $scope.listoperador=[];
    $scope.listusers=[];
    $scope.searchUserObj=null;
    

    $scope.createUsuario = function (updateForm) {
        if (!updateForm.$valid) {
            $scope.displayValidationError = true;
            return;
        }
        var url = $scope.url;
        if($scope.usuario.idUsu!==undefined){
            url = $scope.url + 'updateusuario/'+$scope.usuario.idUsu;
        }else{
            url = $scope.url + 'createusuario/';
        }
        
        
        $scope.startDialogAjaxRequest();      

        var usuariotest ={
            'idUsu':$scope.usuario.idUsu,
            'nombre':$scope.usuario.nombre,
            'usuario':$scope.usuario.usuario,
            'apellidoPat':$scope.usuario.apellidoPat,
            'apellidoMat':$scope.usuario.apellidoMat,
            'telefono':$scope.usuario.telefono,
            //'foto':$scope.usuario.nombre,
            'email':$scope.usuario.email,
            'status':$scope.usuario.estatusid.status,
            'pass':new Date().getTime()
        };    
        
        var config = {headers: { 'Content-Type': "application/json; charset=utf-8"}};
        
        $http.post(url, usuariotest, config)
            .success(function (data) {
                $scope.finishAjaxCallOnSuccess(data, "#createOperador", false);
                sweet.show('Ok', 'Datos guardados correctamente', 'success');
                console.log($scope.f);
                console.log(data);
                console.log(data.data);
                if($scope.f!== undefined || $scope.f!==''){
                    if(usuariotest.idUsu===undefined){
                        usuariotest.idUsu=data.data.idUsu;
                    }
                    $scope.addImageToUser(usuariotest);
                    usuariotest={};
                }
                $scope.resetUsuario();                
                $scope.reloadTableData();
                
            })
            .error(function(data, status, headers, config) {
                $scope.resetUsuario();
                $scope.handleErrorInDialogs(status);
                $scope.finishAjaxCallOnSuccess(data, "#createOperador", false);
                sweet.show('Ok', 'Error en el proceso de creación '+data.actionMessage, 'error');
            });        
        
    };
    
    $scope.addImageToUser=function(user){
        var urlimages = $scope.url + 'addimage/'+user.idUsu;
        var formData=new FormData();
        formData.append('file',$scope.f);
        var request = {
            method: 'POST',
            url: urlimages,
            headers: {'Content-Type': undefined},
            data:formData,
            transformRequest: function(data, headersGetterFunction) {
                            return data;
            }    
        };
        if($scope.f !== undefined && $scope.f !==null){
            $http(request).success(function (data) {
                 $scope.f=null;
                    //sweet.show('Correcto', 'Carga completa de la imagen', 'success');
                })
                .error(function(data, status, headers, config) {
                    $scope.handleErrorInDialogs(status);
                    sweet.show('Error', 'Error en el proceso de carga de imagen', 'error');
                });                        
        }
        $scope.f=null;
    }
    
    $scope.resetUsuario = function () {
        $scope.usuario={};
        $scope.updateuser=false;
        $scope.displayValidationError=false;
        $scope.isButtonDisabled=false;
        $scope.button_clicked=false;
        $scope.f=null;
    };
    
    $scope.handleErrorInDialogs = function (status) {
        $("#loadingModal").modal('hide');
        $scope.state = $scope.previousState;
        // illegal access
//        console.log(status);
        if(status === 403 || status === 401 || status === 302){
            $scope.errorIllegalAccess = true;
            $scope.state = 'error';  
//            console.log('status: '+status)
            window.location.assign('../welcome');
        }

        $scope.errorOnSubmit = true;
        $scope.lastAction = '';
    }    
    
    $scope.startDialogAjaxRequest = function () {
        $scope.isButtonDisabled=true;
        $scope.button_clicked=true;
        $scope.displayValidationError = false;
        $("#loadingModal").modal('show');
        $scope.previousState = $scope.state;
        $scope.state = 'busy';
    };   
    
    $scope.finishAjaxCallOnSuccess = function (data, modalId, isPagination) {
        //$scope.tableParams.reload();
        $("#loadingModal").modal('hide');

        if(!isPagination){
            if(modalId){
                $scope.exit(modalId);
            }
        }

        $scope.lastAction = '';
    };
    
    $scope.selectUsuario = function (usuario) {
        $scope.usuario={};
        $scope.usuario=usuario;
        $scope.updateuser = true;
        $scope.imageUrl="../protected/paqueteact/getimage/"+$scope.usuario.idUsu;
        $scope.f=null;
        angular.forEach($scope.estatus, function(obj, key) {
            if(obj.status===usuario.status){
                $scope.usuario.estatusid=obj;
            }
        });        
    };
    
    $scope.exit = function (modalId) {
        $(modalId).modal('hide');
        $scope.usuario = {};
        $scope.errorOnSubmit = false;
        $scope.errorIllegalAccess = false;
        $scope.displayValidationError = false;
        $scope.imageUrl='';
        $scope.usuario.passwordnvo=null;
        $scope.usuario.passwordnvorep=null;
        $scope.error_password_match=false;
        $scope.error_pattern_match=false;
    };    
    
    $scope.serverUsuarioFilter = function(tablestate){
        $scope.tablestateUsuario=tablestate;
            
        var pagination = tablestate.pagination;
        var start = pagination.start || 0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
        var number = pagination.number || 10;  // Number of entries showed per page.    
        var config = {params: {page: Math.floor(start / number) + 1}, headers: { 'Content-Type': 'application/json'}};
        var complete_url = $scope.url+"getusuarios/"; 
        
        $scope.startDialogAjaxRequest();
        
        $http.get(complete_url,config).success(function(response){
            $scope.usuarios = response.data;
            $scope.usuarioCollection = [].concat($scope.usuarios);
            $scope.tablestateUsuario.pagination.numberOfPages = $scope.getNumberOfPages(response.totalData);
            $scope.finishAjaxCallOnSuccess(response.data, "#changePasswordModal", false);
        }).error(function(response){
            sweet.show('Error', 'Error en el proceso de obtención históricos', 'error');
        });
        $scope.resetUsuario();
    };    
    
    $scope.reloadTableData=function(){
        var config = {params: {page: 0}, headers: { 'Content-Type': 'application/json'}};
        var complete_url = $scope.url+"getusuarios/"; 
        $http.get(complete_url,config).success(function(response){
            $scope.usuarios = response.data;
            $scope.usuarioCollection = [].concat($scope.usuarios);
            $scope.tablestateUsuario.pagination.numberOfPages = $scope.getNumberOfPages(response.totalData);
            //sweet.show('OK', 'Datos recargados', 'success');
        }).error(function(response){
            sweet.show('Error', 'Error en el proceso de obtención históricos', 'error');
        });        
    };
    
    $scope.getNumberOfPages = function(totaldata){
        return Math.ceil(totaldata / $scope.itemsByPage);
    };     
    
    $scope.showManagePermission=function(data){
      $scope.user.roles = [];  
      $scope.user.roles = data.roles;
      $scope.usuario=data;
//       window.location.assign('/GnklTracking/welcome/');
    };
    
    $scope.modificarPassword = function(data){
      $scope.usuario=data;  
    };
    
    $scope.savePermission = function(newPermissionForm){
        if (!newPermissionForm.$valid) {
            $scope.displayValidationError = true;
            return;
        }
        var url = $scope.url+'saveroles/'+$scope.usuario.idUsu;        
        
        $scope.startDialogAjaxRequest();      
        
        //var config = {params:{'roles':$scope.user.roles},headers: { 'Content-Type': "application/json; charset=utf-8"}};     
        
        $http.post(url, $scope.user.roles)
            .success(function (data) {
                $scope.resetUsuario();
                $scope.usuarios = data.data;
                $scope.usuarioCollection = [].concat($scope.usuarios);
                if($scope.tablestateUsuario!=='undefined'){
                    $scope.tablestateUsuario.pagination.start = 0
                    $scope.tablestateUsuario.pagination.numberOfPages = $scope.getNumberOfPages(data.totalData);
                }                        
                $scope.finishAjaxCallOnSuccess(data, "#addPermissionModal", false);
                sweet.show('Ok', 'Datos guardados correctamente', 'success');
            })
            .error(function(data, status, headers, config) {
                $scope.resetUsuario();
                $scope.handleErrorInDialogs(status);
                $scope.finishAjaxCallOnSuccess(data, "", true);
                sweet.show('Ok', 'Error en el proceso de creación', 'error');
            });             
    };
    
    $scope.changePasswordUser = function(passwordForm){
        if (!passwordForm.$valid) {
            $scope.displayValidationError = true;
            return;
        }
        var url = $scope.url+'changepassword/'+$scope.usuario.idUsu;        
        
        $scope.startDialogAjaxRequest();
        
        if($scope.usuario.passwordnvo===$scope.usuario.passwordnvorep){
            var passwords = [$scope.usuario.passwordant, $scope.usuario.passwordnvo, $scope.usuario.passwordnvorep];

            $http.post(url, passwords)
                .success(function (data) {
                    $scope.resetUsuario();
                    $scope.finishAjaxCallOnSuccess(data, "#changePasswordModal", false);
                    sweet.show('Ok', 'Datos guardados correctamente', 'success');
                })
                .error(function(data, status, headers, config) {
                    $scope.resetUsuario();
                    $scope.handleErrorInDialogs(status);
                    $scope.finishAjaxCallOnSuccess(data, "", true);
                    sweet.show('Ok', 'Error en el proceso de creación', 'error');
                });        
            
        }else{
            $scope.resetUsuario();
            $scope.finishAjaxCallOnSuccess(null, "#changePasswordModal", false);            
            sweet.show('Error', 'Las contraseñas no coinciden', 'error');
        }
    };
    
    $scope.getAllRoles=function(){
        var config = {headers: { 'Content-Type': 'application/json'}};
        var url = $scope.url+'getroles';
        $http.get(url,config).success(function(response){
            $scope.roles=response.data;
        });        
    };
    
    $scope.uploadFiles = function(files, errFiles) {
        $scope.f = files;
        $scope.errFile = errFiles && errFiles[0];
    };
    
    $scope.removeMask = function () {
       if ($scope.usuario.telefono.length === 10) {
            $scope.phoneMask= "(999) 999-9999";
        }
    };
    $scope.placeMask = function () {
      $scope.phoneMask= "(999) 999-9999 ext. ?9?9?9";
    };
    
/* PARA LA PARTE DE HISTORICO */    
    $scope.searchUsuario = function (searchUsuarioForm, isPagination) {
        if (!($scope.searchUser) && (!searchUsuarioForm.$valid)) {
            $scope.displayValidationError = true;
            return;
        }

        $scope.lastAction = 'search';
        var url = $scope.url +  "search/"+$scope.searchUser;
        $scope.startDialogAjaxRequest();
        var pagination = $scope.tablestateUsuario.pagination;
        var start = pagination.start || 0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
        var number = pagination.number || 10;  // Number of entries showed per page.           
        var config = {params: {page: Math.floor(start / number) + 1}, headers: { 'Content-Type': 'application/json'}};
        $http.get(url, config)
            .success(function (response) {
                //console.log(data.data[0].archivos.length);
                $scope.displaySearchMessage = true;
                if(response.data!==undefined && response.data.length>0){
                    $scope.usuarios = response.data;
                    $scope.usuarioCollection = [].concat($scope.usuarios);
                    if($scope.tablestateUsuario!==undefined || $scope.tablestateUsuario!=='undefined'){
                        $scope.tablestateUsuario.pagination.start = 0
                        $scope.tablestateUsuario.pagination.numberOfPages = $scope.getNumberOfPages(response.totalData);
                    }                    
                }else{
                    sweet.show('Warning', 'No se encontraron datos', 'warning');
                }
                $scope.finishAjaxCallOnSuccess(response, null, isPagination);
            })
            .error(function(data, status, headers, config) {
                $scope.finishAjaxCallOnSuccess(data, null, isPagination);
                $scope.handleErrorInDialogs(status);
                sweet.show('Error', 'Error en el proceso de búsqueda', 'error');
            });
    };   

    $scope.getNumberOfPages = function(totaldata){
        return Math.ceil(totaldata / $scope.itemsByPage);
    };
    
    $scope.resetSearchUser = function(){
        $scope.searchUserObj=null;
        $scope.selected=null;
        $scope.searchUser=null;
        $scope.pageToGet = 0;
        $scope.displaySearchMessage = false;
        $scope.displayValidationError=false;
        $scope.tablestateUsuario.sort.predicate = undefined;
        $scope.tablestateUsuario.search.predicateObject={};

        $scope.usuarios = {};
        $scope.usuarioCollection = [];
        if($scope.tablestate!=='undefined'){
            $scope.tablestateUsuario.pagination.start = 0;
            $scope.tablestateUsuario.pagination.numberOfPages = 0;
        }
        $scope.reloadTableData();
    };

    $scope.getAllRoles();
    
    $scope.error_password_match=false;
    $scope.error_pattern_match=false;
    //$scope.strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])(?=.{6,8})");
    $scope.strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{6,8})");
    
    $scope.analizepassword=function(){
        if($scope.strongRegex.test($scope.usuario.passwordnvo)) {
            $scope.error_pattern_match=false;
        }else{
            $scope.error_pattern_match=true;
        }
    };

    $scope.verify=function(password){
       if(password===$scope.usuario.passwordnvo){
        $scope.error_password_match=false;
       }else{
        $scope.error_password_match=true;
       }
    };
    
    $scope.resetFiles = function() {
        $scope.f = null;
        $scope.errFile = null;
    };    
    
    $scope.proccesMultipartFile = function(imgPaqueteForm, flag){
        if ((!imgPaqueteForm.$valid)) {
            $scope.displayValidationError = true;
            return;
        }
        
        console.log(imgPaqueteForm.$valid);
        
        if($scope.f===null){
            sweet.show('Error', 'Se debe cargar un archivo antes de procesarce', 'error');
            $scope.displayValidationError = true;
            return;
        }
        
        console.log($scope.f.type);
        if($scope.f.type!=='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'){
            sweet.show('Error', 'Debe ser un archivo de tipo XLSX', 'error');
            $scope.displayValidationError = true;
            return;            
        }
        
        var urlimages = $scope.url + 'uploadfile';
            
        var formData=new FormData();
        formData.append('file',$scope.f);

        var request = {
            method: 'POST',
            url: urlimages,
            headers: {'Content-Type': undefined},
            data:formData,
            transformRequest: function(data, headersGetterFunction) {
                            return data;
            }    
        };
        $scope.startDialogAjaxRequest();    
        $http(request).success(function (data) {
            $scope.reloadTableData();
            $scope.finishAjaxCallOnSuccess(data, "null", false);
            sweet.show('OK', 'El archivo fue procesado correctamente', 'success');
        })
        .error(function(data, status, headers, config) {
            $scope.handleErrorInDialogs(status);
            if(flag!==null && flag===true)
                sweet.show('Error', 'Error en la carga del archivo', 'error');
        });     
        $scope.f=null;        
    };
    
  $scope.onSelectCallback = function (item, model){
      if(item!==null && item!==undefined && item!==''){
          console.log(item);
          $scope.searchUser=item.usuario;
          $scope.searchUserObj=item;
          //$scope.searchPaqueteAct('searchPaqueteActForm', false);
          $scope.searchUsuario('searchUsuarioForm', false);
      }
  };  

  /*$scope.getOperadorSearch = function() {
    var completeurl = $scope.url+'searchoperador/';
    var config = {headers: { 'Content-Type': 'application/json', 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}};
    $http.get(completeurl, config)
        .then(function(response) {
          $scope.searchformoperador = response.data.data;
        });  
  }; */
    
    $scope.getUserByName = function($select) {
      if($select.search!==null && $select.search !== undefined && $select.search!=='' && $select.search.length>=4){
          //var completeurl = $scope.urlpaq+'operadorbyname';
          var completeurl = $scope.url +  "search/"+$select.search;
          //var config = {params:{'name':$select.search},headers: { 'Content-Type': 'application/json', 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}};
          var config = {sheaders: { 'Content-Type': 'application/json', 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}};
          $http.get(completeurl, config)
              .then(function(response) {
                  console.log(response.data);
                if(response.data!=null){
                    $scope.listusers = response.data.data;
                }
                
                console.log(response);
              });  
      }
    };         
        
}]);

ope.filter('propsFilter', function() {
  return function(items, props) {
    var out = [];

    if (angular.isArray(items)) {
      items.forEach(function(item) {
        var itemMatches = false;

        var keys = Object.keys(props);
        for (var i = 0; i < keys.length; i++) {
          var prop = keys[i];
          var text = props[prop].toLowerCase();
          if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
            itemMatches = true;
            break;
          }
        }

        if (itemMatches) {
          out.push(item);
        }
      });
    } else {
      // Let the output be the input untouched
      out = items;
    }

    return out;
  };
});