var pto = angular.module('punto', ['smart-table', 'ngAnimate', 'ui.bootstrap', 'ngSanitize', 'ui.select','hSweetAlert','ngFileUpload']);

pto.factory("sessionInjector", ['$log', 'sweet', function($log,sweet){
    return {
        request: function(config) {return config;},
        response: function(response) {
            if (typeof response.data === "string" && response.data.indexOf("forma-login") > -1) {
                location.reload();
            }
            return response;
        }
    };
}]);

pto.config(function ($httpProvider) {
    $httpProvider.interceptors.push("sessionInjector");
    $httpProvider.defaults.headers.get = {"X-Requested-With": "XMLHttpRequest"};
    $httpProvider.defaults.headers.put = {"X-Requested-With": "XMLHttpRequest"};
}); 

pto.controller('CrudPuntoCtrl', ['$scope', '$http', '$location','$q', 'sweet', 'Upload','$timeout',function($scope, $http, $location,$q, sweet, Upload, $timeout) {

    $scope.displayValidationError = false;
//    $scope.url = "/GnklTracking/protected/punto/";    
    $scope.url = $location.absUrl()+'/';
    $scope.origenes = [];
    $scope.origenCollection = [].concat($scope.origenes);
    
    $scope.entregas = [];
    $scope.entregaCollection = [].concat($scope.entregas);
    
    $scope.itemsByPage = "10";    
    $scope.tablestateOrigen={};
    $scope.tablestateEntrega={};
    
    $scope.origen={};
    $scope.entrega={};
    $scope.punto={};
    
    $scope.puntodescription='';
    $scope.puntodirection='';
    $scope.estatusid='';
    //$scope.options={};
    $scope.estatus=[{'status':1,'descripcion':'Activo'}, {'status':0,'descripcion':'Inactivo'}];
    $scope.nombrePunto=null;
    $scope.direccion=null;
    $scope.selectestatus=null;
    $scope.errFile = null;
    $scope.listnombrepunto = [];
    $scope.listdireccionpunto = [];

    
    $scope.createPunto = function (updateForm) {
        if (!updateForm.$valid) {
            $scope.displayValidationOError = true;
            return;
        }
        //$scope.lastAction = 'update';
        var url = $scope.url + 'createpto/';
        
        $scope.startDialogAjaxRequest();      
        
        var origentest ={
            'id':$scope.punto.id,
            'nombrePunto':$scope.punto.nombrePunto,
            'direccion':$scope.punto.direccion,
            'estatus':1,
            'grupo':$scope.punto.grupo!==null && $scope.punto.grupo!==undefined && $scope.punto.grupo!==''?$scope.punto.grupo:'JALOMA',
            'cliente':$scope.punto.cliente!==null && $scope.punto.cliente!==undefined && $scope.punto.cliente!==''?$scope.punto.cliente:'JALOMA',
            'entrega':$scope.punto.entrega===true?1:0,
            'origen':$scope.punto.origen===true?1:0,
            'ciudad':$scope.punto.ciudad,
            'estado':$scope.punto.estado,
            'rfc':$scope.punto.rfc,
            'comision':$scope.punto.comision!==null && $scope.punto.comision!==undefined && $scope.punto.comision!==''?$scope.punto.comision:'0',
            'telefono':$scope.punto.telefono,
            'cp':$scope.punto.cp
        };    
        console.log(origentest);
        
        var config = {headers: { 'Content-Type': "application/json; charset=utf-8"}};
        
        $http.post(url, origentest, config)
            .success(function (data) {
                $scope.resetTablePunto();
                $scope.finishAjaxCallOnSuccess(data, "", true);
                sweet.show('Ok', 'Datos guardados correctamente', 'success');
                $scope.resetOrigen();
            })
            .error(function(data, status, headers, config) {
                $scope.handleErrorInDialogs(status);
                sweet.show('Ok', 'Error en el proceso de creación', 'error');
                $scope.resetOrigen();
            });        
        
    };        
    
    $scope.resetTablePunto = function(){
        $scope.origenes = [];
        $scope.origenCollection = [].concat($scope.origenes);
        if($scope.tablestateOrigen!=='undefined'){
            $scope.tablestateOrigen.pagination.start = 0
            $scope.tablestateOrigen.pagination.numberOfPages = 0;
        }
        
        var pagination = $scope.tablestateOrigen.pagination;
        var start = pagination.start || 0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
        var number = pagination.number || 10;  // Number of entries showed per page.    
        var config = {params: {page: Math.floor(start / number) + 1}, headers: { 'Content-Type': 'application/json'}};
        var complete_url = $scope.url+"getpunto/";         
        $http.get(complete_url,config).success(function(response){
            $scope.origenes = response.data;
            $scope.origenCollection = [].concat($scope.origenes);
            $scope.tablestateOrigen.pagination.numberOfPages = $scope.getNumberOfPages(response.totalData);
            $scope.finishAjaxCallOnSuccess(response.data, "null", false);
        }).error(function(response){
            sweet.show('Error', 'Error en el proceso de obtención históricos', 'error');
        });      
    };    
        
    $scope.createNewPunto=function(){
        $scope.punto={};
        $scope.displayValidationOError=false;        
    }; 
    
        
    $scope.resetOrigen = function () {
        $scope.punto={};
        $scope.displayValidationOError=false;
    }
        
    $scope.handleErrorInDialogs = function (status) {
        $("#loadingModal").modal('hide');
        $scope.state = $scope.previousState;
        // illegal access
        if(status == 403 || status == 401){
            $scope.errorIllegalAccess = true;
            $scope.state = 'error';  
            return;
        }

        $scope.errorOnSubmit = true;
        $scope.lastAction = '';
    }    
    
    $scope.startDialogAjaxRequest = function () {
        $scope.displayValidationError = false;
        $("#loadingModal").modal('show');
        $scope.previousState = $scope.state;
        $scope.state = 'busy';
    }    
    
    $scope.finishAjaxCallOnSuccess = function (data, modalId, isPagination) {
        //$scope.tableParams.reload();
        $("#loadingModal").modal('hide');

        if(!isPagination){
            if(modalId){
                $scope.exit(modalId);
            }
        }

        $scope.lastAction = '';
    };
            
    
    $scope.selectPunto = function (punto) {
        $scope.punto={};
        $scope.punto.id=punto.id;
        $scope.punto.nombrePunto=punto.nombrePunto;
        $scope.punto.direccion=punto.direccion;
        $scope.punto.direccion=punto.direccion;
        $scope.punto.grupo=punto.grupo;
        $scope.punto.cliente=punto.cliente;
        //$scope.punto.origen=punto.origen;
        $scope.punto.origen=punto.origen===1?true:false;
        $scope.punto.entrega=punto.entrega===1?true:false;
        $scope.punto.rfc=punto.rfc;
        $scope.punto.ciudad=punto.ciudad;
        $scope.punto.estado=punto.estado;
        $scope.punto.comision=punto.comision;
        $scope.punto.telefono=punto.telefono;
        $scope.punto.cp=punto.cp;
        angular.forEach($scope.estatus, function(obj, key) {
            if(obj.status===punto.estatus){
                $scope.punto.estatus=obj;
            }
        });
        console.log($scope.punto);
    };
        
    $scope.handleErrorInDialogs = function (status) {
        $("#loadingModal").modal('hide');
        $scope.state = $scope.previousState;
        // illegal access
        if(status === 403 || status === 401 || status === 302){
            $scope.errorIllegalAccess = true;
            $scope.state = 'error';
            //window.location.reload();
            window.location = "../welcome";
            //$location.path('/login');
            //return $q.reject(response);
            
            return;
        }

        $scope.errorOnSubmit = true;
        $scope.lastAction = '';
    };    
    
    $scope.startDialogAjaxRequest = function () {
        $scope.displayValidationError = false;
        $("#loadingModal").modal('show');
        $scope.previousState = $scope.state;
        $scope.state = 'busy';
    };    
    
    $scope.finishAjaxCallOnSuccess = function (data, modalId, isPagination) {
        $("#loadingModal").modal('hide');
        if(!isPagination){
            if(modalId){
                $scope.exit(modalId);
            }
        }

        $scope.lastAction = '';
    };    
    
    $scope.exit = function (modalId) {
        $(modalId).modal('hide');
        $scope.punto={};
        $scope.errorOnSubmit = false;
        $scope.errorIllegalAccess = false;
        $scope.displayValidationOError=false;
        $scope.f=null;
    };
//
//    $scope.rowClick = function (idx){
//        $scope.editor = $scope.leads[idx];
//    };

    $scope.serverOrigenFilter = function(tablestate){
        $scope.tablestateOrigen=tablestate;
            
        var pagination = tablestate.pagination;
//        var start = pagination.start || 0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
//        var number = pagination.number || 10;  // Number of entries showed per page.    
        var start = pagination.start;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
        var number = pagination.number;  // Number of entries showed per page.    
        var config = {params: {page: Math.floor(start / number) + 1}, headers: { 'Content-Type': 'application/json'}};
        var complete_url = $scope.url+"getpunto/"; 
    
        if($scope.nombrePunto!==null || $scope.direccion!==null || $scope.selectestatus!==null){
            $scope.searchPunto('searchPuntoForm', false, Math.floor(start / number) + 1);
        }else{
            $scope.startDialogAjaxRequest();

            $http.get(complete_url,config).success(function(response){
                $scope.origenes = response.data;
                $scope.origenCollection = [].concat($scope.origenes);
                $scope.tablestateOrigen.pagination.numberOfPages = $scope.getNumberOfPages(response.totalData);
                $scope.finishAjaxCallOnSuccess(response.data, "null", false);
            }).error(function(response){
                sweet.show('Error', 'Error en el proceso de obtención históricos', 'error');
            });             
        }
           
    };
    
    
    
    $scope.getNumberOfPages = function(totaldata){
        return Math.ceil(totaldata / $scope.itemsByPage);
    }; 
    
    $scope.uploadFiles = function(files, errFiles) {
        $scope.f = files;
        $scope.errFile = errFiles && errFiles[0];
    };    
    
    $scope.resetFiles = function() {
        $scope.f = null;
        $scope.errFile = null;
    };    
    
    $scope.proccesMultipartFile = function(imgPaqueteForm, flag){
        if ((!imgPaqueteForm.$valid)) {
            $scope.displayValidationError = true;
            return;
        }
        
        console.log(imgPaqueteForm.$valid);
        
        if($scope.f===null){
            sweet.show('Error', 'Se debe cargar un archivo antes de procesarce', 'error');
            $scope.displayValidationError = true;
            return;
        }
        console.log($scope.f.type);
        if($scope.f.type!=='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'){
            sweet.show('Error', 'Debe ser un archivo de tipo CSV', 'error');
            $scope.displayValidationError = true;
            return;            
        }
        
        var urlimages = $scope.url + 'uploadfile';
            
        var formData=new FormData();
        formData.append('file',$scope.f);

        var request = {
            method: 'POST',
            url: urlimages,
            headers: {'Content-Type': undefined},
            data:formData,
            transformRequest: function(data, headersGetterFunction) {
                            return data;
            }    
        };
        $scope.startDialogAjaxRequest();    
        $http(request).success(function (data) {
            $scope.resetTablePunto();
            $scope.finishAjaxCallOnSuccess(data, "null", false);
            sweet.show('OK', 'El archivo fue procesado correctamente', 'success');
        })
        .error(function(data, status, headers, config) {
            $scope.handleErrorInDialogs(status);
            if(flag!==null && flag===true)
                sweet.show('Error', 'Error en la carga del archivo', 'error');
        });     
        $scope.f=null;        
    };
    
    $scope.searchPunto = function (searchPuntoForm, isPagination, pageNumber) {
        if ((!searchPuntoForm.$valid)) {
            $scope.displayValidationError = true;
            
        }

        $scope.lastAction = 'search';
        var url = $scope.url +  "search";
        $scope.startDialogAjaxRequest();
                
        var config = {headers: { 'Content-Type': "application/json; charset=utf-8"},params:{
            'nombre':($scope.nombrePunto!==null && $scope.nombrePunto!==undefined)?$scope.nombrePunto:undefined,
            'direccion':($scope.direccion!==null && $scope.direccion!==undefined)?$scope.direccion:undefined,
            'estatus':($scope.selectestatus!==null && $scope.selectestatus.status!==undefined)?$scope.selectestatus.status:undefined,
            'page':(pageNumber>0?pageNumber-1:0)
        }};
    
        console.log(config);
        
        $scope.clearTableData();
        
        $http.get(url, config)
            .success(function (response) {
                console.log(response);
                    
                $scope.displaySearchMessage = true;
                $scope.origenes = response.data;
                $scope.origenCollection = [].concat($scope.origenes);
                if($scope.tablestateOrigen!=='undefined'){
                    $scope.tablestateOrigen.pagination.totalItemCount = response.totalData;
                    $scope.tablestateOrigen.pagination.numberOfPages = $scope.getNumberOfPages(response.totalData);
                    $scope.tablestateOrigen.pagination.start = pageNumber;
                    console.log($scope.tablestateOrigen.pagination);
                }
                $scope.finishAjaxCallOnSuccess(response.data, "#searchContactsModal", isPagination);
            })
            .error(function(data, status, headers, config) {
                $scope.handleErrorInDialogs(status);
                sweet.show('Error', 'Error en el proceso de busqueda', 'error');
            });
    };   

    $scope.resetSearch = function(){
        $scope.nombrePunto=null;
        $scope.direccion=null;
        $scope.selectestatus=null;        
        $scope.searchFor = "";
        $scope.pageToGet = 0;
        $scope.displaySearchMessage = false;
        $scope.tablestateOrigen.sort.predicate = undefined;
        $scope.tablestateOrigen.search.predicateObject={};
        $scope.serverOrigenFilter($scope.tablestateOrigen);
        $scope.selected={};
    };    

    $scope.clearTableData = function(){
        $scope.origenes = [];
        $scope.origenCollection = [].concat($scope.leads);
        if($scope.tablestateOrigen!=='undefined'){
            $scope.tablestateOrigen.pagination.start = 0;
            $scope.tablestateOrigen.pagination.numberOfPages = 0;
        }
    };
    
    $scope.getStringByNombre = function($select) {
      if($select.search!==null && $select.search !== undefined && $select.search!=='' && $select.search.length>=4){
          var completeurl = $scope.url +  "getnombre";
          var config = {params:{'name':$select.search},headers: { 'Content-Type': 'application/json', 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}};
          $http.get(completeurl, config)
              .then(function(response) {
                console.log(response);  
                $scope.listnombrepunto=response.data;
              });  
      }
    };       

    $scope.getStringByDireccion = function($select) {
      if($select.search!==null && $select.search !== undefined && $select.search!=='' && $select.search.length>=4){
          var completeurl = $scope.url +  "getdireccion";
          var config = {params:{'name':$select.search},headers: { 'Content-Type': 'application/json', 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'}};
          $http.get(completeurl, config)
              .then(function(response) {
                console.log(response);  
                $scope.listdireccionpunto=response.data;
              });  
      }
    };     
    
  $scope.onSelectNombreCallback = function (item, model){
      if(item!==null && item!==undefined && item!==''){
        console.log(item);
        $scope.nombrePunto=item;
        $scope.searchPunto('searchPuntoForm', false,0);                    
      }
  };      

  $scope.onSelectDireccionCallback = function (item, model){
      if(item!==null && item!==undefined && item!==''){
        console.log(item);
        $scope.direccion=item;
        $scope.searchPunto('searchPuntoForm', false,0);                    
      }
  };      



//    angular.element(document).ready(function () {
//        console.log($location.absUrl());
//        var path = $location.absUrl(); 
//        var pathArray = path.split('/');
//        console.log(pathArray);
//    });

}]);

pto.filter('propsFilter', function() {
  return function(items, props) {
    var out = [];

    if (angular.isArray(items)) {
      items.forEach(function(item) {
        var itemMatches = false;

        var keys = Object.keys(props);
        for (var i = 0; i < keys.length; i++) {
          var prop = keys[i];
          var text = props[prop].toLowerCase();
          if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
            itemMatches = true;
            break;
          }
        }

        if (itemMatches) {
          out.push(item);
        }
      });
    } else {
      // Let the output be the input untouched
      out = items;
    }

    return out;
  };
});
